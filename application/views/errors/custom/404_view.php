<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view(get_theme_view('include/head')); ?>
</head>
<body>
    <div class="wrapper">
        <?php $this->load->view(get_theme_view('include/header')); ?>
    
        <div class="content-wr">
            <div class="container">
                <div class="error error__404">
                    <h1 class="h1-title">
                        <div class="icon">
                            <i class="fai fai-warning-o"></i>
                        </div>
                        <?php lang_line('error_404_header');?>
                    </h1>
                    <p class="error__404-message"><?php lang_line('error_404_text');?></p>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view(get_theme_view('include/footer')); ?>
</body>
</html>
