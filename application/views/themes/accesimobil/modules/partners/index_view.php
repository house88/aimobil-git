<div class="container partners">
    <div class="row">
        <div class="d-flex flex-column col-12">
            <h1 class="heading-title-1"><?php echo $first_category[lang_column('category_title')];?></h1>
        </div>
        <section class="partners__list col-12">
            <div class="row">
                <div class="col-12 col-lg-3 left-side">
                    <p><?php echo $first_category[lang_column('category_text')];?></p>
                </div>
                <div class="col-12 col-lg-9 middle-side">
                    <div class="row">
                        <?php foreach($first_category['partners'] as $partner){?>
                            <div class="col-12 col-sm-6 col-md-4">
                        <a class="partner__item" <?php if(!empty($partner['partner_link'])){?>href="<?php echo $partner['partner_link'];?>" target="_blank"<?php }?>>
                                    <div class="image">
                                        <img src="<?php echo site_url('files/partners/'.$partner['partner_logo']);?>" alt="<?php echo image_title($partner['partner_name']);?>" title="<?php echo image_title($partner['partner_name']);?>">
                                    </div>
                                    <div class="name">
                                        <?php echo $partner['partner_name'];?>
                                    </div>
                                </a>
                            </div>                
                        <?php }?>
                    </div>
                </div>
            </div>
        </section>
        <?php if(!empty($categories)){?>
            <?php foreach($categories as $category){?>
                <section class="partners__list col-12">
                    <div class="row">
                        <div class="col-12 col-lg-3 left-side">
                            <h2 class="h2-title"><?php echo $category[lang_column('category_title')];?></h2>
                            <p><?php echo $category[lang_column('category_text')];?></p>
                        </div>
                        <div class="col-12 col-lg-9 middle-side">
                            <div class="row">
                                <?php foreach($category['partners'] as $partner){?>
                                    <div class="col-12 col-sm-6 col-md-4">
                                        <a class="partner__item" href="<?php echo $partner['partner_link'];?>" target="_blank">
                                            <div class="image">
                                                <img src="<?php echo site_url('files/partners/'.$partner['partner_logo']);?>" alt="<?php echo image_title($partner['partner_name']);?>" title="<?php echo image_title($partner['partner_name']);?>">
                                            </div>
                                            <div class="name">
                                                <?php echo $partner['partner_name'];?>
                                            </div>
                                        </a>
                                    </div>                
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php }?>
        <?php }?>
    </div>
</div>