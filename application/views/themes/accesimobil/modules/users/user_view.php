<div class="container about_us">
    <div class="row">
        <div class="d-flex col-12">
            <article class="user-card flex-sm-row flex-column">
                <div class="user-card__details pl-sm-4 pl-lg-8 order-2">
                    <div class="position pt-3 pt-md-0 pt-lg-0"><?php echo $manager[lang_column('position_name')];?></div>
                    <div class="name"><?php echo $manager[lang_column('user_name')];?></div>
                    <div class="region"><?php echo (($manager['plocation_in_title'] == 1)?$manager[lang_column('plocation_name')].', ':'') . $manager[lang_column('location_name')];?></div>
                    <div class="description"><?php echo $manager[lang_column('user_description')];?></div>
                    <div class="call-buttons flex-sm-row flex-column">
                        <a href="tel:<?php echo $manager['user_phone'];?>" class="btn btn-flat btn-phone text-uppercase bg-orange mr-0 mb-31 mr-sm-31 mb-sm-0">
                            <i class="fai fai-phone"></i>
                            <?php echo $manager['user_phone'];?>
                        </a>
                        <a href="viber://chat?number=<?php echo $manager['user_phone'];?>" class="btn btn-flat btn-viber text-uppercase bg-viber">
                            <i class="fai fai-viber"></i>
                            <span>Viber</span>
                        </a>
                    </div>
                </div>
                <div class="user-card__image order-1">
                    <div class="img img-object-fit">
                        <img src="<?php echo site_url('files/users/'.$manager['user_photo']);?>" alt="<?php echo image_title($manager[lang_column('user_name')]);?>" title="<?php echo image_title($manager[lang_column('user_name')]);?>">
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="row" id="manager-catalog_wr">
        <div class="col-lg-12 col-xl-9">
            <script>
                var manager_catalog_page = 1;
                var manager_catalog_category = null;
                var manager_catalog_url = '<?php echo site_url(get_dinamyc_uri('manager', $manager[lang_column('user_url')], $lang));?>';
            </script>
            <?php if(!empty($categories)){?>
                <div class="w100 text-left pb-2em">
                    <div class="flex">
                        <div class="top-page-navigation">
                            <a href="#" class="active call-function" data-callback="reset_manager_catalog_filter"><?php lang_line('discounts_page_filter_reset_text');?></a>
                            <?php foreach($categories as $category){?>
                                <a href="#" class="call-function" data-callback="set_manager_catalog_category" data-category="<?php echo $category['category_group'];?>"><?php echo $category[lang_column('category_title')];?></a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>
            <div id="manager-catalog_list-wr">
                <?php $this->load->view(get_theme_view('modules/users/user_catalog_view')); ?>
            </div>
            <div class="w100 text-center pb-3_5em">
                <div class="container flex center" id="manager-catalog_pagination">
                    <?php echo $pagination;?>
                </div>
            </div>
        </div>
        <div class="d-none d-xl-block col-xl-3">
            <?php $_banner = Modules::run('banner/_get_by_alias', 'real_estate_right');?>
            <?php if(!empty($_banner)){?>
                <div id="sidebar-banner-list" class="sidebar-banner d-block mb-2em">
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                    <?php }?>
                        <div class="sidebar-banner-catalog">
                            <div class="image">
                                <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                            </div>
                            <div class="banner_body-wr">
                                <?php echo $_banner[lang_column('banner_text')]?>
                            </div>
                        </div>
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        </a>
                    <?php }?>
                </div>
            <?php }?>
        </div>
    </div>
</div>