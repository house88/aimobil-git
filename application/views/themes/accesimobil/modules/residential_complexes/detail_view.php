<section class="pt-3em pb-3_5em">
	<div class="container article-container">
		<h2 class="f-medium c-black pb-0_9em"><?php echo $complex[lang_column('complex_title')];?></h2>
		<div class="flex-1em">
            <?php $complex_photos = json_decode($complex['complex_photos'], true);?>
			<div class="col-12 col-lg-7 col-xl-8 mb-2em-xs mb-2em-sm mb-0-md">
				<div class="single-complex-carousel owl-carousel">
					<div class="item">
						<img src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/'.$complex['complex_photo']);?>" alt="<?php echo image_title($complex[lang_column('complex_title')], 1);?>" title="<?php echo image_title($complex[lang_column('complex_title')], 1);?>">
					</div>
					<?php $_photo_index = 2;?>
                    <?php foreach($complex_photos as $complex_photo){?>
						<?php if($complex['complex_photo'] != $complex_photo){?>
							<div class="item">
								<img src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/'.$complex_photo);?>" alt="<?php echo image_title($complex[lang_column('complex_title')], $_photo_index);?>" title="<?php echo image_title($complex[lang_column('complex_title')], $_photo_index);?>">
							</div>
							<?php $_photo_index++;?>
						<?php }?>
                    <?php }?>
				</div>
			</div>
			<div class="col-12 col-lg-5 col-xl-4 flex column">
				<div class="pad mb-1_8em no-wrap d-none d-sm-flex">
					<div class="shrink-0">
                        <div class="w-140 h-140" style="background-size:cover;background-position: top center;background-repeat:no-repeat;background-image:url(<?php echo site_url(getImage('files/users/'.$complex_manager['user_photo']));?>);"></div>
					</div>
					<div class="flex-grow-1 br-1-grey br-left-0">
						<div class="w100 pl-1em pt-0_4em">
							<div><small class="text-muted-grey"><?php echo $complex_manager[lang_column('position_name')];?></small></div>
							<div class="size-1_1em f-medium c-black lh-1_2em"><a class="c-black hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $complex_manager[lang_column('user_url')], $lang));?>"><?php echo $complex_manager[lang_column('user_name')];?></a></div>
							<div class="mb-0_2em mt-0_4em size-0_9em"><a class="c-orange hover-black" href="mailto:<?php echo $complex_manager['user_email'];?>"><?php echo $complex_manager['user_email'];?></a></div>
							<div class="flex mt-0_7em">
								<div><a class="project-phone" href="tel:<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-phone"></i> <?php echo $complex_manager['user_phone'];?></a></div>
								<div class="ml-0_4em">
									<a class="project-viber" href="viber://chat?number=<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-viber"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pad mb-1_8em no-wrap d-flex d-sm-none">
					<div class="shrink-0">
                        <div class="w-70 h-70" style="background-size:cover;background-position: top center;background-repeat:no-repeat;background-image:url(<?php echo site_url(getImage('files/users/'.$complex_manager['user_photo']));?>);"></div>
					</div>
					<div class="flex-grow-1 br-1-grey br-left-0">
						<div class="w100 pl-0_5em pt-0_2em">
							<div class="size-1_1em f-medium c-black lh-1em"><a class="c-black hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $complex_manager[lang_column('user_url')], $lang));?>"><?php echo $complex_manager[lang_column('user_name')];?></a></div>
							<div class="flex mt-0_2em">
								<div><a class="project-phone" href="tel:<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-phone"></i> <?php echo $complex_manager['user_phone'];?></a></div>
								<div class="ml-0_4em">
									<a class="project-viber" href="viber://chat?number=<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-viber"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="flex-grow-1 flex column w100">
					<div class="single-page-map flex-grow-1 flex column">
						<div id="rc_map_<?php echo $complex['id_complex'];?>" class="single_map h100 w100 flex-grow-1 flex column"></div>
					</div>
				</div>
		        <?php $comples_markers = array();?>
                <?php
                    $complex_markers[] = array(
                        'latitude' => $complex['complex_lat'],
                        'longitude' => $complex['complex_lng'],
                        'container' => 'rc_map_'.$complex['id_complex']
                    );	
                ?>
                <script type="text/javascript">
                    init_google_maps = true;
                    markers = <?php echo json_encode($complex_markers);?>;
                </script>
			</div>
		</div>
	</div>
	<?php $complex_plans = json_decode($complex['complex_plans'], true);?>
	<div class="container mt-1_8em">
		<div class="flex-1em">
			<div class="col-12 col-xl-8">
				<div class="complex-header bc-grey mb-1_6em flex items-center space-between">
					<em class="fai fai-finger-up c-orange"></em>
					<div>
						<div class="size-1_6em f-medium c-black"><?php echo $complex[lang_column('complex_head')];?></div>
					</div>
					<div class="size-1_1em c-grey">
						<?php lang_line('label_price_from');?> <span class="c-orange size-1_7em f-medium"><?php echo formatNumber($complex['complex_price_from']);?> &euro;/m2</span>
					</div>
				</div>
				<div class="single-article html-text">
                    <?php echo $complex[lang_column('complex_text')];?>
                    <?php if(!empty($complex_plans)){?>
                        <div class="w100">
                            <div class="size-1_7em f-medium c-black"><?php lang_line('residential_complexes_plans_header');?></div>
                        </div>
                        <div class="flex-1em pt-2em child-pb-1_8em">
                            <?php foreach($complex_plans as $complex_plan){?>
                                <div class="col-12 col-sm-6">
                                    <div class="w100 h100 box-shadow">
                                        <img class="w100" src="<?php echo site_url(getImage('files/residential_complexes/'.$complex['id_complex'].'/'.$complex_plan['file']));?>" alt="">
                                        <div class="flex space-between nowrap pb-1_8em pt-0_8em">
                                            <div class="pl-1em pr-1em flex-grow-1">
                                                <div class="size-0_9em c-mid-grey"><?php if(!empty($complex_plan['block'])){lang_line('residential_complexes_plans_label_block_number', true, $complex_plan['block']);}?></div>
                                                <div class="f-medium c-black size-1_125em">
                                                    <?php if(!empty($complex_plan[lang_column('title')])){echo $complex_plan[lang_column('title')];}?>
                                                </div>
                                                <div class="pt-1em size-0_9em c-grey-deep"><?php if(!empty($complex_plan['floors'])){lang_line('residential_complexes_plans_label_floor', true, $complex_plan['floors']);}?></div>
                                            </div>
                                            <div class="f-medium c-black col-5 pl-2em pr-1em">
                                                <div class="size-0_9em c-mid-grey"><?php if($complex_plan['price_from'] > 0){lang_line('label_price_from');}?></div>
                                                <div class="c-orange size-1_125em"><?php if($complex_plan['price_from'] > 0){echo formatNumber($complex_plan['price_from']).' &euro;/m<sup>2</sup>';}?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>
				</div>
				<div class="w100 p-1em-xs p-2em-sm bc-grey mt-1_3em">
					<div class="flex-1em pad nowrap-md items-center">
						<div class="col-12 col-lg-6 mb-2em-xs mb-2em-sm mb-0-md">
							<div class="pl-1em-md">
								<h2 class="manager-title title-1 text-left"><?php lang_line('residential_complexes_contact_block_text');?></h2>
							</div>
						</div>
						<div class="col-12 col-lg-6 flex-1em-md">
							<div class="w-100 p-0 flex pad nowrap">
								<div class="shrink-0 manager-photo photo-1 img-object-fit">
									<img class="mw-140" src="<?php echo site_url(getImage('files/users/'.$complex_manager['user_photo']))?>" alt="<?php echo image_title($complex_manager[lang_column('user_name')]);?>" title="<?php echo image_title($complex_manager[lang_column('user_name')]);?>">
								</div>
								<div class="flex-grow-1 br-1-grey br-left-0 bc-white">
									<div class="w100 h100 pl-0_5em-xs  pl-1em-sm pt-0_4em">
										<div class="d-none d-sm-flex"><small class="text-muted-grey"><?php echo $complex_manager[lang_column('position_name')];?></small></div>
										<div class="size-1_1em f-medium c-black lh-1_2em"><a class="hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $complex_manager[lang_column('user_url')], $lang));?>"><?php echo $complex_manager[lang_column('user_name')];?></a></div>
										<div class="manager-mail mail-1 mb-0_2em mt-0_4em size-0_9em"><a class="c-orange hover-black" href="mailto:<?php echo $complex_manager['user_email'];?>"><?php echo $complex_manager['user_email'];?></a></div>
										<div class="manger-social-card card-1 flex mt-0_7em">
											<div><a class="project-phone phone-1" href="tel:<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-phone"></i> <?php echo $complex_manager['user_phone'];?></a></div>
											<div class="ml-0_4em">
												<a class="project-viber" href="viber://chat?number=<?php echo $complex_manager['user_phone'];?>"><i class="fai fai-viber"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php $complex_icons = json_decode($complex['complex_icons'], true);?>
            <?php if(!empty($complex_icons)){?>
                <div class="col-12 col-xl-4 page-sidebar pt-2em-xs pt-2em-sm pt-0-lg">
                    <div class="f-medium size-1_6em lh-1em"><?php lang_line('residential_complexes_icons_header');?></div>
                    <div class="characteristic-icons flex-1em mt-1_5em child-mb-1em">
                        <?php foreach($complex_icons as $complex_icon){?>
                            <div class="col-6 col-sm-6">
                                <div class="flex items-center nowrap">
                                    <div class="icon shrink-0">
                                        <div class="icon-o">
                                            <em class="<?php echo $icons[$complex_icon['id_icon']]['icon_class'];?>"></em>
                                        </div>
                                    </div>
                                    <div class="pl-2em">
                                        <p class="mb-0 c-grey size-0_9em"><?php echo $complex_icon[lang_column('icon_name')];?></p>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
			<?php }?>
			
			
			<?php if(!empty($re_icons)){?>
				<div class="w100 page-icons pt-1em-xs pt-2em-sm pb-2_5em">
					<div class="mobile-collapse-parent">
						<div class="mobile-collapse-toggle caracter f-medium  c-black size-1_1-xs size-1_5em-sm lh-1em"><?php lang_line('label_icons');?></div>
						<div class="mobile-collapse-inner w100">
							<div class="characteristic-icons flex-1em mt-1em-xs mt-1_7em-sm child-mb-0_6em-xs child-mb-1em-sm">
								<?php foreach($icons as $icon){?>
									<?php if(array_key_exists($icon['id_icon'], $re_icons)){?>
										<div class="col-6 col-sm-6 col-md-3">
											<div class="flex items-center nowrap">
												<div class="icon shrink-0">
													<div class="icon-o">
														<em class="<?php echo $icon['icon_class'];?>"></em>
													</div>
												</div>
												<div class="pl-2em">
													<p class="mb-0 c-grey size-0_9em"><?php echo $icon[lang_column('icon_name')];?></p>
												</div>
											</div>
										</div>										
									<?php }?>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
			<?php }?>
		</div>
	</div>
</section>