<div class="home-sections">
    <section class="home-real-estate">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4">sal;kdad</div>
                <div class="col-12 col-lg-4">sal;kdad</div>
                <div class="col-12 col-lg-4">sal;kdad</div>
            </div>
        </div>
    </section>
    
    <?php if($this->lsettings->item('home_residential_complexes_show') == 1){?>
        <section class="pt-1_5em-xs pt-3_5em-sm pb-0_5em-xs pb-1_5em-sm bc-grey home-complexe">
            <div class="container">
                <div class="flex items-center pb-1em-sm">
                    <h2 class="size-1_2em-xs size-2em-sm f-bold pr-1_5rem c-black"><?php lang_line('home_section_residential_complexes_header');?></h2>
                    <div class="pl-1em flex items-center mb-0_1em-sm">
                        <a class="c-orange up size-0_8em f-medium pt-1em-sm" href="">Vezi toate <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                    </div>
                </div>
                <div class="pt-4 pb-4 complex-carousel owl-carousel">
                    <?php foreach($complexes as $complex){?>
                        <div class="item">
                            <div class="card">
                                <a href="">
                                    <img class="card-img-top" src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/'.$complex['complex_photo']);?>" alt="">
                                </a>
                                <div class="card-body">
                                    <a href="">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <div class="">
                                                    <small><?php echo $complex[lang_column('complex_title')];?></small>
                                                </div>
                                                <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                                    <?php echo $complex['complex_address'];?>
                                                </div>
                                            </div>
                                            <div class="f-medium c-black col-3">
                                                <div class="row">
                                                    <div class="pl-0-xs pl-1em-sm">
                                                        <small>De la</small>
                                                        <span class="c-orange size-1_3em pt-0_3em"><?php echo formatNumber($complex['complex_price_from']);?> &euro;</span>
                                                    </div>
                                                </div>
        
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>
    <section class="home-services">
        <div class="container">
            <div class="row">
                <div class="d-flex flex-column col-12 col-lg-4 mb-5 mb-lg-0">
                    <div class="pr-0 pr-lg-11 ">
                        <h1 class="heading-title-1 pr-lg-41"><?php lang_line('home_section_service_header');?></h1>
                        <div class="heading-text pr-lg-41">
                            <?php lang_line('home_section_services_sidebar_left_text');?>
                        </div>
                        <a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="btn btn-flat btn-contact text-uppercase bg-orange"><?php lang_line('home_btn_contact_us');?></a>
                    </div>
                </div>
                <div class="col-12 col-lg-8">
                    <?php if(!empty($services)){?>
                        <div class="row home-services-items">
                            <?php foreach($services as $service){?>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <a class="service-item service-item-4" href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>">
                                        <div class="icon">
                                            <div class="icon-o">
                                                <span class="<?php echo $service['service_icon'];?>"></span>                                    
                                            </div>
                                        </div>
                                        <div class="details pl-31">
                                            <div class="name">
                                                <?php echo $service[lang_column('service_name')];?>
                                            </div>
                                            <div class="intro">
                                                <?php echo $service[lang_column('service_stext')];?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </section>
    <?php if(!empty($blogs)){?>
        <section class="blog-same home-articles">
            <div class="container">
                <h1 class="heading-title-1 block-pre-footer__title">
                    <?php lang_line('home_section_blog_header');?>
                    <a href="<?php echo site_url(get_static_uri('blog', $lang));?>" class="more-link">
                        <?php lang_line('home_section_blog_more_link');?> <span class="fai fai-right-arrow"></span>
                    </a>
                </h1>
                <div class="row">
                    <?php foreach($blogs as $blog){?>
                        <div class="col-xs col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <?php $this->load->view(get_theme_view('modules/blog/item_view'), array('blog' => $blog));?>
                        </div>            
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>
</div>