    <section class="home--bg parallax-window" data-parallax="scroll" data-image-src="<?php echo site_url('files/'.$page_detail[lang_column('page_image')]);?>">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1 class="home-title f-bold"><?php lang_line('home_section_paralax_h1');?></h1>
				</div>
				<div class="col-md-6">
					<div class="w100 position-relative">
						<div class="home-top_filter text-center">
                            <?php $_re_total_records = Modules::run('real_estate/_get_count', array('real_estate_active' => 1));?>
							<div class="filter-title f-medium"><?php lang_line('home_section_paralax_h1_sub_text', true, $_re_total_records);?></div>
							<div class="filter-tabs">
								<div class="tab-nav d-flex justify-content-center align-items-center f-medium up">
									<div><a class="custom-tab" href="#" data-open="#tab1" data-close=".tab-content"><?php lang_line('label_link_selling');?></a></div>
									<div><a class="custom-tab" href="#" data-open="#tab2" data-close=".tab-content"><?php lang_line('label_link_rent');?></a></div>
                                </div>
								<div class="filter-content">
									<div class="tab-content" id="tab1">
										<div class="panel row">
                                            <?php $paralax_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'sale'));?>
                                            <?php foreach($paralax_categories as $paralax_category){?>
                                                <div class="col-6 col-sm-3">
                                                    <div class="row">
                                                        <a class="custom-tab" href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $paralax_category[lang_column('url')], $lang));?>">
                                                            <div class="image-block">
                                                                <em class="<?php echo $paralax_category['category_icon'];?> c-white size-1_2em-xs size-2em-sm"></em>
                                                            </div>
                                                            <div class="description">
                                                                <?php echo $paralax_category[lang_column('category_title')];?>    
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
										</div>
									</div>
									<div class="tab-content" id="tab2">
										<div class="panel row">
                                            <?php $paralax_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'rent'));?>
                                            <?php foreach($paralax_categories as $paralax_category){?>
                                                <div class="col-6 col-sm-3">
                                                    <div class="row">
                                                        <a class="custom-tab" href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $paralax_category[lang_column('url')], $lang));?>">
                                                            <div class="image-block">
                                                                <em class="<?php echo $paralax_category['category_icon'];?> c-white size-1_2em-xs size-2em-sm"></em>
                                                            </div>
                                                            <div class="description">
                                                                <?php echo $paralax_category[lang_column('category_title')];?>    
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="mobile-menu-block">
		<div class="page_menu">
            <?php $this->load->view(get_theme_view('include/mobile_menu')); ?>
		</div>
	</div>
    <?php if($real_estates_count > 0){?>
        <script>
            is_home = true;
        </script>
        <section class="pt-2em-xs pt-5_5em-sm pb-1_5em-xs pb-4em-sm">
            <div class="container article-container">
                <h2 class="text-center f-bold pb-1_5em"><?php lang_line('home_section_real_estate_header');?></h2>
                <div class="row child-mb-2em mnh-350" id="vitrine-section_home-wr" style="opacity:0;"></div>
            </div>
            <div class="w100 text-center pt-1em">
                <a class="btn btn-more-transparent" href="<?php echo site_url(get_static_uri('discounts', $lang))?>"><?php lang_line('home_section_real_estate_more_link');?></a>
            </div>
        </section>
    <?php }?>
	<?php if($this->lsettings->item('home_residential_complexes_show') == 1 && !empty($complexes)){?>
        <section class="pt-1_5em-xs pt-3_5em-sm pb-0_5em-xs pb-1_5em-sm bc-grey home-complexe">
            <div class="container">
                <div class="flex items-center pb-1em-sm">
                    <h2 class="size-1_2em-xs size-2em-sm f-bold pr-1_5rem c-black"><?php lang_line('home_section_residential_complexes_header');?></h2>
                    <div class="pl-1em flex items-center mb-0_1em-sm">
                        <a class="c-orange up size-0_8em f-medium pt-1em-sm" href="<?php echo site_url(get_static_uri('residential-complexes', $lang));?>"><?php lang_line('home_section_residential_complexes_view_all');?> <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                    </div>
                </div>
                <div class="pt-4 pb-4 complex-carousel owl-carousel">
                    <?php foreach($complexes as $complex){?>
                        <div class="item">
                            <div class="card">
                                <a href="<?php echo site_url(get_dinamyc_uri('residential-complexes/detail/id', $complex[lang_column('url')], $lang))?>" target="_blank" data-tblank>
                                    <img class="card-img-top" src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/thumb_'.$complex['complex_photo']);?>" alt="<?php echo image_title($complex[lang_column('complex_title')]);?>" title="<?php echo image_title($complex[lang_column('complex_title')]);?>">
                                </a>
                                <div class="card-body">
                                    <a href="<?php echo site_url(get_dinamyc_uri('residential-complexes/detail/id', $complex[lang_column('url')], $lang))?>" target="_blank" data-tblank>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <div class="">
                                                    <small><?php echo $complex[lang_column('complex_title')];?></small>
                                                </div>
                                                <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                                    <?php echo $complex[lang_column('complex_address')];?>
                                                </div>
                                            </div>
                                            <div class="f-medium c-black col-3">
                                                <div class="row">
                                                    <div class="pl-0-xs pl-1em-sm">
                                                        <small><?php lang_line('label_price_from');?></small>
                                                        <span class="c-orange size-1_3em pt-0_3em"><?php echo formatNumber($complex['complex_price_from']);?> &euro;</span>
                                                    </div>
                                                </div>
        
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>
    <?php if(!empty($services)){?>
        <section class="pt-2em-xs pt-4_5em-sm pb-0 pb-4em-sm home-services">
            <div class="container">
                <div class="row">
                    <div class="d-flex flex-column col-12 col-lg-4">
                        <div class="pr-7em-md text-center-xs">
                            <h2 class="heading-title-1 f-bold pb-0_8em"><?php lang_line('home_section_service_header');?></h2>
                            <div class="heading-text">
                                <?php lang_line('home_section_services_sidebar_left_text');?>
                            </div>
                            <a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="btn btn-flat btn-contact text-uppercase btn-orange display-none-xs display-inline-block-sm"><?php lang_line('home_btn_contact_us');?></a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <?php if(!empty($services)){?>
                            <div class="row home-services-items">
                                <?php foreach($services as $service){?>
                                    <div class="col-sm col-md-6 col-lg-6">
                                        <a class="service-item service-item-4" href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>">
                                            <div class="icon">
                                                <div class="icon-o">
                                                    <span class="<?php echo $service['service_icon'];?>"></span>
                                                </div>
                                            </div>
                                            <div class="details pl-2em-xs pl-1em-sm">
                                                <div class="lh-1_2em-xs lh-1_1em-sm pt-0_5em-sm size-1_2em-xs size-1_4em-sm f-medium c-black hover-orange">
                                                    <?php echo $service[lang_column('service_name')];?>
                                                </div>
                                                <div class="intro pr-3em-md">
                                                    <?php echo $service[lang_column('service_stext')];?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        <?php }?>
                    </div>
                    <div class="w100 flex center pt-1em">
                        <a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="display-block-xs display-none-sm btn btn-flat btn-contact text-uppercase btn-orange"><?php lang_line('home_btn_contact_us');?></a>
                    </div>
                </div>
            </div>
        </section>
    <?php }?>
    <?php if(!empty($blogs)){?>
        <section class="pt-1_5em-xs pt-3_5em-sm bc-grey article-section">
            <div class="container">
                <div class="flex bottom pb-1_5em-xs">
                    <h2 class="f-bold pr-1_5rem"><?php lang_line('home_section_blog_header');?></h2>
                    <div class="pl-1em flex items-center mb-0_1em">
                        <a class="c-orange up size-0_8em f-medium" href="<?php echo site_url(get_static_uri('blog', $lang));?>"><?php lang_line('home_section_blog_more_link');?> <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                    </div>
                </div>
                <div class="row pt-3em-sm pb-1em-xs pb-1_5em-sm">
                    <?php $articles_aditional_class = '';?>
                    <?php foreach($blogs as $blog){?>
                        <div class="<?php echo $articles_aditional_class;?> col-sm col-md-6 col-lg-4">
                            <div class="card">
                                <?php if(image_exist('files/'.$blog['blog_photo'])){?>
                                    <a href="<?php echo site_url(get_dinamyc_uri('blog', '/'.$blog[lang_column('url')], $lang));?>">
                                        <img class="card-img-top" src="<?php echo site_url(get_image_thumb('files/'.$blog['blog_photo'], 'h215', array('h' => 215)));?>" alt="<?php echo image_title($blog[lang_column('mt')]);?>">
                                    </a>
                                <?php }?>
                                <div class="card-body">
                                    <a href="<?php echo site_url(get_dinamyc_uri('blog', '/'.$blog[lang_column('url')], $lang));?>">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <div class="">
                                                    <small>
                                                    <?php 
                                                        $blog_date = array(
                                                            formatDate($blog['blog_date'], 'j'),
                                                            $this->lang->line('cal_'.mb_strtolower(formatDate($blog['blog_date'], 'F'))),
                                                            formatDate($blog['blog_date'], 'Y')
                                                        );
                                                    ?>
                                                    <?php echo implode(' ', $blog_date);?>
                                                    </small>
                                                </div>
                                                <div class="lh-1_1em-xs f-medium c-black size-1_5em-xs size-1_3em-sm pt-0_3em pb-0_3em hover-orange">
                                                    <?php echo $blog[lang_column('blog_title')];?>
                                                </div>
                                                <div class="c-black">
                                                    <p><?php echo $blog[lang_column('blog_small_description')];?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php $articles_aditional_class = 'd-none d-sm-block';?>
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>