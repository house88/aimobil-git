<div class="container">
    <div class="row">
        <?php if(!empty($page_detail[lang_column('page_image')])){?>
            <div class="col-12">
                <div class="vacancies_header">
                    <img src="<?php echo site_url('files/'.$page_detail[lang_column('page_image')]);?>" alt="<?php echo image_title($page_detail[lang_column('mt')]);?>" title="<?php echo image_title($page_detail[lang_column('mt')]);?>">
                </div>
            </div>
        <?php }?>
        <div class="d-lg-flex col-xl-3">
            <aside>
                <aside class="sidebar">
                    <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
                    <div class="sidebar__block mb-0">
                        <div class="sidebar__block-text">
                            <?php echo $page_detail[lang_column('page_text')];?>
                        </div>
                    </div>
                </aside>
            </aside>
        </div>
        <div class="d-lg-flex col-xl-9">
            <div class="ai-acordion-wr" id="ai-vacancies-accordion">
                <?php if(!empty($vacancies)){?>
                    <?php foreach($vacancies as $vacancy){?>
                        <article class="ai-acordion">
                            <div class="ai-acordion__head" data-toggle="collapse" data-target="#vacancy-body-<?php echo $vacancy['id_vacancy'];?>">
                                <div class="fai fai-down-arrow-o"></div>
                                <?php echo $vacancy[lang_column('vacancy_name')];?>
                            </div>
                            <div class="ai-acordion__body collapse" id="vacancy-body-<?php echo $vacancy['id_vacancy'];?>" class="collapse" data-parent="#ai-vacancies-accordion">
                                <div class="ai-acordion__body-text">
                                    <?php echo $vacancy[lang_column('vacancy_text')];?>
                                </div>
                                <div class="ai-acordion__body-bottom">
                                    <div class="row">
                                        <div class="col-lg col-xl-5">
                                            <div class="ai-acordion__body-bottom-text mb-3 mb-lg-0">
                                                <?php lang_line('vacancy_cv_call_to_action_text');?>
                                            </div>
                                        </div>
                                        <div class="col-lg col-xl-7">
                                            <div class="btn-group w-100pr">
                                                <span class="btn btn-flat btn-file btn-block btn-h50 bg-white text-left">
                                                    <span class="text" data-placeholder="<span class='placeholder-text'><?php lang_line('cv_file_placeholder');?></span>"><span class='placeholder-text'><?php lang_line('cv_file_placeholder');?></span></span>
                                                    <input class="select_cv" type="file" name="userfile" title=" ">
                                                </span>
                                                <span class="btn btn-flat btn-h50 text-uppercase bg-orange flex-shrink-0 call-function disabled" data-callback="send_cv" data-vacancy="<?php echo $vacancy['id_vacancy'];?>">
                                                    <?php lang_line('cv_file_upload_btn_text');?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php }?>            
                <?php } else{?>
                    <div class="col">
                        <div class="error error__no-data">
                            <div class="error__no-data-message">
                                <div class="icon">
                                    <i class="fai fai-warning-o"></i>
                                </div>
                                <?php lang_line('no_data');?>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>