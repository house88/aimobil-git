<?php $_price = getPrice($real_estate);?>
<?php $_credit = null;?>
<?php if($real_estate['real_estate_offer_type'] == 'sale'){?>
	<?php $_credit = getCredit($real_estate);?>
<?php }?>
<section class="pt-1em-xs pt-3em-sm pb-1_5em real-estate-detail-page">
	<div class="container article-container">
		<div class="h1-fix flex items-center space-between pb-0_7em">
			<h1>
				<?php echo $real_estate[lang_column('real_estate_full_title')];?>
			</h1>
			<span class="display-none-xs">ID <?php echo orderNumberOnly($real_estate['id_real_estate']);?></span>
		</div>
		<div class="c-mid-grey flex items-center lh-0_7em pb-0_9em-xs pb-1_4em-sm space-between-xs">
			<div class="page-viewed">
				<div class="page-viewed-icon">
					<em class="fai fai-eye-o"></em>
				</div>
				<div class="page-viewed-text">
					<?php echo $real_estate['real_estate_viewed'];?> <?php lang_line('label_viewed');?>
				</div>
			</div>
			<div class="display-flex display-none-sm size-1em-xs">
				ID <?php echo orderNumberOnly($real_estate['id_real_estate']);?>
			</div>
		</div>
		
		<?php 
			$_is_sold_reserved_class = '';
			$_is_sold_reserved_title = '';
			if($real_estate['real_estate_sold'] == 1){
				$_is_sold_reserved_class = 'grid-item sold';
				$_is_sold_reserved_title = '<div class="sold-title">'.lang_line('label_sold', false).'</div>';
			} elseif($real_estate['real_estate_reserved'] == 1){
				$_is_sold_reserved_class = 'reserved';
				$_is_sold_reserved_title = '<div class="reserved-title">'.lang_line('label_reserved', false).'</div>';
			}
		?>
		<!-- NEW SLIDER -->		
		<div class="w100-sm single-object" style="background-color: rgba(0,0,0,.6)">
			<div class="flex-0_5em-xs flex-1em-sm block-itr">
				<div class="col-xs-12 col-sm-8 pb-2em-xs carousel-grand-parent">
					<div class="pr-1_8em-sm carousel-parent pos-rel">
						<a class="wish_list fai fai-like <?php echo set_favorite($real_estate['id_real_estate'], 'imobil', 'hasWishList');?> call-function" data-callback="set_favorite" data-item="<?php echo $real_estate['id_real_estate'];?>" data-type="imobil" href="#"></a>
						<a class="see-map fai fai-marker-stroke" href="#"><span class="pl-0_5em size-0_8em display-block-xs display-none-sm"><?php lang_line('btn_hsow_map_text');?></span></a>
						<div class="single-page-carousel owl-carousel">
							<a class="item open-modal <?php echo $_is_sold_reserved_class;?>" href="<?php echo current_url();?>#slide1" >
								<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate['real_estate_photo']);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>"  title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>">
								<?php echo $_is_sold_reserved_title;?>
							</a>
                            <?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
                            <?php if(!empty($real_estate_photos)){?>
                            	<?php $_photo_index = 2;?>
                                <?php foreach($real_estate_photos as $real_estate_photo){?>
                                    <?php if($real_estate_photo != $real_estate['real_estate_photo']){?>
                                        <a class="item open-modal <?php echo $_is_sold_reserved_class;?>" href="<?php echo current_url();?>#slide<?php echo $_photo_index;?>">
											<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate_photo);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>"  title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>">
											<?php echo $_is_sold_reserved_title;?>
                                        </a>
                                    	<?php $_photo_index++;?>
                                    <?php }?>
                                <?php }?>
							<?php }?>
						</div>
						<div class="single__buttons">
							<div class="prev">
								<em class="fai fai-left-arrow-o" aria-hidden="true"></em>
							</div>
							<div class="next">
								<em class="fai fai-right-arrow-o" aria-hidden="true"></em>
							</div>
						</div>
						<?php if($real_estate['real_estate_exclusive'] == 1){?>
							<div class="exclusive-wr">
								<div class="exclusive"><?php lang_line('label_exclusive');?></div>
							</div>
                        <?php }?>
					</div>
				</div>
				<div class="col-xs-12 col-md-4 map-parent" style="display:block;">
					<div class="map-inner h100 w100 p-1_7em-sm pl-0 pr-2em-sm">
						<a class="close-map fai fai-x-o" href=""></a>
						<div id="<?php echo 're_map_'.$real_estate['id_real_estate'];?>" class="single_map h100 w100"></div>
                        <?php
                            $markers[] = array(
                                'latitude' => $real_estate['real_estate_lat'],
                                'longitude' => $real_estate['real_estate_lng'],
                                'container' => 're_map_'.$real_estate['id_real_estate']
                            );	
                        ?>
						<script type="text/javascript">
							init_google_maps = true;
                            markers = <?php echo json_encode($markers);?>;
                        </script>
					</div>
				</div>
			</div>
			<div class="back-carousels-parent">
				<div class="flex pad">
					<div class="col-sm-6">
						<div class="back-carousel owl-carousel">
                            <?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
							<?php $_first_background = array_pop($real_estate_photos);?>
							<div class="back-item" style="background: url('<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$_first_background);?>') no-repeat center center /cover"></div>
							<?php if(!empty($real_estate_photos)){?>
                                <?php foreach($real_estate_photos as $real_estate_photo){?>
									<div class="back-item" style="background: url('<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate_photo);?>') no-repeat center center /cover"></div>
                                <?php }?>
							<?php }?>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="back-carousel owl-carousel">
                            <?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
							<?php $_last_background = array_shift($real_estate_photos);?>
							<?php if(!empty($real_estate_photos)){?>
                                <?php foreach($real_estate_photos as $real_estate_photo){?>
									<div class="back-item" style="background: url('<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate_photo);?>') no-repeat center center /cover"></div>
                                <?php }?>
							<?php }?>
							<div class="back-item" style="background: url('<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$_last_background);?>') no-repeat center center /cover"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- NEW SLIDER END -->
	</div>
	<div class="container mt-1em-xs mt-1_2em-sm" id="real_estate_print-wrapper">
		<div class="flex-1em">
			<div class="col-12 col-xl-8 parent-123">
				<div class="mobile-bottom-border flex pad items-center space-between pb-1_3em-xs">
					<div class="display-none-xs display-none-sm items-center">
						<div class="size-1_1em pr-1em lh-1em">
							<?php if($real_estate['real_estate_discount_type'] == 'new_price'){?>
								<span class="old-price"><?php echo $_price['display_discount_text'];?></span>
							<?php }?>

							<span class="c-orange size-1_1em-xs size-1_7em-sm f-medium"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
							<?php if($real_estate['real_estate_offer_type'] == 'rent'){?>
								<small class="c-grey-2"><?php lang_line('label_rent_month');?></small>
							<?php }?>
						</div>
						<?php if($real_estate['real_estate_offer_type'] == 'sale'){?>
							<?php if($_credit){?>
								<div class="c-grey-2 lh-1em size-0_9em-xs size-0_9em-sm">
									<?php lang_line('label_mortgage_text');?> <?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?>
								</div>
							<?php }?>
						<?php }?>
					</div>
					<div class="display-block-xs flex items-center">
						<div class="size-1_1em pr-0_5em flex">
							<span class="c-orange size-1_7em f-bold"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
							<?php if($real_estate['real_estate_discount_type'] == 'new_price'){?>
								<span class="old-price"><?php echo $_price['display_discount_text'];?></span>
							<?php }?>
							<?php if($real_estate['real_estate_offer_type'] == 'rent'){?>
								<small class="c-grey-2"><?php lang_line('label_rent_month');?></small>
							<?php }?>
						</div>
						<?php if($real_estate['real_estate_offer_type'] == 'sale'){?>
							<?php if($_credit){?>
								<div class="real-estate_credit-available">
									<span class="text mr-1"><?php lang_line('label_mortgage_text');?></span>
									<span class="amount">
										<?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?>
									</span>
								</div>
							<?php }?>
						<?php }?>
					</div>
					<div class="offer-share-wr">
						<span class="btn btn-primary bg-white b-orange offer-price-btn call-popup" id="offer-price-btn" data-popup="general-popup" href="#" data-href="<?php echo site_url('notify/popup_forms/send_offer/'.$real_estate['id_real_estate'])?>">
							<span class="icon-wr">
								<em class="fai fai-bell-o c-orange"></em> 
							</span>
							<span class="text-wr"><?php lang_line('label_offer_price');?></span>
						</span>
						<span class="btn btn-primary share-link-mobile">
							<em class="fai fai-share-stroke" aria-hidden="true"></em>
							<span><?php lang_line('label_share_text');?></span>
						</span>
					</div>
				</div>
				<div class="social-share-block">
					<div class="flex-0-xs items-center space-between">
						<div class="size-1_1em pr-0_3em">
							<a href="#" class="call-function" data-callback="popup_share" data-social="facebook" data-title="<?php echo $real_estate[lang_column('real_estate_full_title')];?>" data-url="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>">
								<img src="<?php echo site_url('theme/access-imobil/css/images/img/icons/facebook-like.png');?>" alt="fb">
							</a>
						</div>
						<div class="size-1_1em pr-0_3em">
							<a href="#" class="call-function" data-callback="popup_share" data-social="twitter" data-title="<?php echo $real_estate[lang_column('real_estate_full_title')];?>" data-url="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>">
								<img src="<?php echo site_url('theme/access-imobil/css/images/img/icons/tweet-like.png');?>" alt="tw">
							</a>
						</div>
						<div class="size-1_1em">
							<a href="" class="call-function" data-callback="popup_share" data-social="gplus" data-title="<?php echo $real_estate[lang_column('real_estate_full_title')];?>" data-url="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>">
								<img src="<?php echo site_url('theme/access-imobil/css/images/img/icons/google-like.png');?>" alt="gl">
							</a>
						</div>
					</div>
				</div>
				
				<div class="w100 display-none-xs display-block-sm p-1_2em br-1-grey mt-1_1em-sm mb-1_9em">
					<div class="flex items-center space-between">
						<div class="flex-0_5em  items-center">
							<div class="shrink-0 w-100_i h-100_i">
								<a href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>" class="img-object-fit w-100_i h-100_i">
									<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" alt="<?php echo image_title($re_manager[lang_column('user_name')]);?>" title="<?php echo image_title($re_manager[lang_column('user_name')]);?>">
								</a>
							</div>
							<div>
								<div class="c-mid-grey size-1em"><?php echo $re_manager[lang_column('position_name')];?></div>
								<a class="size-1_5em f-medium c-black hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>"><?php echo $re_manager[lang_column('user_name')];?></a>
							</div>
						</div>

						<div class="pt-2em-xs">
							<div class="manager-card pt-0_5em">
								<div class="flex">
									<div><a class="desktop-page-phone" href="tel:<?php echo $re_manager['user_phone'];?>"><?php echo $re_manager['user_phone'];?></a></div>
									<div class="ml-1_3em">
										<a class="page-viber" href="viber://chat?number=<?php echo $re_manager['user_phone'];?>">Viber</a>
									</div>
									<div class="ml-1_2em">
										<a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')].'?t=pdf', $lang));?>" target="_blank" data-tblank class="btn btn-icon-pdf">
											<em class="fai fai-file-pdf"></em>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="w100 mt-1_3em-xs display-block-xs display-none-sm">
					<div class="user-card-mobile">
						<div class="user-card-mobile__image">
							<a href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>" class="image-cover">
								<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" alt="<?php echo image_title($re_manager[lang_column('user_name')]);?>" title="<?php echo image_title($re_manager[lang_column('user_name')]);?>">
							</a>
						</div>
						<div class="user-card-mobile__contacts">
							<div class="contact-name">
								<a href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>"><?php echo $re_manager[lang_column('user_name')];?></a>
							</div>
							<div class="contact-phone">
								<a class="btn btn-flat bg-orange" href="tel:<?php echo $re_manager['user_phone'];?>">
									<em class="fai fai-phone"></em> <?php echo $re_manager['user_phone'];?>
								</a>
							</div>
							<div class="contact-info">
								<a class="btn btn-flat bg-viber" href="viber://chat?number=<?php echo $re_manager['user_phone'];?>">
									<em class="fai fai-viber"></em> Viber
								</a>
								<a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')].'?t=pdf', $lang));?>" target="_blank" data-tblank class="btn btn-icon-pdf">
									<em class="fai fai-file-pdf"></em>
								</a>
							</div>
						</div>
					</div>
				</div>

				<?php if(!empty($real_estate['re_properties'])){?>
					<div class="flex-1em pb-1_9em">
						<?php foreach($real_estate['re_properties'] as $re_property){?>
							<div class="col-6">
								<div class="pt-1_1em pb-1_2em br-bottom-1-grey size-0_9em">
									<div class="c-grey-1 lh-1_4em">
										<?php echo $re_property['property_name'];?>
									</div>
									<div class="f-bold c-black lh-1em">
										<?php echo $re_property['property_value'];?>
										<?php if($re_property['show_unit']){?>
											<?php echo $re_property['unit_name'];?>
										<?php }?>
									</div>
								</div>
							</div>
						<?php }?>
					</div>
				<?php }?>
				<!-- Button trigger modal -->
				
				<div class="mobile-colapse-wr">
					<div class="mobile-collapse-parent">
						<div class="mobile-collapse-toggle f-medium size-1_1-xs size-1_6em-sm lh-1em c-black"><?php lang_line('label_description');?></div>
						<div class="mobile-collapse-inner w100 single-article">
							<div class="html-text pt-20">
								<?php echo $real_estate[lang_column('real_estate_text')];?>
							</div>
						</div>
					</div>
				</div>
				
				<?php $re_icons = json_decode($real_estate['real_estate_icons'], true);?>
				<?php if(!empty($re_icons)){?>
					<div class="mobile-colapse-wr page-icons">
						<div class="mobile-collapse-parent">
							<div class="mobile-collapse-toggle f-medium size-1_1-xs size-1_6em-sm lh-1em c-black"><?php lang_line('label_icons');?></div>
							<div class="mobile-collapse-inner w100">
								<div class="characteristic-icons flex-1em mt-1em-xs mt-1_7em-sm child-mb-0_6em-xs child-mb-1em-sm">
									<?php foreach($icons as $icon){?>
										<?php if(array_key_exists($icon['id_icon'], $re_icons)){?>
											<div class="col-6 col-sm-6 col-md-3">
												<div class="flex items-center nowrap">
													<div class="icon shrink-0">
														<div class="icon-o">
															<em class="<?php echo $icon['icon_class'];?>"></em>
														</div>
													</div>
													<div class="pl-2em">
														<p class="mb-0 c-grey size-0_9em"><?php echo $icon[lang_column('icon_name')];?></p>
													</div>
												</div>
											</div>										
										<?php }?>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				<?php }?>

				<?php if($real_estate['real_estate_offer_type'] == 'sale'){?>
					<div class="mobile-colapse-wr calculator-block calculator-wr">
						<div class="mobile-collapse-parent">
							<div class="mobile-collapse-toggle f-medium size-1_1-xs size-1_6em-sm lh-1em c-black"><?php lang_line('calc_header');?></div>
							<div class="mobile-collapse-inner w100">
								<div class="size-0_8em-xs lh-1_3em-xs size-0_9em-sm mobile-collapse-inner-text w100">
									<?php lang_line('calc_header_note');?>
								</div>
								<div class="w100 pt-1em">
									<?php echo Modules::run('calculator/_show_body', array('price_default' => $_price['display_price'], 'currency_symbol' => $_price['currency_symbol'], 'calc_on_item' => true));?>
								</div>
								<div class="w100 size-0_9em-xs pr-1em-xs">
									<p><?php lang_line('calc_info_link_more');?></p>
								</div>
							</div>
						</div>
					</div>
				<?php }?>

				<div class="w100 display-none-xs display-block-sm p-1_2em br-1-grey mb-2em">
					<div class="flex items-center space-between">
						<div class="flex items-center">
							<a href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>" class="shrink-0 img-object-fit w-100_i h-100_i">
								<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" alt="<?php echo image_title($re_manager[lang_column('user_name')]);?>" title="<?php echo image_title($re_manager[lang_column('user_name')]);?>">
							</a>
							<div class="pl-30">
								<div class="c-mid-grey size-1em"><?php echo $re_manager[lang_column('position_name')];?></div>
								<a class="size-1_5em f-medium c-black hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>"><?php echo $re_manager[lang_column('user_name')];?></a>
							</div>
						</div>

						<div class="pt-2em-xs">
							<div class="manager-card pt-0_5em">
								<div class="flex">
									<div><a class="desktop-page-phone" href="tel:<?php echo $re_manager['user_phone'];?>"><?php echo $re_manager['user_phone'];?></a></div>
									<div class="ml-1_3em">
										<a class="page-viber" href="viber://chat?number=<?php echo $re_manager['user_phone'];?>">Viber</a>
									</div>
									<div class="ml-1_2em">
										<a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')].'?t=pdf', $lang));?>" target="_blank" data-tblank class="btn btn-icon-pdf">
											<em class="fai fai-file-pdf"></em>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-xl-4 page-sidebar pt-2em-xs">
				<div class="w100 bc-grey p-1_5em-xs pb-2em-xs pt-2em-xs p-2em-sm mb-2em">
					<div class="w100 br-bottom-1-dark-grey flex pb-2em pt-0_2em lh-1_2em align-items-center">
						<div class="width-1_7em c-orange size-1_8em f-medium text-center-sm lh-0_9em-xs">
							0<sup class="sidebar-sup">%</sup>
						</div>
						<div class="size-1_1em c-black pt-0_2em">
							<?php lang_line('label_text_banner_1');?>
						</div>
					</div>
					<div class="w100 br-bottom-1-dark-grey flex pb-2em pt-2em lh-1_2em align-items-center">
						<div class="width-1_7em c-orange size-1_8em f-medium text-center-sm ">
							<span class="fai fai-shield"></span>
						</div>
						<div class="size-1_1em c-black pt-0_2em">
							<?php lang_line('label_text_banner_2');?>
						</div>
					</div>
					<div class="w100 flex pb-0_3em pt-2em lh-1_2em align-items-center">
						<div class="width-1_7em c-orange size-1_8em f-medium text-center-sm">
							<span class="fai fai-label-o"></span>
						</div>
						<div class="size-1_1em c-black pt-0_2em">
							<?php lang_line('label_text_banner_3');?>
						</div>
					</div>
				</div>
				<div class="w100 mt-1_3em-xs display-block-xs display-none-sm pb-0_4em">
					<div class="w-100 p-0 flex pad nowrap">
						<a href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>" class="shrink-0 manager-photo photo-2 img-object-fit mw-60 mh-60">
							<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" alt="<?php echo image_title($re_manager[lang_column('user_name')]);?>" title="<?php echo image_title($re_manager[lang_column('user_name')]);?>">
						</a>
						<div class="flex-grow-1 bc-white">
							<div class="flex column space-between w100 h100 pl-0_5em-xs  pl-1em-sm">
								<a class="size-1em f-medium c-black lh-1_2em hover-orange" href="<?php echo site_url(get_dinamyc_uri('manager', $re_manager[lang_column('user_url')], $lang));?>"><?php echo $re_manager[lang_column('user_name')];?></a>
								<div class="mt-0_4em-xs">
									<a class="phone-single second-phone" href="tel:<?php echo $re_manager['user_phone'];?>">
										<span class="fai fai-phone"></span>
										<?php echo $re_manager['user_phone'];?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if(!empty($last_viewed)){?>
					<div class="w100 single-page display-none-xs display-block-sm">
						<div class="f-medium size-1_6em lh-0_7em c-black"><?php lang_line('sidebar_last_viewed_header');?></div>
						<div class="flex-1em-md flex-0-lg child-mb-2em">
							<?php foreach($last_viewed as $last_real_estate){?>
								<?php $lv_price = getPrice($last_real_estate);?>
								<div class="col-sm-6 col-md-6 col-xl-12">
									<div class="card h100 box-shadow bc-white mt-2em">
										<?php if($last_real_estate['real_estate_discount_type'] != 'no'){?>
											<div class="discount-badge">
												<div class="badge-title">%</div>
												<?php if($_price['display_discount_text'] != ''){?>
													<div class="badge-content"><?php echo $_price['display_discount_text'];?></div>
												<?php }?>
											</div>
										<?php }?>
										<a class="wish_list fai fai-like <?php echo set_favorite($last_real_estate['id_real_estate'], 'imobil', 'hasWishList');?> call-function" data-callback="set_favorite" data-item="<?php echo $last_real_estate['id_real_estate'];?>" data-type="imobil" href="#"></a>
										<a class="grid-item <?php if($last_real_estate['real_estate_sold'] == 1){?>sold<?php } elseif($last_real_estate['real_estate_reserved'] == 1){?>reserved<?php }?>" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $last_real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
											<img class="card-img-top" src="<?php echo site_url('files/real_estate/'.$last_real_estate['id_real_estate'].'/thumb_'.$last_real_estate['real_estate_photo']);?>" <?php echo img_srcset('files/real_estate/'.$last_real_estate['id_real_estate'], $last_real_estate['real_estate_photo']);?> alt="<?php echo image_title($last_real_estate[lang_column('real_estate_full_title')], 1);?>"  title="<?php echo image_title($last_real_estate[lang_column('real_estate_full_title')], 1);?>">
											<?php if($last_real_estate['real_estate_sold'] == 1){?>
												<div class="sold-title"><?php lang_line('label_sold');?></div>
											<?php } elseif($last_real_estate['real_estate_reserved'] == 1){?>
												<div class="reserved-title"><?php lang_line('label_reserved');?></div>
											<?php }?>
										</a>
										<?php if(isset($lv_re_properties[$last_real_estate['id_real_estate']])){?>
											<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
												<?php foreach($lv_re_properties[$last_real_estate['id_real_estate']] as $lv_re_property){?>
													<div class="nowrap">
														<?php echo $lv_re_property['property_value'];?> 
														<small class="text-muted"><?php echo $lv_re_property['unit_name'];?></small>
													</div>
												<?php }?>
											</div>
										<?php }?>
										<div class="card-body">
											<a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $last_real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
												<div class="d-flex justify-content-between">
													<div class="f-medium c-black pl-1_5em pr-2_5em-md lh-1_3em">
														<?php echo $last_real_estate[lang_column('real_estate_title')];?>
													</div>
													<div class="f-medium c-black col-5">
														<div class="row">
															<div class="pl-1_5em">
																<span class="c-orange item-price"><?php echo $lv_price['display_price'];?> <?php echo $lv_price['currency_symbol'];?></span>
																<?php if($last_real_estate['real_estate_offer_type'] == 'sale'){?>
																	<?php $lv_credit = getCredit($last_real_estate);?>
																	<?php if($lv_credit){?>
																		<small><?php lang_line('label_mortgage_text');?></small>
																		<small><?php echo $lv_credit['display_amount'];?> <?php echo $lv_credit['currency_symbol'];?> / <?php lang_line('label_month');?></small>
																	<?php }?>
																<?php } else{?>
																	<small><?php lang_line('label_rent_month');?></small>
																<?php }?>
															</div>
														</div>

													</div>
												</div>
											</a>
										</div>
									</div>
								</div>
							<?php }?>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</section>
<?php if(!empty($re_same)){?>
	<section class="pt-1_8em-xs pt-2em-sm pb-3_5em-sm bc-grey article-section single-page">
		<div class="container article-container mt-0_5em-sm">
			<div class="w100 text-left pb-1_3em-sm">
				<div class="flex items-center space-between pb-1_8em-xs pb-1em-sm">
					<div>
						<h2 class="f-bold c-black"><?php lang_line('same_objects_header');?></h2>
					</div>
					<div class="oferte-button-parent mt-1em-xs">
						<a href="#" data-href="<?php echo site_url('notify/popup_forms/send_subscribe/'.$real_estate['id_real_estate'])?>" class="oferte-button call-popup" data-popup="general-popup"><?php lang_line('btn_subscribe_same_objects');?></a>
					</div>
				</div>
			</div>
			<div class="row child-mb-2em asCustom">
				<?php foreach($re_same as $same_real_estate){?>
					<?php $sr_price = getPrice($same_real_estate);?>
					<div class="col-12 col-sm-6 col-lg-3">
						<div class="card h100 bc-white pb-0_3em">
							<?php if($same_real_estate['real_estate_discount_type'] != 'no'){?>
								<div class="discount-badge">
									<div class="badge-title">%</div>
									<?php if($_price['display_discount_text'] != ''){?>
										<div class="badge-content"><?php echo $_price['display_discount_text'];?></div>
									<?php }?>
								</div>
							<?php }?>
							<a class="wish_list fai fai-like <?php echo set_favorite($same_real_estate['id_real_estate'], 'imobil', 'hasWishList');?> call-function" data-callback="set_favorite" data-item="<?php echo $same_real_estate['id_real_estate'];?>" data-type="imobil" href="#"></a>
							<a class="grid-item <?php if($same_real_estate['real_estate_sold'] == 1){?>sold<?php } elseif($same_real_estate['real_estate_reserved'] == 1){?>reserved<?php }?>" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $same_real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
								<img class="card-img-top" src="<?php echo site_url('files/real_estate/'.$same_real_estate['id_real_estate'].'/thumb_'.$same_real_estate['real_estate_photo']);?>" <?php echo img_srcset('files/real_estate/'.$same_real_estate['id_real_estate'], $same_real_estate['real_estate_photo']);?> alt="<?php echo image_title($same_real_estate[lang_column('real_estate_full_title')], 1);?>" title="<?php echo image_title($same_real_estate[lang_column('real_estate_full_title')], 1);?>">
								<?php if($same_real_estate['real_estate_sold'] == 1){?>
									<div class="sold-title"><?php lang_line('label_sold');?></div>
								<?php } elseif($same_real_estate['real_estate_reserved'] == 1){?>
									<div class="reserved-title"><?php lang_line('label_reserved');?></div>
								<?php }?>
							</a>
							<?php if(isset($same_re_properties[$same_real_estate['id_real_estate']])){?>
								<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
									<?php foreach($same_re_properties[$same_real_estate['id_real_estate']] as $same_re_property){?>
										<div class="nowrap">
											<?php echo $same_re_property['property_value'];?> 
											<small class="text-muted"><?php echo $same_re_property['unit_name'];?></small>
										</div>
									<?php }?>
								</div>
							<?php }?>
							<div class="card-body">
								<a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $same_real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black pl-1em">
											<?php echo $same_real_estate[lang_column('real_estate_title')];?>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange item-price"><?php echo $sr_price['display_price'];?> <?php echo $sr_price['currency_symbol'];?></span>
													<?php if($same_real_estate['real_estate_offer_type'] == 'sale'){?>
														<?php $sr_credit = getCredit($same_real_estate);?>
														<?php if($sr_credit){?>
															<small><?php lang_line('label_mortgage_text');?></small>
															<small><?php echo $sr_credit['display_amount'];?> <?php echo $sr_credit['currency_symbol'];?> / <?php lang_line('label_month');?></small>
														<?php }?>
													<?php } else{?>
														<small><?php lang_line('label_rent_month');?></small>
													<?php }?>
												</div>
											</div>
	
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</section>
<?php }?>

<div class="single-modal">
	<div class="inner">
		<button type="button" class="close-single-modal"><em class="fai fai-x-o"></em></button>
		<div class="schedule-row">
			<div class="schedule-column">
				<div class="flex-1em items-center modal-block-title">
					<div class="modal-title c-white f-medium size-1_1em-xs size-1_1em-sm"><?php echo $real_estate[lang_column('real_estate_full_title')];?></div>
					<div class="flex-1em items-center">
						<div class="size-1_1em c-orange f-medium">
							<div class="pl-1_3em-sm">
								<?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?>
								<?php if($real_estate['real_estate_offer_type'] == 'rent'){?>
									<small class="c-white"><?php lang_line('label_rent_month');?></small>
								<?php }?>
							</div>
						</div>
						<?php if($real_estate['real_estate_offer_type'] == 'sale' && $_credit){?>
							<div>
								<div class="size-0_8em c-mid-grey lh-1_1em"><?php lang_line('label_mortgage_text');?> </div>
								<div class="size-0_8em c-mid-grey lh-1_1em"><?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?></div>
							</div>
						<?php }?>
					</div>
				</div>
				<div class="thumb-carousel">
					<div class="modal-carousel owl-carousel" id="video__about" data-slider-id="1">
						<div class="item-carousel" data-hash="slide1">
							<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate['real_estate_photo']);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>" title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>">
						</div>
						<?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
						<?php if(!empty($real_estate_photos)){?>
							<?php $_photo_index = 2;?>
							<?php foreach($real_estate_photos as $real_estate_photo){?>
								<?php if($real_estate_photo != $real_estate['real_estate_photo']){?>
									<div class="item-carousel" data-hash="slide<?php echo $_photo_index;?>">
										<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate_photo);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>" title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>">
									</div>
									<?php $_photo_index++;?>
								<?php }?>
							<?php }?>
						<?php }?>
					</div>
					<div class="owl-thumbs" data-slider-id="1">
						<div class="owl-thumb-item"><img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate['real_estate_photo']);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>" title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>"></div>
						<?php if(!empty($real_estate_photos)){?>
							<?php $_photo_index = 2;?>
							<?php foreach($real_estate_photos as $real_estate_photo){?>
								<?php if($real_estate_photo != $real_estate['real_estate_photo']){?>
									<div class="owl-thumb-item"><img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate_photo);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>" title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>"></div>
									<?php $_photo_index++;?>
								<?php }?>
							<?php }?>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>