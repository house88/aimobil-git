<div class="remodal-box">
	<div class="box-body">
		<form id="form_subscribe">
            <div class="box-header">
                <h2><?php lang_line('header_popup_abonare');?></h2>
            </div>
            <div class="row">
                <div class="d-flex flex-column col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="<?php lang_line('notify_feedback_label_email');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <div class="form-select">
                            <select name="subscribe_period" class="form-control option-placeholder">
                                <option value="" selected><?php lang_line('label_subscribe_select_period');?></option>
                                <?php $subscribe_months = explode(',', $this->lsettings->item('subscribe_months_list'));?>
                                <?php $subscribe_months = array_filter($subscribe_months);?>
                                <?php if(!empty($subscribe_months)){?>
                                    <?php foreach($subscribe_months as $subscribe_month){?>
                                        <option value="<?php echo $subscribe_month;?>"><?php lang_line('option_subscribe_period_text', true, array($subscribe_month));?></option>
                                    <?php }?>
                                <?php }?>
                            </select>
                            <script>
                                $(function(){
                                    $('select[name="subscribe_period"]').on('change', function(){
                                        var curent_value = $(this).val();
                                        if(curent_value == ''){
                                            $(this).addClass('option-placeholder');
                                        } else{
                                            $(this).removeClass('option-placeholder');
                                        }
                                    });
                                });
                            </script>
                            <div class="icon">
                                <div class="fai fai-down-arrow-o"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
				<input type="hidden" name="id_real_estate" value="<?php echo $id_real_estate;?>">
                <div class="d-flex flex-row align-items-center col-12">
                    <button class="btn btn-flat text-uppercase bg-orange call-function" data-callback="send_subscribe" type="button">
                        <?php lang_line('feedback_btn_send');?>
                    </button>
                </div>
            </div>
        </form>
	</div>
</div>