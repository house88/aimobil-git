<?php if(!empty($active_filters)){?>
    <?php foreach($active_filters as $active_filter){?>
        <div>
            <div class="scroll-filter-item">
                <div>
                    <?php echo $active_filter['property_name'];?>: <?php echo $active_filter['property_value'];?>
                </div>
                <a href="#" class="close-item fai fai-x-o call-function" data-callback="remove_filter" data-target="<?php echo $active_filter['target'];?>" data-target-option="<?php if(isset($active_filter['target_option'])){echo $active_filter['target_option'];}?>" data-target-type="<?php echo $active_filter['target_type'];?>"></a>
            </div>
        </div>
    <?php }?>
<?php }?>
<div class="flex-0_5em shrink-0 reset-filter-button-parent call-function" data-callback="reset_catalog_filters">
    <div class="icon lh-1em">
        <em class="fai fai-x-square"></em>
    </div>
    <div class="size-0_8em lh-1em">
        Resetare filtru
    </div>
</div>