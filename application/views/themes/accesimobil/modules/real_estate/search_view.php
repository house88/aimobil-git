<section class="pt-3em">
	<div class="container article-container">
		<h2 class="f-medium c-black pb-0_9em">Rezultatul cautarii</h2>
		<div class="w100 text-left pb-1em-xs pb-2em-sm mb-0_3em-sm">
			<div class="field-result flex items-center size-0_9em">
                <?php echo tpl_replace(lang_line('category_search_text_result', false), array('{{counter}}' => $records_total, '{{search_keywords}}' => clean_output($get_keywords_text)));?>
			</div>
		</div>
		<div class="row child-mb-2em" id="search_list-wr">
            <?php $this->load->view(get_theme_view('modules/real_estate/search_list_view')); ?>
		</div>
	</div>
    <script>
        var search_page = 1;
        var search_keywords = '<?php echo clean_output(@$get_keywords);?>';
        var search_url = '<?php echo site_url(get_static_uri('catalog/search', $lang))?>';
    </script>
	<div class="w100 text-center pb-3_5em">
		<div class="container flex center" id="search_pagination">
            <?php echo $pagination;?>
		</div>
	</div>
</section>