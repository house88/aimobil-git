<div class="remodal-box">
	<div class="box-body">
		<form id="form_offer">
			<div class="box-header">
				<h2>Oferă prețul tău</h2>
			</div>
            <div class="row">
                <div class="d-flex flex-column col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="offer_price" placeholder="<?php lang_line('form_label_offer_price');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="<?php lang_line('notify_feedback_label_name');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="<?php lang_line('notify_feedback_label_phone');?>" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
				<input type="hidden" name="id_real_estate" value="<?php echo $id_real_estate;?>">
                <div class="d-flex flex-row align-items-center col-12">
                    <button class="btn btn-flat text-uppercase bg-orange call-function" data-callback="send_offer" type="button">
                        <?php lang_line('feedback_btn_send');?>
                    </button>
                </div>
            </div>
        </form>
	</div>
</div>