<?php $_banner_is_displayed = false;?>
<?php $_banner_index = 0;?>
<?php $_banner = Modules::run('banner/_get_by_alias', 'real_estate_inside');?>
<?php if(!empty($real_estates)){?>
    <?php foreach($real_estates as $real_estate){?>
        <?php $_price = getPrice($real_estate);?>
        <div class="flex pad box-shadow mb-1_9em">
            <div class="col-xs-12 card custom-list-left-col">
                <div class="w100 pos-rel">
                    <a class="wish_list fai fai-like <?php echo set_favorite($real_estate['id_real_estate'], 'imobil', 'hasWishList');?> call-function" data-callback="set_favorite" data-item="<?php echo $real_estate['id_real_estate'];?>" data-type="imobil" href="#"></a>
                    <?php if($real_estate['real_estate_discount_type'] != 'no'){?>
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <?php if($_price['display_discount_text'] != ''){?>
                                <div class="badge-content"><?php echo $_price['display_discount_text'];?></div>
                            <?php }?>
                        </div>
                    <?php }?>
                    <div class="list-page-carousel owl-carousel ">
                        <div class="item grid-item h-220_i <?php if($real_estate['real_estate_sold'] == 1){?>sold<?php } elseif($real_estate['real_estate_reserved'] == 1){?>reserved<?php }?>">
                            <img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate['real_estate_photo']);?>" <?php echo img_srcset('files/real_estate/'.$real_estate['id_real_estate'], $real_estate['real_estate_photo']);?> alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>"  title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>">
                            <?php if($real_estate['real_estate_sold'] == 1){?>
                                <div class="sold-title"><?php lang_line('label_sold');?></div>
                            <?php } elseif($real_estate['real_estate_reserved'] == 1){?>
                                <div class="reserved-title"><?php lang_line('label_reserved');?></div>
                            <?php }?>
                        </div>
                        <?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
                        <?php if(!empty($real_estate_photos)){?>
                            <?php $_photo_index = 2;?>
                            <?php foreach($real_estate_photos as $real_estate_photo){?>
                                <?php if($real_estate_photo != $real_estate['real_estate_photo']){?>
                                    <div class="item grid-item h-220_i <?php if($real_estate['real_estate_sold'] == 1){?>sold<?php } elseif($real_estate['real_estate_reserved'] == 1){?>reserved<?php }?>">
                                        <img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate_photo);?>" <?php echo img_srcset('files/real_estate/'.$real_estate['id_real_estate'], $real_estate['real_estate_photo']);?> alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>"  title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], $_photo_index);?>">
                                        <?php if($real_estate['real_estate_sold'] == 1){?>
                                            <div class="sold-title"><?php lang_line('label_sold');?></div>
                                        <?php } elseif($real_estate['real_estate_reserved'] == 1){?>
                                            <div class="reserved-title"><?php lang_line('label_reserved');?></div>
                                        <?php }?>
                                    </div>
                                    <?php $_photo_index++;?>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md custom-parent-cols">
                <div class="h100 w100 pb-0_5em">
                    <?php if(isset($re_properties[$real_estate['id_real_estate']])){?>
                        <div class="list-top-description">
                            <div class="flex-0_5em top-description-child">
                                <?php foreach($re_properties[$real_estate['id_real_estate']] as $re_property){?>
                                    <div>
                                        <span><?php echo $re_property['property_value'];?></span>
                                        <?php echo $re_property['unit_name'];?>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="list-id">
                                ID <?php echo orderNumberOnly($real_estate['id_real_estate']);?>
                            </div>
                        </div>
                    <?php }?>
                    <div class="flex nowrap-sm">
                        <div class="custom-left-list-col">
                            <div class="size-1_1em f-bold c-black lh-1_2em">
                                <a class="hover-orange" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank><?php echo strip_tags_with_whitespace($real_estate[lang_column('real_estate_title')]);?></a>
                            </div>
                            <div class="flex f-regular items-center">
                                <div><span class="fai fai-eye-o c-grey size-0_7em c-mid-grey lh-1_2em"></span></div>
                                <div class="pl-0_5em c-grey size-0_8em c-mid-grey lh-0_7em">
                                    <?php echo $real_estate['real_estate_viewed'];?>
                                </div>
                            </div>
                            <div class="list-catalog-text ">
                                <?php echo text_elipsis($real_estate[lang_column('real_estate_text_clean')], 130);?>
                            </div>
                            <div class="list-more-link">
                                <a class="c-orange up size-0_8em f-medium hover-black" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>Detalii <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                            </div>
                        </div>
                        <div class="custom-right-inner-col">
                            <div class="flex column space-between">
                                <span class="c-orange size-1_3em f-medium lh-1_1em"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
                                <?php $_credit = getCredit($real_estate);?>
                                <?php if($_credit){?>
                                    <small class="c-mid-grey"><?php lang_line('label_mortgage_text');?> <?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?></small>
                                <?php }?>
                                <div class="flex nowrap mt-1_1em">
                                    <a href="<?php echo site_url(get_dinamyc_uri('manager', $re_managers[$real_estate['id_manager']][lang_column('user_url')], $lang));?>" class="shrink-0 list-image">
                                        <img class="" src="<?php echo site_url(getImage('files/users/'.$re_managers[$real_estate['id_manager']]['user_photo']));?>" width="80" height="100" alt="<?php echo image_title($re_managers[$real_estate['id_manager']][lang_column('user_name')]);?>" title="<?php echo image_title($re_managers[$real_estate['id_manager']][lang_column('user_name')]);?>">
                                    </a>
                                    <div class="pl-1em">
                                        <a href="<?php echo site_url(get_dinamyc_uri('manager', $re_managers[$real_estate['id_manager']][lang_column('user_url')], $lang));?>" class="lh-1_1em c-black size-0_8em force-words-nl">
                                            <?php echo $re_managers[$real_estate['id_manager']][lang_column('user_name')];?>
                                        </a>
                                        <a href="tel:<?php echo $re_managers[$real_estate['id_manager']]['user_phone'];?>" class="size-0_9em c-orange mt-0_3em f-medium">
                                            <?php echo $re_managers[$real_estate['id_manager']]['user_phone'];?>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $_banner_index++;?>

        <?php if($_banner_index == 2){?>
            <?php $_banner_is_displayed = true;?>
            <?php if(!empty($_banner)){?>
                <div class="flex-1em">
                    <div class="display-none-xs display-block-sm col-sm-12 mb-2em">
                        <div class="article-banner">
                            <?php if(!empty($_banner[lang_column('banner_link')])){?>
                                <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                            <?php }?>
                                <div class="image">
                                    <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                                </div>
                                <div class="flex flex-end banner-body">
                                    <div class="banner-description">
                                        <?php echo $_banner[lang_column('banner_text')]?>
                                    </div>
                                </div>
                            <?php if(!empty($_banner[lang_column('banner_link')])){?>
                                </a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>
        <?php }?>
    <?php }?>
<?php } else{?>
    <div class="flex-1em">
        <div class="col">
            <div class="error error__no-data mwp-100_i mb-41">
                <div class="error__no-data-message">
                    <div class="icon">
                        <i class="fai fai-warning-o"></i>
                    </div>
                    <?php lang_line('no_data');?>
                </div>
            </div>
        </div>
    </div>
<?php }?>
<?php if(!$_banner_is_displayed && !empty($_banner)){?>
    <div class="flex-1em">
        <div class="display-none-xs display-block-sm col-sm-12 mb-2em">
            <div class="article-banner">
                <?php if(!empty($_banner[lang_column('banner_link')])){?>
                    <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                <?php }?>
                    <div class="image">
                        <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                    </div>
                    <div class="flex flex-end banner-body">
                        <div class="banner-description">
                            <?php echo $_banner[lang_column('banner_text')]?>
                        </div>
                    </div>
                <?php if(!empty($_banner[lang_column('banner_link')])){?>
                    </a>
                <?php }?>
            </div>
        </div>
    </div>
<?php }?>