<div class="scroll-filter-parent bc-light-grey filter-section">
    <div class="container">
        <div class="top-filter-container flex-0_5em">
            <div class="shrink-0 toggle-sticky-filter">
                <em class="fai fai-filter"></em>
            </div>
            <div class="flex items-center filter-items-group" id="active-filters-list">
                <?php $this->load->view(get_theme_view('modules/real_estate/selected_filters_list_view')); ?>
            </div>
            <div class="flex-0_5em items-center reset-filter-button-parent pr-0_5em call-function" data-callback="reset_catalog_filters">
                <div class="icon">
                    <em class="fai fai-x-square"></em>
                </div>
                <div class="size-0_8em lh-1em">
                    Resetare filtru
                </div>
            </div>
        </div>
    </div>
</div>