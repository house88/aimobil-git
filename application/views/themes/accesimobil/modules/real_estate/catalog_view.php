<script>
    var children_locations = <?php echo json_encode($children_locations)?>;
    var catalog_page = 1;
    var catalog_url = '<?php echo site_url(get_dinamyc_uri('catalog/type/category', $category[lang_column('url')], $lang));?>';
    _init_catalog_filters = true;
</script>
<section class="sticky-filter">
	<div class="filter-section-parent">
		<div class="w100 pt-1em-xs pt-1em-sm pb-0_3em bc-light-grey-6 filter-section general-filter">
			<div class="container filter-tab">
				<div class="flex-0_5em nowrap-lg">
					<div class="col-xs-12 filter-col">
						<div class="f-bold c-black  size-0_9em mb-0_5em">
							<?php lang_line('filter_label_category');?>
						</div>
						<div class="filter-tabs-link flex-0em-xs flex-0-sm">
                            <?php foreach($filter_categories as $filter_category){?>
                                <div class="mobile-filter-col col-sm-6 col-md-12">
                                    <a class="filter-tab <?php echo set_menu_active($category['id_category'], $filter_category['id_category']);?>" href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $filter_category[lang_column('url')], $lang));?>"><?php echo $filter_category[lang_column('category_title_single')];?></a>
                                </div>
                            <?php }?>
						</div>
					</div>
					<div class="col-xs-12 flex-grow-1 filter-tabs">
						<div class="tab1 panel filter-content">
                            <form class="category-filters-form" autocomplete="off">
								<div class="flex-0_5em-xs flex-0-sm nowrap-sm">
                                    <?php foreach($category_properties as $category_property){?>
                                        <?php $category_property_values = json_decode($category_property['property_values'], true);?>
                                        <?php $category_property_settings = json_decode($category_property['property_settings'], true);?>
                                        <?php if($category_property['property_type'] == 'multiselect'){?>
                                            <div class="filter-outer">
                                                <div class="f-bold c-black size-0_9em mb-0_6em-xs mb-0_5em-sm">
                                                    <?php echo $category_property[lang_column('property_title')];?>
                                                    <?php if($category_property_settings['unit']['show']){?>
                                                        , <?php echo $category_property_settings['unit'][lang_column('title')];?>
                                                    <?php }?>
                                                </div>
                                                <?php if($category_property['property_values_cols'] == 2){?>
                                                    <?php $add_row_parent = true;?>
                                                    <?php foreach($category_property_values as $category_property_value){?>
                                                        <?php if($add_row_parent){?>
                                                            <div class="checkbox-parent">
                                                        <?php }?>
                                                            <?php $category_property_value_key = "p_{$category_property['id_property']}_{$category_property_value['id_value']}";?>
                                                            <div class="mobile-filter-col">
                                                                <div class="checkbox-block">
                                                                    <p>
                                                                        <input type="checkbox" class="filled-in catalog_filter-js" name="property[fm][<?php echo $category_property['id_property'];?>][<?php echo $category_property_value['id_value'];?>]" <?php if(in_array("fm_{$category_property['id_property']}_{$category_property_value['id_value']}", $selected_filters)){echo 'checked';}?> id="<?php echo $category_property_value_key;?>"/>
                                                                        <label for="<?php echo $category_property_value_key;?>"><span><?php echo $category_property_value[lang_column('title')];?></span></label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        <?php if(!$add_row_parent){?>
                                                            </div>
                                                            <?php $add_row_parent = true;?>
                                                        <?php } else{?>
                                                            <?php $add_row_parent = false;?>
                                                        <?php }?>
                                                    <?php }?>
                                                    <?php if(!$add_row_parent){?>
                                                        </div>
                                                    <?php }?>
                                                <?php } elseif($category_property['property_values_cols'] == 4){?>
                                                    <div class="checkbox-parent">
                                                        <?php $add_row_parent = true;?>
                                                        <?php foreach($category_property_values as $category_property_value){?>
                                                            <?php $category_property_value_key = "p_{$category_property['id_property']}_{$category_property_value['id_value']}";?>
                                                            <?php if($add_row_parent){?>
                                                                <div class="mobile-filter-col">
                                                                    <div class="checkbox-parent parent-between">
                                                            <?php }?>
                                                                    <div class="checkbox-block mobile-filter-col">
                                                                        <p>
                                                                            <input type="checkbox" class="filled-in catalog_filter-js" name="property[fm][<?php echo $category_property['id_property'];?>][<?php echo $category_property_value['id_value'];?>]" <?php if(in_array("fm_{$category_property['id_property']}_{$category_property_value['id_value']}", $selected_filters)){echo 'checked';}?> id="<?php echo $category_property_value_key;?>"/>
                                                                            <label for="<?php echo $category_property_value_key;?>"><span><?php echo $category_property_value[lang_column('title')];?></span></label>
                                                                        </p>
                                                                    </div>
                                                            <?php if(!$add_row_parent){?>
                                                                    </div>
                                                                </div>
                                                                <?php $add_row_parent = true;?>
                                                            <?php } else{?>
                                                                <?php $add_row_parent = false;?>
                                                            <?php }?>
                                                        <?php }?>
                                                        <?php if(!$add_row_parent){?>
                                                                </div>
                                                            </div>
                                                        <?php }?>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        <?php }?>
                                        <?php if($category_property['property_type'] == 'price'){?>
                                            <div class="filter-outer">
                                                <div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
                                                    <div class="filter-lang-parent">
                                                        <div class="f-bold c-black pr-0_4em">
                                                            <?php echo $category_property[lang_column('property_title')];?><?php if($category_property_settings['unit']['show']){echo ', '.$category_property_settings['unit'][lang_column('title')];}?>
                                                        </div>
                                                        <div class="filter-lang-toggle">
                                                            <?php foreach($currencies as $currency){?>
                                                                <a class="<?php if($currency['currency_code'] == $curent_currency){echo 'active';}?> call-function" data-callback="set_currency" data-currency="<?php echo $currency['id_currency'];?>" href="#"><span><?php echo $currency['currency_symbol'];?></span></a>
                                                            <?php }?>
                                                        </div>
                                                        <input type="hidden" name="property[fp][currency]" value="<?php echo $curent_currency;?>">
                                                    </div>
                                                </div>
                                                <div class="checkbox-parent">
                                                    <div class="mobile-filter-col">
                                                        <div class="select-parent">
                                                            <select class="select-filter text-center filter-price_select-from">
                                                                <option value="0" selected data-default><?php lang_line('filter_label_range_min');?></option>
                                                                <?php foreach($category_property_values[$curent_currency] as $category_property_value){?>
                                                                    <option value="<?php echo $category_property_value;?>" <?php if(!empty($selected_filters_price['from'])){echo set_select('', '', $selected_filters_price['from'] == $category_property_value);}?>><?php echo formatNumber($category_property_value).' '.$currencies[$curent_currency]['currency_symbol'];?></option>
                                                                <?php }?>
                                                            </select>
                                                            <input type="text" autocomplete="off" class="filter-price_input-from filter-input catalog_filter-js" value="<?php if(!empty($selected_filters_price['from'])){echo $selected_filters_price['from'];}?>" placeholder="<?php lang_line('label_property_min');?>" name="property[fp][from]">
                                                            <div class="select__dropdown--menu">
                                                                <ul class="">
                                                                    <li>
                                                                        <a class="select_link" data-result="" href=""><?php lang_line('label_property_min');?></a>
                                                                    </li>
                                                                    <?php foreach($category_property_values[$curent_currency] as $category_property_value){?>
                                                                        <li>
                                                                            <a class="select_link" data-result="<?php echo $category_property_value;?>" href=""><?php echo formatNumber($category_property_value).' '.$currencies[$curent_currency]['currency_symbol'];?></a>
                                                                        </li>
                                                                    <?php }?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mobile-filter-col">
                                                        <div class="select-parent">
                                                            <select class="select-filter text-center filter-price_select-to">
                                                                <option value="0" selected data-default><?php lang_line('filter_label_range_max');?></option>
                                                                <?php foreach($category_property_values[$curent_currency] as $category_property_value){?>
                                                                    <option value="<?php echo $category_property_value;?>" <?php if(!empty($selected_filters_price['to'])){echo set_select('', '', $selected_filters_price['to'] == $category_property_value);}?>><?php echo formatNumber($category_property_value).' '.$currencies[$curent_currency]['currency_symbol'];?></option>
                                                                <?php }?>
                                                            </select>
                                                            <input type="text" autocomplete="off" class="filter-price_input-to filter-input catalog_filter-js" value="<?php if(!empty($selected_filters_price['to'])){echo $selected_filters_price['to'];}?>" placeholder="<?php lang_line('label_property_max');?>" name="property[fp][to]">
                                                            <div class="select__dropdown--menu">
                                                                <ul class="">
                                                                    <li>
                                                                        <a class="select_link" data-result="" href=""><?php lang_line('label_property_max');?></a>
                                                                    </li>
                                                                    <?php foreach($category_property_values[$curent_currency] as $category_property_value){?>
                                                                        <li>
                                                                            <a class="select_link" data-result="<?php echo $category_property_value;?>" href=""><?php echo formatNumber($category_property_value).' '.$currencies[$curent_currency]['currency_symbol'];?></a>
                                                                        </li>
                                                                    <?php }?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php if($category_property['property_type'] == 'range'){?>
                                            <div class="filter-outer">
                                                <div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
                                                    <div class="filter-lang-parent">
                                                        <div class="f-bold c-black pr-0_4em">
                                                            <?php echo $category_property[lang_column('property_title')];?><?php if($category_property_settings['unit']['show']){echo ', '.$category_property_settings['unit'][lang_column('title')];}?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkbox-parent">
                                                    <div class="mobile-filter-col">
                                                        <div class="select-parent">
                                                            <select class="select-filter text-center">
                                                                <option value="0" selected data-default><?php lang_line('filter_label_range_min');?></option>
                                                                <?php foreach($category_property_values as $category_property_value){?>
                                                                    <option value="<?php echo $category_property_value;?>" <?php if(!empty($selected_filters_range["fr_{$category_property['id_property']}_from"])){echo set_select('', '', $selected_filters_range["fr_{$category_property['id_property']}_from"] == $category_property_value);}?>><?php echo $category_property_value;?><?php if($category_property_settings['unit']['show']){echo ' '.$category_property_settings['unit'][lang_column('title')];}?></option>
                                                                <?php }?>
                                                            </select>
                                                            <input type="text" autocomplete="off" class="filter-input catalog_filter-js" value="<?php if(!empty($selected_filters_range["fr_{$category_property['id_property']}_from"])){echo $selected_filters_range["fr_{$category_property['id_property']}_from"];}?>" placeholder="<?php lang_line('label_property_min');?>" name="property[fr][<?php echo $category_property['id_property'];?>][from]">
                                                            <div class="select__dropdown--menu">
                                                                <ul class="">
                                                                    <li>
                                                                        <a class="select_link" data-result="" href=""><?php lang_line('label_property_min');?></a>
                                                                    </li>
                                                                    <?php foreach($category_property_values as $category_property_value){?>
                                                                        <li>
                                                                            <a class="select_link" data-result="<?php echo $category_property_value;?>" href=""><?php echo $category_property_value;?><?php if($category_property_settings['unit']['show']){echo ' '.$category_property_settings['unit'][lang_column('title')];}?></a>
                                                                        </li>
                                                                    <?php }?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mobile-filter-col">
                                                        <div class="select-parent">
                                                            <select class="select-filter text-center">
                                                                <option value="0" selected data-default><?php lang_line('filter_label_range_max');?></option>
                                                                <?php foreach($category_property_values as $category_property_value){?>
                                                                    <option value="<?php echo $category_property_value;?>" <?php if(!empty($selected_filters_range["fr_{$category_property['id_property']}_to"])){echo set_select('', '', $selected_filters_range["fr_{$category_property['id_property']}_to"] == $category_property_value);}?>><?php echo $category_property_value;?><?php if($category_property_settings['unit']['show']){echo ' '.$category_property_settings['unit'][lang_column('title')];}?></option>
                                                                <?php }?>
                                                            </select>
                                                            <input type="text" autocomplete="off" class="filter-input catalog_filter-js" value="<?php if(!empty($selected_filters_range["fr_{$category_property['id_property']}_to"])){echo $selected_filters_range["fr_{$category_property['id_property']}_to"];}?>" placeholder="<?php lang_line('label_property_max');?>" name="property[fr][<?php echo $category_property['id_property'];?>][to]">
                                                            <div class="select__dropdown--menu">
                                                                <ul class="">
                                                                    <li>
                                                                        <a class="select_link" data-result="" href=""><?php lang_line('label_property_max');?></a>
                                                                    </li>
                                                                    <?php foreach($category_property_values as $category_property_value){?>
                                                                        <li>
                                                                            <a class="select_link" data-result="<?php echo $category_property_value;?>" href=""><?php echo $category_property_value;?><?php if($category_property_settings['unit']['show']){echo ' '.$category_property_settings['unit'][lang_column('title')];}?></a>
                                                                        </li>
                                                                    <?php }?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                    
									<div class="filter-outer location-select">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											<?php lang_line('filter_label_location');?>
										</div>
										<div class="checkbox-parent">
                                            <?php foreach($parent_locations as $parent_location){?>
                                                <?php if(!empty($children_locations[$parent_location['id_location']])){?>
                                                    <div class="mobile-filter-col">
                                                        <div class="select-parent selectpicker-filter">
                                                            <select class="select-filter catalog_filter-js" name="property[fl][<?php echo $parent_location['id_location'];?>][]" data-style="btn-filter-select" data-width="100%" multiple title="<?php echo $parent_location[lang_column('location_name')];?>">
                                                                <option value="0" disabled selected hidden data-default style="display:none !important;opacity:0 !important;"><?php echo $parent_location[lang_column('location_name')];?></option>
                                                                <?php foreach($children_locations[$parent_location['id_location']] as $children_location){?>
                                                                    <option value="<?php echo $children_location['id_location'];?>" <?php if(in_array("fl_{$parent_location['id_location']}_{$children_location['id_location']}", $selected_filters)){echo 'selected';}?>><?php echo $children_location[lang_column('location_name')];?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            <?php }?>
                                        </div>
									</div>
                                </div>
                                <?php if($category['category_has_floors'] == 1){?>
                                    <div class="exclude-button">
                                        <div class="checkbox-block mr-1em-sm">
                                            <p>
                                                <input type="checkbox" class="filled-in catalog_filter-js" id="reset-etaj" name="property[exclude_last_floor]" <?php if(in_array("exclude_last_floor", $selected_filters)){echo 'checked';}?>/>
                                                <label for="reset-etaj"><?php lang_line('filter_label_exclude_last_floor');?></label>
                                            </p>
                                        </div>
                                    </div>
                                <?php }?>
							</form>
						</div>
					</div>
				</div>
				<div class="flex flex-end items-center">
					<div class="">
						<div class="flex-0_5em items-center reset-filter-button-parent pr-0_5em call-function" data-callback="reset_catalog_filters">
							<div class="lh-1em pb-0_5em-sm">
								<div class="reset-filter-button">
								</div>
							</div>
							<div class="size-0_8em lh-1em pb-0_5em-sm">
								<?php lang_line('filter_label_reset');?>
							</div>
						</div>
					</div>
				</div>
				<div class="w100 mobile-filter-button">
					<button class="w100 btn btn-flat btn-filter call-function" data-callback="call_filter"><span class="pl-0"><span class="fai fai-filter"></span> <?php lang_line('filter_label_filter');?></span></button>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pt-1_8em pb-3_5em">
	<div id="catalog-container" class="container article-container">
		<div class="flex-1em pb-1_2em-xs pb-2em-sm mb-0_2em space-between">
			<div class="flex-1em bottom">
				<div class="title-parent">
					<h1 class="f-medium catalog-title"><?php echo $category[lang_column('category_h1')];?></h1>
				</div>
				<div class="size-0_9em pb-0_1em-sm title-parent">
					<?php echo tpl_replace($category[lang_column('category_text_counter')], array('{{counter}}' => $records_total));?>
				</div>
			</div>
			<div class="gridListToggle d-none d-lg-flex">
				<div class="grid-toggle cur-pointer_i <?php echo set_menu_active(@$catalog_view, 'grid');?> call-function" data-callback="set_catalog_view" data-view="grid">
					<div class="toggle-icon grid">
						<em class="fai fai-grid"></em>
					</div>
					<div>
						<?php lang_line('label_grid');?>
					</div>
				</div>
				<div class="list-toggle cur-pointer_i <?php echo set_menu_active(@$catalog_view, 'list');?> call-function" data-callback="set_catalog_view" data-view="list">
					<div class="toggle-icon list">
						<em class="fai fai-list"></em>
					</div>
					<div>
						<?php lang_line('label_list');?>
					</div>
				</div>
			</div>

		</div>
		<div id="filter-article" class="flex-1em">
			<div class="col-12 col-lg-9">
				<div id="catalog_list-wr">
                    <?php $this->load->view(get_theme_view('modules/real_estate/catalog_'.$catalog_view.'_view')); ?>
				</div>
				<div class="w100 text-center">
					<div class="container flex center" id="catalog_pagination">
                        <?php echo $pagination;?>
					</div>
				</div>
				<div class="container display-none-xs display-none-sm display-block-md">
					<div class="row pad pt-3em">
						<div class="size-0_9em c-grey">
                            <?php echo $category[lang_column('seo')];?>
						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-lg-3 mt-3em-xs mt-3em-sm mt-0-md">
                <?php $_banner = Modules::run('banner/_get_by_alias', 'real_estate_right');?>
                <?php if(!empty($_banner)){?>
                    <div id="sidebar-banner" class="sidebar-banner">
                        <?php if(!empty($_banner[lang_column('banner_link')])){?>
                            <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                        <?php }?>
                            <div class="sidebar-banner-catalog">
                                <div class="image">
                                    <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                                </div>
                                <div class="banner_body-wr">
                                    <?php echo $_banner[lang_column('banner_text')]?>
                                </div>
                            </div>
                        <?php if(!empty($_banner[lang_column('banner_link')])){?>
                            </a>
                        <?php }?>
                    </div>
                <?php }?>
				<div class="container display-block-xs display-block-sm display-none-md pt-1_9em">
					<div class="row pad">
						<div class="size-0_9em c-grey">
                            <?php echo $category[lang_column('seo')];?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>