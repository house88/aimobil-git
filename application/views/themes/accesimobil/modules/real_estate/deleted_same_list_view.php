<?php if(!empty($real_estates)){?>
    <?php foreach($real_estates as $real_estate){?>
        <?php $_price = getPrice($real_estate);?>
        <div class="col-12 col-sm-6 col-lg-3">
            <div class="card h100 box-shadow">
                <?php if($real_estate['real_estate_discount_type'] != 'no'){?>
                    <div class="discount-badge">
                        <div class="badge-title">%</div>
                        <?php if($_price['display_discount_text'] != ''){?>
                            <div class="badge-content"><?php echo $_price['display_discount_text'];?></div>
                        <?php }?>
                    </div>
                <?php }?>
                <a class="wish_list fai fai-like <?php echo set_favorite($real_estate['id_real_estate'], 'imobil', 'hasWishList');?> call-function" data-callback="set_favorite" data-item="<?php echo $real_estate['id_real_estate'];?>" data-type="imobil" href="#"></a>
                <a class="grid-item <?php if($real_estate['real_estate_sold'] == 1){?>sold<?php } elseif($real_estate['real_estate_reserved'] == 1){?>reserved<?php }?>" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
                    <img class="card-img-top" src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate['real_estate_photo']);?>" <?php echo img_srcset('files/real_estate/'.$real_estate['id_real_estate'], $real_estate['real_estate_photo']);?> alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>"  title="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>">
                    <?php if($real_estate['real_estate_sold'] == 1){?>
                        <div class="sold-title"><?php lang_line('label_sold');?></div>
                    <?php } elseif($real_estate['real_estate_reserved'] == 1){?>
                        <div class="reserved-title"><?php lang_line('label_reserved');?></div>
                    <?php }?>
                    <?php if($real_estate['real_estate_exclusive'] == 1){?>
                        <div class="exclusive"><?php lang_line('label_exclusive');?></div>
                    <?php }?>
                </a>
                <?php if(isset($re_properties[$real_estate['id_real_estate']])){?>
                    <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                        <?php foreach($re_properties[$real_estate['id_real_estate']] as $re_property){?>
                            <div>
                                <?php echo $re_property['property_value'];?> 
                                <small class="text-muted"><?php echo $re_property['unit_name'];?></small>
                            </div>
                        <?php }?>
                    </div>
                <?php }?>
                <div class="card-body">
                    <a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
                        <div class="d-flex justify-content-between">
                            <div class="f-medium c-black lh-1_3em hover-orange">
                                <?php echo $real_estate[lang_column('real_estate_title')];?>
                                <div class="flex f-regular items-center mt-0_3em">
                                    <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                    <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                        <?php echo $real_estate['real_estate_viewed'];?>
                                    </div>
                                </div>
                            </div>
                            <div class="f-medium c-black col-5">
                                <div class="row">
                                    <div class="pl-1_5em">
                                        <span class="c-orange item-price"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
                                        <?php $_credit = getCredit($real_estate);?>
                                        <?php if($_credit){?>
                                            <small><?php lang_line('label_mortgage_text');?></small>
                                            <small><?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?></small>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <?php }?>
<?php } else{?>
    <div class="flex-1em">
        <div class="col">
            <div class="error error__no-data mwp-100_i mb-41">
                <div class="error__no-data-message">
                    <div class="icon">
                        <i class="fai fai-warning-o"></i>
                    </div>
                    <?php lang_line('no_data');?>
                </div>
            </div>
        </div>
    </div>
<?php }?>