<div class="scroll-filter-parent bc-light-grey filter-section">
    <div class="container">
        <div class="top-filter-container flex-0_5em items-center nowrap">
            <div class="shrink-0 toggle-sticky-filter">
                <em class="fai fai-filter"></em>
            </div>
            <div class="flex wrap items-center filter-items-group child-mb-0_3em" id="active-filters-list">
                <?php $this->load->view(get_theme_view('modules/real_estate/active_filters_list_view')); ?>
            </div>
        </div>
    </div>
</div>