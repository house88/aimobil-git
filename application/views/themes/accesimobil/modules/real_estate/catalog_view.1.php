<section class="catalog">
    <div class="container">
        <h1 class="heading-title-1">Vinzare apartamente</h1>
        <div class="row">
            <div class="col-md col-lg-9">
                <div class="row">
                    <?php if(!empty($real_estates)){?>
                        <?php foreach($real_estates as $real_estate){?>
                            <div class="col-xs col-sm-6 col-md-6 col-lg-4">
                                <?php $this->load->view(get_theme_view('modules/real_estate/item_view'), array('real_estate' => $real_estate));?>
                            </div>            
                        <?php }?>            
                    <?php } else{?>
                        <div class="col">
                            <div class="error error__no-data">
                                <div class="error__no-data-message">
                                    <div class="icon">
                                        <i class="fai fai-warning-o"></i>
                                    </div>
                                    <?php lang_line('no_data');?>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <?php if(!empty($pagination)){?>
                    <div class="row">
                        <div class="col"><?php echo $pagination;?></div>
                    </div>            
                <?php }?>
            </div>
            <div class="d-none d-md-flex col-md col-lg-3">
                sidebar
                <?php //$this->load->view(get_theme_view('modules/blog/sidebar_view'));?>
            </div>
        </div>
    </div>
</section>
<section class="sticky-filter">
	<div class="filter-section-parent">
		<div class="w100 pt-1em-xs pt-1em-sm pb-0_3em bc-light-grey-6 filter-section general-filter">
			<div class="container filter-tab">
				<div class="flex-0_5em nowrap-sm">
					<div class="col-xs-12 filter-col">
						<div class="f-bold c-black  size-0_9em mb-0_5em">
							Tip imobil
						</div>
						<div class="filter-tabs-link flex-0em-xs flex-0-sm">
							<div class="mobile-filter-col col-sm-6 col-md-12"><a class="filter-tab" href="#" data-open=".tab1" data-close=".panel">Apartament</a></div>
							<div class="mobile-filter-col col-sm-6 col-md-12"><a class="filter-tab" href="#" data-open=".tab2" data-close=".panel">Casă</a></div>
							<div class="mobile-filter-col col-sm-6 col-md-12"><a class="filter-tab" href="#" data-open=".tab3" data-close=".panel">Teren</a></div>
							<div class="mobile-filter-col col-sm-6 col-md-12"><a class="filter-tab" href="#" data-open=".tab4" data-close=".panel">Spațiu comercial</a></div>
						</div>
					</div>
					<div class="col-xs-12 flex-grow-1 filter-tabs">
						<div class="tab1 panel filter-content">
							<form action="">
								<div class="flex-0_5em-xs flex-0-sm nowrap-sm">
									<div class="filter-outer">
										<div class="f-bold c-black size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Nr. de camere
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" />
															<label for="filled-in-box"><span>1</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box1" />
															<label for="filled-in-box1"><span>2</span></label>
														</p>
													</div>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box2" />
															<label for="filled-in-box2"><span>3</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box3"/>
															<label for="filled-in-box3"><span>4+</span></label>
														</p>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Tip construcție
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box4" checked="checked" />
														<label for="filled-in-box4"><span>Bloc nou</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box5" />
														<label for="filled-in-box5"><span>Bloc vechi</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Stare apartament
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box12" checked="checked" />
														<label for="filled-in-box12"><span>Reparație Euro</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box13" />
														<label for="filled-in-box13"><span>Medie</span></label>
													</p>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box14" />
														<label for="filled-in-box14"><span>Fără reparație/<br>Varianta albă</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
											<div class="filter-lang-parent">
												<div class="f-bold c-black pr-0_4em">Preț</div>
												<div class="filter-lang-toggle">
													<a class="active" href="#"><span>€</span></a>
													<a href="#"><span>$</span></a>
													<a href="#"><span>MDL</span></a>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Suprafață
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">50</option>
														<option value="2">100</option>
														<option value="3">350</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">500</option>
														<option value="2">1 000</option>
														<option value="3">5 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer location-select">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Locație
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Orasul</option>
														<option value="1">Chisinau</option>
														<option value="2">Balt</option>
														<option value="3">Orhei</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Suburbie</option>
														<option value="1">Tohatin</option>
														<option value="2">Bubueci</option>
														<option value="3">Aeroport</option>
													</select>
												</div>
											</div>
										</div>
									</div>

								</div>
								<div class="exclude-button">
									<div class="checkbox-block mr-1em-sm">
										<p>
											<input type="checkbox" class="filled-in" id="reset-etaj" />
											<label for="reset-etaj">Exclude ultimul etaj</label>
										</p>
									</div>
								</div>
							</form>
						</div>
						<div class="tab2 panel filter-content">
							<form action="">
								<div class="flex-0_5em-xs flex-0-sm nowrap-sm">
									<div class="filter-outer">
										<div class="f-bold c-black size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Nr. de nivele
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box15" checked="checked" />
															<label for="filled-in-box15"><span>1</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box16" />
															<label for="filled-in-box16"><span>2</span></label>
														</p>
													</div>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box17" />
															<label for="filled-in-box17"><span>3</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box18"/>
															<label for="filled-in-box18"><span>4+</span></label>
														</p>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Nr de odăi
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box19" checked="checked" />
															<label for="filled-in-box19"><span>1</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box20" />
															<label for="filled-in-box20"><span>2</span></label>
														</p>
													</div>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-parent parent-between">
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box21" />
															<label for="filled-in-box21"><span>3</span></label>
														</p>
													</div>
													<div class="checkbox-block mobile-filter-col">
														<p>
															<input type="checkbox" class="filled-in" id="filled-in-box22"/>
															<label for="filled-in-box22"><span>4+</span></label>
														</p>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Anul
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box23" checked="checked" />
														<label for="filled-in-box23"><span>până la 1990</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box24" />
														<label for="filled-in-box24"><span>1990–2000</span></label>
													</p>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box25" />
														<label for="filled-in-box25"><span>2000–2010</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box26"/>
														<label for="filled-in-box26"><span>2010+</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Starea casei
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box27" checked="checked" />
														<label for="filled-in-box27"><span>Reparație Euro</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box28" />
														<label for="filled-in-box28"><span>Medie</span></label>
													</p>
												</div>
											</div>
										</div>

										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box29" />
														<label for="filled-in-box29"><span>Fără reparație/<br>Varianta albă</span></label>
													</p>
												</div>
											</div>

										</div>
									</div>
									<div class="filter-outer">
										<div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
											<div class="filter-lang-parent">
												<div class="f-bold c-black pr-0_4em">Preț</div>
												<div class="filter-lang-toggle">
													<a class="active" href="#"><span>€</span></a>
													<a href="#"><span>$</span></a>
													<a href="#"><span>MDL</span></a>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Suprafață m<sup>2</sup>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">50</option>
														<option value="2">100</option>
														<option value="3">350</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">500</option>
														<option value="2">1 000</option>
														<option value="3">5 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Teren (ari)
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer location-select">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Locație
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Orasul</option>
														<option value="1">Chisinau</option>
														<option value="2">Balt</option>
														<option value="3">Orhei</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Suburbie</option>
														<option value="1">Tohatin</option>
														<option value="2">Bubueci</option>
														<option value="3">Aeroport</option>
													</select>
												</div>
											</div>
										</div>
									</div>

								</div>
							</form>
						</div>
						<div class="tab3 panel filter-content">
							<form action="">
								<div class="flex-0_5em-xs flex-0-sm nowrap-sm">
									<div class="filter-outer">
										<div class="f-bold c-black size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Destinație
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box31" checked="checked" />
														<label for="filled-in-box31"><span>Construcție</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box32" />
														<label for="filled-in-box32"><span>Grădină/Pomicol</span></label>
													</p>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box33" />
														<label for="filled-in-box33"><span>Agricol</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
											<div class="filter-lang-parent">
												<div class="f-bold c-black pr-0_4em">Preț</div>
												<div class="filter-lang-toggle">
													<a class="active" href="#"><span>€</span></a>
													<a href="#"><span>$</span></a>
													<a href="#"><span>MDL</span></a>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Suprafață m<sup>2</sup>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">50</option>
														<option value="2">100</option>
														<option value="3">350</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">500</option>
														<option value="2">1 000</option>
														<option value="3">5 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Teren (ari)
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer location-select">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Locație
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Orasul</option>
														<option value="1">Chisinau</option>
														<option value="2">Balt</option>
														<option value="3">Orhei</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Suburbie</option>
														<option value="1">Tohatin</option>
														<option value="2">Bubueci</option>
														<option value="3">Aeroport</option>
													</select>
												</div>
											</div>
										</div>
									</div>

								</div>
							</form>
						</div>
						<div class="tab4 panel filter-content">
							<form action="">
								<div class="flex-0_5em-xs flex-0-sm nowrap-sm">
									<div class="filter-outer">
										<div class="f-bold c-black size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Destinație spațiu
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box34" checked="checked" />
														<label for="filled-in-box34"><span>Comercial</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box35" />
														<label for="filled-in-box35"><span>Oficii</span></label>
													</p>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box36" />
														<label for="filled-in-box36"><span>Depozit/Producere</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box37" />
														<label for="filled-in-box37"><span>Restaurant/Bar</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Stare spațiu
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box57" checked="checked" />
														<label for="filled-in-box57"><span>Reparație Euro</span></label>
													</p>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box58" />
														<label for="filled-in-box58"><span>Medie</span></label>
													</p>
												</div>
											</div>
										</div>

										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="checkbox-block">
													<p>
														<input type="checkbox" class="filled-in" id="filled-in-box59" />
														<label for="filled-in-box59"><span>Fără reparație/<br>Varianta albă</span></label>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="size-0_9em mb-0_6em-xs mb-0_5em-sm">
											<div class="filter-lang-parent">
												<div class="f-bold c-black pr-0_4em">Preț</div>
												<div class="filter-lang-toggle">
													<a class="active" href="#"><span>€</span></a>
													<a href="#"><span>$</span></a>
													<a href="#"><span>MDL</span></a>
												</div>
											</div>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">25 000</option>
														<option value="2">30 000</option>
														<option value="3">35 000</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">125 000</option>
														<option value="2">300 000</option>
														<option value="3">500 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Suprafață m<sup>2</sup>
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Min</option>
														<option value="1">50</option>
														<option value="2">100</option>
														<option value="3">350</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Max</option>
														<option value="1">500</option>
														<option value="2">1 000</option>
														<option value="3">5 000</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="filter-outer location-select">
										<div class="f-bold c-black  size-0_9em mb-0_6em-xs mb-0_5em-sm">
											Locație
										</div>
										<div class="checkbox-parent">
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Orasul</option>
														<option value="1">Chisinau</option>
														<option value="2">Balt</option>
														<option value="3">Orhei</option>
													</select>
												</div>
											</div>
											<div class="mobile-filter-col">
												<div class="select-parent">
													<select class="select-filter">
														<option value="" selected disabled>Suburbie</option>
														<option value="1">Tohatin</option>
														<option value="2">Bubueci</option>
														<option value="3">Aeroport</option>
													</select>
												</div>
											</div>
										</div>
									</div>

								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="flex flex-end items-center">
					<div class="">
						<div class="flex-0_5em items-center reset-filter-button-parent pr-0_5em">
							<div class="lh-1em pb-0_5em-sm">
								<div class="reset-filter-button">
								</div>
							</div>
							<div class="size-0_8em lh-1em pb-0_5em-sm">
								Resetare filtru
							</div>
						</div>
					</div>
				</div>
				<div class="w100 display-block-xs display-none-sm">
					<button class="w100 btn btn-flat btn-filter up"><span>Filtreaza</span></button>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pt-1_8em pb-3_5em">
	<div id="catalog-container" class="container article-container">
		<div class="flex-1em pb-1_2em-xs pb-2em-sm mb-0_2em space-between">
			<div class="flex-1em bottom">
				<div class="title-parent">
					<h1 class="f-medium catalog-title">Vanzare apartamente în Chișinau</h1>
				</div>
				<div class="size-0_9em pb-0_1em-sm title-parent">
					3565 apartamente disponibile
				</div>
			</div>
			<div class="gridListToggle display-none-xs">
				<div class="grid-toggle active">
					<div class="toggle-icon grid">
						<img src="img/icons/grid-icon-orange.png" alt="grid">
					</div>
					<div>
						Grid
					</div>
				</div>
				<div class="list-toggle">
					<div class="toggle-icon list">
						<img src="img/icons/list-icon-grey.png" alt="list">
					</div>
					<div>
						List
					</div>
				</div>
			</div>

		</div>
		<div class="flex-1em">
			<div class="col-12 col-lg-9">
				<div class="flex-1em">
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/sold-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="grid-item reserved">
								<a class="" href="">
									<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
								</a>
								<div class="sold-title">Vândut</div>
								<div class="reserved-title">Reservat</div>
							</div>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="grid-item sold">
								<a href="">
									<img class="card-img-top" src="img/article/sold-item.jpg" alt="sold apartment">
								</a>
								<div class="sold-title">Vândut</div>
								<div class="reserved-title">Reservat</div>
							</div>

							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="display-none-xs display-block-sm col-sm-12 mb-2em">
						<div class="article-banner">
							<div class="flex flex-end">
								<div class="banner-description">
									Comision zero, pentru cumpărători și chiriași!
								</div>

							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content">-50€ / M<sup>2</sup></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/sold-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/sold-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<div class="card mb-2em box-shadow">
							<div class="discount-badge">
								<div class="badge-title">%</div>
								<div class="badge-content"><del>-75 000€</del></div>
							</div>
							<a class="wish_list fai  fai-like" href="#"></a>
							<a class="grid-item" href="">
								<img class="card-img-top" src="img/article/reserve-item.jpg" alt="Card image cap">
							</a>
							<div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
								<div>3 <small class="text-muted">camere</small></div>
								<div>80 <small class="text-muted">m2</small></div>
								<div>3/5 <small class="text-muted">etaj</small></div>
								<div>1 <small class="text-muted">bai</small></div>
							</div>
							<div class="card-body">
								<a href="">
									<div class="d-flex justify-content-between">
										<div class="f-medium c-black lh-1_3em hover-orange">
											Chisinau, Botanica, Traian
											<div class="flex f-regular items-center mt-0_3em">
												<div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
												<div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
													727
												</div>
											</div>
										</div>
										<div class="f-medium c-black col-5">
											<div class="row">
												<div class="pl-1_5em">
													<span class="c-orange">50 000 $</span>
													<small>Ipoteca</small>
													<small>575 $ / luna</small>
												</div>
											</div>

										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="w100 text-center">
					<div class="container flex center">
						<div class="pagination">
							<a class="nav disabled" href=""><em class="fai fai-left-arrow"></em></a>
							<a class="active" href="">1</a>
							<a href="">2</a>
							<a href="">3</a>
							<a href="">4</a>
							<a class="nav" href=""><em class="fai fai-right-arrow"></em></a>
						</div>
					</div>
				</div>
				<div class="container display-none-xs display-none-sm display-block-md">
					<div class="row pad pt-3em">
						<div class="size-0_9em c-grey">
							Acces Imobil vă propune cele mai bune apartamente cu 1,2,3,4 camere în vânzare sau chirie la preț accesibil.
							Fie că sunteți în căutarea unui apartament în Chișinău (sectorul Centru, Botanica, Râșcani, Poșta Veche, Ciocana,
							Buiucani, Telecentru) sau suburbie, suntem deschiși să vă ajutăm pe tot parcursul procesului de vânzare-cumpărare
							sau chirie a bunului imobil.
						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-lg-3 mt-3em-xs mt-3em-sm mt-0-md">
				<div class="sidebar-banner">
					<div class="top-side">
						<div class="banner-description">
							Asistență juridică deplină pe întreg procesul <br>de vânzare-cumpărare <br>sau chirie:
						</div>
						<ul class="description-list">
							<li>Evaluare imobil</li>
							<li>Pregatire acte </li>
							<li>Notar </li>
							<li>Cadastru</li>
							<li>Credit ipotecar</li>
						</ul>
					</div>
					<div class="bottom-side">
						<div class="flex-0_5em items-center nowrap">
							<div>
								<div class="left-block">
									0<sup>%</sup>
								</div>
							</div>
							<div class="bottom-description">
								Comision zero, pentru cumpărători și chiriași!
							</div>
						</div>

					</div>
				</div>
				<div class="container display-block-xs display-block-sm display-none-md pt-1_9em">
					<div class="row pad">
						<div class="size-0_9em c-grey">
							Acces Imobil vă propune cele mai bune apartamente cu 1,2,3,4 camere în vânzare sau chirie la preț accesibil.
							Fie că sunteți în căutarea unui apartament în Chișinău (sectorul Centru, Botanica, Râșcani, Poșta Veche, Ciocana,
							Buiucani, Telecentru) sau suburbie, suntem deschiși să vă ajutăm pe tot parcursul procesului de vânzare-cumpărare
							sau chirie a bunului imobil.
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</section>