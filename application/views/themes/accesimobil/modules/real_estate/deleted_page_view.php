<div class="container">
	<div class="flex pt-3em pb-3em items-center br-bottom-1-grey">
		<div class="icon shrink-0 sold-icon">
			<div class="icon-o">
                <span class="fai fai-lock-o"></span>
			</div>
		</div>
		<div class="pl-2_5em-sm">
			<h2 class="f-bold c-black size-1_9em pb-0_3em"><?php echo $page_detail[lang_column('page_name')];?></h2>
			<p class="mb-0 size-1_1em"><?php echo @$page_detail['page_blocks'][lang_column('h1_subtext')];?></p>
		</div>
	</div>
</div>
<section class="pt-3em pb-3_5em">
	<div class="container article-container">
		<h2 class="f-medium c-black pb-1_2em"><?php echo @$page_detail['page_blocks'][lang_column('h2_same')];?></h2>
		<div class="row child-mb-2em" id="catalog_list-wr">            
            <?php $this->load->view(get_theme_view('modules/real_estate/deleted_same_list_view')); ?>
		</div>
	</div>
	<div class="w100 text-center">
		<div class="container flex center">
            <?php echo $pagination;?>
		</div>
	</div>
</section>