<article class="real_estate-item">
    <?php $_price = getPrice($real_estate);?>
    <a href="#" class="real_estate-item__link">
        <?php if($_price['show_discount']){?>
            <div class="real_estate-item__discount">
                <div class="real_estate-item__discount-icon">
                    <span>%</span>
                </div>
                <?php if($_price['display_discount_text'] != ''){?>
                    <div class="real_estate-item__discount-detail"><?php echo $_price['display_discount_text'];?></div>
                <?php }?>
            </div>
        <?php }?>
        <div class="real_estate-item__img" style="background-image:url(<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate['real_estate_photo']);?>);"></div>
        <ul class="real_estate-item-properties-list">
            <li class="real_estate-item-properties-list__item">
                <span>3</span> + living
            </li>
            <li class="real_estate-item-properties-list__item">
                <span>80</span> m2
            </li>
            <li class="real_estate-item-properties-list__item">
                <span>3/5</span> etaj
            </li>
            <li class="real_estate-item-properties-list__item">
                <span>1</span> bai
            </li>
        </ul>
        <div class="real_estate-item__info">
            <div class="real_estate-item__info-title">
                <span class="title"><?php echo $real_estate[lang_column('real_estate_title')];?></span>
                <span class="views">
                    <span class="fai fai-eye-o"></span> 400
                </span>
            </div>
            <div class="real_estate-item__info-price">
                <div class="price">
                    <?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?>
                </div>
                <div class="mortgage">
                    <span>Ipoteca</span>
                    <span>500 $/luna</span>
                </div>
            </div>
        </div>
    </a>
</article>