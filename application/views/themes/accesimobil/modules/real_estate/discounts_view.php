<section class="pt-3em pb-3_5em">
	<div class="container article-container">
		<h2 class="f-medium c-black pb-0_9em discount-header"><?php echo $page_detail[lang_column('page_name')];?></h2>
		<?php if(!empty($categories) && count($categories) > 1){?>
			<div class="w100 text-left pb-2em">
				<div class="flex">
					<div class="top-page-navigation">
						<a href="#" class="active call-function" data-callback="reset_discount_filter"><?php lang_line('discounts_page_filter_reset_text');?></a>
						<?php foreach($categories as $category){?>
							<a href="#" class="call-function" data-callback="set_discount_category" data-category="<?php echo $category['id_category'];?>"><?php echo $category[lang_column('category_title')];?></a>
						<?php }?>
					</div>
				</div>
			</div>
		<?php }?>
		<div class="row mt-0_5em child-mb-2em" id="discounts_list-wr">
            <?php $this->load->view(get_theme_view('modules/real_estate/discounts_list_view')); ?>
		</div>
    </div>
    <script>
        var discounts_page = 1;
        var discounts_category = null;
        var discounts_url = '<?php echo site_url(get_static_uri('discounts', $lang))?>';
    </script>
	<div class="w100 text-center">
		<div class="container flex center" id="discounts_pagination">
            <?php echo $pagination;?>
		</div>
	</div>
	<div class="container">
		<div class="row pad pt-3em">
			<div class="col-12 col-md-10 size-0_9em pr-5em-md">
                <?php echo $page_detail[lang_column('page_text')];?>
            </div>
		</div>

	</div>
</section>