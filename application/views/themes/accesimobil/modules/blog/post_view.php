
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5ade79d1429ed50011f9e0ac&product=custom-share-buttons"></script>
<div class="container">
    <a href="javascript: history.go(-1)" class="blog-item__post-link-back">
        <span class="fai fai-left-arrow"></span> <?php lang_line('go_back_btn');?>
    </a>
    <h1 class="heading-title-1 blog-item__post-title">
        <?php echo $blog[lang_column('blog_title')];?>
    </h1>
    <div class="blog-item__post-date">
        <?php
            $blog_date = array(
                mb_strtolower($this->lang->line('cal_'.mb_strtolower(formatDate($blog['blog_date'], 'l')))).',',
                formatDate($blog['blog_date'], 'j'),
                mb_strtolower($this->lang->line('cal_'.mb_strtolower(formatDate($blog['blog_date'], 'F')))),
                formatDate($blog['blog_date'], 'Y')
            );
        ?>
        <?php echo implode(' ', $blog_date);?>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8">
            <article class="blog-item__post">
                <?php if(image_exist('files/'.$blog['blog_photo'])){?>
                    <div class="blog-item__post-img">
                        <img src="<?php echo site_url('files/'.$blog['blog_photo']);?>" alt="<?php echo image_title($blog[lang_column('mt')]);?>" title="<?php echo image_title($blog[lang_column('mt')]);?>">
                    </div>
                <?php }?>
                <div class="blog-item__post-description">
                    <?php echo $blog[lang_column('blog_description')];?>
                </div>
                <div class="blog-item__post-socials">
                    <div class="text"><?php lang_line('label_share_text');?></div>
                    <!-- <div class="sharethis-inline-share-buttons"></div> -->
                    <div data-network="facebook" class="st-custom-button">
                        <div class="btn btn-default btn-flat bg-facebook mr-2" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $blog_counters['facebook'];?>"><span class="fai fai-facebook-o"></span></div>
                    </div> 
                    <div data-network="twitter" class="st-custom-button">
                        <div class="btn btn-default btn-flat bg-twitter mr-2" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $blog_counters['twitter'];?>"><span class="fai fai-twitter-o"></span></div>
                    </div>
                    <div data-network="googleplus" class="st-custom-button">
                        <div class="btn btn-default btn-flat bg-google-plus" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $blog_counters['googleplus'];?>"><span class="fai fai-google-plus"></span></div>
                    </div>
                </div>
            </article>
        </div>
        <div class="d-none d-lg-flex col-12 col-lg-4">
            <?php $this->load->view(get_theme_view('modules/blog/sidebar_view'));?>
        </div>
    </div>
</div>
<?php if(!empty($same_blogs)){?>
    <section class="blog-same">
        <div class="container">
            <h1 class="heading-title-1 block-pre-footer__title">
                <?php lang_line('blog_same_posts');?>
            </h1>
            <div class="row">
                <?php foreach($same_blogs as $same_blog){?>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <?php $this->load->view(get_theme_view('modules/blog/item_view'), array('blog' => $same_blog));?>
                    </div>            
                <?php }?>
            </div>
        </div>
    </section>
<?php }?>