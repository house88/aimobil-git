<aside class="sidebar">
    <div class="sidebar__block">
        <div class="sidebar__block-heading">
            <?php lang_line('blog_sidebar_categories_header');?>
        </div>
        <div class="sidebar__block-items">
            <?php foreach($blog_categories as $blog_category){?>
                <a href="<?php echo site_url($this->uritpl->replace_dynamic_uri($blog_category[lang_column('url')], $links_tpl['category'], 'blog/'));?>" class="sidebar__block-item <?php if(isset($selected_category) && $selected_category == $blog_category['id_category']){echo 'active';}?>"><?php echo $blog_category[lang_column('category_title')];?></a>
            <?php }?>
        </div>
    </div>
    <div class="sidebar__block">
        <div class="sidebar__block-heading">
            <?php lang_line('blog_sidebar_categories_archived');?>
        </div>
        <div class="sidebar__block-items">
            <?php foreach($blog_archives as $blog_archive){?>
                <a href="<?php echo site_url($this->uritpl->replace_dynamic_uri($blog_archive['archive_year'], $links_tpl['archive'], 'blog/'));?>" class="sidebar__block-item <?php if(isset($selected_archive) && $selected_archive == $blog_archive['archive_year']){echo 'active';}?>"><?php echo $blog_archive['archive_year'];?></a>                    
            <?php }?>
        </div>
    </div>
</aside>