<div class="container">
    <h1 class="heading-title-1 justify-content-between">
        <?php echo $page_detail[lang_column('page_name')];?>
        <a class="btn btn-default btn-flat btn-flex d-lg-none call-function" data-callback="call_inline_fancybox" data-content="#blog-sidebar-hidden">
            <span><?php lang_line('btn_filter_text');?></span> <i class="fai fai-bars c-orange"></i>
        </a>
    </h1>
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="row">
                <?php if(!empty($blogs)){?>
                    <?php foreach($blogs as $blog){?>
                        <div class="col-xs col-sm-6 col-lg-6">
                            <?php $this->load->view(get_theme_view('modules/blog/item_view'), array('blog' => $blog));?>
                        </div>            
                    <?php }?>            
                <?php } else{?>
                    <div class="col">
                        <div class="error error__no-data">
                            <div class="error__no-data-message">
                                <div class="icon">
                                    <i class="fai fai-warning-o"></i>
                                </div>
                                <?php lang_line('no_data');?>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
            <?php if(!empty($pagination)){?>
                <div class="row">
                    <div class="col pagination-margin"><?php echo $pagination;?></div>
                </div>            
            <?php }?>
        </div>
        <div class="d-none d-lg-flex col-4">
            <div id="blog-sidebar-hidden" class="w-100pr">
                <?php $this->load->view(get_theme_view('modules/blog/sidebar_view'));?>
            </div>
        </div>
    </div>
</div>