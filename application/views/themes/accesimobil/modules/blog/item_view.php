<article class="blog-item">
    <?php if(image_exist('files/'.$blog['blog_photo'])){?>
        <a href="<?php echo site_url(get_dinamyc_uri('blog', '/'.$blog[lang_column('url')], $lang));?>" class="blog-item__img" style="background-image:url(<?php echo site_url('files/'.$blog['blog_photo']);?>);">
            <img src="<?php echo site_url(get_image_thumb('files/'.$blog['blog_photo'], 'h215', array('h' => 215)));?>" alt="<?php echo image_title($blog[lang_column('mt')]);?>" title="<?php echo image_title($blog[lang_column('mt')]);?>">
        </a>
    <?php }?>
    <div class="blog-item__date">
        <?php 
            $blog_date = array(
                formatDate($blog['blog_date'], 'j'),
                $this->lang->line('cal_'.mb_strtolower(formatDate($blog['blog_date'], 'F'))),
                formatDate($blog['blog_date'], 'Y')
            );
        ?>
        <?php echo implode(' ', $blog_date);?>
    </div>
    <a href="<?php echo site_url(get_dinamyc_uri('blog', '/'.$blog[lang_column('url')], $lang));?>" class="blog-item__title">
        <?php echo $blog[lang_column('blog_title')];?>
    </a>
    <div class="blog-item__description">
        <?php echo $blog[lang_column('blog_small_description')];?>
    </div>
</article>