<div class="container services">
    <div class="row">
        <div class="d-flex flex-column col-12">
            <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
        </div>
        <div class="col-12 col-lg-3 left-side mb-40">
            <?php echo $page_detail['page_blocks'][lang_column('left')];?>
        </div>
        <div class="col-12 col-lg-6 middle-side mb-40">
            <?php $_banner = Modules::run('banner/_get_by_alias', 'services_top');?>
            <?php if(!empty($_banner)){?>
                <?php if(!empty($_banner[lang_column('banner_link')])){?>
                    <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                <?php }?>
                    <div class="steps-header">
                        <?php echo $_banner[lang_column('banner_text')]?>
                    </div>
                <?php if(!empty($_banner[lang_column('banner_link')])){?>
                    </a>
                <?php }?>
            <?php }?>
            <?php echo $page_detail[lang_column('page_text')];?>
        </div>
        <div class="d-none d-lg-block col-lg-3 mb-40">
            <div class="right-side">
                <?php $_banner = Modules::run('banner/_get_by_alias', 'services_right');?>
                <?php if(!empty($_banner)){?>
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                    <?php }?>
                        <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        </a>
                    <?php }?>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<section class="prefooter">
    <div class="container">
        <?php if(!empty($services)){?>
            <div class="row services-items">
                <?php foreach($services as $service){?>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <article class="service-item pb-41 pb-xl-0">
                            <div class="icon">
                                <div class="icon-o">
                                    <span class="<?php echo $service['service_icon'];?>"></span>                                    
                                </div>
                            </div>
                            <div class="details pl-31">
                                <div class="name">
                                    <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>">
                                        <?php echo $service[lang_column('service_name')];?>
                                    </a>
                                </div>
                                <div class="intro">
                                    <?php echo $service[lang_column('service_stext')];?>
                                </div>
                                <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>" class="link-more">Mai mult <i class="fai fai-right-arrow"></i></a>
                            </div>
                        </article>
                    </div>
                <?php }?>
            </div>
        <?php }?>
    </div>
</section>