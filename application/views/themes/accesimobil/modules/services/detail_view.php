<div class="container services-detail">
    <div class="row">
        <div class="col-12 title flex-column flex-md-row">
            <div class="icon mt-50 mt-md-0">
                <div class="icon-o">
                    <span class="<?php echo $service_detail['service_icon'];?>"></span>                                    
                </div>
            </div>
            <h1 class="heading-title-1 pl-0 pl-md-31"><?php echo $service_detail[lang_column('service_name')];?></h1>
        </div>
        <div class="col-12 col-lg-9">
            <div class="html-text pl-0 pl-md-11">
                <?php echo $service_detail[lang_column('service_text')];?>
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-3">
            <div class="right-side">
                <?php $_banner = Modules::run('banner/_get_by_alias', 'service_detail_right');?>
                <?php if(!empty($_banner)){?>
                    <div class="service_banner-wr">
                        <?php if(!empty($_banner[lang_column('banner_link')])){?>
                            <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                        <?php }?>
                            <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                            <div class="service_banner-body">
                                <?php echo $_banner[lang_column('banner_text')]?>
                            </div>
                        <?php if(!empty($_banner[lang_column('banner_link')])){?>
                            </a>
                        <?php }?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<section class="prefooter">
    <div class="container">
        <?php if(!empty($services)){?>
            <h1 class="heading-title-1 block-pre-footer__title pb-0">
                <?php lang_line('services_same_posts');?>
            </h1>
            <div class="row services-items pt-30_i">
                <?php foreach($services as $service){?>
                    <?php if($service['id_service'] == $service_detail['id_service']){continue;}?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <article class="service-item service-item-4 pb-41 pb-xl-0">
                            <div class="icon">
                                <div class="icon-o">
                                    <span class="<?php echo $service['service_icon'];?>"></span>                                    
                                </div>
                            </div>
                            <div class="details pl-31">
                                <div class="name">
                                    <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>">
                                        <?php echo $service[lang_column('service_name')];?>
                                    </a>
                                </div>
                                <div class="intro">
                                    <?php echo $service[lang_column('service_stext')];?>
                                </div>
                                <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $service[lang_column('special_url')], $lang));?>" class="link-more">Mai mult <i class="fai fai-right-arrow"></i></a>
                            </div>
                        </article>
                    </div>
                <?php }?>
            </div>
        <?php }?>
    </div>
</section>