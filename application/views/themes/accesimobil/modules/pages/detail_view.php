<div class="container pages-detail">
    <div class="row mb-4">
        <div class="col-12 title flex-column">
            <h1 class="heading-title-1 pl-0"><?php echo $page_detail[lang_column('page_name')];?></h1>
        </div>
        <div class="col-12 col-lg-8">
            <div class="html-text pl-0">
                <?php echo $page_detail[lang_column('page_text')];?>
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-4">
            <div class="right-side">
                <?php $_banner = Modules::run('banner/_get_by_alias', 'credit-ipotecar');?>
                <?php if(!empty($_banner)){?>
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        <a href="<?php echo site_url($_banner[lang_column('banner_link')]);?>">
                    <?php }?>
                    <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                    <?php if(!empty($_banner[lang_column('banner_link')])){?>
                        </a>
                    <?php }?>
                <?php }?>
            </div>
        </div>
    </div>
    <?php if($page_detail['show_calc'] == 1){?>
        <div class="row">
            <div class="col-12 col-lg-8">
                <section class="calculator-wr pl-md-4 pr-md-4 mb-3em">
                    <h2 class="h2-title mb-0"><?php lang_line('calc_header');?></h2>
                    <?php echo Modules::run('calculator/_show_body');?>
                    <div class="calculator-footer">
                        <div class="icon">
                            <div class="fai fai-multiply"></div>
                        </div>
                        <div class="text">
                            <?php lang_line('calc_note');?></div>
                        </div>
                </section>
            </div>
        </div>    
    <?php }?>
</div>