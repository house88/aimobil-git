<div class="container about_us">
    <div class="row">
        <div class="d-flex flex-column col-12">
            <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
        </div>
        <?php if(!empty($page_detail[lang_column('page_image')])){?>
            <div class="col-12">
                <div class="about_header">
                    <img src="<?php echo site_url('files/'.$page_detail[lang_column('page_image')]);?>" alt="<?php echo image_title($page_detail[lang_column('mt')]);?>" title="<?php echo image_title($page_detail[lang_column('mt')]);?>">
                </div>
            </div>
        <?php }?>
        <div class="col-12 col-lg-6 pr-lg-5">
            <div class="html-text">
                <?php echo $page_detail['page_blocks'][lang_column('left')];?>
            </div>
        </div>
        <div class="col-12 col-lg-6 pl-lg-42">
            <?php echo $page_detail['page_blocks'][lang_column('right')];?>
        </div>
    </div>
    <div class="row mt-20">
        <div class="col-12 col-md-6 pr-lg-5 mb-50">
            <?php if(!empty($page_detail['video'][lang_column('link')])){?>
                <p class="video-iframe">
                    <iframe src="https://www.youtube.com/embed/<?php echo $page_detail['video'][lang_column('link')];?>" width="560" height="314" allowfullscreen="allowfullscreen"></iframe>
                </p>
            <?php }?>
        </div>
        <?php $_banner = Modules::run('banner/_get_by_alias', 'about_us');?>
        <?php if(!empty($_banner)){?>
            <div class="col-12 col-md-6 pl-lg-42 mb-50">
                <div class="block__9-16">
                    <div class="banner-body">
                        <div class="image">
                            <img src="<?php echo site_url('files/'.$_banner['banner_image']);?>" alt="<?php echo $_banner[lang_column('banner_alt')]?>" title="<?php echo $_banner[lang_column('banner_title')]?>">
                        </div>
                        <div class="content">
                            <?php echo $_banner[lang_column('banner_text')]?>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
</div>
<section class="prefooter">
    <div class="container">
        <h1 class="heading-title-1 block-pre-footer__title"><?php lang_line('about_us_team_header');?></h1>
        <?php $team_users = Modules::run('users/_get_users', array('on_about' => 1, 'user_banned' => 0, 'limit' => 4));?>
        <?php if(!empty($team_users)){?>
            <div class="row team-users__vertical">
                <?php foreach($team_users as $team_user){?>
                    <div class="col-sm col-md-6 col-xl-3 team-users__vertical-item-wr">
                        <div class="d-flex flex-column team-users__vertical-item">
                            <div class="img-wr">
                                <a href="<?php echo site_url(get_dinamyc_uri('manager', $team_user[lang_column('user_url')], $lang));?>" class="image" style="background-image:url(<?php echo site_url('files/users/'.$team_user['user_photo']);?>);"></a>
                            </div>
                            <div class="text">
                                <a href="<?php echo site_url(get_dinamyc_uri('manager', $team_user[lang_column('user_url')], $lang));?>">
                                    <div class="location">
                                        <span class="heading"><?php echo $team_user[lang_column('position_name')];?></span>
                                        <span class="location-name"><?php echo (($team_user['plocation_in_title'] == 1)?$team_user[lang_column('plocation_name')].', ':'').$team_user[lang_column('location_name')];?></span>
                                    </div>
                                    <div class="info">
                                        <div class="name"><?php echo $team_user[lang_column('user_name')];?></div>
                                    </div>
                                </a>
                                <div class="info">
                                    <a href="tel:<?php echo $team_user['user_phone'];?>" class="phone">
                                        <span class="fai fai-phone"></span>
                                        <?php echo $team_user['user_phone'];?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
            <?php if(count($team_users) > 4){?>
                <div class="d-flex align-items-center justify-content-center">
                    <span class="btn btn-flat bg-transparent btn-more-team call-function mt-30" data-callback="show_hidden" data-toggle-parent=".team-users__vertical" data-toggle-item=".team-users__vertical-item-wr">Mai mult</span>
                </div>
            <?php }?>
        <?php }?>
    </div>
</section>