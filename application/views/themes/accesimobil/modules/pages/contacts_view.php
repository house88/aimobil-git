<div class="container">
    <div class="row">
        <div class="col-12 col-md-5 col-xl-4">
            <aside class="w-100pr">
                <aside class="sidebar sidebar-contacts" id="sidebar-contacts">
                    <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
                    <div class="sidebar__block">
                        <div class="sidebar__block-menu">
                            <a class="menu-item">
                                <i class="fai fai-marker-stroke"></i>
                                <span><?php echo $this->lsettings->item('office_address');?></span>
                            </a>
                            <a href="tel:<?php echo $this->lsettings->item('office_phone');?>" class="menu-item">
                                <i class="fai fai-phone"></i>
                                <span><?php echo $this->lsettings->item('office_phone');?></span>
                            </a>
                            <a href="mailto:<?php echo $this->lsettings->item('office_support_email');?>" class="menu-item">
                                <i class="fai fai-envelope"></i>
                                <span><?php echo $this->lsettings->item('office_support_email');?></span>
                            </a>
                        </div>
                    </div>
                    <div class="sidebar__block">
                        <div class="sidebar__block-stars">
                            <a href="https://search.google.com/local/writereview?placeid=ChIJVch18Et8yUAR0HW1oP7CBeY" class="google-place__rating" target="_blank">
                                <div class="stars-outer">
                                    <div class="stars-inner" style="width:<?php echo $rating_w;?>%"></div>
                                </div>
                                <span class="rating__block-title">Google Rating: <?php echo $rating;?></span>
                            </a>
                        </div>
                    </div>
                    <div class="sidebar__block">
                        <div class="sidebar__block-working-hours">
                            <?php echo $page_detail[lang_column('page_text')];?>
                        </div>
                    </div>
                    <div class="sidebar__block mb-3 mb-md-0 mnh-200">
                        <div class="sidebar__block-fb-app">
                            <div id="fb-root"></div>
                            <div class="fb-page w-100pr" data-href="https://www.facebook.com/accesimobil.md" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/accesimobil.md" class="fb-xfbml-parse-ignore">
                                    <a href="https://www.facebook.com/accesimobil.md">Facebook</a>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </aside>
            </aside>
        </div>
        <div class="col-12 col-md-7 col-xl-8 d-block">
            <div class="google-maps__contacts pt-0 pt-md-6 pt-xl-7" id="google-maps__contacts">
                <div class="gmap" id="contacts_gmap" style="height:100%;"></div>

                <?php 
                    $markers = array(
                        array(
                            'latitude' => $this->lsettings->item('office_address_google_map_latitude'),
                            'longitude' => $this->lsettings->item('office_address_google_map_longitude'),
                            'container' => 'contacts_gmap'
                        )
                    );
                ?>
                <script type="text/javascript">
                    init_google_maps = true;
                    is_contact = true;
			        markers = <?php echo json_encode($markers);?>;
                </script>
            </div>
            <script>
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.0&appId=<?php echo $this->lsettings->item('facebook_app_id');?>&autoLogAppEvents=1';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            </script>
        </div>
    </div>
    <?php $team_users = Modules::run('users/_get_users', array('on_site' => 1, 'user_banned' => 0));?>
    <?php if(!empty($team_users)){?>
        <div class="row team-users">
            <?php foreach($team_users as $team_user){?>
                <div class="d-flex col-sm col-md-6 col-xl-3 team-users__item">
                    <div class="img-wr">
                        <a href="<?php echo site_url(get_dinamyc_uri('manager', $team_user[lang_column('user_url')], $lang));?>" class="image" style="background-image:url(<?php echo site_url('files/users/'.$team_user['user_photo']);?>);"></a>
                    </div>
                    <div class="text">
                        <div class="location">
                            <span class="heading"><?php lang_line('manager_region_label');?></span>
                            <span class="location-name">&laquo;<?php echo $team_user[lang_column('location_name')];?>&raquo;</span>
                        </div>
                        <div class="info">
                            <div class="name"><?php echo $team_user[lang_column('user_name')];?></div>
                        </div>
                        <div class="info">
                            <a href="tel:<?php echo $team_user['user_phone'];?>" class="phone">
                                <span class="fai fai-phone"></span>
                                <?php echo $team_user['user_phone'];?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    <?php }?>
</div>
<section class="feedback">
    <div class="container">
        <form id="form_feedback">
            <h1 class="heading-title-1 block-pre-footer__title"><?php lang_line('feedback_header');?></h1>
            <div class="row">
                <div class="d-flex flex-column col-sm col-md-6 col-xl-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="<?php lang_line('notify_feedback_label_name');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="<?php lang_line('notify_feedback_label_email');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="<?php lang_line('notify_feedback_label_phone');?>" autocomplete="off">
                    </div>
                </div>
                <div class="d-flex flex-column col-sm col-md-6 col-xl-8 message">
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="<?php lang_line('notify_feedback_label_message');?>"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-flex flex-column col-sm col-md-6 col-xl-4 mt-20">
                    <?php $recaptcha = Modules::run('pages/_get_captcha');?>
                    <?php echo $recaptcha['recaptcha_widget'];?>
                    <?php echo $recaptcha['recaptcha_script'];?>
                </div>
                <div class="d-flex flex-row align-items-center col-sm col-md-6 col-xl-8 mt-20">
                    <button class="btn btn-flat text-uppercase bg-orange call-function" data-callback="send_feedback" type="button">
                        <?php lang_line('feedback_btn_send');?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>