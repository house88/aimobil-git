<div class="container">
    <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
    <div class="row">
        <div class="col-xl-6 order-2 order-xl-1">
            <section class="add-ad-wr mt-41 mt-xl-0">
                <form id="form_add_ad">
                    <?php $categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'sale'));?>
                    <div class="form-group">
                        <div class="form-select">
                            <select name="category" class="form-control">
                                <option value=""><?php lang_line('notify_add_ad_label_category');?></option>
                                <?php foreach($categories as $category){?>
                                    <option value="<?php echo $category['id_category'];?>"><?php echo $category[lang_column('category_title')];?></option>
                                <?php }?>
                            </select>
                            <div class="icon">
                                <div class="fai fai-down-arrow-o"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="<?php lang_line('notify_add_ad_label_name');?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="<?php lang_line('notify_add_ad_label_phone');?>" autocomplete="off">
                    </div>
                    <div class="form-group d-flex flex-column flex-sm-row justify-content-between mt-30 mb-0">
                        <div class="flex-shrink-0">
                            <?php $recaptcha = Modules::run('pages/_get_captcha');?>
                            <?php echo $recaptcha['recaptcha_widget'];?>
                            <?php echo $recaptcha['recaptcha_script'];?>
                        </div>
                        <div class="btn-wr ml-sm-4">
                            <button class="btn btn-flat btn-block text-uppercase bg-orange call-function mt-2 mt-sm-0" data-callback="send_ad" type="button">
                                <?php lang_line('feedback_btn_send');?>
                            </button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
        <div class="col-xl-6 order-1 order-xl-2">
            <div class="add-ad-wr p-0 p-xl-41 mb-md-0">
                <div class="add-ad__description pb-4">
                    <?php if(!empty($page_detail['page_blocks'][lang_column('right')])){?>
                        <?php echo $page_detail['page_blocks'][lang_column('right')];?>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>