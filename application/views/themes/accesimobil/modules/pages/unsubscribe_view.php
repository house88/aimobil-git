<div class="container about_us">
    <div class="row">
        <div class="col-12">
            <section class="unsubscribe-wr">
                <div class="success success__sent">
                    <h1 class="h1-title">
                        <div class="icon">
                            <i class="fai fai-check"></i>
                        </div>
                        <?php lang_line('notify_unsubscribe_success_header');?>
                    </h1>
                    <div class="text">
                        <?php lang_line('notify_unsubscribe_success_text');?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>