<section class="pt-3em pb-3_5em">
	<div class="container article-container">
		<h2 class="f-medium c-black pb-0_9em discount-header"><?php lang_line('favorite_header');?></h2>
		<div class="w100 text-left pb-2em">
			<div class="flex">
				<div class="top-page-navigation">
                    <a href="#" class="active call-function" data-callback="toogle_tabs" data-tab="#real_estate-wr" data-parent="#favorite-tabs"><?php lang_line('favorite_tab_real_estate');?></a>
                    <a href="#" class="call-function" data-callback="toogle_tabs" data-tab="#complexe-wr" data-parent="#favorite-tabs"><?php lang_line('favorite_tab_complexes');?></a>
				</div>
			</div>
		</div>
		<div id="favorite-tabs">
            <div class="row child-mb-2em mt-0_5em favorite-tab"  id="real_estate-wr">
                <?php $this->load->view(get_theme_view('modules/favorite/real_estate_list_view'));?>
            </div>
            <div class="row mt-0_5em favorite-tab"  id="complexe-wr" style="display:none;">
                <?php $this->load->view(get_theme_view('modules/favorite/complexe_list_view'));?>
            </div>
		</div>
    </div>
</section>