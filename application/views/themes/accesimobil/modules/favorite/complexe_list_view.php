<?php if(!empty($residential_complexes)){?>
	<?php $complex_markers = array();?>
	<?php foreach($residential_complexes as $residential_complex){?>
		<?php
			$complex_markers[] = array(
				'latitude' => $residential_complex['complex_lat'],
				'longitude' => $residential_complex['complex_lng'],
				'container' => 'rc_map_'.$residential_complex['id_complex']
			);	
		?>
		<div class="col-12 favorite_item-wr">
			<div class="flex pad box-shadow mb-3em">
				<div class="col-xs-12 custom-left-col">
					<div class="w100 pos-rel">
						<a class="wish_list fai fai-like <?php echo set_favorite($residential_complex['id_complex'], 'complex', 'hasWishList');?> call-function" data-callback="remove_favorite" data-item="<?php echo $residential_complex['id_complex'];?>" data-type="complex" href="#"></a>
						<a href="<?php echo site_url(get_dinamyc_uri('residential-complexes/detail/id', $residential_complex[lang_column('url')], $lang))?>" target="_blank" data-tblank>
							<img class="w100" src="<?php echo site_url('files/residential_complexes/'.$residential_complex['id_complex'].'/'.$residential_complex['complex_photo']);?>" alt="<?php echo image_title($residential_complex[lang_column('complex_title')], 1);?>" title="<?php echo image_title($residential_complex[lang_column('complex_title')], 1);?>">
						</a>
					</div>
				</div>
				<div class="col-xs-12 col-md custom-parent-cols">
					<div class="h100 w100 pb-0_5em pt-1em">
						<div class="h100 flex nowrap-md">
							<div class="custom-left-inner-col">
								<div><small class="text-muted"><?php echo (($locations[$residential_complex['id_location']]['plocation_in_title'] == 1)?$locations[$residential_complex['id_location']][lang_column('plocation_name')].', ':'') . $locations[$residential_complex['id_location']][lang_column('location_name')];?></small></div>
								<a href="<?php echo site_url(get_dinamyc_uri('residential-complexes/detail/id', $residential_complex[lang_column('url')], $lang))?>" target="_blank" data-tblank class="size-1_1em f-medium c-black lh-1_2em pb-0_5em hover-orange">
									<?php echo $residential_complex[lang_column('complex_title')];?>
								</a>
								<div class="c-grey-deep size-0_9em pt-0_5em">
									<?php echo $residential_complex[lang_column('complex_stext')];?>
								</div>
								<div class="complex-more-link">
									<a class="c-orange up size-0_8em f-medium hover-black" href="<?php echo site_url(get_dinamyc_uri('residential-complexes/detail/id', $residential_complex[lang_column('url')], $lang))?>" target="_blank" data-tblank><?php lang_line('residential_complexes_link_label_detail');?> <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
								</div>
							</div>
							<div class="custom-right-inner-col">
								<div class="h100 flex column space-between">
									<div class="mb-1em-xs">
										<div>
											<small class="text-muted"><?php lang_line('label_price_from');?></small>
										</div>
										<div>
											<span class="c-orange size-1_3em f-medium pt-0_3em"><?php echo formatNumber($residential_complex['complex_price_from']);?> &euro;</span>
										</div>
									</div>
									<div class="flex nowrap">
										<a href="<?php echo site_url(get_dinamyc_uri('manager', $managers[$residential_complex['id_manager']][lang_column('user_url')], $lang));?>" class="shrink-0">
											<img class="" src="<?php echo getImage('files/users/'.$managers[$residential_complex['id_manager']]['user_photo'])?>" width="80" height="100" alt="<?php echo image_title($managers[$residential_complex['id_manager']][lang_column('user_name')]);?>" title="<?php echo image_title($managers[$residential_complex['id_manager']][lang_column('user_name')]);?>">
										</a>
										<div class="pl-1em">
											<a href="<?php echo site_url(get_dinamyc_uri('manager', $managers[$residential_complex['id_manager']][lang_column('user_url')], $lang));?>" class="lh-1_1em c-black size-0_8em">
												<?php echo $managers[$residential_complex['id_manager']][lang_column('user_name')];?>
											</a>
											<a href="tel:<?php echo $managers[$residential_complex['id_manager']]['user_phone'];?>" class="flex text-nowrap size-0_9em c-orange mt-0_3em f-medium">
												<?php echo $managers[$residential_complex['id_manager']]['user_phone'];?>
											</a>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-3 map-parent">
					<div class="map w100 h100" id="rc_map_<?php echo $residential_complex['id_complex'];?>"></div>
				</div>
			</div>
		</div>
	<?php }?>
	<script type="text/javascript">
		init_google_maps = true;
		markers = <?php echo json_encode($complex_markers);?>;
	</script>
<?php } else{?>
	<div class="col">
		<div class="error error__no-data mwp-100_i">
			<div class="error__no-data-message">
				<div class="icon">
					<i class="fai fai-warning-o"></i>
				</div>
				<?php lang_line('no_data');?>
			</div>
		</div>
	</div>
<?php }?>