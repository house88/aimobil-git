<script>
    show_calc = true;
    <?php if($calc_on_item == true){?>
        calc_on_item = true;
    <?php }?>
</script>
<div class="calculator-body d-flex flex-column flex-lg-row">
    <div class="calculator-sliders">
        <?php if($calc_on_item == true){?>
            <input type="hidden" id="calc_price" value="<?php echo $price_default;?>">
        <?php } else{?>
            <div class="calculator-sliders__item">
                <div class="label">
                    <div class="text">
                        <?php lang_line('calc_label_price');?>
                    </div>
                    <div class="number">
                        <span class="number-item" id="calc_price_value">0</span>
                        <span class="symbol"><?php echo $currency_symbol;?></span>
                    </div>
                </div>
                <input type="hidden" value="" data-slider-min="<?php echo $price_min;?>" data-slider-max="<?php echo $price_max;?>" data-slider-step="100" data-slider-value="<?php echo $price_default;?>" data-slider-handle="custom" id="calc_price" data-slider-tooltip="hide" />
                <div class="min-max">
                    <div class="min"><?php echo formatNumber($price_min);?> <?php echo $currency_symbol;?></div>
                    <div class="max"><?php echo formatNumber($price_max);?> <?php echo $currency_symbol;?></div>
                </div>
            </div>
        <?php }?>
        <div class="calculator-sliders__item">
            <div class="label">
                <div class="text">
                    <?php lang_line('calc_label_interest_rate');?>
                </div>
                <div class="number">
                    <span class="number-item" id="calc_rate_value">0</span>
                    <span class="symbol">%</span>
                </div>
            </div>
            <input type="hidden" value="" data-slider-enabled="false" data-slider-min="<?php echo $interest_rate_min;?>" data-slider-max="<?php echo $interest_rate_max;?>" data-slider-step="1" data-slider-value="<?php echo $interest_rate_default;?>" data-slider-handle="custom" id="calc_rate" data-slider-tooltip="hide" />
            <div class="min-max">
                <div class="min"><?php echo $interest_rate_min;?>%</div>
                <div class="max"><?php echo $interest_rate_max;?>%</div>
            </div>
        </div>
        <div class="calculator-sliders__item">
            <div class="label">
                <div class="text">
                    <?php lang_line('calc_label_first_rate');?>
                </div>
                <div class="number">
                    <span class="number-item" id="calc_rate_client_value">0</span>
                    <span class="symbol"><?php echo $currency_symbol;?></span>
                    <div class="percent">
                        (<span class="number-item" id="calc_rate_client_value_percent">0</span>
                        <span class="symbol">%</span>)
                    </div>
                </div>
            </div>
            <input type="hidden" value="" data-slider-min="<?php echo $first_rate_min;?>" data-slider-max="<?php echo $first_rate_max;?>" data-slider-step="1" data-slider-value="<?php echo $first_rate_default;?>" data-slider-handle="custom" id="calc_rate_client" data-slider-tooltip="hide" />
            <div class="min-max">
                <div class="min"><?php echo $first_rate_min;?>%</div>
                <div class="max"><?php echo $first_rate_max;?>%</div>
            </div>
        </div>
        <div class="calculator-sliders__item">
            <div class="label">
                <div class="text">
                    <?php lang_line('calc_label_period');?>
                </div>
                <div class="number">
                    <span class="number-item" id="calc_years_value">0</span>
                    <span class="symbol" id="calc_years_value_sufix">ani</span>
                </div>
            </div>
            <input type="hidden" value="" data-slider-min="<?php echo $period_min;?>" data-slider-max="<?php echo $period_max;?>" data-slider-step="1" data-slider-value="<?php echo $period_default;?>" data-slider-handle="custom" id="calc_years" data-slider-tooltip="hide" />
            <div class="min-max">
                <div class="min"><?php echo $period_min;?> <?php echo plural_text('year', $period_min, $this->lang->lang());?></div>
                <div class="max"><?php echo $period_max;?> <?php echo plural_text('year', $period_max, $this->lang->lang());?></div>
            </div>
        </div>
    </div>
    <div class="calculator-results pl-lg-5">
        <?php if($calc_on_item == true){?>
            <div class="calculator-results__item mb-2 mb-md-5">
                <div class="text"><?php lang_line('calc_label_price');?></div>
                <div class="value">
                    <span class="number" style="color:#5c5c5c;"><?php echo $real_price;?></span>
                    <span class="currency" style="color:#5c5c5c;"><?php echo $currency_symbol;?></span>
                </div>
            </div>
        <?php }?>
        <div class="calculator-results__item mb-2 mb-md-5">
            <div class="text">
                <?php lang_line('calc_label_amount_funded');?>
            </div>
            <div class="value">
                <span class="number" id="calc_credit_price">0</span>
                <span class="currency"><?php echo $currency_symbol;?></span>
            </div>
        </div>
        <div class="calculator-results__item">
            <div class="text">
                <?php lang_line('calc_label_monthly_payment');?>
            </div>
            <div class="value">
                <span class="number" id="calc_month_amount">0</span>
                <span class="currency"><?php echo $currency_symbol;?></span>
            </div>
        </div>
    </div>
</div>