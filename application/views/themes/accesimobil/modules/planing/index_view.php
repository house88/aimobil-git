<div class="container planing-apartment">
    <div class="row">
        <div class="d-flex flex-column col-12">
            <h1 class="heading-title-1"><?php echo $page_detail[lang_column('page_name')];?></h1>
            <div class="row">
                <div class="col-md-8">
                    <?php echo $page_detail[lang_column('page_text')];?>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="plans-series">
<?php foreach($planing_series as $plan_serie){?>
    <section class="plans-series__item">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="image text-center text-sm-left">
                        <img src="<?php echo site_url(get_image_thumb('files/planing/'.$plan_serie['plan_photo'], 'h215', array('h' => 215)));?>" alt="<?php echo image_title($plan_serie[lang_column('plan_name')]);?>" title="<?php echo image_title($plan_serie[lang_column('plan_name')]);?>">
                    </div>
                </div>
                <div class="col-12 col-sm-8">
                    <h2 class="h2-title mt-41 mt-sm-0 pl-sm-4"><?php echo $plan_serie[lang_column('plan_name')];?></h2>
                    <div class="text pl-sm-4"><?php echo $plan_serie[lang_column('plan_text')];?></div>
                </div>
                <div class="col-12">
                    <?php $photos = json_decode($plan_serie['plan_photos'], true);?>
                    <div class="row serie-gallery">
                        <?php foreach($photos as $photo){?>
                            <a href="<?php echo site_url('files/planing/'.$photo['file']);?>" class="gallery__item" data-fancybox="images-<?php echo $plan_serie['id_plan'];?>" data-caption="<?php echo $photo[lang_column('title')];?>">
                                <div class="image">
                                    <img src="<?php echo site_url(get_image_thumb('files/planing/'.$photo['file'], 'h200', array('h' => 200)));?>" alt="<?php echo image_title($photo[lang_column('title')]);?>" title="<?php echo image_title($photo[lang_column('title')]);?>">
                                </div>
                                <div class="title"><?php echo $photo[lang_column('title')];?></div>
                            </a>                        
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
</section>