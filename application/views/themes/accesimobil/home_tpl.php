<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang();?>">
<head>
    <?php $this->load->view(get_theme_view('include/head'));?>
</head>
<body <?php if(isset($has_paralax)){echo 'class="page-home"';}?>>
    <div class="fixed-menu hide-fixed-menu"></div>
    <header id="masthead" class="home-header d-flex ">
        <div class="container">
            <div class="header-nav w100 d-flex">
                <div class="header-nav__logo__parent">
                    <div class="header-nav__logo">
                        <a href="#"></a>
                    </div>
                </div>
                <div class="header-nav__menu">
                    <div class="header-nav__menu-top">
                        <div class="header-nav__menu-top-left">
                            <a class="header-nav__menu-top-left-item icon" href="#"><i class="fai fai-home"></i></a>
                            <a href="#" class="header-nav__menu-top-left-item">Adauga Anunt</a>
                            <a href="#" class="header-nav__menu-top-left-item">Ipoteca</a>
                            <a href="#" class="header-nav__menu-top-left-item">Planificare apartamente</a>
                        </div>
                        <div class="header-nav__menu-top-right d-flex align-items-center">
                            <div class="secondary-menu__menu-item secondary-menu__search-container">
                                <form class="navbar-form search-form">
                                    <div class="form-group">
                                        <label for="search-input-secondary-menu">Search</label>
                                        <input id="search-input-secondary-menu" value="" name="search-input" type="text" placeholder="Search" class="form-control">
                                    </div>
                                    <button type="submit" class="btn search-button search-button-toggler"><span class="fai fai-search"></span></button>
                                </form>
                                <button type="button" class="btn search-button search-button-toggler"><span class="fai fai-search header-nav__menu-top-right-item icon"></span></button>
                            </div>
                            <a class="header-nav__menu-top-right-item icon" href="#">
                                <i class="fai fai-marker-stroke"></i>
                            </a>
                            <a class="header-nav__menu-top-right-item icon" href="#">
                                <i class="fai fai-like"></i>
                            </a>
                            <div class="dropdown dropdown-langs">
                                <span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                Ru
                                <i class="fai fai-arrow-down"></i>
                            </span>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Ro</a>
                                    <a class="dropdown-item" href="#">En</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-nav__menu-bottom flex items-center space-between">
                        <ul class="header-nav__menu-bottom-left m-0">
                            <li class="menu-item-has-children">
                                <a href="filter-catalog-grid.html" class="header-nav__menu-bottom-left-item">VÂNZARE</a>
                                <div class="submenu-parent">
                                    <div class="w100 flex-1em">
                                        <div class="col-md-3">
                                            <ul class="submenu">
                                                <li class="f-medium c-black pb-0_3em"><a href="">Apartamente</a></li>
                                                <li><a href="#">1 camera</a></li>
                                                <li><a href="#">2 camere</a></li>
                                                <li><a href="#">3 camere</a></li>
                                                <li><a href="#">4 camere</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul class="submenu">
                                                <li class="f-medium c-black"><a href="">Case</a></li>
                                                <li><a href="#">1 nivele</a></li>
                                                <li><a href="#">2 nivele</a></li>
                                                <li><a href="#">3 nivele</a></li>
                                                <li><a href="#">4+ nivele</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul class="submenu">
                                                <li class="f-medium c-black"><a href="">Spatii comerciale</a></li>
                                                <li><a href="#">Comercial</a></li>
                                                <li><a href="#">Oficii</a></li>
                                                <li><a href="#">Depozit / Producere</a></li>
                                                <li><a href="#">Restaurant / Bar</a></li>
                                                <li><a href="#">Afacere</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul class="submenu">
                                                <li class="f-medium c-black"><a href="">Terenuri</a></li>
                                                <li><a href="#">Constructie</a></li>
                                                <li><a href="#">Agricol</a></li>
                                                <li><a href="#">Gradina / Pomicol</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="filter-catalog-list.html" class="header-nav__menu-bottom-left-item">Chirie</a>
                                <div class="submenu-parent-block has-post-block p-2em pl-4em">
                                    <div class="w100 flex">
                                        <div class="col-md-9">
                                            <div class="flex pt-1em">
                                                <div class="col-md-3">
                                                    <ul class="submenu">
                                                        <li class="f-medium c-black pb-0_3em"><a href="">Apartamente</a></li>
                                                        <li><a href="#">1 camera</a></li>
                                                        <li><a href="#">2 camere</a></li>
                                                        <li><a href="#">3 camere</a></li>
                                                        <li><a href="#">4 camere</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="submenu">
                                                        <li class="f-medium c-black"><a href="">Case</a></li>
                                                        <li><a href="#">1 nivele</a></li>
                                                        <li><a href="#">2 nivele</a></li>
                                                        <li><a href="#">3 nivele</a></li>
                                                        <li><a href="#">4+ nivele</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="submenu">
                                                        <li class="f-medium c-black"><a href="">Spatii comerciale</a></li>
                                                        <li><a href="#">Comercial</a></li>
                                                        <li><a href="#">Oficii</a></li>
                                                        <li><a href="#">Depozit / Producere</a></li>
                                                        <li><a href="#">Restaurant / Bar</a></li>
                                                        <li><a href="#">Afacere</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="submenu">
                                                        <li class="f-medium c-black"><a href="">Terenuri</a></li>
                                                        <li><a href="#">Constructie</a></li>
                                                        <li><a href="#">Agricol</a></li>
                                                        <li><a href="#">Gradina / Pomicol</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="card box-shadow">
                                                <div class="discount-badge">
                                                    <div class="badge-title">%</div>
                                                    <div class="badge-content">-50€ / m2</div>
                                                </div>
                                                <a href="page-single.html">
                                                    <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                                                </a>
                                                <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                                                    <div>3 <small class="text-muted">camere</small></div>
                                                    <div>80 <small class="text-muted">m2</small></div>
                                                    <div>3/5 <small class="text-muted">etaj</small></div>
                                                    <div>1 <small class="text-muted">bai</small></div>
                                                </div>
                                                <div class="card-body">
                                                    <a href="page-single.html">
                                                        <div class="d-flex justify-content-between">
                                                            <div class="f-medium c-black">
                                                                Chisinau, Botanica, Traian
                                                            </div>
                                                            <div class="f-medium c-black col-4">
                                                                <div class="row">
                                                                    <div class="pl-0_3em">
                                                                        <span class="c-orange size-0_9em">50 000 $</span>
                                                                        <div class="c-mid-grey size-0_9em">Ipoteca</div>
                                                                        <div class="c-mid-grey size-0_9em">575 $/luna</div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li><a href="location-complex.html" class="header-nav__menu-bottom-left-item">Complexe Locative</a></li>
                            <li><a href="discount.html" class="header-nav__menu-bottom-left-item">Reduceri</a></li>
                            <li class="menu-item-has-children small-children"><a href="#" class="header-nav__menu-bottom-left-item">Servicii</a>
                                <ul class="small-submenu">
                                    <li><a href="">Posturi vacante</a></li>
                                    <li><a href="">Parteneri</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children small-children"><a href="#" class="header-nav__menu-bottom-left-item">О нас</a>
                                <ul class="small-submenu">
                                    <li><a href="">Posturi vacante</a></li>
                                    <li><a href="">Parteneri</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="header-nav__menu-bottom-left-item">Blog</a></li>
                            <li><a href="#" class="header-nav__menu-bottom-left-item">Contacte</a></li>
                        </ul>
                        <div class="header-nav__menu-bottom-right flex items-center">
                            <div class="shrink-0 lh-0_8em pr-0_5em">
                                <img src="img/icons/phone-icon.png" alt="phone icon">
                            </div>

                            <a href="tel:+37378888900" class="header-nav__menu-bottom-right-item phone">(+373) 78 888 900</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="mobile-header" class="mobile-nav">
        <div class="mobile-head">
            <div class="container">
                <div class="mobile-head-inner h100">
                    <a href="tel:0123456789" class="header-nav__menu-top-right-item icon">
                        <img src="img/icons/phone-icon.png" alt="phone">
                    </a>
                    <a href="#" class="search-open header-nav__menu-top-right-item icon">
                        <img src="img/icons/icon-search.png" alt="phone">
                    </a>
                    <a class="header-nav__menu-top-right-item icon" href="#">
                        <img src="img/icons/icon-marker.png" alt="phone">
                    </a>
                    <a class="header-nav__menu-top-right-item icon" href="#">
                        <img src="img/icons/icon-heart.png" alt="phone">
                    </a>
                    <div class="dropdown dropdown-langs">
                                <span class="dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown">
                                Ru
                                <i class="fai fai-arrow-down"></i>
                            </span>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Ro</a>
                            <a class="dropdown-item" href="#">En</a>
                        </div>
                    </div>
                </div>
                <div class="w100 mobile-search-parent">
                    <form class="mobile-form">
                        <label for="search-input-mobile-menu" style="display: none">Search</label>
                        <input id="search-input-mobile-menu" value="" name="search-input" type="text"  class=" form-control">
                        <button type="submit" class="mobile-search-button"><span class="fai fai-search"></span></button>
                    </form>
                    <a href="" class="mobile-search-close fai fai-x-o"></a>
                </div>
            </div>
        </div>
        <div class="mobile-second">
            <div class="menu_toggle">
                <button></button>
            </div>
            <div class="header-nav__logo__parent">
                <div class="header-nav__logo">
                    <a href="index.html">
                        <img src="img/icons/main-logo.png" alt="logo">
                    </a>
                </div>
            </div>
        </div>
        <div class="mobile_menu">
            <ul class="top_mobile_menu">
                <li class="menu-item-has-children f-bold"><a href="#">Vanzare</a>
                    <ul class="sub-menu">
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-apart.png" alt="icon"></div> Apartamente</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-case.png" alt="icon"></div>Case</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-commert.png" alt="icon"></div>Spatii comerciale</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-teren.png" alt="icon"></div>Terenuri</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children f-bold"><a class="" href="#">Chirie</a>
                    <ul class="sub-menu">
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-apart.png" alt="icon"></div> Apartamente</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-case.png" alt="icon"></div>Case</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-commert.png" alt="icon"></div>Spatii comerciale</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-teren.png" alt="icon"></div>Terenuri</a></li>
                    </ul>
                </li>
                <li class="f-bold"><a class="" href="index.html">Complexe locative</a></li>
                <li class="f-bold"><a class="" href="index.html">Reduceri</a></li>
                <li class="menu-item-has-children">
                    <a href="#">Servicii</a>
                    <ul class="sub-menu">
                        <li><a href="index.html">Apartamente</a></li>
                        <li><a href="index.html">Case</a></li>
                        <li><a href="index.html">Spatii comerciale</a></li>
                        <li><a href="index.html">Terenuri</a></li>
                    </ul>
                </li>
                <li><a href="#">Adauga anunt</a></li>
                <li><a href="#">Ipoteca</a></li>
                <li><a href="#">Planificare apartamente</a></li>
                <li class="menu-item-has-children"><a href="#">Despre noi</a>
                    <ul class="sub-menu">
                        <li><a href="index.html">Apartamente</a></li>
                        <li><a href="index.html">Case</a></li>
                        <li><a href="index.html">Spatii comerciale</a></li>
                        <li><a href="index.html">Terenuri</a></li>
                    </ul>
                </li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contacte</a></li>
            </ul>
        </div>
    </div>
    <section class="home--bg parallax-window" data-parallax="scroll" data-image-src="img/bg/home_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="home-title f-bold">Experti in<br>
                        vanzare-cumparare<br> bunuri imobiliare</h1>
                </div>
                <div class="col-md-6">
                    <div class="w100 position-relative">
                        <div class="home-top_filter text-center">
                            <div class="filter-title f-medium">Caută în 3000 cele mai bune<br>
                                oferte imobiliare din Chișinău
                            </div>
                            <div class="filter-tabs">
                                <div class="tab-nav d-flex justify-content-center align-items-center f-medium up">
                                    <div><a class="custom-tab" href="#" data-open="#tab1" data-close=".tab-content">VÂNZARE</a></div>
                                    <div><a class="custom-tab" href="#" data-open="#tab2" data-close=".tab-content">Chirie</a></div>
                                </div>
                                <div class="filter-content">
                                    <div class="tab-content" id="tab1">
                                        <div class="panel row">
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/apartments-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Apartamente</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/home-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Case</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/commerciale-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Spatii comerciale</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/terenuri-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Terenuri</div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-content" id="tab2">
                                        <div class="panel row">
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/apartments-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Apartamente</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/home-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Case</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/commerciale-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Spatii comerciale</div>
                                                    </a>
                                                </div>


                                            </div>
                                            <div class="col-6 col-sm-3">
                                                <div class="row">
                                                    <a class="custom-tab" href="#">
                                                        <div class="image-block">
                                                            <img src="img/icons/svg-icons/terenuri-filter.svg" alt="icon home">
                                                        </div>
                                                        <div class="description">Terenuri</div>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="mobile-menu-block">
        <div class="page_menu">
            <ul class="top_mobile_menu ">
                <li class="menu-item-has-children f-bold"><a href="#">Vanzare</a>
                    <ul class="sub-menu">
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-apart.png" alt="icon"></div> Apartamente</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-case.png" alt="icon"></div>Case</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-commert.png" alt="icon"></div>Spatii comerciale</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-teren.png" alt="icon"></div>Terenuri</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children f-bold"><a class="" href="#">Chirie</a>
                    <ul class="sub-menu">
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-apart.png" alt="icon"></div> Apartamente</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-case.png" alt="icon"></div>Case</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-commert.png" alt="icon"></div>Spatii comerciale</a></li>
                        <li><a href="index.html"><div class="pr-2"><img src="img/icons/mob-teren.png" alt="icon"></div>Terenuri</a></li>
                    </ul>
                </li>
                <li class="f-bold"><a class="" href="index.html">Complexe locative</a></li>
                <li class="f-bold"><a class="" href="index.html">Reduceri</a></li>
                <li class="menu-item-has-children">
                    <a href="#">Servicii</a>
                    <ul class="sub-menu">
                        <li><a href="index.html">Apartamente</a></li>
                        <li><a href="index.html">Case</a></li>
                        <li><a href="index.html">Spatii comerciale</a></li>
                        <li><a href="index.html">Terenuri</a></li>
                    </ul>
                </li>
                <li><a href="#">Adauga anunt</a></li>
                <li><a href="#">Ipoteca</a></li>
                <li><a href="#">Planificare apartamente</a></li>
                <li class="menu-item-has-children"><a href="#">Despre noi</a>
                    <ul class="sub-menu">
                        <li><a href="index.html">Apartamente</a></li>
                        <li><a href="index.html">Case</a></li>
                        <li><a href="index.html">Spatii comerciale</a></li>
                        <li><a href="index.html">Terenuri</a></li>
                    </ul>
                </li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contacte</a></li>
            </ul>
        </div>
    </div>
    <section class="pt-2em-xs pt-5_5em-sm pb-1_5em-xs pb-4em-sm">
        <div class="container article-container">
            <h2 class="text-center f-bold pb-1_5em">Imobile la pret redus</h2>
            <div class="row vitrine-section">
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content"><del>-1 580 000MDL</del></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content"><del>-75 000€</del></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content"><del>-75 000€</del></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content"><del>-75 000€</del></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content">-50€ / M<sup>2</sup></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card mb-2em box-shadow">
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <div class="badge-content"><del>-75 000€</del></div>
                        </div>
                        <a class="wish_list fai  fai-like" href="#"></a>
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <div>3 <small class="text-muted">camere</small></div>
                            <div>80 <small class="text-muted">m2</small></div>
                            <div>3/5 <small class="text-muted">etaj</small></div>
                            <div>1 <small class="text-muted">bai</small></div>
                        </div>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div class="f-medium c-black lh-1_3em">
                                        Chisinau, Botanica, Traian
                                        <div class="flex f-regular items-center mt-0_3em">
                                            <div><span class="fai fai-eye-o c-grey size-0_8em c-mid-grey lh-1_2em"></span></div>
                                            <div class="pl-0_5em c-grey size-0_9em c-mid-grey lh-0_7em">
                                                727
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-5">
                                        <div class="row">
                                            <div class="pl-1_5em">
                                                <span class="c-orange">50 000 $</span>
                                                <small>Ipoteca</small>
                                                <small>575 $ / luna</small>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w100 text-center pt-1em">
            <a class="btn button-transparent " href="">Mai mult</a>
        </div>
    </section>
    <section class="pt-1_5em-xs pt-3_5em-sm pb-0_5em-xs pb-1_5em-sm bc-grey">
        <div class="container">
            <div class="flex items-center pb-1_2em-sm">
                <h2 class="size-1_1em-xs size-2em-sm f-bold pr-1_5rem c-black">Complexe Locative</h2>
                <div class="pl-1em-sm flex items-center mb-0_1em-sm">
                    <a class="c-orange up size-0_8em f-medium pt-1em-sm" href="">Vezi toate <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                </div>
            </div>
            <div class="pt-4 pb-4 complex-carousel owl-carousel">
                <div class="item">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>Complex rezidential Dragolina</small>
                                        </div>
                                        <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                            Cuza-Voda 13/7
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-3">
                                        <div class="row">
                                            <div class="pl-0-xs pl-1em-sm">
                                                <small>De la</small>
                                                <span class="c-orange size-1_3em pt-0_3em">50 000 $</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>Complex rezidential Dragolina</small>
                                        </div>
                                        <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                            Tolstoy City Residence
                                        </div>
                                    </div>
                                    <div class="f-medium c-black col-3">
                                        <div class="row">
                                            <div class="pl-0-xs pl-1em-sm">
                                                <small>De la</small>
                                                <span class="c-orange size-1_3em pt-0_3em">50 000 $</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>Complex rezidential Dragolina</small>
                                        </div>
                                        <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                            Decebal 70
                                        </div>
                                    </div>

                                    <div class="f-medium c-black col-3">
                                        <div class="row">
                                            <div class="pl-0-xs pl-1em-sm">
                                                <small>De la</small>
                                                <span class="c-orange size-1_3em pt-0_3em">50 000 $</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/imobile-img.png" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>Complex rezidential Dragolina</small>
                                        </div>
                                        <div class="f-medium c-black size-1_3em pt-0_3em hover-orange">
                                            Decebal 70
                                        </div>
                                    </div>

                                    <div class="f-medium c-black col-3">
                                        <div class="row">
                                            <div class="pl-0-xs pl-1em-sm">
                                                <small>De la</small>
                                                <span class="c-orange size-1_3em pt-0_3em">50 000 $</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-2em-xs pt-4_5em-sm pb-0 pb-4em-sm home-services">
        <div class="container">
            <div class="row">
                <div class="d-flex flex-column col-12 col-lg-4">
                    <div class="pr-7em-md text-center-xs">
                        <h2 class="heading-title-1 f-bold pb-0_8em pr-2em-md">Servicii imobiliare</h2>
                        <div class="heading-text">
                            <p class="f-medium c-black size-1_1em">Oferim servicii profesioniste de vânzare-cumpărare, chirie a
                                bunurilor imobiliare: apartamente, case, oficii, spații comerciale, terenuri în Chișinău și
                                suburbiile acestuia.
                            </p>
                            <p class="size-0_9em lh-1_6em"> Suntem deschiși spre o colaborare reciproc avantajoasă. Avem 0% comision pentru cumpărători.</p>
                        </div>
                        <a href="#" class="btn btn-flat btn-contact text-uppercase btn-orange display-none-xs display-inline-block-sm">Contact us</a>
                    </div>
                </div>
                <div class="col-12 col-lg-8">
                    <div class="row home-services-items">
                        <div class="col-sm col-md-6 col-lg-6">
                            <a class="service-item service-item-4" href="#">
                                <div class="icon">
                                    <div class="icon-o">
                                        <span class="fai fai-home"></span>
                                    </div>
                                </div>
                                <div class="details pl-2em-xs pl-1em-sm">
                                    <div class="lh-1_2em-xs lh-1_1em-sm pt-0_5em-sm size-1_2em-xs size-1_4em-sm f-medium c-black hover-orange">
                                        Vânzare-cumpărare<br> bunuri imobiliare
                                    </div>
                                    <div class="intro pr-3em-md">
                                        Suntem disponibili să vă oferim asistență profesionistă la oricare din aceste etape: negociere,
                                        plata în avans, verificarea actelor, organizarea și desfășurarea tranzacției.
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm col-md-6 col-lg-6">
                            <a class="service-item service-item-4" href="#">
                                <div class="icon">
                                    <div class="icon-o">
                                        <span class="fai fai-calendar"></span>
                                    </div>
                                </div>
                                <div class="details pl-2em-xs pl-1em-sm">
                                    <div class="lh-1_2em-xs lh-1_1em-sm pt-0_5em-sm size-1_2em-xs size-1_4em-sm f-medium c-black hover-orange">
                                        Inchirierea bunuri<br> imobiliare
                                    </div>
                                    <div class="intro pr-3em-md">
                                        Compania „ACCES IMOBIL” oferă servicii de închiriere a apartamentelor, caselor și spațiilor comerciale în Chișinău și în suburbii.                                             </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm col-md-6 col-lg-6">
                            <a class="service-item service-item-4" href="#">
                                <div class="icon">
                                    <div class="icon-o">
                                        <span class="fai fai-shield"></span>
                                    </div>
                                </div>
                                <div class="details pl-2em-xs pl-1em-sm">
                                    <div class="lh-1_2em-xs lh-1_1em-sm pt-0_5em-sm size-1_2em-xs size-1_4em-sm f-medium c-black hover-orange">
                                        Asistenta juridica in domeniul imobiliar
                                    </div>
                                    <div class="intro pr-3em-md">
                                        Fiecare tranzacție imobiliară necesită a fi tratată meticulos și cu seriozitate în vederea evitării anumitor erori sau dificultăți care ar putea apărea din cauza nerespectării legislației în vigoare a RM.                                            </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm col-md-6 col-lg-6 pb-0_5em-xs">
                            <a class="service-item service-item-4" href="#">
                                <div class="icon">
                                    <div class="icon-o">
                                        <span class="fai fai-blocks"></span>
                                    </div>
                                </div>
                                <div class="details pl-2em-xs pl-1em-sm">
                                    <div class="lh-1_2em-xs lh-1_1em-sm pt-0_5em-sm size-1_2em-xs size-1_4em-sm f-medium c-black hover-orange">
                                        Reparatie la cheie a apartamentelor si caselor
                                    </div>
                                    <div class="intro pr-3em-md">
                                        Servicii de reparație la cheie pot fi solicitate doar de clienții Acces Imobil care au procurat un apartament sau casă prin intermediul companiei.                                            </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="w100 flex center pt-1em">
                    <a href="#" class="display-block-xs display-none-sm btn btn-flat btn-contact text-uppercase btn-orange">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-1_5em-xs pt-3_5em-sm bc-grey article-section">
        <div class="container">
            <div class="flex bottom pb-1_5em-xs">
                <h2 class="f-bold pr-1_5rem">Articole Utile</h2>
                <div class="pl-1em flex items-center mb-0_1em">
                    <a class="c-orange up size-0_8em f-medium" href="">Vezi toate <em class="fai fai-right-arrow ml-0_5em hover-move-right"></em></a>
                </div>
            </div>
            <div class="row pt-3em-sm pb-1em-xs pb-1_5em-sm">
                <div class="col-sm col-md-6 col-lg-4">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/article-image-1.jpg" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>19 Ianuarie 2018</small>
                                        </div>
                                        <div class="lh-1_1em-xs f-medium c-black size-1_5em-xs size-1_3em-sm pt-0_3em pb-0_3em hover-orange">
                                            Complex locativ „Solomon Construct”-<br> Ciocana – de la 500€/m2
                                        </div>
                                        <div class="c-black">
                                            <p>Suntem disponibili să vă oferim asistență profesionistă la oricare din aceste etape..</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="d-none d-sm-block col-sm col-md-6 col-lg-4">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/article-image-1.jpg" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>19 Ianuarie 2018</small>
                                        </div>
                                        <div class="lh-1_1em-xs f-medium c-black size-1_5em-xs size-1_3em-sm pt-0_3em pb-0_3em hover-orange">
                                            Cum să alegi o companie imobiliară<br> profesionistă?
                                        </div>
                                        <div class="c-black">
                                            <p>În societatea noastră există prejudecăți cu referire la serviciile prestate de agențiile și companiile..</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="d-none d-sm-block  col-sm col-md-6 col-lg-4">
                    <div class="card">
                        <a href="">
                            <img class="card-img-top" src="img/article/article-image-1.jpg" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <div class="">
                                            <small>19 Ianuarie 2018</small>
                                        </div>
                                        <div class="lh-1_1em-xs f-medium c-black size-1_5em-xs size-1_3em-sm pt-0_3em pb-0_3em hover-orange">
                                            Cum să alegi o companie imobiliară profesionistă?
                                        </div>
                                        <div class="c-black">
                                            <p>În societatea noastră există prejudecăți cu referire la serviciile prestate de agențiile și companiile..</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $this->load->view(get_theme_view('include/footer')); ?>
</body>
</html>