<!DOCTYPE html>
<html lang="en" style="width:100%;height:100%;margin:0;padding:0;">
<head>
    <meta charset="UTF-8">
</head>

<body style="width:100%;height:100%;margin:0;padding:0;">
<div style="background: #ffffff; width: 100%;">
<table width="620" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0">
    <tr>
        <td style="height: 10px;">
        	<div style="height: 10px; width: 100%;"></div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="620" height="80" border="0" cellpadding="0" cellspacing="0" style="margin: 0;padding: 15px;background: #f4f3ee;border-bottom: 5px solid #ff6600;">
                <tr>
					<td><a href="<?php echo base_url();?>"><img style="vertical-align: top;" src="<?php echo base_url('files/images/hlogoRx60.png');?>" alt="<?php echo $this->lsettings->item('default_title');?>" border="0" height="100%"></a></td>
                    <td style="width: 250px;">
                    	<div style="border: 0px; width: 250px; height: 30px; text-align: right;">
                    		<div style="padding-top: 10px; line-height: 18px; font-size: 12px; font-family: Arial,sans-serif; color: #333333; font-weight: bold;"><?php echo $this->lsettings->item('office_phone');?></div>
                    	</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border:0px; padding: 15px; background: #ffffff;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding: 0; background: #ffffff;">
            	<tr>
                    <td style="">
                        <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                            <tr>
                                <td>
                                    <p style="font: 16px Arial, sans-serif; color: #555555;">Добрый день,</p>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                            <tr>
                                <td>
                                    <?php $this->load->view(get_theme_view('email_templates/'.$email_content));?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    		</table>
        </td>
    </tr>
    <tr>
        <td style="    padding: 15px 0;font-family: Arial,sans-serif;vertical-align: middle;text-align: center;color: #333333;font-size: 11px;line-height: 20px;background: #f4f3ee;">
            &copy; <?php echo $this->lsettings->item('default_title');?> 2010 - <?php echo date('Y');?> 
        </td>
    </tr>
</table>
</div>
</body>
</html>
