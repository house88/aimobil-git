<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{title}}</title>
  </head>
  <body style="margin:0;padding:0;background-color:#f4f3ee;color:#676767;font-size:16px;line-height:26px;font-family:'Segoe UI', 'Helvetica Neue', Arial, sans-serif">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td style="font-size:0;line-height:40px;color:#f4f3ee;background-color:#f4f3ee" width="600"></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="background-color:#ffffff;padding:30px;" width="520">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tbody>
                <tr>
                    <td style="padding:0 10px 10px;">
                        <a href="<?php echo site_url();?>" target="_blank" style="text-decoration:none;">
                            <img src="<?php echo base_url('files/images/email-logo-header.png');?>" alt="" style="border:none;width:140px;height:46px;">
                        </a>
                    </td>
                </tr>
                <tr>
                  <td>
                    <?php $this->load->view(get_theme_view('email_templates/'.$email_content));?>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="background-color:#3e3e3d;padding:40px;font-size:14px;line-height:20px;" width="520">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tbody>
                <tr>
                  <td style="padding:0 0 10px;" colspan="2"><a href="<?php echo base_url('files/images/email-logo-header.png');?>" target="_blank" style="text-decoration:none;"><img src="<?php echo base_url('files/images/email-logo-footer.png');?>" alt="" style="border:none;width:100px;height:33px;"></a></td>
                </tr>
                <tr>
                  <td width="60">Telefon:</td>
                  <td style="color:#ffffff"><?php echo $this->lsettings->item('office_phone');?></td>
                </tr>
                <tr>
                  <td width="60">Adresa:</td>
                  <td style="color:#ffffff"><?php echo $this->lsettings->item('office_address');?></td>
                </tr>
              </tbody>
            </table>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="padding-top:30px;padding-bottom:30px;font-size:14px;line-height:20px;" width="600">Dacă doriți să va dezabonați <a href="{{unsubscribe_link}}" style="color:#676767;text-decoration:underline;">accesați aici</a></td>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </body>
</html>