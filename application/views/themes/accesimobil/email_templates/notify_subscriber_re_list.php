<table width="100%" cellpadding="10" cellspacing="0" border="0">
    <tbody>
        <?php $index = 0;?>
        <?php $is_sclosed_tr = false;?>
        <?php foreach($real_estates as $real_estate){?>
            <?php if($index % 2 == 0){?>
                <tr>
                <?php $is_sclosed_tr = false;?>
            <?php }?>
                <td style="vertical-align:top;width:50%;">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#ffffff;-webkit-box-shadow:0 5px 20px rgba(0, 0, 0, 0.1);box-shadow:0 5px 20px rgba(0, 0, 0, 0.1)">
                        <tbody>
                            <tr>
                                <td style="vertical-align:top;">
                                    <a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate[lang_column('full_url')], $lang));?>" target="_blank" style="color:#676767;text-decoration:none;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="vertical-align:top;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" width="250" style="vertical-align:top;">
                                                            <tr>
                                                                <td width="250" height="180" align="center" style="vertical-align:top;">
                                                                    <img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate['real_estate_photo']);?>" alt="<?php echo image_title($real_estate[lang_column('real_estate_full_title')], 1);?>" width="250" style="border:none;object-fit:cover;display: block; max-width: 100%; max-height:100%;">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="250">
                                                        <table cellpadding="0" cellspacing="0" width="250">
                                                            <tr>
                                                                <?php if(isset($re_properties[$real_estate['id_real_estate']])){?>
                                                                    <?php foreach($re_properties[$real_estate['id_real_estate']] as $re_property){?>
                                                                        <td style="font-size:12px;line-height:20px;text-align:center;padding:5px 0;border-bottom:1px solid #e7e7e8;" width="125"><span style="color:#000000;font-weight:bold;"><?php echo $re_property['property_value'];?> </span><span style="color:#676767;"><?php echo $re_property['unit_name'];?></span></td>
                                                                    <?php }?>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" height="70" cellpadding="10" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="color:#000000;font-weight:bold;font-size:14px;line-height:20px;" valign="top">
                                                                        <?php $title = explode('</br>', $real_estate[lang_column('real_estate_title')]);?>
                                                                        <?php foreach($title as $title_part){?>
                                                                            <p style="margin:0;"><?php echo $title_part;?></p>
                                                                        <?php }?>
                                                                        <?php //echo $real_estate[lang_column('real_estate_title')];?>
                                                                    </td>
                                                                    <td width="80" valign="top">
                                                                        <?php $_price = getPrice($real_estate);?>
                                                                        <?php $_credit = getCredit($real_estate);?>
                                                                        <p style="color:#ff6600;font-weight:bold;font-size:14px;line-height:20px;margin:0"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></p>
                                                                        <?php if($_credit){?>
                                                                            <p style="color:#a1a1a5;font-size:12px;line-height:15px;margin:0"><?php lang_line('label_mortgage_text');?><br><?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?></p>
                                                                        <?php }?>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            <?php if($index % 2 != 0){?>
                </tr>
                <?php $is_sclosed_tr = true;?>
            <?php }?>
            <?php $index++;?>
        <?php }?>
        <?php if(!$is_sclosed_tr){?>
                <td style="vertical-align:top;width:50%;"></td>
            </tr>
        <?php }?>
    </tbody>
</table>