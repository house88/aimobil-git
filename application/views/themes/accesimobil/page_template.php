<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang();?>">
<head>
    <?php $this->load->view(get_theme_view('include/head'));?>
</head>
<body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WZHRNWC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div class="wrapper main <?php if(@$remove_wrapper_bg){echo 'wrapper-no-bg';}?>">
        <?php $this->load->view(get_theme_view('include/header')); ?>
    
        <div class="content-wr">
            <?php $this->load->view(get_theme_view($main_content)); ?>
        </div>
    </div>

    <?php $this->load->view(get_theme_view('include/footer')); ?>
</body>
</html>