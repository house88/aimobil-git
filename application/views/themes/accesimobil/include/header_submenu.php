
<div class="submenu-parent">
    <div class="w100 flex-1em">
        <?php foreach($submenu_categories as $submenu_category){?>
            <div class="col-md-3">
                <ul class="submenu">
                    <li class="f-medium c-black pb-0_3em">
                        <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')], $lang));?>">
                            <em class="<?php echo $submenu_category['category_icon'];?>"></em>
                            <?php echo $submenu_category[lang_column('category_title')];?>
                        </a>
                    </li>
                    <?php $category_menu = json_decode($submenu_category['category_menu'], true);?>
                    <?php if(!empty($category_menu)){?>
                        <?php foreach($category_menu as $category_menu_item){?>
                            <li class="<?php echo set_menu_active($this->input->get('filters'), str_replace('?filters=', '', $category_menu_item['url']), 'current-page-item');?>"><a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')] . $category_menu_item['url'], $lang));?>"><?php echo $category_menu_item[lang_column('name')];?></a></li>
                        <?php }?>
                    <?php }?>
                </ul>
            </div>
        <?php }?>
    </div>
</div>