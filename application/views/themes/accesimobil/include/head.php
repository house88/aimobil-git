<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, shrink-to-fit=no, user-scalable=no">
<meta name="author" content="Cravciuc Andrei - Senior PHP Developer">
<base href="<?php echo base_url();?>">

<title><?php echo ((isset($stitle))?clean_output($stitle):clean_output($settings['default_title'][lang_column('setting_value')]));?></title>
<meta name="keywords" content="<?php echo ((isset($skeywords))?clean_output($skeywords):clean_output($settings['mk'][lang_column('setting_value')]));?>">
<meta name="description" content="<?php echo ((isset($sdescription))?clean_output($sdescription):clean_output($settings['md'][lang_column('setting_value')]));?>">

<script type="text/javascript">
    var site_url = '<?php echo site_url();?>';
    var site_lang_url = '<?php echo site_url((!$this->lang->is_default())?$lang.'/':'/');?>';
    var site_lang = '<?php echo $lang;?>';
    var show_calc = false;
    var calc_on_item = false;
    var init_google_maps = false;
    var _init_catalog_filters = false;
    var is_home = false;
    var is_contact = false;
    var maps = [];
    var markers = [];
    function is_mobile_filter(){
        return $(window).width() < 1024;
    }
    <?php if($this->uagent->is_mobile()){?>
        var is_mobile_app = true;
    <?php } else{?>
        var is_mobile_app = false;
    <?php }?>
</script>

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url('files/images/favicon/apple-icon-57x57.png');?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url('files/images/favicon/apple-icon-60x60.png');?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url('files/images/favicon/apple-icon-72x72.png');?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('files/images/favicon/apple-icon-76x76.png');?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url('files/images/favicon/apple-icon-114x114.png');?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url('files/images/favicon/apple-icon-120x120.png');?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url('files/images/favicon/apple-icon-144x144.png');?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url('files/images/favicon/apple-icon-152x152.png');?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url('files/images/favicon/apple-icon-180x180.png');?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url('files/images/favicon/android-icon-192x192.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url('files/images/favicon/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url('files/images/favicon/favicon-96x96.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url('files/images/favicon/favicon-16x16.png');?>">
<link rel="manifest" href="<?php echo site_url('files/images/favicon/manifest.json');?>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" type="image/png" content="<?php echo site_url('files/images/favicon/ms-icon-144x144.png');?>">
<meta name="theme-color" content="#ffffff">

<!-- og_meta -->
<meta property="og:type" content="website" />
<meta property="og:site_name" content="<?php echo @$settings['default_title'][lang_column('setting_value')];?>">
<meta property="og:title" content="<?php echo clean_output(@$og_meta[lang_column('page_title')]);?>" />
<meta property="og:description" content="<?php echo clean_output(@$og_meta[lang_column('page_md')]);?>" />
<meta property="og:url" content="<?php echo current_url();?>" />
<meta property="fb:app_id" content="<?php echo $this->lsettings->item('facebook_app_id');?>" />

<?php if(!empty($og_meta['page_image'])){?>
    <meta property="og:image" content="<?php echo $og_meta['page_image']['src'];?>" />
    <meta property="og:image:width" content="<?php echo $og_meta['page_image']['width'];?>">
    <meta property="og:image:height" content="<?php echo $og_meta['page_image']['height'];?>">
<?php }?>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="<?php echo clean_output(@$og_meta[lang_column('page_title')]);?>">
<meta name="twitter:description" content="<?php echo clean_output(@$og_meta[lang_column('page_md')]);?>">
<meta name="twitter:url" content="<?php echo current_url();?>">
<meta name="twitter:domain" content="<?php echo base_url();?>">
<meta name="twitter:site" content="@<?php echo @$settings['default_title'][lang_column('setting_value')];?>">
<?php if(!empty($og_meta['page_image'])){?>
    <meta name="twitter:image" content="<?php echo $og_meta['page_image']['src'];?>">
<?php }?>

<style>
    body{
        display: none;
    }
</style>

<!-- og_meta END -->
<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false){?>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WZHRNWC');</script>
<?php } ?>