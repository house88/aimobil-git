<footer class="footer-wr pt-2_6em pb-1_5em">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg">
                <div class="footer-container">
                    <div class="footer-container__logo">
                        <a href="<?php echo site_url((!$this->lang->is_default())?$lang.'/':'/');?>"></a>
                    </div>
                    <div class="footer-container__logo-text">
                        <p><?php lang_line('footer_logo_subtext');?></p>
                    </div>
                    <div class="footer-container__copyright display-none-xs display-block-sm">&copy; Copyright 2010 - <?php echo date('Y');?></div>
                </div>
            </div>
            <div class="col-sm-6 col-lg display-none-xs">
                <div class="footer-container footer-container-menu">
                    <div class="header"><?php lang_line('footer_menu_header_real_estate');?></div>
                    <div class="menu-items">
                        <?php $footer_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'sale'));?>
                        <?php if(!empty($footer_categories)){?>
                            <?php foreach($footer_categories as $footer_category){?>
                                <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $footer_category[lang_column('url')], $lang));?>" class="menu-item"><?php echo $footer_category[lang_column('category_title')];?></a>
                            <?php }?>
                        <?php }?>
                        <a href="<?php echo site_url(get_static_uri('discounts', $lang));?>" class="menu-item"><?php lang_line('footer_menu_discounts_title');?></a>                            
                        <a href="<?php echo site_url(get_static_uri('credit-ipotecar', $lang));?>" class="menu-item"><?php lang_line('footer_menu_credit_title');?></a>                            
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg display-none-xs">
                <div class="footer-container footer-container-menu">
                    <div class="header"><?php lang_line('footer_menu_header_services');?></div>
                    <div class="menu-items">
                        <?php $footer_services = Modules::run('services/_get_services');?>
                        <?php if(!empty($footer_services)){?>
                            <?php foreach($footer_services as $footer_service){?>
                                <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $footer_service[lang_column('special_url')], $lang));?>" class="menu-item">
                                    <?php echo $footer_service[lang_column('service_name')];?>
                                </a>
                            <?php }?>
                        <?php }?>
                        <a href="<?php echo site_url(get_static_uri('about_us', $lang));?>" class="menu-item"><?php lang_line('footer_menu_about_us_title');?></a>                            
                        <a href="<?php echo site_url(get_static_uri('blog', $lang));?>" class="menu-item"><?php lang_line('footer_menu_blog_title');?></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg">
                <div class="footer-container footer-container-menu">
                    <div class="header">
                        <?php lang_line('footer_menu_header_contacts');?>
                    </div>
                    <div class="menu-items  footer-contact-links pr-3em-md">
                        <a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="menu-item">
                            <i class="fai fai-marker-stroke"></i>
                            <span><?php echo $this->lsettings->item('office_address');?></span>
                        </a>
                        <a href="tel:<?php echo $this->lsettings->item('office_phone');?>" class="menu-item">
                            <i class="fai fai-phone"></i>
                            <span><?php echo $this->lsettings->item('office_phone');?></span>
                        </a>
                        <a href="mailto:<?php echo $this->lsettings->item('office_support_email');?>" class="menu-item">
                            <i class="fai fai-envelope"></i>
                            <span><?php echo $this->lsettings->item('office_support_email');?></span>
                        </a>
                    </div>
                    <div class="social-items">
                        <a href="<?php echo $this->lsettings->item('facebook_page');?>" class="social-item fai fai-facebook" target="_blank"></a>
                        <a href="<?php echo $this->lsettings->item('instagram_page');?>" class="social-item fai fai-instagram" target="_blank"></a>
                        <a href="<?php echo $this->lsettings->item('youtube_page');?>" class="social-item fai fai-youtube" target="_blank"></a>
                        <a href="<?php echo $this->lsettings->item('twitter_page');?>" class="social-item fai fai-twitter" target="_blank"></a>
                    </div>
                    <div class="header display-block-xs display-none-sm">
                        &copy; Copyright 2010 - <?php echo date('Y');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<a class="scroll-top" id="toTop" href="#"><em class="fai fai-up-arrow-o"></em></a>
<div class="system-text-messages-b" style="<?php if(empty($system_messages)){echo 'display: none;';}?>">	
    <ul>
        <?php if(!empty($system_messages)){echo $system_messages;}?>
    </ul>
</div>

<div class="remodal general-popup-wr" data-remodal-id="general-popup" tabindex="-1">
	<div class="modal-form">
		<button data-remodal-action="cancel" class="remodal-close">
            <em class="fai fai-x-o"></em>
        </button>
		<div class="modal-content-wr"></div>
	</div>
</div>

<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false){?>
    <link rel="stylesheet" href="<?php echo site_url('theme/access-imobil/css/style.css');?>">
    <link rel="stylesheet" href="<?php echo site_url('theme/access-imobil/css/fonts.css?v=1.2');?>">

    <script src="<?php echo site_url('theme/access-imobil/js/all-scripts-min.js');?>"></script>

    <script type="text/javascript">
        $(function(){
            $('body').show();
        });
    </script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-103134045-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-103134045-1');
    </script>

    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                facebook: "545112578902960", // Facebook page ID
                whatsapp: "+37378888900", // WhatsApp number
                viber: "+37378888900", // Viber number
                call: "+37378888900", // Call phone number
                company_logo_url: "//storage.whatshelp.io/widget/7f/7fd5/7fd5812fc12b15b2748b4b8b83294caa/26229782_1534050116675863_6006207589261837752_n.jpg", // URL of company logo (png, jpg, gif)
                greeting_message: "Salut! Te putem ajuta să cumperi sau să vinzi un bun imobiliar. Contactează-ne!", // Text of greeting message
                call_to_action: "Bună ziua!", // Call to action
                button_color: "#FF6600", // Color of button
                position: "right", // Position may be 'right' or 'left'
                order: "facebook,whatsapp,viber,call", // Order of buttons
                ga: true, // Google Analytics enabled
                branding: true, // Show branding string
                mobile: true, // Mobile version enabled
                desktop: true, // Desktop version enabled
                greeting: true, // Greeting message enabled
                shift_vertical: 0, // Vertical position, px
                shift_horizontal: 0, // Horizontal position, px
                domain: "accesimobil.md", // site domain
                key: "rIrs3d1mSZa9kYuziYmwVw", // pro-widget key
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->
<?php } else{?>
    <script src="<?php echo site_url('theme/access-imobil/js/loadCss.js');?>"></script>
    <script>
        loadCSS( "<?php echo site_url('theme/access-imobil/css/style.css');?>" );
        loadCSS( "<?php echo site_url('theme/access-imobil/css/fonts.css?v=1.1');?>" );
    </script>
<?php } ?>