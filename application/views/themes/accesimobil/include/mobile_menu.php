<ul class="top_mobile_menu ">
    <?php $submenu_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'sale'));?>
    <?php if(!empty($submenu_categories)){?>
        <li class="menu-item-has-children f-bold">
            <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_categories[0][lang_column('url')], $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'catalog/sale');?>"><?php lang_line('header_menu_selling_title');?></a>
            <ul class="sub-menu">
                <?php foreach($submenu_categories as $submenu_category){?>
                    <li>
                        <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')], $lang));?>">
                            <div class="pr-2">
                                <em class="<?php echo $submenu_category['category_icon'];?> c-orange"></em>
                            </div>
                            <?php echo $submenu_category[lang_column('category_title')];?>
                        </a>
                    </li>
                <?php }?>
            </ul>
        </li>
    <?php }?>
    <?php $submenu_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'rent'));?>
    <?php if(!empty($submenu_categories)){?>
        <li class="menu-item-has-children f-bold">
            <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_categories[0][lang_column('url')], $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'catalog/rent');?>"><?php lang_line('header_menu_rent_title');?></a>
            <ul class="sub-menu">
                <?php foreach($submenu_categories as $submenu_category){?>
                    <li>
                        <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')], $lang));?>">
                            <div class="pr-2">
                                <em class="<?php echo $submenu_category['category_icon'];?> c-orange"></em>
                            </div>
                            <?php echo $submenu_category[lang_column('category_title')];?>
                        </a>
                    </li>
                <?php }?>
            </ul>
        </li>
    <?php }?>
    <li class="f-bold"><a href="<?php echo site_url(get_static_uri('residential-complexes', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'residential_complexes');?>"><?php lang_line('header_menu_residential_complexes_title');?></a></li>
    <li class="f-bold"><a href="<?php echo site_url(get_static_uri('discounts', $lang))?>" class="<?php echo set_menu_active(@$active_menu, 'discounts');?>"><?php lang_line('header_menu_discounts_title');?></a></li>
    <li class="menu-item-has-children">
        <a href="<?php echo site_url(get_static_uri('services', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'services');?>"><?php lang_line('header_menu_services_title');?></a>
        <?php $menu_services = Modules::run('services/_get_services');?>
        <?php if(!empty($menu_services)){?>
            <ul class="sub-menu">
                <?php foreach($menu_services as $menu_service){?>
                    <li>
                        <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $menu_service[lang_column('special_url')], $lang));?>">
                            <?php echo $menu_service[lang_column('service_name')];?>
                        </a>
                    </li>
                <?php }?>
            </ul>
        <?php }?>
    </li>
    <li>
        <a href="<?php echo site_url(get_static_uri('add_ad', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'add_ad');?>">
            <?php echo lang_line('header_menu_top_add_ad');?>
        </a>
    </li>
    <li>
        <a href="<?php echo site_url(get_static_uri('credit-ipotecar', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'credit-ipotecar');?>">
            <?php echo lang_line('header_menu_top_credit');?>
        </a>
    </li>
    <li>
        <a href="<?php echo site_url(get_static_uri('planing', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'planing');?>">
            <?php echo lang_line('header_menu_top_planing_apartments');?>
        </a>
    </li>
    <li class="menu-item-has-children">
        <a href="<?php echo site_url(get_static_uri('about_us', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'about_us');?>"><?php lang_line('header_menu_about_us_title');?></a>
        <ul class="sub-menu">
            <li><a href="<?php echo site_url(get_static_uri('vacancies', $lang));?>"><?php lang_line('header_menu_submenu_vacancies_title');?></a></li>
            <li><a href="<?php echo site_url(get_static_uri('partners', $lang));?>"><?php lang_line('header_menu_submenu_parteners_title');?></a></li>
        </ul>
    </li>
    <li><a href="<?php echo site_url(get_static_uri('blog', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'blog');?>"><?php lang_line('header_menu_blog_title');?></a></li>
    <li><a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="<?php echo set_menu_active(@$active_menu, 'contacts');?>"><?php lang_line('header_menu_contacts_title');?></a></li>
</ul>