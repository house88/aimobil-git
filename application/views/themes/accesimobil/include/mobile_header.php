<div id="mobile-header" class="mobile-nav">
    <div class="mobile-head">
        <div class="container">
            <div class="mobile-head-inner h100">
                <a href="tel:<?php echo $this->lsettings->item('office_phone');?>" class="header-nav__menu-top-right-item icon">
                    <em class="fai fai-phone"></em>
                </a>
                <a class="search-open header-nav__menu-top-right-item icon">
                    <em class="fai fai-search"></em>
                </a>
                <a class="header-nav__menu-top-right-item icon" href="<?php echo site_url(get_static_uri('contacts', $lang));?>">
                    <em class="fai fai-marker-stroke"></em>
                </a>
                <a class="header-nav__menu-top-right-item icon" href="<?php echo site_url(get_static_uri('favorite', $lang));?>">
                    <em class="fai fai-like"></em>
                </a>
                <div class="dropdown dropdown-langs">
                    <?php $dropdown_languages = $this->config->item('supported_languages');?>
                    <span class="dropdown-toggle" id="dropdownMenuButtonMobileLang" data-toggle="dropdown">
                        <?php echo $dropdown_languages[$lang]['name'];?> 
                        <i class="fai fai-arrow-down"></i>
                    </span>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonMobileLang">
                        <?php unset($dropdown_languages[$lang])?>
                        <?php foreach($dropdown_languages as $lang_key => $dropdown_language){?>
                            <a class="dropdown-item" href="<?php echo $this->lang->switch_uri($lang_key, (!empty($langs_uri))?$langs_uri[$lang_key]:'');?>"><?php echo $dropdown_language['name'];?></a>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="w100 mobile-search-parent">
                <form class="mobile-form" autocomplete="off" action="<?php echo site_url(get_static_uri('catalog/search', $lang));?>" method="get">
                    <label for="search-input-mobile-menu" style="display: none"><?php lang_line('label_search');?></label>
                    <input id="search-input-mobile-menu" autocomplete="off" value="<?php echo @$get_keywords;?>" name="q" type="search" placeholder="<?php lang_line('label_search');?>" class=" form-control">
                    <button type="submit" class="mobile-search-button"><span class="fai fai-search"></span></button>
                </form>
                <a href="" class="mobile-search-close fai fai-x-o"></a>
            </div>
        </div>
    </div>
    <div class="mobile-second">
        <div class="menu_toggle">
            <div class="mobile-button-title">
                menu
            </div>
            <button></button>
        </div>
        <div class="header-nav__logo__parent">
            <div class="header-nav__logo">
                <a href="<?php echo site_url((!$this->lang->is_default())?$lang.'/':'/');?>"></a>
            </div>
        </div>
        <div class="filter_toggle">
            <div class="mobile-button-title">filtru</div>
            
            <div class="mobile-button-icon"><em class="fai fai-filter"></em></div>
        </div>
    </div>
    <div class="mobile_menu">
        <?php $this->load->view(get_theme_view('include/mobile_menu')); ?>
    </div>
</div>