<div class="fixed-menu hide-fixed-menu">
    <div class="w100 parent-fixed-block">
        <div class="container">
            <div class="flex-1em">
                <div class="col-12 col-xl-8 inner-fixed-block">

                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($active_filters)){?>
        <?php echo $active_filters;?>
    <?php }?>
</div>
<header id="masthead" class="header-wr d-flex">
    <div class="container">
        <div class="header-nav w100 d-flex">
            <div class="header-nav__logo__parent">
                <div class="header-nav__logo">
                    <a href="<?php echo site_url((!$this->lang->is_default())?$lang:'');?>"></a>
                </div>
            </div>
            <div class="header-nav__menu">
                <div class="header-nav__menu-top">
                    <div class="header-nav__menu-top-left">
                        <a class="header-nav__menu-top-left-item icon" href="#"><i class="fai fai-home"></i></a>
                        <a href="<?php echo site_url(get_static_uri('add_ad', $lang));?>" class="header-nav__menu-top-left-item">
                            <?php echo lang_line('header_menu_top_add_ad');?>
                        </a>
                        <a href="<?php echo site_url(get_static_uri('credit-ipotecar', $lang));?>" class="header-nav__menu-top-left-item">
                            <?php echo lang_line('header_menu_top_credit');?>
                        </a>
                        <a href="<?php echo site_url(get_static_uri('planing', $lang));?>" class="header-nav__menu-top-left-item">
                            <?php echo lang_line('header_menu_top_planing_apartments');?>
                        </a>
                    </div>
                    <div class="header-nav__menu-top-right d-flex">
                        <form role="form" autocomplete="off" action="<?php echo site_url(get_static_uri('catalog/search', $lang));?>">
                            <input class="form-control form-control-sm" autocomplete="off" value="<?php echo @$get_keywords;?>" name="q" type="text" placeholder="<?php lang_line('label_search');?>">
                            <button type="submit" class="btn search-button-single"><span class="fai fai-search"></span></button>
                        </form>
                        <a class="header-nav__menu-top-right-item icon" href="<?php echo site_url(get_static_uri('contacts', $lang));?>">
                            <i class="fai fai-marker-stroke"></i>
                        </a>
                        <a class="header-nav__menu-top-right-item icon" href="<?php echo site_url(get_static_uri('favorite', $lang));?>">
                            <i class="fai fai-like"></i>
                        </a>
                        <div class="dropdown dropdown-langs">
                            <?php 
                                $dropdown_languages = $this->config->item('supported_languages');
                            ?>
                            <span class="dropdown-toggle" id="dropdownMenuButtonMobileLang" data-toggle="dropdown">
                                <?php echo $dropdown_languages[$lang]['name'];?> 
                                <i class="fai fai-arrow-down"></i>
                            </span>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonMobileLang">
                                <?php unset($dropdown_languages[$lang])?>
                                <?php foreach($dropdown_languages as $lang_key => $dropdown_language){?>
                                    <a class="dropdown-item" href="<?php echo $this->lang->switch_uri($lang_key, (!empty($langs_uri))?$langs_uri[$lang_key]:'');?>"><?php echo $dropdown_language['name'];?></a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-nav__menu-bottom flex items-center space-between">
                    <ul class="header-nav__menu-bottom-left">
                        <?php $submenu_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'sale'));?>
                        <?php if(!empty($submenu_categories)){?>
                            <?php $real_estate_best = Modules::run('real_estate/_get_best', array('real_estate_type' => 'sale'));?>
                            <li class="menu-item-has-children">
                                <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_categories[0][lang_column('url')], $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'catalog/sale');?>"><?php lang_line('header_menu_selling_title');?></a>
                                <?php if(!empty($real_estate_best)){?>
                                    <?php $this->load->view(get_theme_view('include/header_submenu_best'), array('submenu_categories' => $submenu_categories, 'real_estate_best' => $real_estate_best)); ?>
                                <?php } else{?>
                                    <?php $this->load->view(get_theme_view('include/header_submenu'), array('submenu_categories' => $submenu_categories, 'real_estate_best' => $real_estate_best)); ?>
                                <?php }?>
                            </li>
                        <?php }?>
                        <?php $submenu_categories = Modules::run('real_estate/_get_categories', array('category_active' => 1, 'category_deleted' => 0, 'category_type' => 'rent'));?>
                        <?php if(!empty($submenu_categories)){?>
                            <?php $real_estate_best = Modules::run('real_estate/_get_best', array('real_estate_type' => 'rent'));?>
                            <li class="menu-item-has-children">
                                <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_categories[0][lang_column('url')], $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'catalog/rent');?>"><?php lang_line('header_menu_rent_title');?></a>
                                <?php if(!empty($real_estate_best)){?>
                                    <?php $this->load->view(get_theme_view('include/header_submenu_best'), array('submenu_categories' => $submenu_categories, 'real_estate_best' => $real_estate_best)); ?>
                                <?php } else{?>
                                    <?php $this->load->view(get_theme_view('include/header_submenu'), array('submenu_categories' => $submenu_categories, 'real_estate_best' => $real_estate_best)); ?>
                                <?php }?>
                            </li>
                        <?php }?>
                        <li><a href="<?php echo site_url(get_static_uri('residential-complexes', $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'residential_complexes');?>"><?php lang_line('header_menu_residential_complexes_title');?></a></li>
                        <li><a href="<?php echo site_url(get_static_uri('discounts', $lang))?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'discounts');?>"><?php lang_line('header_menu_discounts_title');?></a></li>
                        <li class="menu-item-has-children small-children">
                            <a href="<?php echo site_url(get_static_uri('services', $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'services');?>"><?php lang_line('header_menu_services_title');?></a>
                            <ul class="small-submenu">
                                <?php $menu_services = Modules::run('services/_get_services');?>
                                <?php if(!empty($menu_services)){?>
                                    <?php foreach($menu_services as $menu_service){?>
                                        <li>
                                            <a href="<?php echo site_url(get_dinamyc_uri('services/detail/id', $menu_service[lang_column('special_url')], $lang));?>">
                                                <?php echo $menu_service[lang_column('service_name')];?>
                                            </a>
                                        </li>
                                    <?php }?>
                                <?php }?>
                            </ul>
                        </li>
                        <li class="menu-item-has-children small-children">
                            <a href="<?php echo site_url(get_static_uri('about_us', $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'about_us');?>"><?php lang_line('header_menu_about_us_title');?></a>
                            <ul class="small-submenu">
                                <li><a href="<?php echo site_url(get_static_uri('vacancies', $lang));?>"><?php lang_line('header_menu_submenu_vacancies_title');?></a></li>
                                <li><a href="<?php echo site_url(get_static_uri('partners', $lang));?>"><?php lang_line('header_menu_submenu_parteners_title');?></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url(get_static_uri('blog', $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'blog');?>"><?php lang_line('header_menu_blog_title');?></a></li>
                        <li><a href="<?php echo site_url(get_static_uri('contacts', $lang));?>" class="header-nav__menu-bottom-left-item <?php echo set_menu_active(@$active_menu, 'contacts');?>"><?php lang_line('header_menu_contacts_title');?></a></li>
                    </ul>
                    <div class="header-nav__menu-bottom-right flex items-center">
                        <div class="shrink-0 lh-0_8em pr-0_5em">
                            <em class="fai fai-phone c-orange"></em>
                        </div>

                        <a href="tel:<?php echo $this->lsettings->item('office_phone');?>" class="header-nav__menu-bottom-right-item phone"><?php echo $this->lsettings->item('office_phone');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php $this->load->view(get_theme_view('include/mobile_header')); ?>