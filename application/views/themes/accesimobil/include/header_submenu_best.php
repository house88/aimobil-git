
<div class="submenu-parent-block has-post-block p-2em pl-5em-lg">
    <div class="w100 flex">
        <div class="col-md-9">
            <div class="flex pt-1em">
                <?php foreach($submenu_categories as $submenu_category){?>
                    <div class="col-md-3">
                        <ul class="submenu">
                            <li class="f-medium c-black pb-0_3em">
                                <a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')], $lang));?>">
                                    <em class="<?php echo $submenu_category['category_icon'];?>"></em>
                                    <?php echo $submenu_category[lang_column('category_title')];?>
                                </a>
                            </li>
                            <?php $category_menu = json_decode($submenu_category['category_menu'], true);?>
                            <?php if(!empty($category_menu)){?>
                                <?php foreach($category_menu as $category_menu_item){?>
                                    <li class="<?php echo set_menu_active($this->input->get('filters'), str_replace('?filters=', '', $category_menu_item['url']), 'current-page-item');?>"><a href="<?php echo site_url(get_dinamyc_uri('catalog/type/category', $submenu_category[lang_column('url')] . $category_menu_item['url'], $lang));?>"><?php echo $category_menu_item[lang_column('name')];?></a></li>
                                <?php }?>
                            <?php }?>
                        </ul>
                    </div>
                <?php }?>
            </div>
        </div>
        <?php if(!empty($real_estate_best)){?>
            <?php $_price = getPrice($real_estate_best);?>
            <div class="col-md-3">
                <div class="card box-shadow">
                    <?php if($real_estate_best['real_estate_discount_type'] != 'no'){?>
                        <div class="discount-badge">
                            <div class="badge-title">%</div>
                            <?php if($_price['display_discount_text'] != ''){?>
                                <div class="badge-content"><?php echo $_price['display_discount_text'];?></div>
                            <?php }?>
                        </div>
                    <?php }?>
                    <a class="grid-item h-auto mh-180 <?php if($real_estate_best['real_estate_sold'] == 1){?>sold<?php } elseif($real_estate_best['real_estate_reserved'] == 1){?>reserved<?php }?>" href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate_best[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
                        <img class="card-img-top" src="<?php echo site_url('files/real_estate/'.$real_estate_best['id_real_estate'].'/thumb_'.$real_estate_best['real_estate_photo']);?>" <?php echo img_srcset('files/real_estate/'.$real_estate_best['id_real_estate'], $real_estate_best['real_estate_photo']);?> alt="<?php echo image_title($real_estate_best[lang_column('real_estate_full_title')], 1);?>"  title="<?php echo image_title($real_estate_best[lang_column('real_estate_full_title')], 1);?>">
                        <?php if($real_estate_best['real_estate_sold'] == 1){?>
                            <div class="sold-title"><?php lang_line('label_sold');?></div>
                        <?php } elseif($real_estate_best['real_estate_reserved'] == 1){?>
                            <div class="reserved-title"><?php lang_line('label_reserved');?></div>
                        <?php }?>
                    </a>
                    <?php if(!empty($real_estate_best['re_properties'])){?>
                        <div class="card-header d-flex justify-content-between align-items-center c-black br-bottom-1-grey">
                            <?php foreach($real_estate_best['re_properties'] as $re_best_property){?>
                                <div>
                                    <?php echo $re_best_property['property_value'];?> 
                                    <small class="text-muted"><?php echo $re_best_property['unit_name'];?></small>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>
                    <div class="card-body">
                        <a href="<?php echo site_url(get_dinamyc_uri('catalog/category/real_estate', $real_estate_best[lang_column('full_url')], $lang));?>" target="_blank" data-tblank>
                            <div class="d-flex justify-content-between">
                                <div class="f-medium c-black">
                                    <?php echo $real_estate_best[lang_column('real_estate_title')];?>
                                </div>
                                <div class="f-medium c-black col-4">
                                    <div class="row">
                                        <div class="pl-0_3em">
                                            <span class="c-orange item-price"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
                                            <?php $_credit = getCredit($real_estate_best);?>
                                            <?php if($_credit){?>
                                                <div class="c-mid-grey size-0_9em"><?php lang_line('label_mortgage_text');?></div>
                                                <div class="c-mid-grey size-0_9em"><?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?></div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
</div>