<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang();?>">
<head>
    <?php $this->load->view(get_theme_view('include/head'));?>
</head>
<body <?php if(isset($has_paralax)){echo 'class="page-home"';}?>>
    <div class="wrapper">
        <?php $this->load->view(get_theme_view('include/header_paralax')); ?>
        <div class="content-wr">
            <?php $this->load->view(get_theme_view($main_content)); ?>
        </div>
    </div>
    <?php $this->load->view(get_theme_view('include/footer')); ?>
</body>
</html>