<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo base_url();?>">
                                <i class="fa fa-home"></i> Перейти на сайт
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/users/edit_user');?>">
                                <i class="fa fa-lock"></i> <span>Личные данные</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('signout');?>">
                                <i class="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>        
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php $dashboard_menu_items = Modules::run('menu/_get_dashboard_menu');?>
        <ul class="sidebar-menu" id="side-menu">
            <?php foreach($dashboard_menu_items as $item){?>
                <?php if($this->lauth->have_right_or($item->rights)){?>                    
                    <li class="<?php if(!empty($item->submenu)){?>treeview<?php }?>">
                        <a href="<?php echo site_url($item->url); ?>">
                            <?php echo $item->name; ?>
                            <?php if(!empty($item->submenu)){?>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            <?php }?>
                        </a>
						<?php if(!empty($item->submenu)){?>
                            <ul class="treeview-menu">
                                <?php foreach($item->submenu as $item_submenu){?>
                                    <?php if($this->lauth->have_right_or($item_submenu['rights'])){?>
                                        <li>
                                            <a href="<?php echo site_url($item_submenu['url']);?>">
                                                <?php echo $item_submenu['name']; ?>
                                            </a>
                                        </li>
                                    <?php }?>
                                <?php }?>
                            </ul>
                        <?php }?>
                    </li>				
				<?php }?>
			<?php }?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>