<?php $_price = getPrice($real_estate);?>
<?php $_credit = getCredit($real_estate);?>
<head>
	<title><?php echo $real_estate[lang_column('real_estate_full_title')];?></title>
	<style>
		h1, h2, h3, h4, h5{
			color: #000000 !important;
			font-weight: normal;
		}
	</style>
</head>
<body>
    <div style="width:794px;padding:0 50px;margin:0 auto;">
        <table style="width: 100%; border-collapse:collapse; color:#555555;" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="width:50%;">
				   <table style="width: 100%; vertical-align: middle;">
						<tr>
							<td rowspan="2"><img width="auto" height="55" src="<?php echo site_url('theme/access-imobil/css/images/logo.jpg')?>" alt="Acces Imobil"/></td>
						</tr>
					</table>
				</td>
				<td style="width:50%;">
					<table style="width: 100%; vertical-align: middle;text-align:right;">
						<tr>
							<td style="font: 14px/18x Arial,sans-serif; color: #676767;text-align:right;">
								<p style="margin-bottom:5px;"><?php echo $this->lsettings->item('office_address');?></p>
								<p style="margin-bottom:5px;"><?php echo $this->lsettings->item('office_phone');?></p>
								<p style="margin-bottom:5px;">www.accesimobil.md</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width:50%;vertical-align: top;">
					<table style="width: 100%; vertical-align: top;margin-top:10px;">
						<tr>
							<td>
								<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/thumb_'.$real_estate['real_estate_photo']);?>" style="width:350px;height:auto;" alt="dsfsdfsdf">
							</td>
						</tr>
					</table>
				</td>
				<td style="width:50%;padding-left:10px;vertical-align: top;">
					<table style="width: 100%; height:100%;margin-top:10px;">
						<tr>
							<td style="vertical-align: top;">
								<div><strong style="font: normal 16px/22px Arial,sans-serif; color: #676767;">ID <?php echo orderNumberOnly($real_estate['id_real_estate']);?></strong></div>
								<div><strong style="font: normal 22px/26px Arial,sans-serif; color: #000000;"><?php echo $real_estate[lang_column('real_estate_full_title')];?></strong></div>
							</td>
						</tr>
						<tr>
							<td style="vertical-align: top;padding-top:10px;">
								<div>
									<span style="font-weight: bold;color: #ff6600;font-size: 20px;line-height:24px;vertical-align: middle;"><?php echo $_price['display_price'];?> <?php echo $_price['currency_symbol'];?></span>
									<span style="font-weight: normal;color: #676767;font-size: 16px;line-height:24px;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if($_credit){?><?php lang_line('label_mortgage_text');?> <?php echo $_credit['display_amount'];?> <?php echo $_credit['currency_symbol'];?> / <?php lang_line('label_month');?><?php }?></span>
								</div>
							</td>
						</tr>
						<tr style="height:100px;">
							<td style="height:100px;padding-top:10px; vertical-align: bottom;">
								<table style="width: 100%; height:100%; vertical-align: top;margin-top:10px;border:1px solid #dddddd;">
									<tr>
										<td style="padding:10px;width:80px;height:80px;">
											<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" width="80">
										</td>
										<td style="padding:10px 0;text-align:left;">
											<table style="width: 100%; height:100%;">
												<tr>
													<td style="height:40px;">
														<p style="font-weight: normal;color: #676767;font-size: 14px;line-height:20px;"><?php echo $re_manager[lang_column('position_name')];?></p>
														<p style="font-weight: normal;color: #676767;font-size: 18px;line-height:20px;"><?php echo $re_manager[lang_column('user_name')];?></p>
													</td>
												</tr>
												<tr style="">
													<td style="font-weight: normal;color: #676767;font-size: 14px;height:20px;padding-top:5px;">
														<table style="width: 100%; height:100%;">
															<tr>
																<td style="width:20px;">
																	<img style="width:16px;height:16px;" align="left" src="<?php echo site_url('theme/access-imobil/pdf/img/phone-icon.png');?>" alt="Phone">
																</td>
																<td style="text-align:left;font-size:16px;">
																	<span><?php echo $re_manager['user_phone'];?></span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<?php if(!empty($real_estate['re_properties'])){?>
			<div style="width:690px;margin-top:30px;">
				<?php $_pindex = 0;?>
				<?php foreach($real_estate['re_properties'] as $re_property){?>
					<div style="width:210px;padding-bottom:15px;margin-bottom:15px;<?php if($_pindex%3 != 0){echo 'margin-left:30px;';}?>float:left;border-bottom:1px solid #d2d2d2;font-size:14px;">
						<span style="color:#676767"><?php echo $re_property['property_name'];?></span><br>
						<strong>
							<?php echo $re_property['property_value'];?>
							<?php if($re_property['show_unit']){?>
								<?php echo $re_property['unit_name'];?>
							<?php }?>
						</strong>
					</div>
					<?php $_pindex++;?>
				<?php }?>
			</div>
		<?php }?>
		<div style="width:690px;margin-top:10px;color:#676767;">
			<?php echo $real_estate[lang_column('real_estate_text')];?>
		</div>
		<?php $re_icons = json_decode($real_estate['real_estate_icons'], true);?>
		<?php if(!empty($re_icons)){?>
			<div style="width:690px;margin-top:0px;">
				<h2><?php lang_line('label_icons');?></h2>
				<div style="width:690px;margin-top:30px;">
					<?php foreach($icons as $icon){?>
						<?php if(array_key_exists($icon['id_icon'], $re_icons)){?>
							<div style="width:170px;float:left;line-height:20px;padding-bottom:15px;color:#676767">
								<img style="width:16px;height:16px;" align="left" src="<?php echo site_url('theme/access-imobil/pdf/img/'.$icon['icon_image']);?>">
								<span><?php echo $icon[lang_column('icon_name')];?></span>
							</div>
						<?php }?>
					<?php }?>
				</div>
			</div>
		<?php }?>
		<?php $real_estate_photos = json_decode($real_estate['real_estate_photos'], true);?>
		<?php if(!empty($real_estate_photos)){?>
			<table cellpadding="0" cellspacing="0" style="width:690px;margin-top:50px;">
				<?php $index = 0;?>
				<?php $is_closed_tr = false;?>
				<?php foreach($real_estate_photos as $real_estate_photo){?>
					<?php if($index % 2 == 0){?>
						<tr>
						<?php $is_closed_tr = false;?>
					<?php }?>
					<td style="width:50%;height:auto;overflow:hidden;vertical-align:middle;padding-top:20px;<?php if($index % 2 == 0){echo 'text-align:left;padding-right:10px;';} else{echo 'text-align:right;padding-left:10px;';}?>">
						<img src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$real_estate_photo);?>" style="width:100%;object-fit:cover;">
					</td>
					<?php if($index % 2 != 0){?>
						</tr>
						<?php $is_sclosed_tr = true;?>
					<?php }?>
					<?php $index++;?>
				<?php }?>
				<?php if(!$is_sclosed_tr){?>
					</tr>
				<?php }?>
			</table>
		<?php }?>
		<table style="width: 100%; height:100%; vertical-align: top;margin-top:50px;border:1px solid #dddddd;">
			<tr>
				<td style="padding:10px;width:60px;height:60px;">
					<img src="<?php echo site_url(getImage('files/users/'.$re_manager['user_photo']));?>" width="60">
				</td>
				<td style="padding:10px 0;text-align:left;vertical-align:middle;">
					<p>
						<span>
							<span style="font-weight: normal;color: #676767;font-size: 14px;line-height:20px;white-space:nowrap;"><?php echo $re_manager[lang_column('position_name')];?></span>
							<br>
							<span style="font-weight: normal;color: #676767;font-size: 18px;line-height:20px;white-space:nowrap;"><?php echo $re_manager[lang_column('user_name')];?></span>
						</span>
						<span>
							<img style="margin-left:30px;width:16px;height:16px;" align="left" src="<?php echo site_url('theme/access-imobil/pdf/img/phone-icon.png');?>" alt="Phone">
							<span style="text-align:left;font-size:18px;"><?php echo $re_manager['user_phone'];?></span>
						</span>
					</p>
				</td>
			</tr>
		</table>
    </div>
</body>