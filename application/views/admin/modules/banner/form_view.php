<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
                <div class="form-group">
                    <label>Фон баннера</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="banner_image" id="banner_image" value="<?php echo $banner['banner_image'];?>">
                        <div class="input-group-btn">
                            <span class="btn btn-default call-function" data-callback="clear_image" data-clear="#banner_image"><i class="fa fa-times"></i></span>
                            <span class="btn btn-default call-function" data-callback="preview_image" data-image="#banner_image"><i class="fa fa-eye"></i></span>
                            <span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=banner_image&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
                                <i class="fa fa-folder-open"></i> Добавить фото
                            </span>
                        </div>
                    </div>
                </div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Ссылка</label>
								<div class="input-group">
									<span class="input-group-addon"><?php echo site_url();?></span>
									<input class="form-control" placeholder="Ссылка" name="banner_link_ro" value="<?php echo $banner['banner_link_ro'];?>">
								</div>
							</div>
							<div class="form-group">
								<label>Атрибут title</label>
								<input class="form-control" placeholder="Атрибут title" name="banner_title_ro" value="<?php echo clean_output($banner['banner_title_ro']);?>">
							</div>
							<div class="form-group">
								<label>Атрибут alt</label>
								<input class="form-control" placeholder="Атрибут alt" name="banner_alt_ro" value="<?php echo clean_output($banner['banner_alt_ro']);?>">
							</div>
							<?php if($banner['banner_text_active'] == 1){?>
								<div class="form-group">
									<label>Текст</label>
									<textarea name="banner_text_ro" rows="10" class="form-control"><?php echo $banner['banner_text_ro'];?></textarea>
								</div>
							<?php }?>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Ссылка</label>
								<div class="input-group">
									<span class="input-group-addon"><?php echo site_url('ru/');?></span>
									<input class="form-control" placeholder="Ссылка" name="banner_link_ru" value="<?php echo $banner['banner_link_ru'];?>">
								</div>
							</div>
							<div class="form-group">
								<label>Атрибут title</label>
								<input class="form-control" placeholder="Атрибут title" name="banner_title_ru" value="<?php echo clean_output($banner['banner_title_ru']);?>">
							</div>
							<div class="form-group">
								<label>Атрибут alt</label>
								<input class="form-control" placeholder="Атрибут alt" name="banner_alt_ru" value="<?php echo clean_output($banner['banner_alt_ru']);?>">
							</div>
							<?php if($banner['banner_text_active'] == 1){?>
								<div class="form-group">
									<label>Текст</label>
									<textarea name="banner_text_ru" rows="10" class="form-control"><?php echo $banner['banner_text_ru'];?></textarea>
								</div>
							<?php }?>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Ссылка</label>
								<div class="input-group">
									<span class="input-group-addon"><?php echo site_url('en/');?></span>
									<input class="form-control" placeholder="Ссылка" name="banner_link_en" value="<?php echo $banner['banner_link_en'];?>">
								</div>
							</div>
							<div class="form-group">
								<label>Атрибут title</label>
								<input class="form-control" placeholder="Атрибут title" name="banner_title_en" value="<?php echo clean_output($banner['banner_title_en']);?>">
							</div>
							<div class="form-group">
								<label>Атрибут alt</label>
								<input class="form-control" placeholder="Атрибут alt" name="banner_alt_en" value="<?php echo clean_output($banner['banner_alt_en']);?>">
							</div>
							<?php if($banner['banner_text_active'] == 1){?>
								<div class="form-group">
									<label>Текст</label>
									<textarea name="banner_text_en" rows="10" class="form-control"><?php echo $banner['banner_text_en'];?></textarea>
								</div>
							<?php }?>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="banner" value="<?php echo $banner['id_banner'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>	
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

	var preview_image = function(btn){
		var $this = $(btn);
		var image = $($this.data('image')).val();
		if(image != ''){
			$.fancybox.open([
				{
					src  : base_url+'files/'+image
				}
			]);
		} else{
			systemMessages( 'Ошибка: Нет баннера.', 'error' );
		}
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/banner/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
