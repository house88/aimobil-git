<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
						<li class="">
							<a href="#form_poster" data-toggle="tab" aria-expanded="false">Постер</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ro" value="<?php echo $page['mt_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo $page['mk_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo $page['md_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ru" value="<?php echo $page['mt_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo $page['mk_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo $page['md_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_en" value="<?php echo $page['mt_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo $page['mk_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo $page['md_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_poster">
							<div class="form-group">
								<div class="input-group">
									<input type="text" class="form-control" name="page_poster" id="page_poster" value="<?php echo $page['page_poster'];?>">
									<div class="input-group-btn">
										<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#page_poster"><i class="fa fa-times"></i></span>
										<span class="btn btn-default call-function" data-callback="preview_image" data-image="#page_poster"><i class="fa fa-eye"></i></span>
										<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=page_poster&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
											<i class="fa fa-folder-open"></i> Добавить фото
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="page" value="<?php echo $page['id_page'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
        $('input[name="show_calc"]').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '20%'
		});
	});
	
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

	var preview_image = function(btn){
		var $this = $(btn);
		var image = $($this.data('image')).val();
		if(image != ''){
			$.fancybox.open([
				{
					src  : base_url+'files/'+image
				}
			]);
		} else{
			systemMessages( 'Ошибка: Нет баннера.', 'error' );
		}
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/pages/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
