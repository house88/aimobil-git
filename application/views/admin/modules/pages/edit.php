<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/pages/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить страницу
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_ro" id="sidebar_image_ro" value="<?php echo $page['page_image_ro'];?>">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_ro"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_ro&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo $page['page_name_ro'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Спец. ссылка</label>
								<input class="form-control" placeholder="Спец. ссылка" name="url_ro" value="<?php echo $page['url_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ro"><?php echo $page['page_text_ro'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo $page['mk_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo $page['md_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_ru" id="sidebar_image_ru" value="<?php echo $page['page_image_ru'];?>">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_ru"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_ru&relative_url=1">
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo $page['page_name_ru'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Спец. ссылка</label>
								<input class="form-control" placeholder="Спец. ссылка" name="url_ru" value="<?php echo $page['url_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ru"><?php echo $page['page_text_ru'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo $page['mk_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo $page['md_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_en" id="sidebar_image_en" value="<?php echo $page['page_image_en'];?>">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_en"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_en&relative_url=1">
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo $page['page_name_en'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Спец. ссылка</label>
								<input class="form-control" placeholder="Спец. ссылка" name="url_en" value="<?php echo $page['url_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_en"><?php echo $page['page_text_en'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo $page['mk_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo $page['md_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="show_calc" <?php echo (!empty($page))?set_checkbox('show_calc', '',$page['show_calc'] == 1):''?>>
                        Показать Калькулятор ипотеки
                    </label>
                </div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="page" value="<?php echo $page['id_page'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
        $('input[name="show_calc"]').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '20%'
		});
	});
	
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/pages/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
