<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/vacancies/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить вакансию
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo $vacancy['vacancy_name_ro'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ro"><?php echo $vacancy['vacancy_text_ro'];?></textarea>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo $vacancy['vacancy_name_ru'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ru"><?php echo $vacancy['vacancy_text_ru'];?></textarea>
							</div>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo $vacancy['vacancy_name_en'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_en"><?php echo $vacancy['vacancy_text_en'];?></textarea>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="vacancy" value="<?php echo $vacancy['id_vacancy'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/vacancies/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
