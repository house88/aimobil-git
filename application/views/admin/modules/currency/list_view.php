<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="#" data-href="<?php echo base_url('admin/currency/popup_forms/add');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить валюту
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_name">Название</th>
						<th class="dt_code">ISO Код</th>
						<th class="dt_symbol">Символ</th>
						<th class="dt_rate">Курс</th>
						<th class="dt_default">Базовая</th>
						<th class="dt_actions"></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<script>
	var dtTable;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"sDom": '',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/currency/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_code"], "mData": "dt_code", "bSortable": false},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_symbol"], "mData": "dt_symbol", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_rate"], "mData": "dt_rate", "bSortable": false},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_default"], "mData": "dt_default", "bSortable": false},
				{ "sClass": "w-95 vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						if(data.mess_type == 'info'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
			}

		});
	});

	function manage_currency_callback(){
		dtTable.fnDraw(false);
	}

	var change_currency = function(btn){
		var $this = $(btn);
		var currency = $this.data('currency');
		var action = $this.data('action');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/currency/ajax_operations/change_status/'+action,
			data: {currency:currency},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_currency_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
	}
	var delete_action = function(btn){
		var $this = $(btn);
		var currency = $this.data('currency');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/currency/ajax_operations/delete',
			data: {currency:currency},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_currency_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
