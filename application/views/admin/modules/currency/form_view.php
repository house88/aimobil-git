<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
                <?php if(!empty($currency)){?>Редактировать<?php } else{?>Добавить<?php }?> валюту
            </h4>
        </div>
        <form autocomplete="off">			
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" placeholder="Название" name="title" value="<?php if(!empty($currency)){echo $currency['currency_name'];}?>">
                            <p class="help-block">Не более 30 символов.</p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>ISO Код</label>
                            <input class="form-control" placeholder="ex. USD" name="iso_code" value="<?php if(!empty($currency)){echo $currency['currency_code'];}?>">
                            <p class="help-block">Не более 3-х символов.</p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Символ</label>
                            <input class="form-control" placeholder="ex. $" name="symbol" value="<?php if(!empty($currency)){echo $currency['currency_symbol'];}?>">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Курс</label>
                            <input class="form-control" placeholder="Курс" name="exchange_rate" value="<?php if(!empty($currency)){echo $currency['currency_rate'];}?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($currency)){?>
                    <input type="hidden" name="currency" value="<?php echo $currency['id_currency'];?>">
                <?php }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_currency" data-action="<?php if(!empty($currency)){?>edit<?php } else{?>add<?php }?>">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var manage_currency = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/currency/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_currency_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>