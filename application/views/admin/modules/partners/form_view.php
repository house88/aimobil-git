<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($partner)){echo 'Редактирование';} else{echo 'Добавление';}?> партнера</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
				<div class="form-group">
					<label class="lh-30">
						<span class="btn btn-default btn-sm btn-file pull-left mr-10">
							<i class="fa fa-picture-o"></i>
							Добавить лого <input id="select_photo" type="file" name="userfile">
						</span>
						Фото <small>(Мин. ширина: 200px, высота: 50px, не превышать 5Мб)</small>
					</label>
					<div id="partner_logo" class="files in-row-multiple pull-left">
						<?php if(!empty($partner)){?>
							<div class="user-image-thumbnail-wr">
								<div class="user-image-thumb">
									<img class="img-thumbnail" src="<?php echo base_url('files/partners/'.$partner['partner_logo']);?>">
								</div>
								<input type="hidden" name="partner_logo" value="<?php echo $partner['partner_logo'];?>">
							</div>
						<?php }?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label>Тип</label>
					<select name="category" class="form-control">
						<option value="">Выберите тип</option>
						<?php foreach($categories as $category){?>
							<option value="<?php echo $category['id_category'];?>" <?php if(!empty($partner)){echo set_select('category', $category['id_category'], $category['id_category'] == $partner['id_category']);}?>><?php echo $category['category_title_ru'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<label>Название</label>
					<input class="form-control" placeholder="Название" name="title" maxlength="250" value="<?php if(!empty($partner)){echo $partner['partner_name'];}?>">
					<p class="help-block">Название не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Ссылка</label>
					<input class="form-control" placeholder="Ссылка" name="link" maxlength="250" value="<?php if(!empty($partner)){echo $partner['partner_link'];}?>">
					<p class="help-block">Название не должно содержать более 250 символов.</p>
				</div>
            </div>
            <div class="modal-footer">                
                <label class="pull-left lh-34 mb-0">
                    <input type="checkbox" name="partner_active" <?php echo (!empty($partner))?set_checkbox('partner_active', '', $partner['partner_active'] == 1):'checked';?> class="flat-red nice-input">
                    Виднен на сайте
                </label>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($partner)){?>
                    <input type="hidden" name="id_partner" value="<?php echo $partner['id_partner'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_partner" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_partner" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
		'use strict';

		$('#select_photo').fileupload({
			url: base_url+'admin/partners/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
                    var template = '<div class="user-image-thumbnail-wr">\
                                        <div class="user-image-thumb">\
                                            <img class="img-thumbnail" src="'+base_url+'files/partners/'+data.result.file.filename+'"/>\
                                        </div>\
                                        <input type="hidden" name="partner_logo" value="'+data.result.file.filename+'">\
                                    </div>';

                    if($('#partner_logo .user-image-thumbnail-wr').length > 0){
                        var unused_photo = $('#partner_logo .user-image-thumbnail-wr').find('input[name="partner_logo"]').val();
                        $('#partner_logo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
                    }
                    $('#partner_logo').html(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });
	
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

    var manage_partner = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/partners/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_partners_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>