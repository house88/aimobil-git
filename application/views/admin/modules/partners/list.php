<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="#" class="btn btn-box-tool call-function" data-callback="toggle_dt_filter" data-action="open">
					<i class="fa fa-filter"></i> Фильтр
				</a>
				<a href="#" data-href="<?php echo base_url('admin/partners/popup_forms/add');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить партнера
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_photo">Фото</th>
						<th class="dt_name">Название</th>
						<th class="dt_actions">Операций</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<?php $this->load->view('admin/modules/partners/filter');?>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/partners/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam_i", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "text-left vam_i", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-95 text-center vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						use_cookie: true,
						cookie_page: 'partners',
						callBack: function(){ 
							$.cookie( 'cookie_page', 'partners');
							$.cookie( '_dtFilters', $.param(dtFilter.getDTFilter()));
							dtTable.fnDraw(); 
						}
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});
	});

	function manage_partners_callback(){
		dtTable.fnDraw(false);
	}
	var delete_action = function(btn){
		var $this = $(btn);
		var partner = $this.data('partner');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/partners/ajax_operations/delete',
			data: {partner:partner},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_partners_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var change_status = function(btn){
		var $this = $(btn);
		var partner = $this.data('partner');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/partners/ajax_operations/change_status',
			data: {partner:partner},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_partners_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
