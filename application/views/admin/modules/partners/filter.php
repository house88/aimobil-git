<div class="wr-filter-admin-panel filters-container">
	<div class="filter-admin-panel">
		<div class="title-b">
            Фильтры
            <a href="#" class="fa fa-times pull-right call-function" data-callback="toggle_dt_filter" data-action="close"></a>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Тип</label>
					<select name="category" class="form-control dt_filter" data-title="Тип">
						<option value="">Выберите тип</option>
						<?php foreach($categories as $category){?>
							<option value="<?php echo $category['id_category'];?>"><?php echo $category['category_title_ru'];?></option>
						<?php }?>
					</select>
                </div>
            </div>
        </div>

		<div class="wr-filter-list clearfix mt-10 "><ul class="filter-plugin-list"></ul></div>
	</div>

	<div class="wr-hidden call-function" data-callback="toggle_dt_filter" data-action="close" style="display: none;"></div>
</div>