<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/services/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить услугу
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="add_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Иконка, ex. fai fai-home</label>
					<input class="form-control" placeholder="ex. fai fai-home" name="icon" value="">
				</div>
				
				<div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_ro" id="sidebar_image_ro">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_ro"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_ro&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст, введение</label>
								<textarea class="form-control h-100" name="stext_ro"></textarea>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ro"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_ru" id="sidebar_image_ru">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_ru"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_ru&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст, введение</label>
								<textarea class="form-control h-100" name="stext_ru"></textarea>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ru"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="input-group">
								<input type="text" class="form-control" name="sidebar_image_en" id="sidebar_image_en">
								<div class="input-group-btn">
									<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#sidebar_image_en"><i class="fa fa-times"></i></span>
									<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=sidebar_image_en&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
										<i class="fa fa-folder-open"></i> Добавить фото
									</span>
								</div>
							</div>
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст, введение</label>
								<textarea class="form-control h-100" name="stext_en"></textarea>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_en"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary btn-flat">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

	var preview_image = function(btn){
		var $this = $(btn);
		var image = $($this.data('image')).val();
		if(image != ''){
			$.fancybox.open([
				{
					src  : base_url+'files/'+image
				}
			]);
		} else{
			systemMessages( 'Ошибка: Нет баннера.', 'error' );
		}
	}

	var add_form = $('#add_form');
	add_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = add_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/services/ajax_operations/add',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					add_form.replaceWith('<div class="box-body"><div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div></div>');
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
