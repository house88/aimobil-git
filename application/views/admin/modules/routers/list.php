<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->		
		<form id="router_form">
			<div class="box-body">
				<?php foreach($routers as $router){?>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<label><?php echo $router['route_name'];?></label>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="input-group">
									<span class="input-group-addon">RO</span>
									<input type="text" class="form-control" name="routers[<?php echo $router['route_key'];?>][ro]" value="<?php echo $router['route_ro'];?>">
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="input-group">
									<span class="input-group-addon">RU</span>
									<input type="text" class="form-control" name="routers[<?php echo $router['route_key'];?>][ru]" value="<?php echo $router['route_ru'];?>">
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="input-group">
									<span class="input-group-addon">EN</span>
									<input type="text" class="form-control" name="routers[<?php echo $router['route_key'];?>][en]" value="<?php echo $router['route_en'];?>">
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
		<!-- /.box-body -->
	</div>
</div>
<script>	
	var router_form = $('#router_form');
	router_form.submit(function () {
		var fdata = router_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/update_routers',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлен. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
