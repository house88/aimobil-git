<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/planing/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить серию
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="manage_form">			
			<div class="box-body">
				<div class="form-group">
					<p class="help-block"><label>Фото <small>(Мин. ширина: 350px, высота: 250px, не превышать 5Мб)</small></label></p>
					<span class="btn btn-default btn-sm btn-file pull-left mb-15">
						<i class="fa fa-picture-o"></i>
						Добавить фото серий<input id="select_photo" type="file" name="userfile">
					</span>
					<div class="clearfix"></div>
					<div id="default_photo" class="files in-row-multiple pull-left">
						<?php if(!empty($plan)){?>
							<div class="user-image-thumbnail-wr">
								<div class="user-image-thumb">
									<img class="img-thumbnail" src="<?php echo site_url('files/planing/'.$plan['plan_photo']);?>"/>
								</div>
								<input type="hidden" name="default_photo" value="<?php echo $plan['plan_photo'];?>">
							</div>
						<?php }?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php if(!empty($plan)){echo $plan['plan_name_ro'];}?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ro"><?php if(!empty($plan)){echo $plan['plan_text_ro'];}?></textarea>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($plan)){echo $plan['plan_name_ru'];}?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ru"><?php if(!empty($plan)){echo $plan['plan_text_ru'];}?></textarea>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($plan)){echo $plan['plan_name_en'];}?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_en"><?php if(!empty($plan)){echo $plan['plan_text_en'];}?></textarea>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
				<div class="form-group">
					<p class="help-block"><label>Фото <small>(Мин. ширина: 200px, высота: 200px, не превышать 5Мб)</small></label></p>
					<span class="btn btn-default btn-sm btn-file pull-left mb-15">
						<i class="fa fa-picture-o"></i>
						Добавить планировку <input id="select_photos" type="file" name="userfile" multiple>
					</span>
					<div class="clearfix"></div>
					<div class="files">
						<div class="row" id="plan_photos">
							<?php if(!empty($plan)){?>
								<?php $plan_photos = json_decode($plan['plan_photos'], true);?>
								<?php if(!empty($plan_photos)){?>
									<?php foreach($plan_photos as $photo_key => $plan_photo){?>
										<div class="upload-plans-files col-xs-12 col-sm-6 col-md-4 col-lg-3">
											<div class="upload-plans-file__wr">
												<div class="image">
													<div class="thumb">
														<img src="<?php echo site_url('files/planing/'.$plan_photo['file']);?>"/>
													</div>
													<div class="btn-group btn-remove">
														<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
														<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
														<a href="#" class="btn btn-danger btn-flat confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $plan_photo['file'];?>">
															<span class="fa fa-times"></span>
														</a>
													</div>
												</div>
												<input type="hidden" name="photos[<?php echo $photo_key;?>][file]" value="<?php echo $plan_photo['file'];?>">
												<div class="detail">
													<div class="form-group">
														<input class="form-control" placeholder="Название, Ro" name="photos[<?php echo $photo_key;?>][title_ro]" value="<?php echo $plan_photo['title_ro'];?>">
													</div>
													<div class="form-group">
														<input class="form-control" placeholder="Название, Ru" name="photos[<?php echo $photo_key;?>][title_ru]" value="<?php echo $plan_photo['title_ru'];?>">
													</div>
													<div class="form-group mb-0">
														<input class="form-control" placeholder="Название, En" name="photos[<?php echo $photo_key;?>][title_en]" value="<?php echo $plan_photo['title_en'];?>">
													</div>
												</div>
											</div>
										</div>
									<?php }?>
								<?php }?>
							<?php }?>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<?php if(!empty($plan)){?>
					<div class="form-group pull-left w-205">
						<div class="input-group date" id="datetimepicker">
							<input type="text" class="form-control" name="plan_date" value="<?php echo formatDate($plan['plan_date'], 'd.m.Y H:i:s');?>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<input type="hidden" name="id_plan" value="<?php echo $plan['id_plan'];?>">
				<?php }?>
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		'use strict';
		$('#select_photo').fileupload({
			url: base_url+'admin/planing/ajax_operations/upload_photo',
			dataType: 'json',
			beforeSend: function(){
				clearSystemMessages();
			},
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
                    var template = '<div class="user-image-thumbnail-wr">\
                                        <div class="user-image-thumb">\
                                            <img class="img-thumbnail" src="'+base_url+'files/planing/'+data.result.file.filename+'"/>\
                                        </div>\
                                        <input type="hidden" name="default_photo" value="'+data.result.file.filename+'">\
                                    </div>';

                    if($('#default_photo .user-image-thumbnail-wr').length > 0){
                        var unused_photo = $('#default_photo .user-image-thumbnail-wr').find('input[name="default_photo"]').val();
                        $('#default_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
                    }
                    $('#default_photo').html(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#select_photos').fileupload({
			url: base_url+'admin/planing/ajax_operations/upload_photos',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					var template = '<div class="upload-plans-files col-xs-12 col-sm-6 col-md-4 col-lg-3">\
										<div class="upload-plans-file__wr">\
											<div class="image">\
												<div class="thumb">\
													<img src="'+base_url+'files/planing/'+data.result.file.filename+'"/>\
												</div>\
												<div class="btn-group btn-remove">\
													<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>\
													<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>\
													<a href="#" class="btn btn-danger btn-flat confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'">\
														<span class="fa fa-times"></span>\
													</a>\
												</div>\
											</div>\
											<input type="hidden" name="photos['+data.result.file.fileid+'][file]" value="'+data.result.file.filename+'">\
											<div class="detail">\
												<div class="form-group">\
													<input class="form-control" placeholder="Название, Ro" name="photos['+data.result.file.fileid+'][title_ro]" value="">\
												</div>\
												<div class="form-group">\
													<input class="form-control" placeholder="Название, Ru" name="photos['+data.result.file.fileid+'][title_ru]" value="">\
												</div>\
												<div class="form-group mb-0">\
													<input class="form-control" placeholder="Название, En" name="photos['+data.result.file.fileid+'][title_en]" value="">\
												</div>\
											</div>\
										</div>\
                                    </div>';

                    $('#plan_photos').append(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.upload-plans-files');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}
	
	var manage_form = $('#manage_form');
	manage_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = manage_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/planing/ajax_operations/<?php if(!empty($plan)){?>edit<?php } else{?>add<?php }?>',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					<?php if(!empty($plan)){?>
						systemMessages(resp.message, resp.mess_type);
					<?php } else{?>
						window.location.href = base_url+'admin/planing';
					<?php }?>
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.upload-plans-files').remove();
	}
</script>
