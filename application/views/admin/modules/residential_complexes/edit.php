<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/residential_complexes/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить жилой комплекс
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_general" data-toggle="tab" aria-expanded="true">Общая информация</a>
						</li>
						<li class="">
							<a href="#form_photos" data-toggle="tab" aria-expanded="false">Фото</a>
						</li>
						<li class="">
							<a href="#form_filters" data-toggle="tab" aria-expanded="false">Опции</a>
						</li>
						<li class="">
							<a href="#form_location" data-toggle="tab" aria-expanded="false">Местоположение</a>
						</li>
						<li class="">
							<a href="#form_plans" data-toggle="tab" aria-expanded="false">Планировки</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_general">
							<div class="form-group">
								<label>Цена, от (<?php echo $default_currency['currency_symbol'];?>)</label>
								<input class="form-control" placeholder="0.00" name="price_from" value="<?php echo $complex['complex_price_from'];?>">
							</div>
							<?php if($this->lauth->is_admin_only()){?>
								<div class="form-group">
									<label>Менеджер</label>
									<select name="manager" class="form-control">
										<?php $users = Modules::run('users/_get_users', array('user_banned' => 0, 'group_alias' => array('manager')));?>
										<?php foreach($users as $_manager){?>
											<option value="<?php echo $_manager['id_user'];?>" <?php echo set_select('manager', $_manager['id_user'], $_manager['id_user'] == $complex['id_manager']);?>><?php echo $_manager[lang_column('user_name')];?></option>
										<?php }?>
									</select>
								</div>
							<?php }?>
							<div class="nav-tabs-custom mb-0">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
									</li>
									<li class="">
										<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
									</li>
									<li class="">
										<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="form_ro">
										<div class="form-group">
											<label>Название</label>
											<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo clean_output($complex['complex_title_ro']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст блока с ценой</label>
											<input class="form-control" placeholder="Текст блока с ценой" name="complex_head_ro" value="<?php echo clean_output($complex['complex_head_ro']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Адрес</label>
											<input class="form-control" placeholder="Адрес" name="complex_address_ro" value="<?php echo clean_output($complex['complex_address_ro']);?>">
										</div>
										<div class="form-group">
											<label>Краткое описание</label>
											<textarea class="form-control" name="stext_ro"><?php echo $complex['complex_stext_ro'];?></textarea>
											<p class="help-block">Не должно содержать более 500 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_ro"><?php echo $complex['complex_text_ro'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_ro" value="<?php echo clean_output($complex['mt_ro']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo clean_output($complex['mk_ro']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo clean_output($complex['md_ro']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_ru">
										<div class="form-group">
											<label>Название</label>
											<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo clean_output($complex['complex_title_ru']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст блока с ценой</label>
											<input class="form-control" placeholder="Текст блока с ценой" name="complex_head_ru" value="<?php echo clean_output($complex['complex_head_ru']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Адрес</label>
											<input class="form-control" placeholder="Адрес" name="complex_address_ru" value="<?php echo clean_output($complex['complex_address_ru']);?>">
										</div>
										<div class="form-group">
											<label>Краткое описание</label>
											<textarea class="form-control" name="stext_ru"><?php echo $complex['complex_stext_ru'];?></textarea>
											<p class="help-block">Не должно содержать более 500 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_ru"><?php echo $complex['complex_text_ru'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_ru" value="<?php echo clean_output($complex['mt_ru']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo clean_output($complex['mk_ru']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo clean_output($complex['md_ru']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_en">
										<div class="form-group">
											<label>Название</label>
											<input class="form-control" placeholder="Название" name="title_en" value="<?php echo clean_output($complex['complex_title_en']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст блока с ценой</label>
											<input class="form-control" placeholder="Текст блока с ценой" name="complex_head_en" value="<?php echo clean_output($complex['complex_head_en']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Адрес</label>
											<input class="form-control" placeholder="Адрес" name="complex_address_en" value="<?php echo clean_output($complex['complex_address_en']);?>">
										</div>
										<div class="form-group">
											<label>Краткое описание</label>
											<textarea class="form-control" name="stext_en"><?php echo $complex['complex_stext_en'];?></textarea>
											<p class="help-block">Не должно содержать более 500 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_en"><?php echo $complex['complex_text_en'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_en" value="<?php echo clean_output($complex['mt_en']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo clean_output($complex['mk_en']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo clean_output($complex['md_en']);?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form_photos">
							<div class="form-group">
								<label>Фото <small>(Мин. ширина: 770px, высота: 515px, не превышать 10Мб)</small></label>
								<div class="clearfix"></div>
								<span class="btn btn-default btn-sm btn-file pull-left mb-15">
									<i class="fa fa-picture-o"></i>
									Добавить фото <input id="select_photo" type="file" name="userfile" multiple>
								</span>
								<div class="clearfix"></div>
								<div id="uploaded_files" class="files in-row-multiple">
									<?php $photos = json_decode($complex['complex_photos'], true);?>
									<?php if(!empty($photos)){?>
										<?php foreach($photos as $photo){?>
											<div class="user-image-thumbnail-wr">
												<div class="user-image-thumb">
													<img class="img-thumbnail" src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/'.$photo);?>"/>
												</div>

												<div class="btn-group btn-remove">
													<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_existent_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $photo;?>">
														<span class="fa fa-trash"></span>
													</a>
													<?php $is_main_photo = $complex['complex_photo'] == $photo;?>
													<a href="#" class="btn <?php if($is_main_photo){?>btn-success<?php } else{?>btn-default<?php }?> btn-xs btn-main_photo call-function" data-callback="main_photo" data-title="Сделать главной" data-photo="<?php echo $photo;?>">
														<span class="fa <?php if($is_main_photo){?>fa-check-square-o<?php } else{?>fa-square-o<?php }?>"></span>
													</a>
												</div>
												<input type="hidden" name="uploaded_files[]" value="<?php echo $photo;?>">
											</div>
										<?php }?>
									<?php }?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="tab-pane" id="form_filters">
							<?php $this->load->view('admin/modules/real_estate/icons/icons_view');?>
						</div>
						<div class="tab-pane" id="form_location">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="form-group">
										<label>Тип</label>
										<select name="location_parent" class="form-control">
											<?php foreach($parent_locations as $parent_location){?>
												<option value="<?php echo $parent_location['id_location'];?>" data-text="<?php if($parent_location['location_in_title']){echo $parent_location[lang_column('location_name')];}?>" <?php echo set_select('location_parent', $parent_location['id_location'], $location['id_parent'] == $parent_location['id_location']);?>><?php echo $parent_location[lang_column('location_name')];?></option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="form-group">
										<label>Местоположение</label>
										<select name="location_child" class="form-control" disabled></select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<label>Адрес</label>
									<input type="text" name="address" class="form-control" placeholder="Адрес" value="<?php echo $complex['complex_address'];?>">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<button class="btn btn-primary btn-flat mt-25 call-function" data-callback="map_search">Наити</button>
									</div>
								</div>
							</div>
							<input type="hidden" name="latitude" value="<?php echo $complex['complex_lat'];?>">
							<input type="hidden" name="longitude" value="<?php echo $complex['complex_lng'];?>">
							<div class="h-600 w-100pr" id="map-element"></div>
						</div>
						<div class="tab-pane" id="form_plans">
							<div class="form-group">
								<label>Фото <small>(Мин. ширина: 370px, высота: 270px, не превышать 10Мб)</small></label>
								<div class="clearfix"></div>
								<span class="btn btn-default btn-sm btn-file pull-left mb-15">
									<i class="fa fa-picture-o"></i>
									Добавить планировку <input id="select_photo_plan" type="file" name="userfile" multiple>
								</span>
								<div class="clearfix"></div>
								<div class="files">
									<div class="row" id="uploaded_files_plans">
										<?php $complex_plans = json_decode($complex['complex_plans'], true);?>
										<?php if(!empty($complex_plans)){?>
											<?php foreach($complex_plans as $complex_plan_key => $complex_plan){?>
												<div class="upload-plans-files col-xs-12 col-sm-6 col-md-4 col-lg-3">
													<div class="upload-plans-file__wr">
														<div class="image">
															<div class="thumb">
																<img src="<?php echo site_url('files/residential_complexes/'.$complex['id_complex'].'/'.$complex_plan['file']);?>"/>
															</div>
															<a href="#" class="btn btn-danger btn-flat btn-remove confirm-dialog" data-callback="remove_existent_plan" data-message="Вы уверены что хотите удалить планировку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $complex_plan['file'];?>">
																<span class="fa fa-times"></span>
															</a>
														</div>
														<input type="hidden" name="plans[<?php echo $complex_plan_key;?>][file]" value="<?php echo $complex_plan['file'];?>">
														<div class="detail">
															<div class="nav-tabs-custom mb-0">
																<ul class="nav nav-tabs">
																	<li class="active">
																		<a href="#plan_<?php echo $complex_plan_key;?>_ro" data-toggle="tab" aria-expanded="true">Romana</a>
																	</li>
																	<li class="">
																		<a href="#plan_<?php echo $complex_plan_key;?>_ru" data-toggle="tab" aria-expanded="true">Русский</a>
																	</li>
																	<li class="">
																		<a href="#plan_<?php echo $complex_plan_key;?>_en" data-toggle="tab" aria-expanded="false">English</a>
																	</li>
																</ul>
																<div class="tab-content">
																	<div class="tab-pane active" id="plan_<?php echo $complex_plan_key;?>_ro">
																		<div class="form-group mb-0">
																			<input class="form-control" placeholder="Название" name="plans[<?php echo $complex_plan_key;?>][title_ro]" value="<?php echo $complex_plan['title_ro'];?>">
																		</div>
																	</div>
																	<div class="tab-pane" id="plan_<?php echo $complex_plan_key;?>_ru">
																		<div class="form-group mb-0">
																			<input class="form-control" placeholder="Название" name="plans[<?php echo $complex_plan_key;?>][title_ru]" value="<?php echo $complex_plan['title_ru'];?>">
																		</div>
																	</div>
																	<div class="tab-pane" id="plan_<?php echo $complex_plan_key;?>_en">
																		<div class="form-group mb-0">
																			<input class="form-control" placeholder="Название" name="plans[<?php echo $complex_plan_key;?>][title_en]" value="<?php echo $complex_plan['title_en'];?>">
																		</div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<label>Цена, от (<?php echo $default_currency['currency_symbol'];?>)</label>
																<input class="form-control" placeholder="0.00" name="plans[<?php echo $complex_plan_key;?>][price_from]" value="<?php echo $complex_plan['price_from'];?>">
															</div>
															<div class="form-group">
																<label>Этажи</label>
																<input class="form-control" placeholder="ex. 1, 2-4" name="plans[<?php echo $complex_plan_key;?>][floors]" value="<?php echo $complex_plan['floors'];?>">
															</div>
															<div class="form-group mb-0">
																<label>Блок</label>
																<input class="form-control" placeholder="1" name="plans[<?php echo $complex_plan_key;?>][block]" value="<?php echo $complex_plan['block'];?>">
															</div>
														</div>
													</div>
												</div>
											<?php }?>
										<?php }?>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<input type="hidden" name="main_photo" value="<?php echo $complex['complex_photo'];?>">
				<input type="hidden" name="id_complex" value="<?php echo $complex['id_complex'];?>">
				<label class="pull-left lh-34 mb-0">
					<input type="checkbox" name="complex_active" class="flat-red nice-input" <?php echo set_checkbox('complex_active', '', $complex['complex_active'] == 1);?>>
					Доступен к показу на сайте
				</label>
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	var children_locations = <?php echo json_encode($children_locations)?>;
	var selected_location = <?php echo $complex['id_location'];?>;
	$(function(){
		'use strict';
		$('#select_photo').fileupload({
			url: base_url+'admin/residential_complexes/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					var template = '<div class="user-image-thumbnail-wr">\
										<div class="user-image-thumb">\
											<img class="img-thumbnail" src="'+base_url+'files/residential_complexes/temp/'+data.result.file.filename+'"/>\
										</div>\
										<div class="btn-group btn-remove">\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'">\
												<span class="fa fa-trash"></span>\
											</a>\
											<a href="#" class="btn btn-default btn-xs btn-main_photo call-function" data-callback="main_photo" data-title="Сделать главной" data-photo="'+data.result.file.filename+'">\
												<span class="fa fa-square-o"></span>\
											</a>\
										</div>\
										<input type="hidden" name="uploaded_files[]" value="'+data.result.file.filename+'">\
									</div>';

					$('#uploaded_files').append(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#select_photo_plan').fileupload({
			url: base_url+'admin/residential_complexes/ajax_operations/upload_photo_plan',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					var template = '<div class="upload-plans-files col-xs-12 col-sm-6 col-md-4 col-lg-3">\
										<div class="upload-plans-file__wr">\
											<div class="image">\
												<div class="thumb">\
													<img src="'+base_url+'files/residential_complexes/temp/'+data.result.file.filename+'"/>\
												</div>\
												<a href="#" class="btn btn-danger btn-flat btn-remove confirm-dialog" data-callback="remove_plan" data-message="Вы уверены что хотите удалить планировку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'">\
													<span class="fa fa-times"></span>\
												</a>\
											</div>\
											<input type="hidden" name="plans['+data.result.file.fileid+'][file]" value="'+data.result.file.filename+'">\
											<div class="detail">\
												<div class="nav-tabs-custom mb-0">\
													<ul class="nav nav-tabs">\
														<li class="active">\
															<a href="#plan_'+data.result.file.fileid+'_ro" data-toggle="tab" aria-expanded="true">Romana</a>\
														</li>\
														<li class="">\
															<a href="#plan_'+data.result.file.fileid+'_ru" data-toggle="tab" aria-expanded="true">Русский</a>\
														</li>\
														<li class="">\
															<a href="#plan_'+data.result.file.fileid+'_en" data-toggle="tab" aria-expanded="false">English</a>\
														</li>\
													</ul>\
													<div class="tab-content">\
														<div class="tab-pane active" id="plan_'+data.result.file.fileid+'_ro">\
															<div class="form-group mb-0">\
																<input class="form-control" placeholder="Название" name="plans['+data.result.file.fileid+'][title_ro]" value="">\
															</div>\
														</div>\
														<div class="tab-pane" id="plan_'+data.result.file.fileid+'_ru">\
															<div class="form-group mb-0">\
																<input class="form-control" placeholder="Название" name="plans['+data.result.file.fileid+'][title_ru]" value="">\
															</div>\
														</div>\
														<div class="tab-pane" id="plan_'+data.result.file.fileid+'_en">\
															<div class="form-group mb-0">\
																<input class="form-control" placeholder="Название" name="plans['+data.result.file.fileid+'][title_en]" value="">\
															</div>\
														</div>\
													</div>\
												</div>\
												<div class="form-group">\
													<label>Цена, от (<?php echo $default_currency['currency_symbol'];?>)</label>\
													<input class="form-control" placeholder="0.00" name="plans['+data.result.file.fileid+'][price_from]" value="">\
												</div>\
												<div class="form-group">\
													<label>Этажи</label>\
													<input class="form-control" placeholder="ex. 1, 2-4" name="plans['+data.result.file.fileid+'][floors]" value="">\
												</div>\
												<div class="form-group mb-0">\
													<label>Блок</label>\
													<input class="form-control" placeholder="1" name="plans['+data.result.file.fileid+'][block]" value="">\
												</div>\
											</div>\
										</div>\
									</div>';

					$('#uploaded_files_plans').append(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('select[name="location_parent"]').on('change', function(){
			update_children_locations();
		});

		update_children_locations();

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if(e.target.hash == '#form_location'){
				initMap();
			}
		});
	});

	function update_children_locations(){
		var parent_location = intval($('select[name="location_parent"] option:selected').val());
		var $child_locations = $('select[name="location_child"]');
		if(parent_location == ''){
			$child_locations.html('<option value="">Выберите сначало тип</option>').prop('disabled', true);
			return false;
		}

		var options = [];
		$.each(children_locations[parent_location], function(key, location){
			options.push('<option value="'+location.id_location+'" '+((location.id_location == selected_location)?'selected':'')+'>'+location.location_name_ru+'</option>');
		});

		$child_locations.html(options.join('')).prop('disabled', false);
	}

	function init_icheck(){
		$('input.icon-checkbox').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '0%'
		});
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/residential_complexes/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});

	var main_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').find('input[name="main_photo"]').val(photo);
		$('#uploaded_files .btn-main_photo').each(function(){
			$(this).removeClass('btn-success').addClass('btn-default');
			$(this).find('.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
		});

		$this.removeClass('btn-default').addClass('btn-success');
		$this.find('.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
	}

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	var remove_existent_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		var main_photo = $this.closest('form').find('input[name="main_photo"]').val();
		if(photo == main_photo){
			$this.closest('form').find('input[name="main_photo"]').val('');
		}
		$this.closest('form').append('<input type="hidden" name="remove_existent_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	var remove_plan = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.upload-plans-files').remove();
	}

	var remove_existent_plan = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_existent_plans[]" value="'+photo+'"/>');
		$this.closest('.upload-plans-files').remove();
	}

	// OS MAP
	var mymap = null, 
		marker = null,
		latitude = <?php echo $complex['complex_lat'];?>,
		longitude = <?php echo $complex['complex_lng'];?>;
	
	function initMap(){
		$('input[name="latitude"]').val(latitude);
		$('input[name="longitude"]').val(longitude);		
		mymap = L.map(document.getElementById('map-element'), {
			center: {lat: latitude, lng: longitude},
			zoom: 17,
			scrollWheelZoom: true
		});
	
		L.tileLayer('//i.simpalsmedia.com/map/1/{z}/{x}/{y}.png', {
			attribution: '<a href="'+base_url+'" target="_blank">AccesImobil, </a><a href="http://www.openstreetmap.org/#map=15/47.770685/27.929283999999996" target="_blank">© OpenStreetMap</a>',
			maxZoom: 18
		}).addTo(mymap);

		var aIcon = L.icon({
			iconUrl: base_url + 'files/images/gm_marker.png'
		});

		marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
		
		mymap.on('click', function(e) {
			latitude = e.latlng.lat;
			longitude = e.latlng.lng;
			if(marker == null){
				marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
			} else{
				var latlng = new L.latLng(latitude, longitude);
				marker.setLatLng(latlng);
			}
			
			$('input[name="latitude"]').val(latitude);
			$('input[name="longitude"]').val(longitude);

			marker.on('dragend', markerDrag);
		});

		var options = {
			bounds: null,
			email: null,
			callback: function (results) {
				setTimeout(() => {
					if(Object.keys(results).length > 0){
						var bbox = results[0].boundingbox,
							first = new L.LatLng(bbox[0], bbox[2]),
							second = new L.LatLng(bbox[1], bbox[3]),
							bounds = new L.LatLngBounds([first, second]);
						this._map.fitBounds(bounds);
		
						latitude = results[0].lat;
						longitude = results[0].lon;
						if(marker == null){
							marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
						} else{
							var latlng = new L.latLng(latitude, longitude);
							marker.setLatLng(latlng);
						}
						
						$('input[name="latitude"]').val(latitude);
						$('input[name="longitude"]').val(longitude);
					} else{
						systemMessages( 'Ошибка: Не могу найти указанный адрес.', 'error' );
					}
				}, 400);
			}
		};

		var osmGeocoder = new L.Control.OSMGeocoder(options);

		mymap.addControl(osmGeocoder);
	}

	function markerDrag(e){
		var marker_position = e.target.getLatLng();
		$('input[name="latitude"]').val(marker_position.lat);
		$('input[name="longitude"]').val(marker_position.lng);		
	}

	var map_search = function(btn){
		var map_search_params = [];
		var $parent_location = $('select[name="location_parent"] option:selected');
		var $child_location = $('select[name="location_child"] option:selected');
		var $address_location = $('input[name="address"]');
		
		if($parent_location.data('text') != undefined && $parent_location.data('text') != ''){
			map_search_params.push($parent_location.data('text'));
		}

		if($child_location.val() != ''){
			map_search_params.push($child_location.text());
		}

		if($address_location.val() != ''){
			map_search_params.push($address_location.val());
		}
		
		if(map_search_params.length > 0){
			$('.leaflet-control-geocoder input[type="text"]').val(map_search_params.join(', '));
			$('.leaflet-control-geocoder #search_on_map').click();
		}
	}
</script>