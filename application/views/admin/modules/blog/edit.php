<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/blog/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить блог
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Фото</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="blog_image" id="blog_image" value="<?php echo $blog['blog_photo'];?>" readonly>
                        <div class="input-group-btn">
                            <span class="btn btn-default call-function" data-callback="clear_image" data-clear="#blog_image"><i class="fa fa-times"></i></span>
                            <span class="btn btn-default call-function" data-callback="preview_image" data-image="#blog_image"><i class="fa fa-eye"></i></span>
                            <span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=blog_image&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
                                <i class="fa fa-folder-open"></i> Добавить фото
                            </span>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label>Категория</label>
                    <select name="category" class="form-control">
                        <option value="">Выберите категорию</option>
                        <?php foreach($categories as $category){?>
                            <option value="<?php echo $category['id_category'];?>" <?php echo set_select('category', $category['id_category'], $blog['id_category'] == $category['id_category']);?>><?php echo $category['category_title_ru'];?></option>                                        
                        <?php }?>
                    </select>
                </div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo clean_output($blog['blog_title_ro']);?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control" name="stext_ro"><?php echo $blog['blog_small_description_ro'];?></textarea>
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ro"><?php echo $blog['blog_description_ro'];?></textarea>
							</div>
							<div class="form-group">
								<label>URL</label>
								<input class="form-control" placeholder="URL" name="url_ro" value="<?php echo clean_output($blog['url_ro']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ro" value="<?php echo clean_output($blog['mt_ro']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo clean_output($blog['mk_ro']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo clean_output($blog['md_ro']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo clean_output($blog['blog_title_ru']);?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control" name="stext_ru"><?php echo $blog['blog_small_description_ru'];?></textarea>
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ru"><?php echo $blog['blog_description_ru'];?></textarea>
							</div>
							<div class="form-group">
								<label>URL</label>
								<input class="form-control" placeholder="URL" name="url_ru" value="<?php echo clean_output($blog['url_ru']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ru" value="<?php echo clean_output($blog['mt_ru']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo clean_output($blog['mk_ru']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo clean_output($blog['md_ru']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo clean_output($blog['blog_title_en']);?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control" name="stext_en"><?php echo $blog['blog_small_description_en'];?></textarea>
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_en"><?php echo $blog['blog_description_en'];?></textarea>
							</div>
							<div class="form-group">
								<label>URL</label>
								<input class="form-control" placeholder="URL" name="url_en" value="<?php echo clean_output($blog['url_en']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_en" value="<?php echo clean_output($blog['mt_en']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo clean_output($blog['mk_en']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo clean_output($blog['md_en']);?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<div class="form-group pull-left w-205">
					<div class="input-group date" id="datetimepicker">
						<input type="text" class="form-control" name="blog_date" value="<?php echo formatDate($blog['blog_date'], 'd.m.Y H:i:s');?>"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<input type="hidden" name="blog" value="<?php echo $blog['id_blog'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		'use strict';
		$('#datetimepicker').datetimepicker({
			locale: 'ru',
			format: 'DD.MM.YYYY HH:mm:ss',
			tooltips:{
				today:"Сегодня",
				clear:"Очистить",
				close:"Закрыть",
				selectMonth:"Выберите месяц",
				prevMonth:"Пред. месяц",
				nextMonth:"След. месяц",
				selectYear:"Выберите год",
				prevYear:"Пред. год",
				nextYear:"След. год",
				selectDecade:"Выберите десятилетие",
				prevDecade:"Пред. десятилетие",
				nextDecade:"След. десятилетие",
				prevCentury:"Пред. век",
				nextCentury:"След. век",
				pickHour:"Выберите час",
				incrementHour:"Добавить час",
				decrementHour:"Убавить час",
				pickMinute:"Выберите минуты",
				incrementMinute:"Добавить минуту",
				decrementMinute:"Убавить минуту",
				pickSecond:"Выберите секунду",
				incrementSecond:"Добавить секунду",
				decrementSecond:"Убавить секунду",
				togglePeriod:"Переключить период",
				selectTime:"Выберите время"
			}
		});
	});
	
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

	var preview_image = function(btn){
		var $this = $(btn);
		var image = $($this.data('image')).val();
		if(image != ''){
			$.fancybox.open([
				{
					src  : base_url+'files/'+image
				}
			]);
		} else{
			systemMessages( 'Ошибка: Нет баннера.', 'error' );
		}
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/blog/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});
</script>
