<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools">
				<a href="#" class="btn btn-box-tool call-function" data-callback="toggle_dt_filter" data-action="open">
					<i class="fa fa-filter"></i> Фильтр
				</a>
				<a href="#" class="btn btn-box-tool confirm-dialog" data-dtype="primary" data-message="Вы уверенны что хотите обновить файлы переводов?" data-title="Обновить файлы переводов" data-callback="update_translations_files">
					<i class="fa fa-refresh"></i> Обновить файлы переводов
				</a>
				<a href="#" data-href="<?php echo base_url('admin/popup_forms/add_translation');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить перевод
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_text_ro">Текст RO</th>
						<th class="dt_key">Ключ</th>
						<th class="dt_actions"></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<?php $this->load->view('admin/modules/translations/filter');?>
<script>
	var dtTable, dtFilter;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/ajax_operations/translations_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100000,
			"aoColumnDefs": [
				{ "sClass": "w-75pr text-left vam_i", "aTargets": ["dt_text_ro"], "mData": "dt_text_ro" , "bSortable": false},
				{ "sClass": "text-left vam_i", "aTargets": ["dt_key"], "mData": "dt_key" , "bSortable": false},
				{ "sClass": "w-95 text-center vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						use_cookie: true,
						cookie_page: 'translations',
						callBack: function(){ 
							$.cookie( 'cookie_page', 'translations');
							$.cookie( '_dtFilters', $.param(dtFilter.getDTFilter()));
							dtTable.fnDraw(); 
						}
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						if(data.mess_type == 'info'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});
	});

	function manage_translation_callback(){
		dtTable.fnDraw(false);
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var id_translation = $this.data('translation');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/delete_translation',
			data: {id_translation:id_translation},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_translation_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var update_translations_files = function(btn){
		var $this = $(btn);
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/update_translations_files',
			data: {},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
