<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
                <?php if(!empty($translation)){?>Редактировать<?php } else{?>Добавить<?php }?> перевод
            </h4>
        </div>
        <form autocomplete="off">			
            <div class="modal-body">
                <div class="form-group">
                    <label>Ключ</label>
                    <?php if(empty($translation)){?>
                        <input class="form-control" placeholder="Ключ" name="key" value="">                    
                    <?php } else{?>
                        <p><?php if(!empty($translation)){echo $translation['translation_key'];}?></p>
                    <?php }?>
                </div>
                <div class="form-group">
                    <label>Модуль</label>
                    <select name="id_section" class="form-control">
                        <option value="">Выберите модуль</option>
                        <?php foreach($translations_sections as $translation_section){?>
                            <option value="<?php echo $translation_section['id_section'];?>" <?php if(!empty($translation)){echo set_select('id_section',$translation_section['id_section'],$translation_section['id_section'] == $translation['id_section']);}?>><?php echo $translation_section['section_name'];?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Текст</label>
                                <textarea name="text_ro" rows="5" class="form-control"><?php if(!empty($translation)){echo $translation['translation_text_ro'];}?></textarea>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="text_ru" rows="5" class="form-control"><?php if(!empty($translation)){echo $translation['translation_text_ru'];}?></textarea>
                            </div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="text_en" rows="5" class="form-control"><?php if(!empty($translation)){echo $translation['translation_text_en'];}?></textarea>
                            </div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($translation)){?>
                    <input type="hidden" name="id_translation" value="<?php echo $translation['id_translation'];?>">
                <?php }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_translation" data-action="<?php if(!empty($translation)){?>edit_translation<?php } else{?>add_translation<?php }?>">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var manage_translation = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_translation_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>