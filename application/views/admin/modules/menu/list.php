<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools">
				<a href="#" data-href="<?php echo base_url('admin/menu/popup_forms/add');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить меню
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_name">Название</th>
						<th class="dt_hash">Ключ</th>
						<th class="dt_actions"></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<script>
	var dtTable, dtFilter;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/menu/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100000,
			"aoColumnDefs": [
				{ "sClass": "text-left vam_i", "aTargets": ["dt_name"], "mData": "dt_name" , "bSortable": false},
				{ "sClass": "w-300 text-left vam_i", "aTargets": ["dt_hash"], "mData": "dt_hash" , "bSortable": false},
				{ "sClass": "w-80 text-center vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						if(data.mess_type == 'info'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});
	});

	function manage_menu_callback(){
		dtTable.fnDraw(false);
    }
    
    var delete_action = function(btn){
		var $this = $(btn);
		var menu = $this.data('menu');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/menu/ajax_operations/delete',
			data: {menu:menu},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					manage_menu_callback();
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
