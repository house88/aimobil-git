<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
                <?php if(!empty($menu)){?>Редактировать<?php } else{?>Добавить<?php }?> меню
            </h4>
        </div>
        <form autocomplete="off">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" placeholder="Название" name="title" value="<?php if(!empty($menu)){echo $menu['menu_title'];}?>">
                        </div>
					</div>
					<div class="col-xs-12">
						<div class="menu_values">
							<?php if(!empty($menu)){?>
								<?php $values = json_decode($menu['menu_elements'], true);?>
								<?php if(!empty($values)){?>
									<?php foreach($values as $value){?>
										<div class="value-item">
											<div class="form-group">
												<label>Ссылка</label>
												<input type="text" class="form-control" name="menu_values[links][]" placeholder="Ссылка" value="<?php echo $value['link'];?>">
											</div>
											<div class="input-group input-group-sm mb-10">
												<input class="form-control" placeholder="Romana" name="menu_values[title_ro][]" value="<?php echo $value['title_ro'];?>">
												<span class="input-group-addon">&mdash;</span>
												<input class="form-control" placeholder="Русский" name="menu_values[title_ru][]" value="<?php echo $value['title_ru'];?>">
												<span class="input-group-addon">&mdash;</span>
												<input class="form-control" placeholder="English" name="menu_values[title_en][]" value="<?php echo $value['title_en'];?>">
												<div class="input-group-btn">
													<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
													<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
													<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_value"><i class="fa fa-trash"></i></span>
												</div>
											</div>
										</div>
									<?php }?>
								<?php }?>						
							<?php }?>
						</div>
					</div>					
                </div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($menu)){?>
                    <input type="hidden" name="menu" value="<?php echo $menu['id_menu'];?>">
                <?php }?>
                <button type="button" class="btn btn-default pull-left call-function" data-callback="add_value">Добавить значение</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_menu" data-action="<?php if(!empty($menu)){?>edit<?php } else{?>add<?php }?>">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
	var add_value = function(){
		var template = '<div class="value-item">\
							<div class="form-group">\
								<label>Ссылка</label>\
								<input type="text" class="form-control" name="menu_values[links][]" placeholder="Ссылка" value="">\
							</div>\
							<div class="input-group input-group-sm mb-10">\
								<input class="form-control" placeholder="Romana" name="menu_values[title_ro][]" value="">\
								<span class="input-group-addon">&mdash;</span>\
								<input class="form-control" placeholder="Русский" name="menu_values[title_ru][]" value="">\
								<span class="input-group-addon">&mdash;</span>\
								<input class="form-control" placeholder="English" name="menu_values[title_en][]" value="">\
								<div class="input-group-btn">\
									<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>\
									<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>\
									<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_value"><i class="fa fa-trash"></i></span>\
								</div>\
							</div>\
						</div>';
		$('.menu_values').append(template);
	}

	var delete_value = function(btn){
		$(btn).closest('.value-item').remove();
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.value-item');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

    var manage_menu = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/menu/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_menu_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>