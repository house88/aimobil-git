<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($location)){echo 'Редактирование';} else{echo 'Добавление';}?> местонахождение</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
				<?php if(empty($location) || $location['id_parent'] > 0){?>
					<div class="form-group">
						<label>Тип</label>
						<select name="id_parent" class="form-control">
							<?php foreach($parent_locations as $parent_location){?>
								<option value="<?php echo $parent_location['id_location'];?>" <?php if(!empty($location)){echo set_select('id_parent', $parent_location['id_location'], $parent_location['id_location'] == $location['id_parent']);}?>><?php echo $parent_location[lang_column('location_name')];?></option>
							<?php }?>
						</select>
					</div>				
				<?php }?>
                <div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php if(!empty($location)){echo $location['location_name_ro'];}?>">
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($location)){echo $location['location_name_ru'];}?>">
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($location)){echo $location['location_name_en'];}?>">
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">                
                <label class="pull-left lh-34 mb-0">
                    <input type="checkbox" name="location_active" <?php echo (!empty($location))?set_checkbox('location_active', '', $location['location_active'] == 1):'checked';?> class="flat-red nice-input">
                    Видна на сайте
                </label>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($location)){?>
                    <input type="hidden" name="id_location" value="<?php echo $location['id_location'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_location" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_location" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });
    var manage_location = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/locations/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_location_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>