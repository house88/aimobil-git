<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Редактировать шаблон</h4>
        </div>
        <form autocomplete="off">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" placeholder="Название" name="title" value="<?php echo $template['template_name'];?>">
                        </div>
					</div>
					<div class="col-xs-12">
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
								</li>
								<li class="">
									<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
								</li>
								<li class="">
									<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
								</li>
							</ul>
							<div class="tab-content pl-0_i pr-0_i pb-0_i">
								<div class="tab-pane active" id="form_ro">
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Тема</label>
												<input class="form-control" placeholder="Тема" name="subject_ro" maxlength="250" value="<?php echo $template['template_subject_ro'];?>">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Текст</label>
												<textarea name="text_ro" class="form-control" rows="10"><?php echo $template['template_text_ro'];?></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="form_ru">
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Тема</label>
												<input class="form-control" placeholder="Тема" name="subject_ru" maxlength="250" value="<?php echo $template['template_subject_ru'];?>">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Текст</label>
												<textarea name="text_ru" class="form-control" rows="10"><?php echo $template['template_text_ru'];?></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="form_en">
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Тема</label>
												<input class="form-control" placeholder="Тема" name="subject_en" maxlength="250" value="<?php echo $template['template_subject_en'];?>">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Текст</label>
												<textarea name="text_en" class="form-control" rows="10"><?php echo $template['template_text_en'];?></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.tab-content -->
						</div>
					</div>					
                </div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($template)){?>
                    <input type="hidden" name="template" value="<?php echo $template['id_template'];?>">
                <?php }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_template">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var manage_template = function(btn){
        var $this = $(btn);
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/notify/ajax_operations/edit',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_notify_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>