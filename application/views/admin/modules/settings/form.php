<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
                <?php if(!empty($setting)){?>Редактировать<?php } else{?>Добавить<?php }?> настройку
            </h4>
        </div>
        <form autocomplete="off">			
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" placeholder="Название" class="form-control" name="title" value="<?php if(!empty($setting)){echo $setting['setting_title'];}?>">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Ключ</label>
                            <?php if(empty($setting)){?>
                                <input class="form-control" placeholder="Ключ" name="key" value="">                    
                            <?php } else{?>
                                <p class="lh-34 mb-0"><?php echo $setting['setting_alias'];?></p>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Модуль</label>
                    <select name="id_section" class="form-control">
                        <option value="">Выберите модуль</option>
                        <?php foreach($settings_sections as $settings_section){?>
                            <option value="<?php echo $settings_section['id_section'];?>" <?php if(!empty($setting)){echo set_select('id_section',$settings_section['id_section'],$settings_section['id_section'] == $setting['id_section']);}?>><?php echo $settings_section['section_name'];?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="setting_different_values" <?php echo (!empty($setting))?set_checkbox('setting_different_values', '',$setting['setting_different_values'] == 1):''?>>
                        Добавить отдельно значения для разных языков
                    </label>
                </div>
                <div id="setting-values">
                    <?php if(!empty($setting)){?>
                        <?php if($setting['setting_different_values'] == 1){?>
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
                                    </li>
                                    <li class="">
                                        <a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
                                    </li>
                                    <li class="">
                                        <a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="form_ro">
                                        <div class="form-group">
                                            <label>Значение</label>
                                            <input type="text" placeholder="Значение" class="form-control" name="text_ro" value="<?php if(!empty($setting)){echo $setting['setting_value_ro'];}?>">
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="form_ru">
                                        <div class="form-group">
                                            <label>Значение</label>
                                            <input type="text" placeholder="Значение" class="form-control" name="text_ru" value="<?php if(!empty($setting)){echo $setting['setting_value_ru'];}?>">
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="form_en">
                                        <div class="form-group">
                                            <label>Значение</label>
                                            <input type="text" placeholder="Значение" class="form-control" name="text_en" value="<?php if(!empty($setting)){echo $setting['setting_value_en'];}?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-content -->
                            </div>
                        <?php } else{?>
                            <div class="form-group">
                                <label>Значение</label>
                                <input type="text" placeholder="Значение" class="form-control" name="text" value="<?php echo $setting['setting_value_ro'];?>">
                            </div>
                        <?php }?>
                    <?php } else{?>
                        <div class="form-group">
                            <label>Значение</label>
                            <input type="text" placeholder="Значение" class="form-control" name="text" value="">
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($setting)){?>
                    <input type="hidden" name="id_setting" value="<?php echo $setting['id_setting'];?>">
                <?php }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_setting" data-action="<?php if(!empty($setting)){?>edit_setting<?php } else{?>add_setting<?php }?>">Сохранить</button>
            </div>
        </form>
        <div id="same_value" style="display:none;">
            <div class="form-group">
                <label>Значение</label>
                <input type="text" placeholder="Значение" class="form-control" name="text" value="<?php if(!empty($setting)){echo $setting['setting_value_ro'];}?>">
            </div>
        </div>
        <div id="different_values" style="display:none;">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
                    </li>
                    <li class="">
                        <a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
                    </li>
                    <li class="">
                        <a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="form_ro">
                        <div class="form-group">
                            <label>Значение</label>
                            <input type="text" placeholder="Значение" class="form-control" name="text_ro" value="<?php if(!empty($setting)){echo $setting['setting_value_ro'];}?>">
                        </div>
                    </div>
                    <div class="tab-pane" id="form_ru">
                        <div class="form-group">
                            <label>Значение</label>
                            <input type="text" placeholder="Значение" class="form-control" name="text_ru" value="<?php if(!empty($setting)){echo $setting['setting_value_ru'];}?>">
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="form_en">
                        <div class="form-group">
                            <label>Значение</label>
                            <input type="text" placeholder="Значение" class="form-control" name="text_en" value="<?php if(!empty($setting)){echo $setting['setting_value_en'];}?>">
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('input[name="setting_different_values"]').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '20%'
		}).on('ifChecked', function(event){
            $('#setting-values').html($('#different_values').html());
        }).on('ifUnchecked', function(event){
            $('#setting-values').html($('#same_value').html());
        });
    });
    var manage_setting = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_settings_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>