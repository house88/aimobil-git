<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools">
				<a href="#" class="btn btn-box-tool call-function" data-callback="toggle_dt_filter" data-action="open">
					<i class="fa fa-filter"></i> Фильтр
				</a>
				<a href="#" data-href="<?php echo base_url('admin/popup_forms/add_setting');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить настройку
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_title">Название</th>
						<th class="dt_text_ro">Значение RO</th>
						<th class="dt_actions"></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<?php $this->load->view('admin/modules/settings/filter');?>
<script>
	var dtTable, dtFilter;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/ajax_operations/settings_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100000,
			"aoColumnDefs": [
				{ "sClass": "mnw-20pr text-left vam_i", "aTargets": ["dt_title"], "mData": "dt_title" , "bSortable": false},
				{ "sClass": "w-20pr text-left vam_i", "aTargets": ["dt_text_ro"], "mData": "dt_text_ro" , "bSortable": false},
				{ "sClass": "w-80 text-center vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						use_cookie: true,
						cookie_page: 'translations',
						callBack: function(){ 
							$.cookie( 'cookie_page', 'translations');
							$.cookie( '_dtFilters', $.param(dtFilter.getDTFilter()));
							dtTable.fnDraw(); 
						}
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						if(data.mess_type == 'info'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
				}
				
				init_logo_upload();
			}
		});
	});

	function manage_settings_callback(){
		dtTable.fnDraw(false);
	}

	function init_logo_upload(){
		$('#select_logo_1').fileupload({
			url: base_url+'admin/ajax_operations/logo_1',
			dataType: 'json',
			beforeSend: function(){},
			done: function (e, data) {
				if(data.result.mess_type == 'success'){
					manage_settings_callback();
				}

				systemMessages( data.result.message, data.result.mess_class );
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#select_logo_2').fileupload({
			url: base_url+'admin/ajax_operations/logo_2',
			dataType: 'json',
			beforeSend: function(){},
			done: function (e, data) {
				if(data.result.mess_type == 'success'){
					manage_settings_callback();
				}
				
				systemMessages( data.result.message, data.result.mess_class );
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	}
</script>
