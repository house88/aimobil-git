<div class="wr-filter-admin-panel filters-container">
	<div class="filter-admin-panel">
		<div class="title-b">
            Фильтры
            <a href="#" class="fa fa-times pull-right call-function" data-callback="toggle_dt_filter" data-action="close"></a>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Поиск</label>
                    <div class="input-group">
                        <input type="text" name="keywords" class="dt_filter form-control" value="" data-title="Поиск" placeholder="Что искать?">
                        <span class="input-group-btn">
                            <a class="dt-filter-apply dt-filter-apply-buttons btn btn-default btn-flat"><i class="fa fa-search"></i></a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Модуль</label>
                    <select name="id_section" class="form-control dt_filter" data-title="Модуль">
                        <option value="">Выберите модуль</option>
                        <?php foreach($settings_sections as $settings_section){?>
                            <option value="<?php echo $settings_section['id_section'];?>"><?php echo $settings_section['section_name'];?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
        </div>

		<div class="wr-filter-list clearfix mt-10 "><ul class="filter-plugin-list"></ul></div>
	</div>

	<div class="wr-hidden call-function" data-callback="toggle_dt_filter" data-action="close" style="display: none;"></div>
</div>