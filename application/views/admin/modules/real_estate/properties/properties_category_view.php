<?php if(!empty($properties)){?>
    <div class="row">            
        <?php foreach($properties as $property){?>
            <?php $selected_property = (isset($selected_properties[$property['id_property']]))?$selected_properties[$property['id_property']]:FALSE;?>
            <div class="col-xs-4">
                <div class="form-group">
                    <label><?php echo $property['property_title_ru'];?></label>
                    <?php $property_values = json_decode($property['property_values'], true);?>
                    <?php if($property['property_type'] == 'multiselect'){?>
                        <div class="input-group w-100pr">
                            <select name="properties[<?php echo $property['id_property'];?>][value]" class="form-control">
                                <option value="">Выберите значение</option>
                                <?php foreach($property_values as $property_value){?>
                                    <option value="<?php echo $property_value['id_value'];?>" <?php if($selected_property){echo set_select('properties['.$property['id_property'].'][value]', $property_value['id_value'], $selected_property['property_value'] == $property_value['id_value']);}?>><?php echo $property_value['title_ru'];?></option>                                        
                                <?php }?>
                            </select>
                            <?php if($property['property_values_has_suffix']){?>
                                <span class="input-group-addon pt-5 pb-5">
                                    <label style="display:flex;align-items:center;margin-bottom:0;">
                                        <input class="property_sufix" type="checkbox" name="properties[<?php echo $property['id_property'];?>][sufix]" <?php if($selected_property){echo set_checkbox('properties['.$property['id_property'].'][sufix]', '', $selected_property['property_value_sufix'] == 1);}?>>
                                        <span class="pl-10">+ гостиная</span>
                                    </label>
                                </span>
                            <?php }?>
                        </div>
                    <?php }?>
                    <?php if($property['property_type'] == 'range'){?>
                        <?php $property_settings = json_decode($property['property_settings'], true);?>
                        <?php if(isset($property_settings['repeat_values']) && $property_settings['repeat_values']){?>
                            <?php $property_values = json_decode($property['property_values'], true);?>
                            <div class="input-group w-100pr">
                                <select name="properties[<?php echo $property['id_property'];?>][value]" class="form-control">
                                    <?php foreach($property_values as $property_value){?>
                                        <option value="<?php echo $property_value;?>" <?php if($selected_property){echo set_select('properties['.$property['id_property'].'][value]', $property_value, $selected_property['property_value'] == $property_value);}?>><?php echo $property_value;?></option>
                                    <?php }?>
                                </select>
                                <span class="input-group-addon"><?php echo $property_settings['unit']['title_ru'];?> из</span>
                                <select name="properties[<?php echo $property['id_property'];?>][value_max]" class="form-control">
                                    <?php foreach($property_values as $property_value){?>
                                        <option value="<?php echo $property_value;?>" <?php if($selected_property){echo set_select('properties['.$property['id_property'].'][value]', $property_value, $selected_property['property_value_max'] == $property_value);}?>><?php echo $property_value;?></option>
                                    <?php }?>
                                </select>
                            </div>
                        <?php } else{?>
                            <div class="input-group w-100pr">
                                <input class="form-control" placeholder="0" name="properties[<?php echo $property['id_property'];?>][value]" value="<?php if($selected_property){echo $selected_property['property_value'];}?>">
                                <span class="input-group-addon"><?php echo $property_settings['unit']['title_ru'];?></span>
                            </div>
                        <?php }?>
                    <?php }?>
                </div>
            </div>            
        <?php }?>
    </div>
<?php }?>