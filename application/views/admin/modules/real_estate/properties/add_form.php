<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Добавление свойства</h4>
        </div>
        <form id="manage_property_form">
            <div class="modal-body">
				<div class="form-group">
					<label>Категорий</label>
					<select name="categories[]" class="form-control" multiple>
						<?php foreach($categories as $category){?>
							<option value="<?php echo $category['id_category'];?>" data-subtext="<?php echo ($category['category_type'] == 'rent')?'Аренда':'Продажа';?>"><?php echo $category['category_title_ru'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<label>Название</label>
					<div class="input-group input-group-sm mb-10">
						<input class="form-control" placeholder="Romana" name="title_ro">
						<span class="input-group-addon">&mdash;</span>
						<input class="form-control" placeholder="Русский" name="title_ru">
						<span class="input-group-addon">&mdash;</span>
						<input class="form-control" placeholder="English" name="title_en">
					</div>
					<p class="help-block">Название не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label class="w-100pr">
						<input type="checkbox" name="property_in_filter" class="flat-red nice-input" checked>
						Показать в фильтре					
					</label>
					<label class="w-100pr">
						<input type="checkbox" name="property_on_item" class="flat-red nice-input" checked>
						Показать на странице обьекта
					</label>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label>Тип значений</label>
							<select name="type_property" class="form-control">
								<option value="">Выберите тип значений</option>
								<option value="multiselect" data-template="select">Много значений, множественный выбор</option>
								<option value="range" data-template="range">Числовое значение</option>
								<option value="price" data-template="price">Ценовые значения</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label>Порядковый номер</label>
							<input type="text" name="property_weight" class="form-control" placeholder="Порядковый номер" value="0">
						</div>
					</div>
				</div>
				<div style="display:none;" id="property_aditional"></div>
				<div class="form-group">
					<label>Значения</label>
					<div class="mb-10" id="property_values"></div>
					<span class="btn btn-default btn-sm call-function" data-callback="add_value">Добавить значение</span>
				</div>
            </div>
            <div class="modal-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">Моб. плитка значений</div>
								<select name="property_values_cols" class="form-control">
									<option value="2">2</option>
									<option value="4">4</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						<button type="button" class="btn btn-success call-function" data-callback="manage_property" data-action="add_property">Сохранить</button>
					</div>
				</div>
            </div>
        </form>
		<div id="template_select" style="display:none;">
			<div class="input-group input-group-sm mb-10">
				<input class="form-control" placeholder="Romana" name="property_values[ro][]">
				<span class="input-group-addon">&mdash;</span>
				<input class="form-control" placeholder="Русский" name="property_values[ru][]">
				<span class="input-group-addon">&mdash;</span>
				<input class="form-control" placeholder="English" name="property_values[en][]">
				<div class="input-group-btn">
					<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
					<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
					<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_value"><i class="fa fa-trash"></i></span>
				</div>
			</div>
		</div>
		<div id="template_range" style="display:none;">
			<div class="input-group input-group-sm mb-10">
				<input class="form-control" placeholder="0" name="property_values[]">
				<div class="input-group-btn">
					<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
					<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
					<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_value"><i class="fa fa-trash"></i></span>
				</div>
			</div>
		</div>
		<div id="template_price" style="display:none;">
			<?php $currencies = Modules::run('currency/_get_currencies', array('active' => 1));?>
			<?php if(!empty($currencies)){?>
				<div class="input-group input-group-sm mb-10">
					<?php foreach($currencies as $currency){?>
						<span class="input-group-addon"><?php echo $currency['currency_code'];?></span>
						<input class="form-control" placeholder="0" name="property_values[<?php echo $currency['currency_code'];?>][]">				
					<?php }?>
					<div class="input-group-btn">
						<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_value"><i class="fa fa-trash"></i></span>
					</div>
				</div>			
			<?php }?>
		</div>
		<div id="template_property_unit" style="display:none;">
			<div class="form-group">
				<label>Единица измерения</label>
				<div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#property_unit_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#property_unit_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#property_unit_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="property_unit_ro">
							<div class="form-group">
								<label>Название <small class="text-muted">в единственном числе</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[ro]" value="">
							</div>
							<div class="form-group">
								<label>Название <small class="text-muted">в множественном числе</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[plural_ro]" value="">
							</div>
						</div>
						<div class="tab-pane" id="property_unit_ru">
							<div class="form-group">
								<label>Название <small class="text-muted">если число оканчивается на 1</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[ru]" value="">
							</div>
							<div class="form-group">
								<label>Название <small class="text-muted">если число оканчивается на 2,3,4</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[plural_ru][2_4]" value="">
							</div>
							<div class="form-group">
								<label>Название <small class="text-muted">если число >= 5 или принадлежит отрезку [11;19]</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[plural_ru][5_]" value="">
							</div>
						</div>
						<div class="tab-pane" id="property_unit_en">
							<div class="form-group">
								<label>Название <small class="text-muted">в единственном числе</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[en]" value="">
							</div>
							<div class="form-group">
								<label>Название <small class="text-muted">в множественном числе</small></label>
								<input class="form-control" placeholder="Название" name="property_unit[plural_en]" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>
					<input type="checkbox" name="property_unit[show]" class="flat-red">
					Отображать единицу измерения на странице товара
				</label>
			</div>
		</div>
		<div id="template_property_repeat_values" style="display:none;">
			<div class="form-group">
				<label>
					<input type="checkbox" name="property_repeat_values" class="flat-red">
					Повторить значения, для выбора максимального
				</label>
			</div>
		</div>
		<div id="template_property_floating_values" style="display:none;">
			<div class="form-group">
				<label>
					<input type="checkbox" name="property_floating_values" class="flat-red">
					Использовать дробные значения
				</label>
			</div>
		</div>
    </div>
</div>
<script>
    $(function(){
		$('select[name="type_property"]').on('change', function(){
			var template_property = $( 'select[name="type_property"] option:selected' ).data('template');
			$('#property_aditional').html('').hide();
			switch (template_property) {
				case 'range':
					$('#property_aditional').html($('#template_property_unit').html() + $('#template_property_repeat_values').html()).show();
					$('input[name="property_repeat_values"]').iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%'
					});					
					$('input[name="property_unit[show]"]').iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%'
					});					
				break;
				case 'select':
				case 'multiselect':
					$('#property_aditional').html($('#template_property_unit').html()+$('#template_property_floating_values').html()).show();
					$('input[name="property_floating_values"]').iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%'
					});				
					$('input[name="property_unit[show]"]').iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%'
					});				
				break;
			}
			
			$('#property_values').html($('#template_'+template_property).html());
		});

		$('select[name="categories[]"]').selectpicker({
			style: 'btn-white',
			button_title: false
		});

		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '20%'
		});
	});

	var add_value = function(btn){
		var $this = $(btn);
		var type_property = $('select[name="type_property"] option:selected').data('template');
		switch (type_property) {
			case 'select':
			case 'multiselect':
				var template = $('#template_select').html();				
			break;
			case 'range':
				var template = $('#template_range').html();
			break;
			case 'price':
				var template = $('#template_price').html();
			break;
		}
		$('#property_values').append(template);
	}

	var delete_value = function(btn){
		$(btn).closest('.input-group').remove();
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.input-group');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

    var manage_property = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/real_estate/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_property_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>