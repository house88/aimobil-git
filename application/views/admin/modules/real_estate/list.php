<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/real_estate/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить обьект
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="row filters-container filters-container-page">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<div class="input-group input-group-sm">
							<input type="text" name="keywords" class="dt_filter form-control" value="" data-title="Поиск" placeholder="Что искать?">
							<span class="input-group-btn">
								<a class="dt-filter-apply dt-filter-apply-buttons btn btn-default btn-flat"><i class="fa fa-search"></i></a>
							</span>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="manager" class="form-control dt_filter input-sm" data-title="Менеджер">
							<option value="">Выберите менеджера</option>
							<?php $users = Modules::run('users/_get_users', array('user_banned' => 0, 'group_alias' => array('manager')));?>
							<?php foreach($users as $_manager){?>
								<option value="<?php echo $_manager['id_user'];?>"><?php echo $_manager[lang_column('user_name')];?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="manager_created" class="form-control dt_filter input-sm" data-title="Контент Менеджер">
							<option value="">Выберите контент менеджера</option>
							<?php $users = Modules::run('users/_get_users', array('user_banned' => 0, 'group_alias' => array('admin', 'content_manager')));?>
							<?php foreach($users as $_manager){?>
								<option value="<?php echo $_manager['id_user'];?>"><?php echo $_manager[lang_column('user_name')];?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="row row-small">
						<?php foreach($parent_locations as $parent_location){?>
							<div class="col-xs-6">
								<div class="form-group">
									<select name="locations_<?php echo $parent_location['id_location'];?>" class="filter-multiselect form-control input-sm dt_filter" data-live-search="true" data-style="btn-flat btn-white btn-sm" title="<?php echo $parent_location[lang_column('location_name')];?>" data-title="<?php echo $parent_location[lang_column('location_name')];?>" multiple>
										<?php foreach($children_locations[$parent_location['id_location']] as $children_location){?>
											<option value="<?php echo $children_location['id_location'];?>"><?php echo $children_location[lang_column('location_name')];?></option>
										<?php }?>
									</select>
								</div>
							</div>
						<?php }?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="form-group">
						<div class="btn-group btn-group-sm w-100pr">
							<a href="#" class="display-n dt_filter" data-name="show_on_home" title="На главной" data-title="Показать на главной" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
							<a href="#" class="display-n dt_filter" data-name="best" title="Самый лучший объект" data-title="Самый лучший объект" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
							<a href="#" class="display-n dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Показать Ипотека/месяц" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
							<div class="btn-toolbar">
								<div class="btn-group btn-group-sm mr-5">
									<a href="#" class="btn btn-success btn-flat dt_filter" data-name="show_on_home" title="На главной" data-title="Признак Показать на главной" data-value-text="Да" data-value="1"><span class="fa fa-home"></span></a>
									<a href="#" class="btn btn-default btn-flat dt_filter" data-name="show_on_home" title="На главной" data-title="Нет признака Показать на главной" data-value-text="Нет" data-value="0"><span class="fa fa-home"></span></a>
								</div>
								<div class="btn-group btn-group-sm mr-5">
									<a href="#" class="btn btn-primary btn-flat dt_filter" data-name="best" title="Самый лучший объект" data-title="Признак Самый лучший объект" data-value-text="Да" data-value="1"><span class="fa fa-star"></span></a>
									<a href="#" class="btn btn-default btn-flat dt_filter" data-name="best" title="Самый лучший объект" data-title="Нет признака Самый лучший объект" data-value-text="Нет" data-value="0"><span class="fa fa-star"></span></a>
								</div>
								<div class="btn-group btn-group-sm">
									<a href="#" class="btn btn-warning btn-flat dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Признак Ипотека/месяц" data-value-text="Да" data-value="1"><span class="fa fa-eur"></span></a>
									<a href="#" class="btn btn-default btn-flat dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Нет признака Ипотека/месяц" data-value-text="Нет" data-value="0"><span class="fa fa-eur"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
					<div class="form-group">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Цена от</span>
							<input type="text" name="price_from" class="dt_filter form-control" value="" data-title="Цена от" placeholder="0">
							<span class="input-group-addon">до</span>
							<input type="text" name="price_to" class="dt_filter form-control" value="" data-title="Цена до" placeholder="0">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="offer_type" class="form-control dt_filter input-sm" data-title="Тип предложения">
							<option value="">Тип</option>
							<option value="sale">Продажа</option>                                        
							<option value="rent">Аренда</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="category" class="form-control dt_filter input-sm" data-title="Тип недвижимости">
							<option value="">Категория</option>
							<?php foreach($categories as $category){?>
								<option value="<?php echo $category['id_category'];?>" data-type="<?php echo $category['category_type'];?>" style="display:none;"><?php echo $category['category_title_ru'];?></option>                                        
							<?php }?>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="discount_type" class="form-control dt_filter input-sm" data-title="Тип скидки">
							<option value="">Скидка</option>
							<option value="no">Без скидки</option>
							<option value="all">Все типы скидок</option>
							<option value="simple">Скидка, %</option>
							<option value="new_price">Новая цена, (<?php echo $default_currency['currency_symbol'];?>)</option>
							<option value="by_m2">Скидка __<?php echo $default_currency['currency_symbol'];?>/м2</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<div class="form-group">
						<select name="re_active" class="form-control dt_filter input-sm" data-title="Признак Активен">
							<option value="">Признак Активен</option>
							<option value="1">Да</option>
							<option value="0">Нет</option>
						</select>
					</div>
				</div>
			</div>
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_id">#</th>
						<th class="dt_photo">Фото</th>
						<th class="dt_name">Название</th>
						<th class="dt_price">Цена, &euro;</th>
						<th class="dt_options">Опции</th>
						<th class="dt_date">Дата</th>
						<th class="dt_actions">Операций</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<?php //$this->load->view('admin/modules/real_estate/filter');?>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/real_estate/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam_i", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "w-50 text-center vam_i", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "text-left vam_i", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-105 text-right vam_i", "aTargets": ["dt_price"], "mData": "dt_price"},
				{ "sClass": "w-105 text-right vam_i", "aTargets": ["dt_options"], "mData": "dt_options" , "bSortable": false },
				{ "sClass": "w-120 text-center vam_i", "aTargets": ["dt_date"], "mData": "dt_date"},
				{ "sClass": "w-110 text-right vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						use_cookie: true,
						cookie_page: 'real_estate',
						callBack: function(){ 
							$.cookie( 'cookie_page', 'real_estate');
							$.cookie( '_dtFilters', $.param(dtFilter.getDTFilter()));
							dtTable.fnDraw(); 
						},
						onSet: function(filterObj){
							if(filterObj.name == 'offer_type'){
								dtFilter.removeFilter('category');
								$('select[name="category"] option').each(function( index ) {
									if(filterObj.value == ''){
										$(this).prop('selected', false).hide();
									}

									if(filterObj.value != '' && filterObj.value == $(this).data('type') || $(this).val() == ''){
										$(this).show();
									}
								});
							}
						},
						onDelete: function(filterObj){
							if(filterObj.name == 'locations_1' || filterObj.name == 'locations_2'){
								$('select[name="'+filterObj.name+'"]').selectpicker('deselectAll');
							}

							if(filterObj.name == 'offer_type'){
								dtFilter.removeFilter('category');
								$('select[name="category"] option').each(function( index ) {
									if(filterObj.value == ''){
										$(this).prop('selected', false).hide();
									}

									if($(this).val() == ''){
										$(this).show();
									}
								});
							}
						}
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
				$('.filter-multiselect').selectpicker('refresh');
			}
		});

		$('.filter-multiselect').selectpicker();
		$('select[name="offer_type"]').on('change', function(){
			var type = $(this).val();
			$('select[name="category"] option').each(function( index ) {
				$(this).prop('selected', false).hide();
				if(type != '' && type == $(this).data('type') || $(this).val() == ''){
					$(this).show();
				}
			});
		});
	});
	var delete_action = function(btn){
		var $this = $(btn);
		var real_estate = $this.data('real-estate');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/real_estate/ajax_operations/delete',
			data: {real_estate:real_estate},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var change_status = function(btn){
		var $this = $(btn);
		var real_estate = $this.data('real-estate');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/real_estate/ajax_operations/change_status',
			data: {real_estate:real_estate},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var change_sold = function(btn){
		var $this = $(btn);
		var real_estate = $this.data('real-estate');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/real_estate/ajax_operations/change_sold',
			data: {real_estate:real_estate},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var change_marketing = function(btn){
		var $this = $(btn);
		var marketing = $this.data('marketing');
		var real_estate = $this.data('real-estate');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/real_estate/ajax_operations/change_marketing',
			data: {real_estate:real_estate,marketing:marketing},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
