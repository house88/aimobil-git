<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Сделать объект неактивным</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
				<div class="form-group">
					<label>Укажите причину</label>
					<textarea name="reason" class="form-control" rows="5"></textarea>
				</div>
            </div>
            <div class="modal-footer">
				<input type="hidden" name="real_estate" value="<?php echo $real_estate['id_real_estate'];?>">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<button type="button" class="btn btn-success call-function" data-callback="change_sold_from">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var change_sold_from = function(btn){
        var $this = $(btn);
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/real_estate/ajax_operations/change_sold',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
					$form.trigger( 'reset' );
					dtTable.fnDraw(false);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлен. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>