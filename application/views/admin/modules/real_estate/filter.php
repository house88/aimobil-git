<div class="wr-filter-admin-panel filters-container">
	<div class="filter-admin-panel">
		<div class="title-b">
            Фильтры
            <a href="#" class="fa fa-times pull-right call-function" data-callback="toggle_dt_filter" data-action="close"></a>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<div class="form-group">
					<label>Поиск</label>
					<div class="input-group">
						<input type="text" name="keywords" class="dt_filter form-control" value="" data-title="Поиск" placeholder="Что искать?">
						<span class="input-group-btn">
							<a class="dt-filter-apply dt-filter-apply-buttons btn btn-default btn-flat"><i class="fa fa-search"></i></a>
						</span>
					</div>
				</div>
                <div class="form-group">
                    <label>Тип предложения</label>
					<select name="offer_type" class="form-control dt_filter" data-title="Тип предложения">
						<option value="">Выберите тип предложения</option>
						<option value="sale">Продажа</option>                                        
						<option value="rent">Аренда</option>
					</select>
                </div>
				<div class="form-group">
					<label>Тип недвижимости</label>
					<select name="category" class="form-control dt_filter" data-title="Тип недвижимости">
						<option value="">Выберите тип недвижимости</option>
						<?php foreach($categories as $category){?>
							<option value="<?php echo $category['id_category'];?>"><?php echo $category['category_title_ru'];?></option>                                        
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<label>Тип скидки</label>
					<div class="input-group w-100pr">
						<select name="discount_type" class="form-control dt_filter" data-title="Тип скидки">
							<option value="">Выберите тип скидки</option>
							<option value="no">Без скидки</option>
							<option value="all">Все типы скидок</option>
							<option value="simple">Скидка, %</option>
							<option value="new_price">Новая цена, (<?php echo $default_currency['currency_symbol'];?>)</option>
							<option value="by_m2">Скидка __<?php echo $default_currency['currency_symbol'];?>/м2</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label>Опции</label>
					<div class="btn-group w-100pr">
						<a href="#" class="display-n dt_filter" data-name="show_on_home" title="На главной" data-title="Показать на главной" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
						<a href="#" class="display-n dt_filter" data-name="best" title="Самый лучший объект" data-title="Самый лучший объект" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
						<a href="#" class="display-n dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Показать Ипотека/месяц" data-value-text="" data-value="" data-default="true"><span class="fa fa-times"></span></a>
						<div class="btn-toolbar">
							<div class="btn-group mb-5 mr-5 btn-group-xs">
								<a href="#" class="btn btn-success dt_filter" data-name="show_on_home" title="На главной" data-title="Признак Показать на главной" data-value-text="Да" data-value="1"><span class="fa fa-home"></span></a>
								<a href="#" class="btn btn-default dt_filter" data-name="show_on_home" title="На главной" data-title="Нет признака Показать на главной" data-value-text="Нет" data-value="0"><span class="fa fa-home"></span></a>
							</div>
							<div class="btn-group mb-5 mr-5 btn-group-xs">
								<a href="#" class="btn btn-primary dt_filter" data-name="best" title="Самый лучший объект" data-title="Признак Самый лучший объект" data-value-text="Да" data-value="1"><span class="fa fa-star"></span></a>
								<a href="#" class="btn btn-default dt_filter" data-name="best" title="Самый лучший объект" data-title="Нет признака Самый лучший объект" data-value-text="Нет" data-value="0"><span class="fa fa-star"></span></a>
							</div>
							<div class="btn-group mb-5 mr-5 btn-group-xs">
								<a href="#" class="btn btn-warning dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Признак Ипотека/месяц" data-value-text="Да" data-value="1"><span class="fa fa-eur"></span></a>
								<a href="#" class="btn btn-default dt_filter" data-name="mortgage" title="Ипотека/месяц" data-title="Нет признака Ипотека/месяц" data-value-text="Нет" data-value="0"><span class="fa fa-eur"></span></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>

		<div class="wr-filter-list clearfix mt-10 "><ul class="filter-plugin-list"></ul></div>
	</div>

	<div class="wr-hidden call-function" data-callback="toggle_dt_filter" data-action="close" style="display: none;"></div>
</div>