<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/real_estate/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить обьект
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">
			<div class="box-body">
				<div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_filters" data-toggle="tab" aria-expanded="false">Свойства</a>
						</li>
						<li class="">
							<a href="#form_general" data-toggle="tab" aria-expanded="true">Общая информация</a>
						</li>
						<li class="">
							<a href="#form_photos" data-toggle="tab" aria-expanded="false">Фото</a>
						</li>
						<li class="">
							<a href="#form_location" data-toggle="tab" aria-expanded="false">Местоположение</a>
						</li>
						<?php if($this->lauth->is_admin_only()){?>
							<li class="">
								<a href="#form_owner" data-toggle="tab" aria-expanded="false">Владелец</a>
							</li>
						<?php }?>
						<li class="">
							<a href="#form_999" data-toggle="tab" aria-expanded="false">999.md</a>
						</li>
						<?php if($real_estate['real_estate_sold'] == 1 || $real_estate['real_estate_active'] == 0){?>
							<li class="">
								<a href="#form_hide_reason" data-toggle="tab" aria-expanded="false">Почему не активен?</a>
							</li>
						<?php }?>
						<li class="pull-right">
							<button type="submit" class="btn btn-primary btn-flat">Сохранить</button>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane" id="form_general">
							<?php if($this->lauth->have_right('manage_real_estate_manager')){?>
								<div class="form-group">
									<label>Менеджер</label>
									<select name="manager" class="form-control">
										<?php $users = Modules::run('users/_get_users', array('user_banned' => 0, 'group_alias' => array('manager')));?>
										<?php foreach($users as $_manager){?>
											<option value="<?php echo $_manager['id_user'];?>" <?php echo set_select('manager', $_manager['id_user'], $_manager['id_user'] == $real_estate['id_manager']);?>><?php echo $_manager[lang_column('user_name')];?></option>
										<?php }?>
									</select>
								</div>
							<?php }?>
							<div class="nav-tabs-custom mb-0">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
									</li>
									<li class="">
										<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
									</li>
									<li class="">
										<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="form_ro">
										<div class="form-group">
											<label>Краткое название</label>
											<input class="form-control" placeholder="Краткое название" name="title_ro" value="<?php echo clean_output($real_estate['real_estate_title_ro']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Полное название</label>
											<input class="form-control" placeholder="Полное название" name="full_title_ro" value="<?php echo $real_estate['real_estate_full_title_ro'];?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_ro"><?php echo $real_estate['real_estate_text_ro'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo $real_estate['mk_ro'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo $real_estate['mk_ro'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_ru">
										<div class="form-group">
											<label>Краткое название</label>
											<input class="form-control" placeholder="Краткое название" name="title_ru" value="<?php echo clean_output($real_estate['real_estate_title_ru']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Полное название</label>
											<input class="form-control" placeholder="Полное название" name="full_title_ru" value="<?php echo $real_estate['real_estate_full_title_ru'];?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_ru"><?php echo $real_estate['real_estate_text_ru'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo $real_estate['mk_ru'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo $real_estate['md_ru'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_en">
										<div class="form-group">
											<label>Краткое название</label>
											<input class="form-control" placeholder="Краткое название" name="title_en" value="<?php echo clean_output($real_estate['real_estate_title_en']);?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Полное название</label>
											<input class="form-control" placeholder="Полное название" name="full_title_en" value="<?php echo $real_estate['real_estate_full_title_en'];?>">
											<p class="help-block">Название не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Текст</label>
											<textarea class="description" name="description_en"><?php echo $real_estate['real_estate_text_en'];?></textarea>
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo $real_estate['mk_en'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
										<div class="form-group">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo $real_estate['md_en'];?>">
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form_photos">
							<div class="form-group">
								<label>Фото <small>(Мин. ширина: 680px, высота: 452px, не превышать 10Мб)</small></label>
								<div class="clearfix"></div>
								<span class="btn btn-default btn-sm btn-file pull-left mb-15">
									<i class="fa fa-picture-o"></i>
									Добавить фото <input id="select_photo" type="file" name="userfile" multiple>
								</span>
								<div class="clearfix"></div>
								<div id="uploaded_files" class="files in-row-multiple sorting-files">
									<?php $photos = json_decode($real_estate['real_estate_photos'], true);?>
									<?php if(!empty($photos)){?>
										<?php foreach($photos as $photo){?>
											<div class="user-image-thumbnail-wr">
												<div class="user-image-thumb">
													<img class="img-thumbnail" src="<?php echo site_url('files/real_estate/'.$real_estate['id_real_estate'].'/'.$photo);?>"/>
												</div>

												<div class="btn-group btn-remove">
													<?php $is_main_photo = $real_estate['real_estate_photo'] == $photo;?>
													<a href="#" class="btn <?php if($is_main_photo){?>btn-success<?php } else{?>btn-default<?php }?> btn-flat btn-xs btn-main_photo call-function" data-callback="main_photo" data-title="Сделать главной" data-photo="<?php echo $photo;?>">
														<span class="fa <?php if($is_main_photo){?>fa-check-square-o<?php } else{?>fa-square-o<?php }?>"></span>
													</a>
													<span class="btn btn-default btn-flat btn-xs call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
													<span class="btn btn-default btn-flat btn-xs call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
													<a href="#" class="btn btn-danger btn-flat btn-xs confirm-dialog" data-callback="remove_existent_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $photo;?>">
														<span class="fa fa-trash"></span>
													</a>
												</div>


												<input type="hidden" name="uploaded_files[]" value="<?php echo $photo;?>">
											</div>
										<?php }?>
									<?php }?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="tab-pane active" id="form_filters">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<?php $_content_manager = Modules::run('users/_get_user', $real_estate['id_manager_created']);?>
										<label>Контент менеджер</label>
										<input type="text" class="form-control" value="<?php echo @$_content_manager->user_name_ru;?>" readonly>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<?php $default_currency = Modules::run('currency/_get_default_currency');?>
										<label>Цена, (<?php echo $default_currency['currency_symbol'];?>)</label>
										<input type="text" class="form-control" name="price" placeholder="0.00" value="<?php echo $real_estate['real_estate_price'];?>">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<label>Скидка</label>
										<div class="input-group w-100pr">
											<select name="discount[type]" class="form-control">
												<option value="no" <?php echo set_select('discount[type]', 'no', $real_estate['real_estate_discount_type'] == 'no');?>>Нет скидки</option>
												<option value="simple" <?php echo set_select('discount[type]', 'simple', $real_estate['real_estate_discount_type'] == 'simple');?>>Скидка, %</option>
												<option value="new_price" <?php echo set_select('discount[type]', 'new_price', $real_estate['real_estate_discount_type'] == 'new_price');?>>Новая цена, (<?php echo $default_currency['currency_symbol'];?>)</option>
												<option value="by_m2" <?php echo set_select('discount[type]', 'by_m2', $real_estate['real_estate_discount_type'] == 'by_m2');?>>Скидка __<?php echo $default_currency['currency_symbol'];?>/м2</option>
											</select>
											<div class="input-group-addon" <?php if(in_array($real_estate['real_estate_discount_type'], array('no','simple'))){?>style="display:none;"<?php }?>>:</div>
											<input type="text" name="discount[value]" class="form-control" value="<?php echo $real_estate['real_estate_discount_value'];?>" <?php if(in_array($real_estate['real_estate_discount_type'], array('no','simple'))){?>style="display:none;"<?php }?>>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<label>Тип предложения</label>
										<select name="offer_type" class="form-control">
											<option value="sale" <?php echo set_select('offer_type', 'sale', $real_estate['real_estate_offer_type'] == 'sale');?>>Продажа</option>                                        
											<option value="rent" <?php echo set_select('offer_type', 'rent', $real_estate['real_estate_offer_type'] == 'rent');?>>Аренда</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<label>Тип недвижимости</label>
										<select name="category" class="form-control">
											<option value="<?php echo $category['id_category'];?>"><?php echo $category['category_title_ru'];?></option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<div class="form-group">
										<label>Признак “Эксклюзив”</label>
										<select name="real_estate_exclusive" class="form-control">
											<option value="0" <?php echo set_select('real_estate_exclusive', 0, $real_estate['real_estate_exclusive'] == 0);?>>Нет</option>
											<option value="1" <?php echo set_select('real_estate_exclusive', 1, $real_estate['real_estate_exclusive'] == 1);?>>Да</option>
										</select>
									</div>
								</div>
							</div>
							<div class="category-settings_wr">
								<?php $this->load->view('admin/modules/real_estate/category_settings_view');?>
							</div>
						</div>
						<div class="tab-pane" id="form_location">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<label>Тип</label>
										<select name="location_parent" class="form-control">
											<?php foreach($parent_locations as $parent_location){?>
												<option value="<?php echo $parent_location['id_location'];?>" data-text="<?php if($parent_location['location_in_title']){echo $parent_location[lang_column('location_name')];}?>" <?php echo set_select('location_parent', $parent_location['id_location'], $location['id_parent'] == $parent_location['id_location']);?>><?php echo $parent_location[lang_column('location_name')];?></option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<label>Населенный пункт / Сектор</label>
										<select name="location_child" class="form-control" disabled></select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<label>Дополнительно</label>
										<select name="location_child_additional[]" class="form-control selectpicker" disabled multiple title="Дополнительно..."></select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<label>Адрес</label>
									<input type="text" name="address" class="form-control" placeholder="Адрес" value="<?php echo $real_estate['real_estate_address'];?>">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<button class="btn btn-primary btn-flat mt-25 call-function" data-callback="map_search">Наити</button>
									</div>
								</div>
							</div>
							<input type="hidden" name="latitude" value="<?php echo $real_estate['real_estate_lat'];?>">
							<input type="hidden" name="longitude" value="<?php echo $real_estate['real_estate_lng'];?>">
							<div class="h-600 w-100pr" id="map-element"></div>
						</div>
						<?php if($this->lauth->is_admin_only()){?>
							<div class="tab-pane" id="form_owner">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>Имя</label>
											<input class="form-control" placeholder="Имя" name="owner_name" value="<?php echo $real_estate['real_estate_owner_name'];?>">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>Телефон</label>
											<input class="form-control" placeholder="Телефон" name="owner_phone" value="<?php echo $real_estate['real_estate_owner_phone'];?>">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>Email</label>
											<input class="form-control" placeholder="Email" name="owner_email" value="<?php echo $real_estate['real_estate_owner_email'];?>">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label>Адрес</label>
											<input class="form-control" placeholder="Адрес" name="owner_address" value="<?php echo $real_estate['real_estate_owner_address'];?>">
										</div>
									</div>
								</div>
							</div>
						<?php }?>
						<div class="tab-pane" id="form_999">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label class="pull-left lh-34 mb-0">
											<input type="checkbox" name="advert_api" class="flat-red nice-input" <?php echo set_checkbox('advert_api','', $real_estate['advert_on_999'] == 1);?>>
											Опубликовать / Обновить на 999.md
										</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<div class="form-group">
										<label>Статус</label>
										<select name="advert_status" class="form-control">
											<option value="public" <?php echo set_select('advert_status', 'public', 'public' == $real_estate['advert_status_999']);?>>Активный</option>
											<option value="private" <?php echo set_select('advert_status', 'private', 'private' == $real_estate['advert_status_999']);?>>Неактивный</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<div class="form-group">
										<label>Регион</label>
										<select name="region_999" class="form-control">
											<option value="">Выберите регион</option>
											<?php foreach($regions_999 as $region){?>
												<option value="<?php echo $region['id'];?>" <?php echo set_select('region_999', $region['id'], $region['id'] == $real_estate['id_region_999']);?>><?php echo $region['title'];?></option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<div class="form-group">
										<label>Населенный пункт</label>
										<select name="raion_999" class="form-control">
											<option value="">Выберите населенный пункт</option>
											<?php if(!empty($raions_999)){?>
												<?php foreach($raions_999 as $raion){?>
													<option value="<?php echo $raion['id'];?>" <?php echo set_select('raion_999', $raion['id'], $raion['id'] == $real_estate['id_raion_999']);?>><?php echo $raion['title'];?></option>
												<?php }?>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<div class="form-group">
										<label>Сектор</label>
										<select name="sector_999" class="form-control">
											<option value="">Выберите сектор</option>
											<?php if(!empty($sectors_999)){?>
												<?php foreach($sectors_999 as $sector){?>
													<option value="<?php echo $sector['id'];?>" <?php echo set_select('sector_999', $sector['id'], $sector['id'] == $real_estate['id_sector_999']);?>><?php echo $sector['title'];?></option>
												<?php }?>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Заголовок объявления</label>
										<input class="form-control" placeholder="Заголовок объявления" name="title_999" value="<?php echo $real_estate['real_estate_title_999'];?>">
									</div>
									<div class="form-group">
										<label>Текст</label>
										<textarea class="form-control" name="text_999" rows="10"><?php echo $real_estate['real_estate_text_999'];?></textarea>
									</div>
								</div>
							</div>
						</div>
						<?php if($real_estate['real_estate_sold'] == 1 || $real_estate['real_estate_active'] == 0){?>
							<div class="tab-pane" id="form_hide_reason">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="form-group">
											<label>Признак "Продан" включен?</label>
											<div class="form-control"><?php echo $real_estate['real_estate_sold'] == 1 ? 'Да':'Нет';?></div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="form-group">
											<label>Дата</label>
											<div class="form-control"><?php echo formatDate($real_estate['real_estate_sold_date']);?></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="form-group">
											<label>Объект "Активен"?</label>
											<div class="form-control"><?php echo $real_estate['real_estate_active'] == 1 ? 'Да':'Нет';?></div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="form-group">
											<label>Дата</label>
											<div class="form-control"><?php echo formatDate($real_estate['real_estate_active_date']);?></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
											<label>Причина</label>
											<div class="form-control">
												<?php if(!empty($real_estate['real_estate_not_active_reason'])){?>
													<?php echo $real_estate['real_estate_not_active_reason'];?>
												<?php } else{?>
													<div class="alert alert-info alert-dismissible">
														<h4><i class="icon fa fa-info"></i> Информация!</h4>
														Причина не указана.
													</div>
												<?php }?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php }?>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<input type="hidden" name="main_photo" value="<?php echo $real_estate['real_estate_photo'];?>">
				<input type="hidden" name="id_real_estate" value="<?php echo $real_estate['id_real_estate'];?>">
				<button type="submit" class="btn btn-primary btn-flat">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<?php 
	$sale_categories = array();
	$rent_categories = array();
	foreach ($categories as $category) {
		if($category['category_type'] == 'sale'){
			$sale_categories[] = array(
				'id_category' => $category['id_category'],
				'name' => $category['category_title_ru']
			);
		} else{
			$rent_categories[] = array(
				'id_category' => $category['id_category'],
				'name' => $category['category_title_ru']
			);
		}
	}
?>
<script>
	var categories = {
		'rent': <?php echo json_encode($rent_categories);?>,
		'sale': <?php echo json_encode($sale_categories);?>
	};

	var selected_category = intval('<?php echo $real_estate['id_category'];?>');
	var first_show_category = true;

	function _get_categories(){
		var category_type = $('select[name="offer_type"]').val();
		var options = ['<option value="">Выберите тип недвижимости</option>'];
		var current_categories = (category_type == 'rent')?categories.rent:categories.sale;
		$.each(current_categories, function(index, category){
			options.push('<option value="'+category.id_category+'" '+((selected_category == category.id_category && first_show_category)?'selected':'')+'>'+category.name+'</option>');
		});

		$('select[name="category"]').html(options.join(''));
	}

	var children_locations = <?php echo json_encode($children_locations)?>;
	var selected_location = <?php echo $real_estate['id_location'];?>;
	var selected_location_aditional = '<?php echo $real_estate['id_location_aditional'];?>';
	selected_location_aditional = selected_location_aditional.split(',');
	var max_files_upload_limit = 20;
	$(function(){
		'use strict';
		$('#uploaded_files').sortable();
		$('#select_photo').fileupload({
			url: base_url+'admin/real_estate/ajax_operations/upload_photo',
			dataType: 'json',
			beforeSend: function(){
				if($('#uploaded_files .user-image-thumbnail-wr').length >= max_files_upload_limit){
					systemMessages( 'Ошибка: Не более '+max_files_upload_limit+' фотографий.', 'error' );
					jqXHR.abort();
				}
			},
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					if($('#uploaded_files .user-image-thumbnail-wr').length >= max_files_upload_limit){
						systemMessages( 'Внимание: Не более 15 фотографий.', 'warning' );
						$('#uploaded_files').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+data.result.file.filename+'"/>');
					} else{
						var template = '<div class="user-image-thumbnail-wr">\
											<div class="user-image-thumb">\
												<img class="img-thumbnail" src="'+base_url+'files/real_estate/temp/'+data.result.file.filename+'"/>\
											</div>\
											<div class="btn-group btn-remove">\
												<a href="#" class="btn btn-default btn-flat btn-xs btn-main_photo call-function" data-callback="main_photo" data-title="Сделать главной" data-photo="'+data.result.file.filename+'">\
													<span class="fa fa-square-o"></span>\
												</a>\
												<span class="btn btn-default btn-flat btn-xs call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>\
												<span class="btn btn-default btn-flat btn-xs call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>\
												<a href="#" class="btn btn-danger btn-flat btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'">\
													<span class="fa fa-trash"></span>\
												</a>\
											</div>\
											<input type="hidden" name="uploaded_files[]" value="'+data.result.file.filename+'">\
										</div>';

						$('#uploaded_files').append(template);
						$("#uploaded_files").sortable('refresh');
					}
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('select[name="offer_type"]').on('change', function(){
			_get_categories();
			$('.category-settings_wr').html('').hide();
		});

		_get_categories();
		first_show_category = false;

		$('select[name="category"]').on('change', function(){
			var category = intval($(this).val());
			var $wrapper = $('.category-settings_wr');
			if(category == 0){
				$wrapper.html('').hide();
				return false;
			}

			$.ajax({
				type: 'POST',
				url: base_url+'admin/real_estate/ajax_operations/get_category_settings',
				data: {category:category},
				dataType: 'JSON',
				success: function(resp){
					if(resp.mess_type == 'success'){
						$wrapper.html(resp.settings).show();
						init_icheck();
					} else{
						systemMessages(resp.message, resp.mess_type);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
					jqXHR.abort();
				}
			});
		});

		$('select[name="location_parent"]').on('change', function(){
			update_children_locations();
		});

		update_children_locations();

		$('select[name="discount[type]"]').on('change', function(e){
			var value = $(this).val();
			switch (value) {
				default:
				case 'simple':
					$(this).siblings('.input-group-addon').hide();
					$(this).siblings('input[name="discount[value]"]').hide();
				break;
				case 'new_price':
				case 'by_m2':
					$(this).siblings('.input-group-addon').show();
					$(this).siblings('input[name="discount[value]"]').show();
				break;
			}
		});

		$('input.icon-checkbox').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '0%'
		});
		$('input.property_sufix').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '0%'
		});

		// 999 API
		$('select[name="region_999"]').on('change', function(){
			var region = intval($(this).val());
			var $select_raion = $('select[name="raion_999"]');
			var $select_sector = $('select[name="sector_999"]');
			var template = ['<option value="">Выберите населенный пункт</option>'];
			
			if(region == 0){
				$select_raion.html(template.join(''));
				return false;
			}

			$select_sector.html('<option value="">Выберите сектор</option>');

			$.ajax({
				type: 'POST',
				url: base_url+'admin/real_estate/ajax_operations/get_api_raions',
				data: {region:region},
				dataType: 'JSON',
				success: function(resp){
					if(resp.mess_type == 'success'){
						if(resp.raions_999_count > 0){
							$.each( resp.raions_999, function( key, raion_999 ) {
								template.push('<option value="'+raion_999.id+'">'+raion_999.title+'</option>');
							});
						}

						$select_raion.html(template.join(''));
					} else{
						systemMessages(resp.message, resp.mess_type);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					systemMessages( 'Ошибка: Запрос не может быть отправлен. Попробуйте позже.', 'error' );
					jqXHR.abort();
				}
			});
		});
		$('select[name="raion_999"]').on('change', function(){
			var raion = intval($(this).val());
			var $select_sector = $('select[name="sector_999"]');
			var template = ['<option value="">Выберите сектор</option>'];
			if(raion == 0){
				$select_sector.html(template.join(''));
				return false;
			}

			$.ajax({
				type: 'POST',
				url: base_url+'admin/real_estate/ajax_operations/get_api_sectors',
				data: {raion:raion},
				dataType: 'JSON',
				success: function(resp){
					if(resp.mess_type == 'success'){
						if(resp.sectors_999_count > 0){
							$.each( resp.sectors_999, function( key, sector_999 ) {
								template.push('<option value="'+sector_999.id+'">'+sector_999.title+'</option>');
							});
						}

						$select_sector.html(template.join(''));
					} else{
						systemMessages(resp.message, resp.mess_type);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					systemMessages( 'Ошибка: Запрос не может быть отправлен. Попробуйте позже.', 'error' );
					jqXHR.abort();
				}
			});
		});

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if(e.target.hash == '#form_location'){
				initMap();
			}
		});
	});

	function init_icheck(){
		$('input.icon-checkbox').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '0%'
		});
		$('input.property_sufix').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			increaseArea: '0%'
		});
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.user-image-thumbnail-wr');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

	function update_children_locations(){
		var parent_location = intval($('select[name="location_parent"] option:selected').val());
		var $child_locations = $('select[name="location_child"]');
		var $child_locations_additional = $('select[name="location_child_additional[]"]');
		if(parent_location == ''){
			$child_locations.html('<option value="">Выберите сначало тип</option>').prop('disabled', true);
			$child_locations_additional.html('').prop('disabled', true);
			return false;
		}

		if(parent_location == 1){
			$child_locations.siblings('label').html('Сектор');
		} else if(parent_location == 2){
			$child_locations.siblings('label').html('Населенный пункт');
		}

		var options = [];
		var options_aditional = [];
		$.each(children_locations[parent_location], function(key, location){
			options.push('<option value="'+location.id_location+'" '+((location.id_location == selected_location)?'selected':'')+'>'+location.location_name_ru+'</option>');
			options_aditional.push('<option value="'+location.id_location+'" '+((location.id_location != selected_location && $.inArray(location.id_location, selected_location_aditional ) > -1)?'selected':'')+'>'+location.location_name_ru+'</option>');
		});
		
		$child_locations.html(options.join('')).prop('disabled', false);
		$child_locations_additional.html(options_aditional.join('')).prop('disabled', false);
		$child_locations_additional.selectpicker('refresh');
	}
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/real_estate/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	var remove_existent_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		var main_photo = $this.closest('form').find('input[name="main_photo"]').val();
		if(photo == main_photo){
			$this.closest('form').find('input[name="main_photo"]').val('');
		}
		$this.closest('form').append('<input type="hidden" name="remove_existent_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	var main_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').find('input[name="main_photo"]').val(photo);
		$('#uploaded_files .btn-main_photo').each(function(){
			$(this).removeClass('btn-success').addClass('btn-default');
			$(this).find('.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
		});

		$this.removeClass('btn-default').addClass('btn-success');
		$this.find('.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
	}
	
	// OS MAP
	var mymap = null, 
		marker = null,
		latitude = <?php echo $real_estate['real_estate_lat'];?>,
		longitude = <?php echo $real_estate['real_estate_lng'];?>;
	
	function initMap(){
		$('input[name="latitude"]').val(latitude);
		$('input[name="longitude"]').val(longitude);		
		mymap = L.map(document.getElementById('map-element'), {
			center: {lat: latitude, lng: longitude},
			zoom: 17,
			scrollWheelZoom: true
		});
	
		L.tileLayer('//i.simpalsmedia.com/map/1/{z}/{x}/{y}.png', {
			attribution: '<a href="'+base_url+'" target="_blank">AccesImobil, </a><a href="http://www.openstreetmap.org/#map=15/47.770685/27.929283999999996" target="_blank">© OpenStreetMap</a>',
			maxZoom: 18
		}).addTo(mymap);

		var aIcon = L.icon({
			iconUrl: base_url + 'files/images/gm_marker.png'
		});

		marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
		
		mymap.on('click', function(e) {
			latitude = e.latlng.lat;
			longitude = e.latlng.lng;
			if(marker == null){
				marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
			} else{
				var latlng = new L.latLng(latitude, longitude);
				marker.setLatLng(latlng);
			}
			
			$('input[name="latitude"]').val(latitude);
			$('input[name="longitude"]').val(longitude);

			marker.on('dragend', markerDrag);
		});

		var options = {
			bounds: null,
			email: null,
			callback: function (results) {
				setTimeout(() => {
					if(Object.keys(results).length > 0){
						var bbox = results[0].boundingbox,
							first = new L.LatLng(bbox[0], bbox[2]),
							second = new L.LatLng(bbox[1], bbox[3]),
							bounds = new L.LatLngBounds([first, second]);
						this._map.fitBounds(bounds);
		
						latitude = results[0].lat;
						longitude = results[0].lon;
						if(marker == null){
							marker = L.marker([latitude, longitude], {icon: aIcon, draggable: true}).addTo(mymap);
						} else{
							var latlng = new L.latLng(latitude, longitude);
							marker.setLatLng(latlng);
						}
						
						$('input[name="latitude"]').val(latitude);
						$('input[name="longitude"]').val(longitude);
					} else{
						systemMessages( 'Ошибка: Не могу найти указанный адрес.', 'error' );
					}
				}, 400);
			}
		};

		var osmGeocoder = new L.Control.OSMGeocoder(options);

		mymap.addControl(osmGeocoder);
	}

	function markerDrag(e){
		var marker_position = e.target.getLatLng();
		$('input[name="latitude"]').val(marker_position.lat);
		$('input[name="longitude"]').val(marker_position.lng);		
	}

	var map_search = function(btn){
		var map_search_params = [];
		var $parent_location = $('select[name="location_parent"] option:selected');
		var $child_location = $('select[name="location_child"] option:selected');
		var $address_location = $('input[name="address"]');
		
		if($parent_location.data('text') != undefined && $parent_location.data('text') != ''){
			map_search_params.push($parent_location.data('text'));
		}

		if($child_location.val() != ''){
			map_search_params.push($child_location.text());
		}

		if($address_location.val() != ''){
			map_search_params.push($address_location.val());
		}
		
		if(map_search_params.length > 0){
			$('.leaflet-control-geocoder input[type="text"]').val(map_search_params.join(', '));
			$('.leaflet-control-geocoder #search_on_map').click();
		}
	}
</script>