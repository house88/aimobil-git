<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($category)){echo 'Редактирование';} else{echo 'Добавление';}?> категории</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
				<div class="form-group">
					<label>Фото</label>
					<div class="input-group">
						<input type="text" class="form-control" name="category_image" id="category_image" value="<?php if(!empty($category)){echo $category['category_image'];}?>" readonly>
						<div class="input-group-btn">
							<span class="btn btn-default call-function" data-callback="clear_image" data-clear="#category_image"><i class="fa fa-times"></i></span>
							<span class="btn btn-default call-function" data-callback="preview_image" data-image="#category_image"><i class="fa fa-eye"></i></span>
							<span class="btn btn-default rfm-iframe-btn" data-fancybox data-type="iframe" data-src="<?php echo base_url();?>theme/admin/plugins/filemanager/dialog.php?type=1&field_id=category_image&relative_url=1" data-options='{"iframe" : {"css" : {"height" : "100%"}}}'>
								<i class="fa fa-folder-open"></i> Добавить фото
							</span>
						</div>
					</div>
				</div>
                <div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
						<li class="">
							<a href="#form_menu" data-toggle="tab" aria-expanded="false">Меню</a>
						</li>
						<?php if(!empty($api_categories)){?>
							<li class="">
								<a href="#form_999_api" data-toggle="tab" aria-expanded="false">Настройки 999.md</a>
							</li>
						<?php }?>
					</ul>
					<div class="tab-content pl-0_i pr-0_i pb-0_i">
						<div class="tab-pane active" id="form_ro">
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label>Название</label>
										<input class="form-control" placeholder="Название" name="title_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_ro'];}?>">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label>Название, в единственном числе</label>
										<input class="form-control" placeholder="Название, в единственном числе" name="title_single_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_single_ro'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>H1 для каталога</label>
										<input class="form-control" placeholder="H1 для каталога" name="h1_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['category_h1_ro'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Текст числа результатов</label>
										<textarea name="counter_ro" class="form-control" rows="3"><?php if(!empty($category)){echo $category['category_text_counter_ro'];}?></textarea>
									</div>
								</div>
							</div>
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_meta_ro" data-toggle="tab" aria-expanded="true">Meta</a>
									</li>
									<li class="">
										<a href="#form_seo_ro" data-toggle="tab" aria-expanded="true">SEO текст</a>
									</li>
								</ul>
								<div class="tab-content pl-0_i pr-0_i pb-0_i">
									<div class="tab-pane active" id="form_meta_ro">
										<div class="form-group">
											<label>URL</label>
											<input class="form-control" placeholder="URL" name="url_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['url_ro'];}?>">
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['mt_ro'];}?>">
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['mk_ro'];}?>">
										</div>
										<div class="form-group mb-0">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ro" maxlength="250" value="<?php if(!empty($category)){echo $category['md_ro'];}?>">
										</div>
									</div>
									<div class="tab-pane" id="form_seo_ro">
										<div class="form-group mb-0">
											<textarea name="seo_ro" class="modal_description"><?php if(!empty($category)){echo $category['seo_ro'];}?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label>Иконка</label>
										<input class="form-control" placeholder="Иконка" name="category_icon" maxlength="250" value="<?php if(!empty($category)){echo $category['category_icon'];}?>">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label class="mt-25 lh-34 mb-0">
											<input type="checkbox" name="category_has_icons" <?php echo (!empty($category))?set_checkbox('category_has_icons', '', $category['category_has_icons'] == 1):'checked';?> class="flat-red nice-input">
											Показать иконки для обьектов
										</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label>Тип</label>
										<select name="category_type" class="form-control">
											<option value="sale" <?php if(!empty($category)){echo set_select('category_type', 'sale', $category['category_type'] == 'sale');}?>>Продажа</option>
											<option value="rent" <?php if(!empty($category)){echo set_select('category_type', 'rent', $category['category_type'] == 'rent');}?>>Аренда</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label>Порядковый номер</label>
										<input class="form-control" placeholder="Порядковый номер" name="category_weight" maxlength="3" value="<?php if(!empty($category)){echo $category['category_weight'];}?>">
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">
							<div class="row">
								<div class="col-xs-12 col-md-6">
									<div class="form-group">
										<label>Название</label>
										<input class="form-control" placeholder="Название" name="title_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_ru'];}?>">
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="form-group">
										<label>Название, в единственном числе</label>
										<input class="form-control" placeholder="Название, в единственном числе" name="title_single_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_single_ru'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>H1 для каталога</label>
										<input class="form-control" placeholder="H1 для каталога" name="h1_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['category_h1_ru'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Текст числа результатов</label>
										<textarea name="counter_ru" class="form-control" rows="3"><?php if(!empty($category)){echo $category['category_text_counter_ru'];}?></textarea>
									</div>
								</div>
							</div>
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_meta_ru" data-toggle="tab" aria-expanded="true">Meta</a>
									</li>
									<li class="">
										<a href="#form_seo_ru" data-toggle="tab" aria-expanded="true">SEO текст</a>
									</li>
								</ul>
								<div class="tab-content pl-0_i pr-0_i">
									<div class="tab-pane active" id="form_meta_ru">
										<div class="form-group">
											<label>URL</label>
											<input class="form-control" placeholder="URL" name="url_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['url_ru'];}?>">
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['mt_ru'];}?>">
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['mk_ru'];}?>">
										</div>
										<div class="form-group mb-0">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_ru" maxlength="250" value="<?php if(!empty($category)){echo $category['md_ru'];}?>">
										</div>
									</div>
									<div class="tab-pane" id="form_seo_ru">
										<div class="form-group mb-0">
											<textarea name="seo_ru" class="modal_description"><?php if(!empty($category)){echo $category['seo_ru'];}?></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form_en">
							<div class="row">
								<div class="col-xs-12 col-md-6">
									<div class="form-group">
										<label>Название</label>
										<input class="form-control" placeholder="Название" name="title_en" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_en'];}?>">
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="form-group">
										<label>Название, в единственном числе</label>
										<input class="form-control" placeholder="Название, в единственном числе" name="title_single_en" maxlength="250" value="<?php if(!empty($category)){echo $category['category_title_single_en'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>H1 для каталога</label>
										<input class="form-control" placeholder="H1 для каталога" name="h1_en" maxlength="250" value="<?php if(!empty($category)){echo $category['category_h1_en'];}?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Текст числа результатов</label>
										<textarea name="counter_en" class="form-control" rows="3"><?php if(!empty($category)){echo $category['category_text_counter_en'];}?></textarea>
									</div>
								</div>
							</div>
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_meta_en" data-toggle="tab" aria-expanded="true">Meta</a>
									</li>
									<li class="">
										<a href="#form_seo_en" data-toggle="tab" aria-expanded="true">SEO текст</a>
									</li>
								</ul>
								<div class="tab-content pl-0_i pr-0_i">
									<div class="tab-pane active" id="form_meta_en">
										<div class="form-group">
											<label>URL</label>
											<input class="form-control" placeholder="URL" name="url_en" maxlength="250" value="<?php if(!empty($category)){echo $category['url_en'];}?>">
										</div>
										<div class="form-group">
											<label>Meta title</label>
											<input class="form-control" placeholder="Meta title" name="mt_en" maxlength="250" value="<?php if(!empty($category)){echo $category['mt_en'];}?>">
										</div>
										<div class="form-group">
											<label>Meta keywords</label>
											<input class="form-control" placeholder="Meta keywords" name="mk_en" maxlength="250" value="<?php if(!empty($category)){echo $category['mk_en'];}?>">
										</div>
										<div class="form-group mb-0">
											<label>Meta description</label>
											<input class="form-control" placeholder="Meta description" name="md_en" maxlength="250" value="<?php if(!empty($category)){echo $category['md_en'];}?>">
										</div>
									</div>
									<div class="tab-pane" id="form_seo_en">
										<div class="form-group mb-0">
											<textarea name="seo_en" class="modal_description"><?php if(!empty($category)){echo $category['seo_en'];}?></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form_menu">
							<div class="form-group">
								<div class="mb-10" id="category-menu_items">
									<?php if(!empty($category)){?>
										<?php $category_menu = json_decode($category['category_menu'], true);?>
										<?php if(!empty($category_menu)){?>
											<?php foreach($category_menu as $category_menu_item){?>
												<div class="form-group">
													<div class="input-group input-group-sm mb-5">
														<span class="input-group-addon">{URL категорий} + </span>
														<input class="form-control" placeholder="URL суфикс" name="category_menu[url][]" value="<?php echo $category_menu_item['url'];?>">
													</div>
													<div class="input-group input-group-sm mb-10">
														<input class="form-control" placeholder="Название RO" name="category_menu[name_ro][]" value="<?php echo $category_menu_item['name_ro'];?>">
														<span class="input-group-addon">&mdash;</span>
														<input class="form-control" placeholder="Название RU" name="category_menu[name_ru][]" value="<?php echo $category_menu_item['name_ru'];?>">
														<span class="input-group-addon">&mdash;</span>
														<input class="form-control" placeholder="Название EN" name="category_menu[name_en][]" value="<?php echo $category_menu_item['name_en'];?>">
														<div class="input-group-btn">
															<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
															<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
															<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_category_menu"><i class="fa fa-trash"></i></span>
														</div>
													</div>
												</div>
											<?php }?>
										<?php }?>
									<?php }?>
								</div>
								<span class="btn btn-default btn-sm call-function" data-callback="add_category_menu">Добавить значение</span>
							</div>
						</div>
						<div class="tab-pane" id="form_999_api">
							<div class="form-group">
								<select name="category_999" class="form-control">
									<option value="">Выберите категорию</option>
									<?php foreach($api_categories as $api_category){?>
										<option value="<?php echo $api_category['id'];?>" <?php if(!empty($category)){echo set_select('category_999', $api_category['id'], $api_category['id'] == $category['id_category_999']);}?>><?php echo $api_category['title'];?></option>
									<?php }?>
								</select>
							</div>
							<div class="form-group">
								<select name="subcategory_999" class="form-control">
									<option value="">Выберите подкатегорию</option>
									<?php if(!empty($api_subcategories)){?>
										<?php foreach($api_subcategories as $api_subcategory){?>
											<option value="<?php echo $api_subcategory['id'];?>" <?php if(!empty($category)){echo set_select('subcategory_999', $api_subcategory['id'], $api_subcategory['id'] == $category['id_subcategory_999']);}?>><?php echo $api_subcategory['title'];?></option>
										<?php }?>
									<?php }?>
								</select>
							</div>
							<div class="form-group">
								<select name="offer_type_999" class="form-control">
									<option value="">Выберите тип</option>
									<?php if(!empty($api_offer_types)){?>
										<?php foreach($api_offer_types as $api_offer_type){?>
											<option value="<?php echo $api_offer_type['id'];?>" <?php if(!empty($category)){echo set_select('offer_type_999', $api_offer_type['id'], $api_offer_type['id'] == $category['id_offer_type_999']);}?>><?php echo $api_offer_type['title'];?></option>
										<?php }?>
									<?php }?>
								</select>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">                
                <label class="pull-left lh-34 mb-0">
                    <input type="checkbox" name="category_active" <?php echo (!empty($category))?set_checkbox('category_active', '', $category['category_active'] == 1):'checked';?> class="flat-red nice-input">
                    Видна на сайте
                </label>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($category)){?>
                    <input type="hidden" name="id_category" value="<?php echo $category['id_category'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_category" data-action="edit_category">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_category" data-action="add_category">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
	function responsive_filemanager_callback(field_id){
		$.fancybox.close();
	}

	var clear_image = function(btn){
		var $this = $(btn);
		$this.closest('.input-group').find($($this.data('clear'))).val('');
	}

	var preview_image = function(btn){
		var $this = $(btn);
		var image = $($this.data('image')).val();
		if(image != ''){
			$.fancybox.open([
				{
					src  : base_url+'files/'+image
				}
			]);
		} else{
			systemMessages( 'Ошибка: Нет баннера.', 'error' );
		}
	}

	var add_category_menu = function(btn){
		var $this = $(btn);
		var template = '<div class="form-group">\
							<div class="input-group input-group-sm mb-5">\
								<span class="input-group-addon">{URL категорий} + </span>\
								<input class="form-control" placeholder="URL суфикс" name="category_menu[url][]">\
							</div>\
							<div class="input-group input-group-sm mb-10">\
								<input class="form-control" placeholder="Название RO" name="category_menu[name_ro][]">\
								<span class="input-group-addon">&mdash;</span>\
								<input class="form-control" placeholder="Название RU" name="category_menu[name_ru][]">\
								<span class="input-group-addon">&mdash;</span>\
								<input class="form-control" placeholder="Название EN" name="category_menu[name_en][]">\
								<div class="input-group-btn">\
									<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>\
									<span class="btn btn-default btn-sm call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>\
									<span class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-title="Удаление" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_category_menu"><i class="fa fa-trash"></i></span>\
								</div>\
							</div>\
						</div>';
		$('#category-menu_items').append(template);
	}

	var delete_category_menu = function(btn){
		$(btn).closest('.form-group').remove();
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.form-group');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

	var id_category_999 = 0;
	var id_subcategory_999 = 0;
	var id_odder_type_999 = 0;

    $(function(){
		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
		
		// API 999.md
		id_category_999 = intval($('select[name="category_999"]').val());
		id_subcategory_999 = intval($('select[name="subcategory_999"]').val());
		id_odder_type_999 = intval($('select[name="offer_type_999"]').val());
		// get_api_subcategories
		$('select[name="category_999"]').on('change', function(){
			var $this = $(this);
			id_category_999 = $this.val();
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url();?>admin/real_estate/ajax_operations/get_api_subcategories',
				data: {id_category_999:id_category_999},
				dataType: 'JSON',
				beforeSend: function(){
					clearSystemMessages();
					$this.addClass('disabled');
				},
				success: function(resp){
					$this.removeClass('disabled');

					if(resp.mess_type == 'success'){
						var options = ['<option>Выберите подкатегорию</option>'];
						if(resp.subcategories_count > 0){
							$.each( resp.subcategories, function( key, subcategory ) {
								options.push('<option value="'+subcategory.id+'">'+subcategory.title+'</option>');
							});
						}

						$('select[name="subcategory_999"]').html(options.join(''));
						$('select[name="offer_type_999"]').html('<option value="">Выберите тип</option>');
						id_subcategory_999 = 0;
						id_odder_type_999 = 0;
					} else{
						systemMessages( resp.message, resp.mess_type );
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
					jqXHR.abort();
				}
			});
		});
		// get_api_offer_types
		$('select[name="subcategory_999"]').on('change', function(){
			var $this = $(this);
			id_subcategory_999 = $this.val();
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url();?>admin/real_estate/ajax_operations/get_api_offer_types',
				data: {id_category_999:id_category_999, id_subcategory_999:id_subcategory_999},
				dataType: 'JSON',
				beforeSend: function(){
					clearSystemMessages();
					$this.addClass('disabled');
				},
				success: function(resp){
					$this.removeClass('disabled');

					if(resp.mess_type == 'success'){
						var options = ['<option>Выберите тип</option>'];
						if(resp.offer_types_count > 0){
							$.each( resp.offer_types, function( key, offer_type ) {
								options.push('<option value="'+offer_type.id+'">'+offer_type.title+'</option>');
							});
						}

						$('select[name="offer_type_999"]').html(options.join(''));
						id_odder_type_999 = 0;
					} else{
						systemMessages( resp.message, resp.mess_type );
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
					jqXHR.abort();
				}
			});
		});
	});
	
	tinymce.EditorManager.remove();
	
	tinymce.EditorManager.init({
        relative_urls: false,
        selector: ".modal_description",
        height: 200,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | fontsizeselect | link unlink anchor | responsivefilemanager image media | forecolor backcolor  | preview fullpage code",
        image_advtab: true ,
        image_title: true,
        fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
        external_filemanager_path:"/theme/admin/plugins/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/theme/admin/plugins/filemanager/plugin.min.js"},
        language_url : '/theme/admin/plugins/tinymce/langs/ru.js',
        language : 'ru'
	});

    var manage_category = function(btn){
		tinymce.EditorManager.triggerSave();
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/real_estate/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_category_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>