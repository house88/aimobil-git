<?php if(!empty($icons)){?>
    <div class="box box-success direct-chat direct-chat-success" id="results-icons">
        <div class="box-header with-border">
            <h3 class="box-title"><label>Иконки</label></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <?php foreach($icons as $icon){?>
                    <div class="col-xs-12 col-md-6">
                        <div class="input-group mb-10">
                            <div class="input-group-addon">
                                <div class="w-40">
                                    <span class="<?php echo $icon['icon_class'];?>"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control" name="icons[<?php echo $icon['id_icon'];?>][name_ro]" placeholder="RO" value="<?php echo (isset($icons_selected) && array_key_exists($icon['id_icon'], $icons_selected))?$icons_selected[$icon['id_icon']]['icon_name_ro']:$icon['icon_name_ro'];?>">
                            <div class="input-group-addon">&mdash;</div>
                            <input type="text" class="form-control" name="icons[<?php echo $icon['id_icon'];?>][name_ru]" placeholder="RU" value="<?php echo (isset($icons_selected) && array_key_exists($icon['id_icon'], $icons_selected))?$icons_selected[$icon['id_icon']]['icon_name_ru']:$icon['icon_name_ru'];?>">
                            <div class="input-group-addon">&mdash;</div>
                            <input type="text" class="form-control" name="icons[<?php echo $icon['id_icon'];?>][name_en]" placeholder="EN" value="<?php echo (isset($icons_selected) && array_key_exists($icon['id_icon'], $icons_selected))?$icons_selected[$icon['id_icon']]['icon_name_en']:$icon['icon_name_en'];?>">
                            <div class="input-group-addon input-icheck">
                                <input class="icon-checkbox" type="checkbox" name="icons[<?php echo $icon['id_icon'];?>][is_checked]" <?php if(isset($icons_selected) && array_key_exists($icon['id_icon'], $icons_selected)){echo set_checkbox('icons['.$icon['id_icon'].'][is_checked]', '', true);}?>>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
<?php }?>