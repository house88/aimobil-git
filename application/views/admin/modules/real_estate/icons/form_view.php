<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($icon)){echo 'Редактирование';} else{echo 'Добавление';}?> иконки</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
				<div class="form-group">
					<label>Css Class</label>
					<input type="text" class="form-control" name="icon_class" placeholder="ex. fai fai-school" value="<?php if(!empty($icon)){echo $icon['icon_class'];}?>">
				</div>
				<div class="form-group">
					<label>Название</label>
					<div class="input-group">
						<input type="text" class="form-control" name="icon_name_ro" placeholder="Romana" value="<?php if(!empty($icon)){echo $icon['icon_name_ro'];}?>">
						<div class="input-group-addon">-</div>
						<input type="text" class="form-control" name="icon_name_ru" placeholder="Русский" value="<?php if(!empty($icon)){echo $icon['icon_name_ro'];}?>">
						<div class="input-group-addon">-</div>
						<input type="text" class="form-control" name="icon_name_en" placeholder="English" value="<?php if(!empty($icon)){echo $icon['icon_name_ro'];}?>">
					</div>
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($icon)){?>
                    <input type="hidden" name="id_icon" value="<?php echo $icon['id_icon'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_icon" data-action="edit_icon">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_icon" data-action="add_icon">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });
    var manage_icon = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/real_estate/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_icon_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>