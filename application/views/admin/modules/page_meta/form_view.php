<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Редактирование</h4>
        </div>
        <form id="add_audio_form">
            <div class="modal-body">
                <div class="nav-tabs-custom no-shadow mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_mt" data-toggle="tab" aria-expanded="false">Meta title</a>
						</li>
						<li class="">
							<a href="#form_mk" data-toggle="tab" aria-expanded="false">Meta keywords</a>
						</li>
						<li class="">
							<a href="#form_md" data-toggle="tab" aria-expanded="false">Meta description</a>
						</li>
						<li class="">
							<a href="#form_seo" data-toggle="tab" aria-expanded="false">SEO текст</a>
						</li>
						<li class="">
							<a href="#form_image" data-toggle="tab" aria-expanded="false">Постер</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_mt">
							<div class="form-group">
								<label>Romana</label>
								<input class="form-control" placeholder="Название Romana" name="title_ro" value="<?php if(!empty($page_meta)){echo $page_meta['page_title_ro'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Русский</label>
								<input class="form-control" placeholder="Название Русский" name="title_ru" value="<?php if(!empty($page_meta)){echo $page_meta['page_title_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>English</label>
								<input class="form-control" placeholder="Название English" name="title_en" value="<?php if(!empty($page_meta)){echo $page_meta['page_title_en'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_mk">
							<div class="form-group">
								<label>Romana</label>
								<input class="form-control" placeholder="Meta keywords Romana" name="mk_ro" value="<?php if(!empty($page_meta)){echo $page_meta['page_mk_ro'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Русский</label>
								<input class="form-control" placeholder="Meta keywords Русский" name="mk_ru" value="<?php if(!empty($page_meta)){echo $page_meta['page_mk_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>English</label>
								<input class="form-control" placeholder="Meta keywords English" name="mk_en" value="<?php if(!empty($page_meta)){echo $page_meta['page_mk_en'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_md">
							<div class="nav-tabs-custom no-shadow mb-0">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_md_ro" data-toggle="tab" aria-expanded="false">Romana</a>
									</li>
									<li class="">
										<a href="#form_md_ru" data-toggle="tab" aria-expanded="false">Русский</a>
									</li>
									<li class="">
										<a href="#form_md_en" data-toggle="tab" aria-expanded="false">English</a>
									</li>
								</ul>
								<div class="tab-content pl-0_i pr-0_i">
									<div class="tab-pane active" id="form_md_ro">
										<div class="form-group">
											<textarea name="md_ro" rows="4" class="form-control" placeholder="Meta description Romana"><?php if(!empty($page_meta)){echo $page_meta['page_md_ro'];}?></textarea>
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_md_ru">
										<div class="form-group">
											<textarea name="md_ru" rows="4" class="form-control" placeholder="Meta description Русский"><?php if(!empty($page_meta)){echo $page_meta['page_md_ru'];}?></textarea>
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
									<div class="tab-pane" id="form_md_en">
										<div class="form-group">
											<textarea name="md_en" rows="4" class="form-control" placeholder="Meta description English"><?php if(!empty($page_meta)){echo $page_meta['page_md_en'];}?></textarea>
											<p class="help-block">Не должно содержать более 250 символов.</p>
										</div>
									</div>
								</div>
							</div>
                        </div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_seo">
							<div class="nav-tabs-custom no-shadow mb-0">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#form_seo_ro" data-toggle="tab" aria-expanded="false">Romana</a>
									</li>
									<li class="">
										<a href="#form_seo_ru" data-toggle="tab" aria-expanded="false">Русский</a>
									</li>
									<li class="">
										<a href="#form_seo_en" data-toggle="tab" aria-expanded="false">English</a>
									</li>
								</ul>
								<div class="tab-content pl-0_i pr-0_i">
									<div class="tab-pane active" id="form_seo_ro">
										<div class="form-group">
											<textarea name="seo_ro" rows="4" class="modal_description"><?php if(!empty($page_meta)){echo $page_meta['page_seo_ro'];}?></textarea>
										</div>
									</div>
									<div class="tab-pane" id="form_seo_ru">
										<div class="form-group">
											<textarea name="seo_ru" rows="4" class="modal_description"><?php if(!empty($page_meta)){echo $page_meta['page_seo_ru'];}?></textarea>
										</div>
									</div>
									<div class="tab-pane" id="form_seo_en">
										<div class="form-group">
											<textarea name="seo_en" rows="4" class="modal_description"><?php if(!empty($page_meta)){echo $page_meta['page_seo_en'];}?></textarea>
										</div>
									</div>
								</div>
							</div>
                        </div>
						<div class="tab-pane" id="form_image">
                            <div class="form-group clearfix">
                                <p class="help-block text-left"><strong><small>(Мин. ширина: 1200px, высота: 100px, не превышать 5Мб)</small></strong></p>
                                <span class="btn btn-default btn-sm btn-file pull-left">
                                    <i class="fa fa-picture-o"></i>
                                    Загрузить постер
                                    <input id="select_photo" type="file" name="userfile">
                                </span>
                            </div>
                            <div class="form-group clearfix mb-0" id="page_image">
                                <?php if(!empty($page_meta) && !empty($page_meta['page_image'])){?>
                                    <div class="user-image-thumbnail-wr mb-0 mr-0">
                                        <div class="user-image-thumb" style="height:auto;">
                                            <img src="<?php echo base_url('files/page_meta/'.$page_meta['page_image']);?>"/>
                                        </div>
                                        <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $page_meta['page_image'];?>">
                                            <span class="glyphicon glyphicon-remove-circle"></span>
                                        </a>
                                        <input type="hidden" name="page_image" value="<?php echo $page_meta['page_image'];?>">
                                    </div>
                                <?php }?>
                            </div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($page_meta)){?>
                    <input type="hidden" name="page_key" value="<?php echo $page_meta['page_key'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_page_meta" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_page_meta" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        'use strict';        
		$('#select_photo').fileupload({
			url: base_url+'admin/page_meta/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr mb-0 mr-0">\
											<div class="user-image-thumb" style="height:auto;">\
												<img src="'+base_url+'files/page_meta/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'">\
												<span class="glyphicon glyphicon-remove-circle"></span>\
											</a>\
											<input type="hidden" name="page_image" value="'+file.filename+'">\
										</div>';

						if($('#page_image .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#page_image .user-image-thumbnail-wr').find('input[name="page_image"]').val();
							$('#page_image').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#page_image').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});

	tinymce.EditorManager.remove();
	
	tinymce.EditorManager.init({
        relative_urls: false,
        selector: ".modal_description",
        height: 200,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | fontsizeselect | link unlink anchor | responsivefilemanager image media | forecolor backcolor  | preview fullpage code",
        image_advtab: true ,
        image_title: true,
        fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
        external_filemanager_path:"/theme/admin/plugins/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/theme/admin/plugins/filemanager/plugin.min.js"},
        language_url : '/theme/admin/plugins/tinymce/langs/ru.js',
        language : 'ru'
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

    var manage_page_meta = function(btn){
		tinymce.EditorManager.triggerSave();
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/page_meta/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_page_meta_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>