<div class="wr-filter-admin-panel filters-container">
	<div class="filter-admin-panel">
		<div class="title-b">
            Фильтры
            <a href="#" class="fa fa-times pull-right call-function" data-callback="toggle_dt_filter" data-action="close"></a>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<div class="form-group">
					<label>Группа</label>
					<select name="id_group" class="form-control dt_filter" data-title="Группа">
						<option value="">Выберите группу</option>
						<?php foreach($groups as $_group){?>
							<option value="<?php echo $_group['id_group'];?>"><?php echo $_group['group_name'];?></option>                                        
						<?php }?>
					</select>
				</div>
            </div>
        </div>

		<div class="wr-filter-list clearfix mt-10 "><ul class="filter-plugin-list"></ul></div>
	</div>

	<div class="wr-hidden call-function" data-callback="toggle_dt_filter" data-action="close" style="display: none;"></div>
</div>