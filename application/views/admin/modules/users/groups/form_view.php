<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
                <?php if(!empty($group)){?>Редактировать<?php } else{?>Добавить<?php }?> группу
            </h4>
        </div>
        <form id="add_form" autocomplete="off">			
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
						<div class="form-group">
							<label>Название</label>
							<input class="form-control" placeholder="Название" name="group_name" value="<?php if(!empty($group)){echo $group['group_name'];}?>">
							<p class="help-block">Название не должно содержать более 100 символов.</p>
						</div>
                    </div>
                    <div class="col-xs-12">
						<div class="form-group">
							<label>Тип</label>
							<select name="group_type" class="form-control">
								<option value="admin" <?php if(!empty($group)){echo set_select('group_type','admin', $group['group_type'] == 'admin');}?>>Администратор</option>
								<option value="manager" <?php if(!empty($group)){echo set_select('group_type','manager', $group['group_type'] == 'manager');}?>>Менеджер</option>
							</select>
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if(!empty($group)){?>
                    <input type="hidden" name="group" value="<?php echo $group['id_group'];?>">
                <?php }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_group" data-action="<?php if(!empty($group)){?>edit<?php } else{?>add<?php }?>_group">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var manage_group = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/users/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                    manage_group_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>