<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
                        <th>#</th>
                        <?php foreach($groups as $group){?>
                            <th class="mw-100 text-center vam"><?php echo $group['group_name'];?></th>
                        <?php }?>
					</tr>
				</thead>
                <tbody>
                    <?php foreach($rights as $right){?>
                        <tr>
                            <td><?php echo $right['right_name'];?></td>
                            <?php foreach($groups as $group){?>
                                <td class="mw-100 vam text-center">
                                    <?php $gr_key = $group['id_group'].'_'.$right['id_right'];?>
                                    <input class="switch-checkbox" type="checkbox" name="<?php echo $gr_key;?>" <?php echo set_checkbox($gr_key, $gr_key, isset($groups_rights[$gr_key]))?> data-group="<?php echo $group['id_group'];?>" data-right="<?php echo $right['id_right'];?>" data-size="mini" data-on-color="success" data-label-text="" data-label-width="0" data-on-text="Да" data-off-text="Нет"/>
                                </td>
                            <?php }?>
                        </tr>
                    <?php }?>
                </tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<script>
    $(function(){
        $(".switch-checkbox").on('ifChecked', function(event){
            var $this = $(this);
            var group = $this.data('group');
            var right = $this.data('right');
            change_groups_right(group, right);
        }).on('ifUnchecked', function(event){
            var $this = $(this);
            var group = $this.data('group');
            var right = $this.data('right');
            change_groups_right(group, right);
        });
    });
    
    function change_groups_right(group, right){
        $.ajax({
            type: 'POST',
            url: base_url+'admin/users/ajax_operations/change_groups_right',
            data: {group:group, right:right},
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
            },
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>
