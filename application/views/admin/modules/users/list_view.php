<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/users/add_user');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить профиль
				</a>
				<a href="#" class="btn btn-box-tool call-function" data-callback="toggle_dt_filter" data-action="open">
					<i class="fa fa-filter"></i> Фильтр
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<div class="dt-filter-list"></div>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
                        <th class="dt_name">Имя</th>
                        <th class="dt_email">Email</th>
                        <th class="dt_phone">Телефон</th>
                        <th class="dt_type">Группа</th>
                        <th class="dt_on_site">На сайте</th>
                        <th class="dt_regdate">Дата регистраций</th>
                        <th class="dt_actions"></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<?php $this->load->view('admin/modules/users/filter');?>
<script>
	var dtFilter; //obj for filters
	var dtTable;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/users/ajax_operations/users_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam", "aTargets": ["dt_name"], "mData": "dt_name" , "bSortable": false},
				{ "sClass": "w-200 vam", "aTargets": ["dt_email"], "mData": "dt_email" , "bSortable": false},
				{ "sClass": "w-150 vam", "aTargets": ["dt_phone"], "mData": "dt_phone" , "bSortable": false },
				{ "sClass": "w-200 vam", "aTargets": ["dt_type"], "mData": "dt_type" , "bSortable": false},
				{ "sClass": "w-120 text-center vam", "aTargets": ["dt_on_site"], "mData": "dt_on_site" , "bSortable": false},
				{ "sClass": "w-90 text-center vam", "aTargets": ["dt_regdate"], "mData": "dt_regdate" , "bSortable": false},
				{ "sClass": "w-95 vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						use_cookie: true,
						cookie_page: 'managers',
						callBack: function(){ 
							$.cookie( 'cookie_page', 'managers');
							$.cookie( '_dtFilters', $.param(dtFilter.getDTFilter()));
							dtTable.fnDraw(); 
						}
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error'){
							systemMessages(data.message, 'message-' + data.mess_type);
                        }

						if(data.mess_type == 'info'){
							systemMessages(data.message, 'message-' + data.mess_type);
                        }

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
            }
		});
	});
	
	var change_on_about = function(btn){
		var $this = $(btn);
		var user = $this.data('user');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/change_on_about',
			data: {user:user},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
	
	var change_on_site = function(btn){
		var $this = $(btn);
		var user = $this.data('user');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/change_on_site',
			data: {user:user},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var change_weight = function(btn){
		var $this = $(btn);
		var id_user = $this.data('user');
		var direction = $this.data('direction');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/change_weight',
			data: {id_user:id_user,direction:direction},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
			}
		});
		return false;
	}

	var change_banned_status = function(btn){
		var $this = $(btn);
		var user = $this.data('user');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/change_banned_status',
			data: {user:user},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var user = $this.data('user');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/delete',
			data: {user:user},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	}
</script>
