<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="add_form">			
			<div class="box-body">
				<div class="form-group">
					<label class="lh-30">
						<span class="btn btn-default btn-sm btn-file pull-left mr-10">
							<i class="fa fa-picture-o"></i>
							Добавить фото <input id="select_photo" type="file" name="userfile">
						</span>
						Фото <small>(Мин. ширина: 800px, высота: 600px, не превышать 10Мб)</small>
					</label>
					<div class="clearfix"></div>
					<div id="user_photo" class="files in-row-multiple pull-left"></div>
					<div class="clearfix"></div>
				</div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_ro">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_ro" value="">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_ro"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_ru" value="">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_ru"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_en" value="">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_en"></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
				<div class="row">
					<div class="col-sm-3 col-xs-12">
						<div class="form-group">
							<label>Группа</label>
							<select name="group" class="form-control">
								<option value="">Выберите группу</option>
								<?php foreach($groups as $group){?>
									<option value="<?php echo $group['id_group'];?>" data-type="<?php echo $group['group_type'];?>"><?php echo $group['group_name'];?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="form-group">
							<label>Должность</label>
							<select name="position" class="form-control">
								<option value="">Выберите должность</option>
								<?php foreach($positions as $position){?>
									<option value="<?php echo $position['id_position'];?>"><?php echo $position[lang_column('position_name')];?></option>
								<?php }?>
							</select>
						</div>  
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Рабочий регион</label>
							<?php $locations = Modules::run('locations/_get_tree');?>
							<select name="location" class="form-control">
								<option value="">Выберите рабочий регион</option>
								<?php foreach($locations as $location){?>
									<optgroup label="<?php echo $location[lang_column('location_name')];?>">
										<?php if(!empty($location['children'])){?>
											<?php foreach($location['children'] as $location_child){?>
												<option value="<?php echo $location_child['id_location'];?>"><?php echo $location_child[lang_column('location_name')];?></option>
											<?php }?>
										<?php }?>
									</optgroup>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Email <small>(используеться как логин)</small></label>
							<input class="form-control" placeholder="Email" name="user_email" value="">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Телефон</label>
							<input class="form-control" placeholder="Телефон" name="user_phone" value="">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Новый пароль</label>
							<input class="form-control" type="password" placeholder="Новый пароль" name="user_password" value="">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Повторите новый пароль</label>
							<input class="form-control" type="password" placeholder="Повторите новый пароль" name="confirm_password" value="">
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<?php if($this->lauth->have_right('manage_users')){?>
					<label class="pull-left lh-34 mb-0">
						<input type="checkbox" name="user_on_site" checked class="flat-red nice-input">
						Показывать на сайте
					</label>
				<?php }?>
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		'use strict';

		$('#select_photo').fileupload({
			url: base_url+'admin/users/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
                    var template = '<div class="user-image-thumbnail-wr">\
                                        <div class="user-image-thumb">\
                                            <img class="img-thumbnail" src="'+base_url+'files/users/'+data.result.file.filename+'"/>\
                                        </div>\
                                        <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-dtype="danger" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'"><span class="fa fa-trash"></span></a>\
                                        <input type="hidden" name="user_photo" value="'+data.result.file.filename+'">\
                                    </div>';

                    if($('#user_photo .user-image-thumbnail-wr').length > 0){
                        var unused_photo = $('#user_photo .user-image-thumbnail-wr').find('input[name="user_photo"]').val();
                        $('#user_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
                    }
                    $('#user_photo').html(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
	
	var add_form = $('#add_form');
	add_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = add_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/add',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					add_form.replaceWith('<div class="box-body"><div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div></div>');
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
</script>
