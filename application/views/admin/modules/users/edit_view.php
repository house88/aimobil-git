<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label class="lh-30">
						<span class="btn btn-default btn-sm btn-file pull-left mr-10">
							<i class="fa fa-picture-o"></i>
							Добавить фото <input id="select_photo" type="file" name="userfile">
						</span>
						Фото <small>(Мин. ширина: 800px, высота: 600px, не превышать 10Мб)</small>
					</label>
					<div class="clearfix"></div>
					<div id="user_photo" class="files in-row-multiple pull-left">
						<?php if($user['user_photo'] != ''){?>
							<div class="user-image-thumbnail-wr">
								<div class="user-image-thumb">
									<img class="img-thumbnail" src="<?php echo base_url('files/users/'.$user['user_photo']);?>">
								</div>
								<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-dtype="danger" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $user['user_photo'];?>">
									<span class="fa fa-trash"></span>
								</a>
								<input type="hidden" name="user_photo" value="<?php echo $user['user_photo'];?>">
							</div>
						<?php }?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ro" data-toggle="tab" aria-expanded="true">Romana</a>
						</li>
						<li class="">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_ro">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_ro" value="<?php echo $user['user_name_ro'];?>">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_ro"><?php echo br2nl($user['user_description_ro']);?></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ro" value="<?php echo $user['mt_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ro" value="<?php echo $user['mk_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ro" value="<?php echo $user['md_ro'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_ru">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_ru" value="<?php echo $user['user_name_ru'];?>">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_ru"><?php echo br2nl($user['user_description_ru']);?></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_ru" value="<?php echo $user['mt_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo $user['mk_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo $user['md_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">							
                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" placeholder="Имя" name="user_name_en" value="<?php echo $user['user_name_en'];?>">
                                <p class="help-block">Не должно содержать более 50 символов.</p>
                            </div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control mnh-200" name="user_description_en"><?php echo br2nl($user['user_description_en']);?></textarea>
							</div>
							<div class="form-group">
								<label>Meta title</label>
								<input class="form-control" placeholder="Meta title" name="mt_en" value="<?php echo $user['mt_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo $user['mk_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo $user['md_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
				<div class="row">
					<?php if($this->lauth->have_right('manage_users')){?>
						<div class="col-sm-3 col-xs-12">
							<div class="form-group">
								<label>Группа</label>
								<select name="group" class="form-control">
									<option value="">Выберите группу</option>
									<?php foreach($groups as $group){?>
										<option value="<?php echo $group['id_group'];?>" data-type="<?php echo $group['group_type'];?>" <?php echo set_select('group', $group['id_group'], $group['id_group'] == $user['id_group']);?>><?php echo $group['group_name'];?></option>
									<?php }?>
								</select>
							</div>  
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="form-group">
								<label>Должность</label>
								<select name="position" class="form-control">
									<option value="">Выберите должность</option>
									<?php foreach($positions as $position){?>
										<option value="<?php echo $position['id_position'];?>" <?php echo set_select('position', $position['id_position'], $position['id_position'] == $user['id_position']);?>><?php echo $position[lang_column('position_name')];?></option>
									<?php }?>
								</select>
							</div>  
						</div>
					<?php }?>
					<div class="<?php if($this->lauth->have_right('manage_users')){?>col-sm-6 <?php }?>col-xs-12">
						<div class="form-group">
							<label>Рабочий регион</label>
							<?php $locations = Modules::run('locations/_get_tree');?>
							<select name="location" class="form-control">
								<option value="">Выберите рабочий регион</option>
								<?php foreach($locations as $location){?>
									<?php if(!empty($location['children'])){?>
										<optgroup label="<?php echo $location[lang_column('location_name')];?>">
											<?php foreach($location['children'] as $location_child){?>
												<option value="<?php echo $location_child['id_location'];?>" <?php echo set_select('location', $location_child['id_location'], $location_child['id_location']==$user['id_location']);?>><?php echo $location_child[lang_column('location_name')];?></option>
											<?php }?>
										</optgroup>
									<?php }?>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Email <small>(используеться как логин)</small></label>
							<input class="form-control" placeholder="Email" name="user_email" value="<?php echo $user['user_email'];?>">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Телефон</label>
							<input class="form-control" placeholder="Телефон" name="user_phone" value="<?php echo $user['user_phone'];?>">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Новый пароль</label>
							<input class="form-control" type="password" placeholder="Новый пароль" name="user_password" value="">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Повторите новый пароль</label>
							<input class="form-control" type="password" placeholder="Повторите новый пароль" name="confirm_password" value="">
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label>API key <small>(999.md, <a href="https://999.md/api" target="_blank">посмотреть</a>)</small></label>
							<input class="form-control" placeholder="API key" name="user_api_key" value="<?php echo $user['user_999_api_key'];?>">
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<?php if($this->lauth->have_right('manage_users')){?>
					<label class="pull-left lh-34 mb-0">
						<input type="checkbox" name="user_on_site" <?php echo set_checkbox('user_on_site', '', $user['user_on_site'] == 1);?> class="flat-red nice-input">
						Показывать на сайте
					</label>
				<?php }?>
				<input type="hidden" name="id_user" value="<?php echo $user['id_user'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		'use strict';
		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});

		$('#select_photo').fileupload({
			url: base_url+'admin/users/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
                    var template = '<div class="user-image-thumbnail-wr">\
                                        <div class="user-image-thumb">\
                                            <img class="img-thumbnail" src="'+base_url+'files/users/'+data.result.file.filename+'"/>\
                                        </div>\
                                        <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-dtype="danger" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+data.result.file.filename+'"><span class="fa fa-trash"></span></a>\
                                        <input type="hidden" name="user_photo" value="'+data.result.file.filename+'">\
                                    </div>';

                    if($('#user_photo .user-image-thumbnail-wr').length > 0){
                        var unused_photo = $('#user_photo .user-image-thumbnail-wr').find('input[name="user_photo"]').val();
                        $('#user_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
                    }
                    $('#user_photo').html(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			},
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
</script>
