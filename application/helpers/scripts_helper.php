<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function br2nl($str) {
	$str = preg_replace("/(\r\n|\n|\r)/", "", $str);
	return preg_replace("/\<br(\s*)?\/?\>/i", "\n", $str);
}

function strip_tags_with_whitespace($string, $allowable_tags = null){
    $string = str_replace('<', ' <', $string);
    $string = strip_tags($string, $allowable_tags);
    $string = str_replace('  ', ' ', $string);
    $string = trim($string);

    return $string;
}

function get_embed_video_link($link){
    $components = explode('=', $link);
    $video_id = $components[1];
    return 'https://youtube.com/embed/'.$video_id;
}

function getImage($img_path, $template_img = 'files/images/no_image.png'){
	$path_parts = pathinfo($img_path);
	if(!empty($path_parts['extension']) && file_exists($img_path)){
		return $img_path;
	}else{
		return $template_img;
	}
}

function image_title($title = "", $index = ""){
	if($index != ""){
		$title .= " {$index}";
	}

	return clean_output("{$title}");
}

function image_exist($link){
	if(is_file($link)) {
		return 1;
    } else {
		return 0;
    }
}

function name_file($file = '', $name = 'template'){
	$name_components = explode('.', $file);
	$file_name = array($name, end($name_components));
	return implode('.', $file_name);
}

function formatNumber($number = 0){
    return number_format($number, 0, '.', ' ');
}

function getDomain($replace = ''){
	$url_parts = parse_url(current_url());
	return str_replace('www.', $replace, $url_parts['host']);
}

function getPrice($item, $default_currency = false){
	$ci = & get_instance();

	if(!$default_currency){
		$currency_default = Modules::run('currency/_get_current', array('active' => 1));
	} else{
		$currency_default = Modules::run('currency/_get_default_currency');
	}
	
	$price_array = array(
		'display_price' => $item['real_estate_price'],
		'display_price_db' => $item['real_estate_price'],
		'display_discount' => $item['real_estate_discount_value'],
		'display_discount_text' => '',
		'show_discount' => ($item['real_estate_discount_type'] != 'no')?true:false,
		'currency_code' => $currency_default['currency_code'],
		'currency_symbol' => $currency_default['currency_symbol'],
		'currency_rate' => $currency_default['currency_rate']
	);
	
	if($item['real_estate_discount_type'] == 'new_price' && $item['real_estate_price'] > $item['real_estate_discount_value']){
		$price_array['display_price'] = $item['real_estate_discount_value'];
		$price_array['display_price_db'] = $item['real_estate_discount_value'];
		$price_array['display_discount'] = $item['real_estate_price'];
	}

	$display_price = formatNumber(ceil($price_array['display_price'] * $price_array['currency_rate']));
	$display_discount = formatNumber(ceil($price_array['display_discount'] * $price_array['currency_rate']));

	switch ($item['real_estate_discount_type']) {
		case 'new_price':
			$price_array['display_discount_text'] = '<del>' . $display_discount . ' ' . $price_array['currency_symbol'] . '</del>';
		break;
		case 'by_m2':
			$price_array['display_discount_text'] = '- '.$display_discount.' '.$price_array['currency_symbol'] . ' / m<sup>2</sup>';
		break;
	}

	$price_array['display_price'] = $display_price;
	$price_array['display_discount'] = $display_discount;
	
	return $price_array;
}

function convertPrice($amount_in = 0, $currency_in = 'EUR', $currency_out = 'EUR'){
	$ci = & get_instance();
	$_currency_in = Modules::run('currency/_get_by_code', $currency_in);
	$_currency_out = Modules::run('currency/_get_by_code', $currency_out);
	$_default_currency = Modules::run('currency/_get_default_currency');

	if(empty($_currency_in) || empty($_currency_out)){
		return false;
	}

	$_default_amount = $amount_in/$_currency_in['currency_rate'];

	$amount_out = ceil($_default_amount*$_currency_out['currency_rate']);
	$price_array = array(
		'amount_default' => $_default_amount,
		'amount_default_display' => formatNumber($_default_amount),
		'amount_in' => $amount_in,
		'amount_in_display' => formatNumber($amount_in),
		'amount_out' => $amount_out,
		'amount_out_display' => formatNumber($amount_out),
		'currency_code_default' => $_default_currency['currency_code'],
		'currency_symbol_default' => $_default_currency['currency_symbol'],
		'currency_rate_default' => $_default_currency['currency_rate'],
		'currency_code_in' => $_currency_in['currency_code'],
		'currency_symbol_in' => $_currency_in['currency_symbol'],
		'currency_rate_in' => $_currency_in['currency_rate'],
		'currency_code_out' => $_currency_out['currency_code'],
		'currency_symbol_out' => $_currency_out['currency_symbol'],
		'currency_rate_out' => $_currency_out['currency_rate']
	);
	
	return $price_array;
}

function getCredit($item){
	$credit_array = false;
	if($item['real_estate_mortgage'] != 1 || $item['real_estate_offer_type'] == 'rent'){
		return $credit_array;
	}

	$ci = & get_instance();
	$currency_default = Modules::run('currency/_get_current', array('active' => 1));

	$real_estate_amount = ($item['real_estate_discount_type'] == 'new_price' && $item['real_estate_price'] > $item['real_estate_discount_value'])?$item['real_estate_discount_value']:$item['real_estate_price'];

	$interest_rate = (int)$ci->lsettings->item('calc_interest_rate_default')/100;
	$client_rate = (int)$ci->lsettings->item('calc_first_rate_default')/100;
	$period_default = (int)$ci->lsettings->item('calc_period_default');

	$total_amount = $real_estate_amount - $real_estate_amount*$client_rate;
	$credit_amount = $total_amount * $interest_rate / (12*(1 - pow(1 + $interest_rate/12, -($period_default*12))));
	
	$credit_array = array(
		'credit_amount' => formatNumber(ceil($credit_amount)),
		'display_amount' => formatNumber(ceil($credit_amount * $currency_default['currency_rate'])),
		'currency_code' => $currency_default['currency_code'],
		'currency_symbol' => $currency_default['currency_symbol'],
		'currency_rate' => $currency_default['currency_rate']
	);
	
	return $credit_array;
}

function formatPhone($phone, $add=''){
    return $add.preg_replace('/\D/','',$phone);
}

function formatDate($date, $format = "d/m/Y H:i:s", $lang = 'en'){
	$str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
		return date($format, $str_to_time);
	} else{
		return '&mdash;';
	} 
}

function format_date_i18n($date, $format = "d/m/Y H:i:s", $lang = 'en'){
	$str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
		if($lang == 'ru'){
			$rus_months = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
			$newDatetime = new Datetime($date);
			$month = $newDatetime->format('n');
			$album_data = $newDatetime->format('j '.mb_ucfirst($rus_months[$month-1]).'');
			$album_data .= $newDatetime->format(' Y');
			return $album_data;	
		} else{
			return date($format, $str_to_time);
		}
	} else{
		return '&mdash;';
	} 
}

function arrayByKey($arrays, $key, $multiple = false){
	$rez = array();
	if(empty($arrays)){
		return $rez;
	}
	
    foreach($arrays as $one){
		if(isset($one[$key])){
			if($multiple){
				$rez[$one[$key]][] = $one;
			} else{
				$rez[$one[$key]] = $one;
			}
		}
    }
    return $rez;
}

function generate_video_html($id, $type, $w, $h, $autoplay = 0){
	if(empty($id)) return false;

	switch($type) {
		case 'vimeo':
			return '<iframe class="player bd-none" src="//player.vimeo.com/video/'.$id.'?autoplay='.$autoplay.'&title=0&amp;byline=0&amp;portrait=0&amp;color=13bdab" width="'.$w.'" height="'.$h.'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		break;
		case 'youtube':
			return  '<iframe class="player bd-none" width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$id.'?autoplay='.$autoplay.'" allowfullscreen></iframe>';
		break;
	}
}

function messageInModal($message, $type = 'errors'){
    $CI = &get_instance();
	$data['message'] = $message;
	$data['type'] = $type;
	echo $CI->load->view('messages_view', $data, true);
	exit();
}

function jsonResponse($message = '',$type = 'error', $additional_params = array()){
    $resp = $additional_params;
    $resp['mess_type'] = $type;
    $resp['mess_class'] = $type;
    $resp['message'] = $message;

    echo json_encode($resp);
    exit();
}

function jsonDTResponse($message = '',$additional_params = array(), $type = 'error'){
    $output = array(
		"iTotalRecords" => 0,
		"iTotalDisplayRecords" => 0,
		"aaData" => array()
    );

    $output = array_merge($output, $additional_params);
    $output['mess_type'] = $type;
    $output['message'] = $message;

    echo json_encode($output);
    exit();
}

function id_from_link($str, $to_int = true){
    $segments  = explode('-', $str);
	
	if($to_int){
    	return (int)end($segments);
	} else{
    	return end($segments);
	}
}

function clean_output($str = ''){
	return htmlspecialchars($str);
}

function lang_title($str='no_title'){
    $CI = get_instance();
	echo $CI->lang->line($str);
}

function cut_str($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	return substr($trimmed_str, 0, $maxlength);
}

function text_elipsis($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	if(mb_strlen($trimmed_str) > $maxlength){
		return mb_substr($trimmed_str, 0, $maxlength).'...';
	}

	return $trimmed_str;
}

function tpl_replace($str = "", $_replace = array()){
	return str_replace(array_keys($_replace), array_values($_replace), $str);
}

function remove_dir($dir,$action = 'delete') {
	if(empty($dir)){
		return false;
	}
	
	if(!is_dir($dir)){
		return false;
	}

	foreach(glob($dir . '/*') as $file) {
		@unlink($file);
	}
	
	if($action == 'delete'){
		rmdir($dir);
	}
}

function create_dir($dir, $mode = 0755){
    if(!is_dir($dir))
        mkdir ($dir, $mode, true);
}

function file_modification_time($link){
    return base_url($link).'?'.md5(filemtime( $link ));
}

if (!function_exists('mb_ucfirst') && function_exists('mb_substr')) {
    function mb_ucfirst($string) {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
        return $string;
    }
}

function check_uri($uri_assoc = array(), $available_uri_segments = array(), $data = array()){
	if(empty($uri_assoc) && empty($available_uri_segments)){
		return true;
	}

	foreach($uri_assoc as $key_uri => $value_uri){
		if(!in_array($key_uri, $available_uri_segments)){
			return_404($data);
		}

		if(empty($value_uri)){
			return_404($data);
		}
	}
}

function date_plus($time_plus, $time_type='days', $from_date=false){
	if(!$from_date)
		$from_date = date('Y-m-d H:i:s');

	$result_date = $from_date;
	switch($time_type){
		default:
		case 'days':
			if($time_plus > 0){
				$result_date = date('Y-m-d H:i:s', strtotime($result_date . '+ '.$time_plus.' days'));
			} elseif($time_plus < 0){
				$result_date = date('Y-m-d H:i:s', strtotime($result_date . '- '.abs($time_plus).' days'));
			}
		break;
	}

	return $result_date;
}

function plural_text($text = '', $number = 0, $lang = 'ro'){
	$return_text = $text;
	switch ($text) {
		case 'year':
			switch ($lang) {
				case 'ro':
					$return_text = 'an';
					if($number == 0 || $number > 1){
						$return_text = 'ani';
					}
				break;
				case 'ru':
					$last_decimal = (int)substr($number, -1, 1);

					if($number == 0){
						$return_text = 'лет';
					}

					if($number == 1){
						$return_text = 'год';
					}

					if($number >= 5){
						$return_text = 'лет';
					}

					if($last_decimal > 1 && $last_decimal < 5 ){
						$return_text = 'года';
					}
				break;
				case 'en':
					$return_text = 'year';
					if($number > 1){
						$return_text = 'years';
					}
				break;
			}
		break;
	}

	return $return_text;
}

function plural_numword($num = 0, $singular_form = '', $plural_forms = '', $lang = 'ro'){
	if($num == 0){
		return $singular_form;
	}

	if(empty($plural_forms)){
		return $singular_form;
	}

	switch ($lang) {
		default:
		case 'en':
		case 'ro':
			if($num == 1){
				return $singular_form;
			} else{
				return $plural_forms;
			}
		break;
		case 'ru':
			$cases = array (2, 0, 1, 1, 1, 2);
			$word = array(
				$singular_form, 
				(!empty($plural_forms['2_4']))?$plural_forms['2_4']:$singular_form, 
				(!empty($plural_forms['5_']))?$plural_forms['5_']:$singular_form
			);

			return $word[ ($num%100 > 4 && $num %100 < 20) ? 2 : $cases[min($num%10, 5)] ];
		break;
	}
}

function get_keywords_combination($keywords = ''){
	$keywords = trim($keywords);
	$result = array();
	if(empty($keywords)){
		return $result;
	}
	
	if(strlen($keywords) <= 3){
		return '';
	}

	$words = explode(' ', $keywords);
	$total_words = count($words);
	
	if($total_words > 3){
		foreach ($words as $word) {
			$trimed_word = trim($word);
			$result[] = "+{$trimed_word}";
		}
		
		return implode(' ', $result);
	} else{
		switch($total_words){
			case 1:
				return "+{$words[0]}";
			break;
			case 2:
				if(strlen($words[0]) <= 3 || strlen($words[1]) <= 3){				
					return "+({$words[0]} {$words[1]})";
				} else{
					return "+{$words[0]} +{$words[1]}";
				}
			break;
			case 3:
				if(strlen($words[0]) <= 3 || strlen($words[1]) <= 3){
					if(strlen($words[2]) <= 3){
						return "+({$words[0]} {$words[1]} {$words[2]})";
					} else{
						return "+({$words[0]} {$words[1]}) +{$words[2]}";
					}			
				} elseif(strlen($words[1]) <= 3 || strlen($words[2]) <= 3){
					if(strlen($words[2]) <= 3){
						return "+{$words[0]} +(>{$words[1]} >{$words[2]})";
					} else{
						return "+{$words[0]} +>{$words[1]} +>{$words[2]}";
					}
				} else{
					return "+{$words[0]} +{$words[1]} +{$words[2]}";
				}
			break;
		}
	}	
}

function orderNumberOnly($id, $length = 5){
	return str_pad($id, $length, "0", STR_PAD_LEFT);
}

function set_menu_active($active_menu = null, $current = null, $class = "active"){
	return (!empty($active_menu) && !empty($current) && $active_menu == $current)?$class:'';
}

function is_plused($str, $compare_str = '+'){
	$last_char = substr($str, -1);
	return $last_char == $compare_str;
}

function clean_plused($str){
	if(is_plused($str))
		return substr($str, 0, -1);
	else
		return $str;
}

function get_og_image($path_to_image = ''){
	if(empty($path_to_image)){
		return false;
	}

	if(!image_exist($path_to_image)){
		return false;
	}

	list($image_width, $image_height, $image_type, $image_attr) = getimagesize($path_to_image);

	$result = array(
		'src' => site_url($path_to_image),
		'width' => $image_width,
		'height' => $image_height
	);
	
	return $result;
}

function set_favorite($id_item = 0, $type = "imobil", $_return_string = "hasWishList"){
	$_cookie_favorite = get_cookie('_ai_favorite');
	if(empty($_cookie_favorite)){
		return;
	}

	$_cookie_favorite = json_decode(base64_decode($_cookie_favorite), true);
	if(empty($_cookie_favorite[$type])){
		return;
	}

	if(in_array($id_item, $_cookie_favorite[$type])){
		return $_return_string;
	}
}

function compress_image($source, $quality = 75) {
	//Get file extension
	// convert "{}" -sampling-factor 4:2:0 -strip -quality 75 -interlace JPEG -colorspace RGB
	// convert "{}" -strip "{}"
    $exploding = explode(".",$source);
    $ext = end($exploding);
	$path = FCPATH.$source;
    switch(strtolower($ext)){
        case "png":
        case "gif":
			$cmd = '/usr/bin/convert '.$path.' -strip '.$path;
		break;
		default:
        case "jpeg":
		case "jpg":
			$cmd = '/usr/bin/convert '.$path.' -sampling-factor 4:2:0 -strip -quality 65 -interlace JPEG -colorspace RGB '.$path;
		break;
	}
	
	exec( $cmd);
}

function image_info($image = '', $key = 'w'){
	$image_info = getimagesize($image);
	$result = '';
	switch ($key) {
		case 'w':
			$result = $image_info['0'];
		break;
		case 'h':
			$result = $image_info['1'];
		break;
	}

	return $result;
}

function img_srcset($path = '', $image = '', $thumbs = array(180, 220)){
	$set = array();
	foreach ($thumbs as $thumb) {
		if(file_exists($path.'/thumb_'.$thumb.'_'.$image)){
			$width = image_info($path.'/thumb_'.$thumb.'_'.$image, 'w');
			$set[] = site_url($path.'/thumb_'.$thumb.'_'.$image).' '.$width.'w';
			// compress_image($path.'/thumb_'.$thumb.'_'.$image);
		} else{
			$new_thumb = get_image_thumb($path.'/'.$image, $thumb, array('h' => $thumb));
			$width = image_info($new_thumb, 'w');
			$set[] = site_url($new_thumb).' '.$width.'w';
		}
	}

	if(!empty($set)){
		$set = implode(', ', $set);
		return 'srcset="'.$set.'"';
	}

	return '';
}

function get_image_thumb($path = '', $thumb = '', $image_size = array()){
	if(!file_exists($path)){
		return 'files/images/no_image.png';
	}

	$components = explode('/', $path);
	$image = array_pop($components);
	$path = implode('/', $components);

	if(file_exists($path.'/thumb_'.$thumb.'_'.$image)){
		// compress_image($path.'/thumb_'.$thumb.'_'.$image);
		return $path.'/thumb_'.$thumb.'_'.$image;
	}

	$ci = & get_instance();
	$ci->load->library('image_lib');
	$config = array(
		'source_image'      => $path.'/'.$image,
		'create_thumb'   	=> TRUE,
		'maintain_ratio'   	=> TRUE,
		'thumb_marker'		=> 'thumb_'.$thumb.'_',
		'quality' 			=> 70
	);

	if(isset($image_size['w'])){
		$config['width'] = $image_size['w'];
	}

	if(isset($image_size['h'])){
		$config['height'] = $image_size['h'];
	}

	$ci->image_lib->initialize($config);
	$ci->image_lib->resize();
	$ci->image_lib->clear();

	compress_image($path.'/thumb_'.$thumb.'_'.$image);
	return $path.'/thumb_'.$thumb.'_'.$image;
}