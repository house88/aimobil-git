<?php
class MY_Image_lib extends CI_Image_lib {

    public function initialize($props = array()) {
        parent::initialize($props);

        $xp = $this->explode_name($this->dest_image);

        $filename = $xp['name'];
        $file_ext = $xp['ext'];

        $this->full_dst_path = $this->dest_folder.$this->thumb_marker.$filename.$file_ext;
    }

    function brightnes($b = 10){
        $im = $this->image_create_gd();
        
        if($im && imagefilter($im, IMG_FILTER_BRIGHTNESS, $b))
        {
            $this->image_save_gd($im);
        }
        imagedestroy($im);
    }

    function contrast($c = -15){
        $im = $this->image_create_gd();
        if($im && imagefilter($im, IMG_FILTER_CONTRAST, $c))
        {
            $this->image_save_gd($im);
        }

        imagedestroy($im);
    }
}
