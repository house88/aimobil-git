<?php
class MY_Upload extends CI_Upload {

    public function __construct($config = array())
    {
            parent::__construct($config);
    }

    /**
	 * Display the error message
	 *
	 * @param	string	$open
	 * @param	string	$close
	 * @return	string
	 */
	public function display_custom_errors($open = '<p>', $close = '</p>'){	
		foreach ($this->error_msg as $key_error => $error) {
			switch ($key_error)
			{
				case 'upload_invalid_filesize':
					$str = array();
					if($this->max_size > 0){
						// $mb_size = $this->max_size/1024;
						$str[] = lang_line('uploaded_file_is_too_big', false);
					}

					if(!empty($str)){
						$this->error_msg[$key_error] = $error.' '.implode(' ', $str);
					}
				break;
				case 'upload_invalid_dimensions':
					$str = array();
					if($this->max_width > 0){
						$str[] = "Ширина не должна превышать {$this->max_width}px.";
					}
					if($this->max_height > 0){
						$str[] = "Высота не должна превышать {$this->max_height}px.";
					}
					if($this->min_width > 0){
						$str[] = "Ширина не должна быть меньше {$this->min_width}px.";
					}
					if($this->min_height > 0){
						$str[] = "Высота не должна быть меньше {$this->min_height}px.";
					}

					if(!empty($str)){
						$this->error_msg[$key_error] = $error.' '.implode(' ', $str);
					}
				break;
			}
		}

		return (count($this->error_msg) > 0) ? $open.implode($close.$open, $this->error_msg).$close : '';
	}
}
