<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pagination_lib{
	public function get_settings($id, $conditions = array()){
        extract($conditions);

        $config = array();
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
        $config['prefix'] = 'page/';
		$config['first_link'] = '<i class="fai fai-left-arrow-end-o"></i>';
		$config['prev_link'] = '<i class="fai fai-left-arrow-o"></i>';
		$config['next_link'] = '<i class="fai fai-right-arrow-o"></i>';
		$config['last_link'] = '<i class="fai fai-right-arrow-end-o"></i>';

		// открывющий тэг перед навигацией
        $config['full_tag_open'] = '<ul class="pagination pagination-centered">';

        // закрывающий тэг после навигации
        $config['full_tag_close'] = '</ul>';

        // первая страница открывающий тэг
        $config['first_tag_open'] = '<li>';

        // первая страница закрывающий тэг
        $config['first_tag_close'] = '</li>';

        // последняя страница открывающий тэг
        $config['last_tag_open'] = '<li>';

        // последняя страница закрывающий тэг
        $config['last_tag_close'] = '</li>';

        // предыдущая страница открывающий тэг
        $config['prev_tag_open'] = '<li>';

        // предыдущая страница закрывающий тэг
        $config['prev_tag_close'] = '</li>';

        // текущая страница открывающий тэг
        $config['cur_tag_open'] = '<li class="active"><span>';

        // текущая страница закрывающий тэг
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li>'; // цифровая ссылка открывающий тэг
        $config['num_tag_close'] = '</li>'; // цифровая ссылка закрывающий тэг

        // следующая страница открывающий тэг
        $config['next_tag_open'] = '<li>';

        // следующая страница закрывающий тэг
        $config['next_tag_close'] = '</li>';

		switch($id){
			case 'blog':
                $config['base_url'] = site_url(get_dinamyc_uri('blog', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('blog', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['use_page_numbers'] = TRUE;
                return $config;
            break;
            case 'residential-complexes':
                $config['base_url'] = site_url(get_dinamyc_uri('residential-complexes', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('residential-complexes', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
            case 'discounts':
                $config['base_url'] = site_url(get_dinamyc_uri('discounts', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('discounts', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['cur_page'] = $cur_page;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
            case 're_same':
                $config['base_url'] = site_url(get_dinamyc_uri('catalog/category/real_estate', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('catalog/category/real_estate', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['cur_page'] = $cur_page;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
            case 'catalog/search':
                $config['base_url'] = site_url(get_dinamyc_uri('catalog/search', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('catalog/search', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['cur_page'] = $cur_page;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
            case 'manager':
                $config['base_url'] = site_url(get_dinamyc_uri('manager', $link, $lang));
                $config['first_url'] = site_url(get_dinamyc_uri('manager', $link, $lang));
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['cur_page'] = $cur_page;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
            case 'catalog/type/category':
                $config['base_url'] = site_url(get_dinamyc_uri('catalog/type/category', $link, $lang));
                // $config['first_link'] = FALSE;
                // $config['last_link'] = FALSE;
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['cur_page'] = $cur_page;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
		}
    }
    
    public function get_pages(){
        $total = 43;
        $limit = 5;
        $total_pages = ceil($total/$limit);
        echo $total_pages;
    }
}
