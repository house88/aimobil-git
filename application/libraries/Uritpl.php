<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uritpl {
	var $replace_template = null;

	function __construct(){
		$this->replace_template = '._-RPC._-';
    }

    public function replace_dynamic_uri($replace = '', $str = '', $prefix = ''){
        return $prefix.str_replace($this->replace_template, $replace, $str);
    }
    
    private function assoc_to_uri($array)
	{
		$temp = array();
		foreach ((array) $array as $key => $val)
		{
			$temp[] = $key;
			$temp[] = $val;
		}

		return implode('/', $temp);
    }
    
    private function query_to_segments($query, $offset = 0){
		$segments = explode('=', trim($query, '='));
		$segments = array_slice($segments, $offset > 0 ? $offset - 1 : 0);
		return $segments;
	}

	private function query_to_assoc($query = null, $offset = 0){
		if(empty($query)){
			return (isset($_GET))?$_GET:array();
		}

		$segments = $this->query_to_segments($query, $offset);
		$i = 0;
		$lastval = '';
		$assoc = array();
		foreach ($segments as $seg)
		{
			if ($i % 2)
			{
				$assoc[$lastval] = $seg;
			}
			else
			{
				$assoc[$seg] = NULL;
				$lastval = $seg;
			}

			$i++;
		}

		return $assoc;
	}

	function make_templates(array $map, array $uri_segments = [], $exclude_uri_segments = false){
		$uri_filter_keys = $uri_map_diff = [];
		foreach($map as $index => $item){
			if(!(isset($item['type']) && $item['type'] === 'uri')){
				continue;
			}
			$uri_filter_keys[$index] = null;
			if(!isset($uri_segments[$index])){
				$uri_map_diff[$index] = 1;
			}
		}

		$uri_map_data = array_merge($uri_filter_keys, $uri_segments);
		$update_get = $update_uri = function(&$arr, $key){
			$arr[$key] = $this->replace_template;
		};

		$query_segments = $this->query_to_assoc();
		foreach($map as $param => $entry){
			$uri = $uri_map_data;
			$get = $query_segments;
			$no_data_clean = $uri_map_diff;

			if(isset($no_data_clean[$param])){
				unset($no_data_clean[$param]);
			}

			$to_clean = (!empty($entry['deny'])) ? array_flip($entry['deny']) : [];
			if(!empty($to_clean)){
				$to_clean = array_merge($to_clean, $no_data_clean);
			} else {
				$to_clean = $no_data_clean;
			}

			$uri = array_diff_key($uri, $to_clean);
			$get = array_diff_key($get, $to_clean);

			if(isset($entry['type']) && isset(${"update_{$entry['type']}"})){
				if(!$exclude_uri_segments){
					${"update_{$entry['type']}"}(${$entry['type']}, $param);
				}
			}

			$uri = $this->assoc_to_uri($uri);
			$get = !empty($get) ? '?' . http_build_query($get) : '';
			$templates[$param] = "{$uri}{$get}";
		}

		return $templates;
	}
}