<?php
require(APPPATH.'/third/mpdf/mpdf.php');
class Pdf Extends mPDF{
    public $model         = '';
    public $format        = 'A4';
    public $font_size     = 0;
    public $font_family   = 'arial';
    public $margin_left   = 0;
    public $margin_right  = 0;
    public $margin_top    = 5;
    public $margin_bottom = 10;
    public $margin_height = 0;
    public $margin_footer = 0;
    public $orientation   = 'P';
    public function __construct(){}

	public function config(array $param = array()){
		extract($param);
        if(isset($model)){
            $this->model         = $model;
        }
        if(isset($format)){
            $this->format        = $format;
        }
        if(isset($font_size)){
            $this->font_size     = $font_size;
        }

        if(isset($font_family)){
            $this->font_family   = $font_family;
        }

        if(isset($margin_left)){
            $this->margin_left   = $margin_left;
        }

        if(isset($margin_right)){
            $this->margin_right  = $margin_right;
        }

        if(isset($margin_top)){
            $this->margin_top    = $margin_top;
        }

        if(isset($margin_bottom)){
            $this->margin_bottom = $margin_bottom;
        }

        if(isset($margin_height)){
            $this->margin_height = $margin_height;
        }

        if(isset($margin_footer)){
            $this->margin_footer = $margin_footer;
        }

        if(isset($orientation)){
            $this->orientation   = $orientation;
        }
	}

    public function new_pdf(){
        return new mPDF($this->model,$this->format,$this->font_size,$this->font_family,$this->margin_left,$this->margin_right,$this->margin_top,$this->margin_bottom,$this->margin_height,$this->margin_footer,$this->orientation);
    }
}
