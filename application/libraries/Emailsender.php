<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once('PHPMailer/PHPMailerAutoload.php');

class Emailsender{
	var $ci = null;
	var $phpmail = null;

	public function __construct(){
		// parent::__construct();

		$this->ci =& get_instance();

		// $this->phpmail = new PHPMailer();
        // $this->phpmail->CharSet = 'utf-8';
		// $this->phpmail->isSMTP();
		// $this->phpmail->SMTPDebug = 0;
		// $this->phpmail->SMTPAuth   = true;
		// $this->phpmail->Debugoutput = 'html';
		// $this->phpmail->DKIM_private = $_SERVER["DOCUMENT_ROOT"].'/.htkeyprivate';
		// $this->phpmail->DKIM_passphrase = '1402781400';
		// $this->phpmail->DKIM_selector = $this->ci->config->item('DKIM_SELECTOR');
	}

	/*
	**	USAGE EXAMPLE
		$params = array(
			'to' => 'cravciucandy@gmail.com',
			'subject' => 'Testing the email class.',
			'template' => 'ru/00010.html',
			'template_replace' => array(
				"{email}" => 'cravciucandy@gmail.com',
				"{name}" => 'Cravciuc Andrei'
			)
		);
		$this->emailsender->send_email($params);
	*/
	public function send_email($params = array()){
		extract($params);

		if(!isset($from,$to,$subject,$template)){
			return false;
		}
		

		$message = $this->ci->load->view(get_theme_view('email_templates/email_tpl_view'), array('email_content' => $template), true);
		if(!empty($template_replace)){
			$message = str_replace(array_keys($template_replace), array_values($template_replace), $message);
		}

		$config['smtp_user'] = "cravciucandy@gmail.com"; 
		$config['smtp_pass'] = "ameritrex.88";
		$this->ci->email->initialize($config);

		$this->ci->email->from($from);
		$this->ci->email->to($to);					
		$this->ci->email->subject($subject);
		$this->ci->email->message($message);
		$this->ci->email->set_mailtype("html");
		$this->ci->email->send();
		// $logs = $this->ci->email->print_debugger();
	}

	public function usend_email($params = array()){
		extract($params);

		if(!isset($from,$to,$subject,$template)){
			return false;
		}
		

		$message = $this->ci->load->view(get_theme_view('email_templates/uemail_tpl_view'), array('email_content' => $template), true);
		if(!empty($template_replace)){
			$message = str_replace(array_keys($template_replace), array_values($template_replace), $message);
		}

		$config['smtp_user'] = "cravciucandy@gmail.com"; 
		$config['smtp_pass'] = "ameritrex.88";
		$this->ci->email->initialize($config);

		$this->ci->email->from($from);
		$this->ci->email->to($to);					
		$this->ci->email->subject($subject);
		$this->ci->email->message($message);
		$this->ci->email->set_mailtype("html");
		$this->ci->email->send();
		// $logs = $this->ci->email->print_debugger();
	}
}
