<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crons extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');
		$this->config->set_item('compress_output', FALSE);

        $this->data = array();
    }
    
    function index(){
        return false;
    }
	
	function re_delete(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
        }

		ini_set('max_execution_time', 0);

		$this->load->model("real_estate/Real_estate_model", "real_estate");

		$params = array(
            'real_estate_sold' => 1,
			'real_estate_sold_date' => date_plus((-1)*$this->lsettings->item('real_estate_delete_sold_days'))
		);
		$records = $this->real_estate->handler_get_all($params);
        if(!empty($records)){
			$id_real_estate = array_column($records, 'id_real_estate');
			$update = array(
				'real_estate_active' => 0,
				'advert_status_999' => 'private',
				'real_estate_active_date' => date('Y-m-d H:i:s')
			);
			$this->real_estate->handler_update_all($update, array('id_real_estate' => $id_real_estate));

			foreach ($records as $record) {
				$record['advert_status_999'] = 'private';
				$this->_advert_status($record);
				
				// remove_dir("files/real_estate/{$record['id_real_estate']}");
				
				// $_same_params = Modules::run('real_estate/_get_same_params', $record['id_real_estate']);
				// $insert_deleted = array(
				// 	'id_real_estate' => $record['id_real_estate'], 
				// 	'full_url_ro' => $record['full_url_ro'], 
				// 	'full_url_ru' => $record['full_url_ru'], 
				// 	'full_url_en' => $record['full_url_en'], 
				// 	'real_estate_same_params' => json_encode($_same_params)
				// );
				// $this->real_estate->handler_insert_deleted($insert_deleted);

				// $this->real_estate->handler_delete($record['id_real_estate']);
			}
        }
	}

	
	function _advert_status($real_estate = array()){
		if($real_estate['id_999'] > 0){
			$manager = Modules::run('users/_get_user', $real_estate['id_manager']);
	
			$http_basic_auth_credentials = "{$manager->user_999_api_key}:password";
			$url = 'https://partners-api.999.md/adverts/'.$real_estate['id_999'].'/access_policy';
			$post_content = json_encode(array('access_policy' => $real_estate['advert_status_999']));
			$result = $this->_curl_999($http_basic_auth_credentials, $url, 'PUT', $post_content);
		}
	}

	function _curl_999($http_basic_auth_credentials = '', $url = '', $method = 'POST', $content = array(), $headers = array("Content-Type: application/json")){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "{$method}");
		curl_setopt($ch, CURLOPT_USERPWD, $http_basic_auth_credentials);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if(curl_errno($ch)) {
			return false;
		}
		curl_close($ch);
		
		$result = json_decode($result, true);
		return $result;
	}
	
	function notify_subscribers(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
        }

		ini_set('max_execution_time', 0);

		$this->load->model("notify/Notify_model", "notify");
		$this->load->model("real_estate/Real_estate_model", "real_estate");
		$this->load->model('subscribers/Subscribers_model', 'subscribers');
	
		$properties_on_item_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties_on_items = $this->real_estate->handler_get_properties_values($properties_on_item_params);

		$this->data['template'] = $this->notify->handler_get_by_alias('notify_subscriber');

		$subscribers = $this->subscribers->handler_get_all(array('subscribe_active' => 1));			
		foreach ($subscribers as $subscriber) {
			$subscribe_settings = json_decode($subscriber['subscribe_settings'], true);
			$same_params = array(
				'id_category' => $subscribe_settings['id_category'],
				'price_from' => $subscribe_settings['price_from'],
				'price_to' => $subscribe_settings['price_to'],
				'real_estate_active' => 1,
				'created_from' => date("Y-m-d H:i:s", strtotime("-1 days")),
				'id_real_estate' => array(0)
			);
			
			$area_params = array(
				'property_range_from' => $subscribe_settings['property_range_from'],
				'property_range_to' => $subscribe_settings['property_range_to']
			);
			$re_by_area = $this->real_estate->handler_get_properties_values($area_params);
	
			if(!empty($re_by_area)){
				foreach ($re_by_area as $re_by_area_item) {
					$same_params['id_real_estate'][] = $re_by_area_item['id_real_estate'];
				}
			}

			$this->data['real_estates'] = $this->real_estate->handler_get_all($same_params);
			
			$this->data['re_properties'] = array();
			if(!empty($this->data['real_estates'])){
				if(!empty($properties_on_items)){
					foreach ($properties_on_items as $property) {
						$property_settings = json_decode($property['property_settings'], true);
		
						switch ($property['property_type']) {
							case 'multiselect':
								$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
								$unit_name = $property_settings['unit'][lang_column('title')];
								if(isset($property_values[$property['property_value']])){
									$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], 'ro');
									if($property['property_value_sufix'] == 1){
										$unit_name = lang_line('label_living', false);
									}
									$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
										'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
										'unit_name' => $unit_name
									);
								}
							break;
							case 'range':
								$property_value = array($property['property_value']);
								
								if($property_settings['repeat_values'] == true){
									$property_value[] = $property['property_value_max'];
								}
								$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], 'ro');
								
								$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => implode('/', $property_value),
									'unit_name' => $unit_name
								);
							break;
						}
					}
				}

				$this->data['lang'] = $subscriber['subscribe_lang'];
				switch ($this->data['lang']) {
					default:
					case 'ro':
						$lang_uri = '';
					break;
					case 'en':
						$lang_uri = 'en/';
					break;
					case 'ru':
						$lang_uri = 'ru/';
					break;
				}

				$items_list = $this->load->view(get_theme_view('email_templates/notify_subscriber_re_list'), $this->data, true);
				// echo $items_list;
				$email_config = array(
					'from' => $this->lsettings->item('no_reply_email'),
					'to' => $subscriber['subscribe_email'],
					'subject' => $this->data['template']["template_subject_{$this->data['lang']}"],
					'template' => 'notify_subscriber_tpl',
					'template_replace' => array(
						'{{title}}' => $this->data['template']["template_subject_{$this->data['lang']}"],
						'{{items}}' => $items_list,
						'{{text}}' => $this->data['template']["template_text_{$this->data['lang']}"],
						'{{unsubscribe_link}}' => site_url($lang_uri.get_static_uri('unsubscribe',$this->data['lang']).'/'.$subscriber['subscribe_token'])
					)
				);
				
				$this->emailsender->usend_email($email_config);
			}
		}
	}
	
	function grating(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
		}
		
		$_gmap_place = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJVch18Et8yUAR0HW1oP7CBeY&key='.$this->lsettings->item('google_maps_api_key')), true);
        if ($_gmap_place['status'] == 'OK') {
            $update = array(
                'setting_value_ro' => (float) @$_gmap_place['result']['rating'], 
                'setting_value_ru' => (float) @$_gmap_place['result']['rating'], 
                'setting_value_en' => (float) @$_gmap_place['result']['rating']
            );
            $this->settings->update_setting_by_alias('google_rating', $update);
        }
	}
}
