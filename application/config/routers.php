<?php 
$site_routers = array (
  'contacts' => 
  array (
    'ro' => 'contacte',
    'ru' => 'kontakty',
    'en' => 'contacts',
  ),
  'about_us' => 
  array (
    'ro' => 'companie-imobiliara',
    'ru' => 'kompanija-po-nedvizhimosti',
    'en' => 'real-estate-company',
  ),
  'services' => 
  array (
    'ro' => 'servicii-imobiliare',
    'ru' => 'rieltorskie-uslugi',
    'en' => 'real-estate-services',
  ),
  'unsubscribe' => 
  array (
    'ro' => 'unsubscribe',
    'ru' => 'unsubscribe',
    'en' => 'unsubscribe',
  ),
  'add_ad' => 
  array (
    'ro' => 'adauga-anunt',
    'ru' => 'podati-obijavlenie',
    'en' => 'add-a-listing',
  ),
  'credit-ipotecar' => 
  array (
    'ro' => 'credit-ipotecar',
    'ru' => 'ipotechnyj-kredit',
    'en' => 'mortgage',
  ),
  'vacancies' => 
  array (
    'ro' => 'posturi-vacante',
    'ru' => 'vacansii',
    'en' => 'job-vacancies',
  ),
  'partners' => 
  array (
    'ro' => 'companii-constructie',
    'ru' => 'stroitelinye-kompanii',
    'en' => 'construction-companies',
  ),
  'planing' => 
  array (
    'ro' => 'planificare-apartamente',
    'ru' => 'planirovki-kvartir',
    'en' => 'apartment-planning',
  ),
  'residential-complexes' => 
  array (
    'ro' => 'complexe-locative',
    'ru' => 'novostroiki-kishinieva',
    'en' => 'residential-complexes',
  ),
  'discounts' => 
  array (
    'ro' => 'imobile-pret-redus',
    'ru' => 'nedvizhimosti-nizkie-ceny',
    'en' => 'real-estate-discount',
  ),
  'catalog/search' => 
  array (
    'ro' => 'cautare-imobil',
    'ru' => 'poisk-nedvizhimosti',
    'en' => 'search-real-estate',
  ),
  'favorite' => 
  array (
    'ro' => 'favorite',
    'ru' => 'izbrannoye',
    'en' => 'favorites',
  ),
  'manager' => 
  array (
    'ro' => 'manager',
    'ru' => 'menedzher',
    'en' => 'manager',
  ),
  'blog' => 
  array (
    'ro' => 'blog',
    'ru' => 'blog',
    'en' => 'blog',
  ),
);