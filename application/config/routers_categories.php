<?php 
$categories_routers = array (
  0 => 
  array (
    'ro' => 'apartamente-vanzare',
    'ru' => 'prodazha-kvartir',
    'en' => 'apartments-for-sale',
  ),
  1 => 
  array (
    'ro' => 'case-vanzare',
    'ru' => 'prodazha-domov',
    'en' => 'houses-for-sale',
  ),
  2 => 
  array (
    'ro' => 'spatii-comerciale-vanzare',
    'ru' => 'prodazha-kommercheskaja-nedvizhimosti',
    'en' => 'commercial-spaces-for-sale',
  ),
  3 => 
  array (
    'ro' => 'terenuri-vanzare',
    'ru' => 'prodazha-zemelinye-uchastki',
    'en' => 'land-for-sale',
  ),
  4 => 
  array (
    'ro' => 'apartamente-chirie',
    'ru' => 'arenda-kvartir',
    'en' => 'apartments-for-rent',
  ),
  5 => 
  array (
    'ro' => 'case-chirie',
    'ru' => 'arenda-domov',
    'en' => 'houses-for-rent',
  ),
  6 => 
  array (
    'ro' => 'spatii-comerciale-chirie',
    'ru' => 'arenda-kommercheskaja-nedvizhimosti',
    'en' => 'commercial-spaces-for-rent',
  ),
);