<?php 
$services_routers = array (
  0 => 
  array (
    'id_service' => '4',
    'ro' => 'vanzare-cumparare-bunuri-imobiliare',
    'ru' => 'kuplja-prodazha-nedvizhimosti',
    'en' => 'buying-selling-real-estate-services',
  ),
  1 => 
  array (
    'id_service' => '3',
    'ro' => 'inchiriere-imobile',
    'ru' => 'arenda-nedvizhimosti',
    'en' => 'real-estate-renting-services',
  ),
  2 => 
  array (
    'id_service' => '2',
    'ro' => 'asistenta-juridica',
    'ru' => 'juridicheskaja-podderzhka',
    'en' => 'legal-assistance',
  ),
  3 => 
  array (
    'id_service' => '1',
    'ro' => 'reparatie-apartamente',
    'ru' => 'remont-kvartir',
    'en' => 'repair-of-apartments',
  ),
);