<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = "home";

function _get_site_routers(){
	require APPPATH . 'config/routers.php';

	return $site_routers;
}

function _get_categories_routers(){
	require APPPATH . 'config/routers_categories.php';

	return $categories_routers;
}

function _get_services_routers(){
	require APPPATH . 'config/routers_services.php';

	return $services_routers;
}

$site_routers = _get_site_routers();
$categories_routers = _get_categories_routers();
$services_routers = _get_services_routers();

function get_static_uri($uri = '', $lang = false){
	$links = _get_site_routers();

	if($lang === false){
		return (isset($links[$uri]))?$links[$uri]:$uri;
	}

	return (isset($links[$uri][$lang]))?$links[$uri][$lang]:$uri;
}

function get_dinamyc_uri($uri_key = '', $uri_dinamyc = false, $lang = false){
	$site_routers = _get_site_routers();
	$links = array(
		'services/detail/id' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'pages/special/id' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'residential-complexes' => array(
			'ro' => $site_routers['residential-complexes']['ro'].'{url_ro}',
			'ru' => $site_routers['residential-complexes']['ru'].'{url_ru}',
			'en' => $site_routers['residential-complexes']['en'].'{url_en}'
		),
		'residential-complexes/detail/id' => array(
			'ro' => $site_routers['residential-complexes']['ro'].'/{url_ro}',
			'ru' => $site_routers['residential-complexes']['ru'].'/{url_ru}',
			'en' => $site_routers['residential-complexes']['en'].'/{url_en}'
		),
		'discounts' => array(
			'ro' => $site_routers['discounts']['ro'].'{url_ro}',
			'ru' => $site_routers['discounts']['ru'].'{url_ru}',
			'en' => $site_routers['discounts']['en'].'{url_en}'
		),
		'blog' => array(
			'ro' => $site_routers['blog']['ro'].'{url_ro}',
			'ru' => $site_routers['blog']['ru'].'{url_ru}',
			'en' => $site_routers['blog']['en'].'{url_en}'
		),
		'catalog/search' => array(
			'ro' => $site_routers['catalog/search']['ro'].'{url_ro}',
			'ru' => $site_routers['catalog/search']['ru'].'{url_ru}',
			'en' => $site_routers['catalog/search']['en'].'{url_en}'
		),
		'catalog/type/category' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'manager' => array(
			'ro' => $site_routers['manager']['ro'].'/{url_ro}',
			'ru' => $site_routers['manager']['ru'].'/{url_ru}',
			'en' => $site_routers['manager']['en'].'/{url_en}'
		),
		'catalog/category/real_estate' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
	);
	
	if($lang === false){
		return array(
			'ro' => str_replace('{url_ro}', $uri_dinamyc['ro'], $links[$uri_key]['ro']),
			'ru' => str_replace('{url_ru}', $uri_dinamyc['ru'], $links[$uri_key]['ru']),
			'en' => str_replace('{url_en}', $uri_dinamyc['en'], $links[$uri_key]['en']),
		);
	}

	return str_replace('{url_'.$lang.'}', $uri_dinamyc, $links[$uri_key][$lang]);
}

foreach ($categories_routers as $category_router) {
	// CATALOG
	$route['^('.$category_router['ro'].'|ru/'.$category_router['ru'].'|en/'.$category_router['en'].')'] 	= "real_estate/index";
	$route['^('.$category_router['ro'].'/page/(:any)|ru/'.$category_router['ru'].'/page/(:any)|en/'.$category_router['en'].'/page/(:any))'] 	= "real_estate/index/page/$1";
	// CATALOG ITEM
	$route['^('.$category_router['ro'].'/(:any)|ru/'.$category_router['ru'].'/(:any)|en/'.$category_router['en'].'/(:any))'] 	= "real_estate/detail/$1";
	// CATALOG ITEM DELETED
	$route['^('.$category_router['ro'].'/(:any)/page/(:num)|ru/'.$category_router['ru'].'/(:any)/page/(:num)|en/'.$category_router['en'].'/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
}

// DISCOUNTS
$route['^('.$site_routers['discounts']['ro'].'|ru/'.$site_routers['discounts']['ru'].'|en/'.$site_routers['discounts']['en'].')'] 	= "real_estate/discounts";
$route['^('.$site_routers['discounts']['ro'].'/page/(:any)|ru/'.$site_routers['discounts']['ru'].'/page/(:any)|en/'.$site_routers['discounts']['en'].'/page/(:any))'] 	= "real_estate/discounts/page/$1";

// SEARCH 
$route['^('.$site_routers['catalog/search']['ro'].'|ru/'.$site_routers['catalog/search']['ru'].'|en/'.$site_routers['catalog/search']['en'].')'] 	= "real_estate/search";

// RESIDENTIAL COMPLEXES
$route['^('.$site_routers['residential-complexes']['ro'].'|ru/'.$site_routers['residential-complexes']['ru'].'|en/'.$site_routers['residential-complexes']['en'].')'] 	= "residential_complexes/index";
$route['^('.$site_routers['residential-complexes']['ro'].'/page/(:any)|ru/'.$site_routers['residential-complexes']['ru'].'/page/(:any)|en/'.$site_routers['residential-complexes']['en'].'/page/(:any))'] 	= "residential_complexes/index/page/$1";
$route['^('.$site_routers['residential-complexes']['ro'].'/(:any)|ru/'.$site_routers['residential-complexes']['ru'].'/(:any)|en/'.$site_routers['residential-complexes']['en'].'/(:any))'] 	= "residential_complexes/detail/$1";

// SPECIAL PAGES
$route['^('.$site_routers['credit-ipotecar']['ro'].'|ru/'.$site_routers['credit-ipotecar']['ru'].'|en/'.$site_routers['credit-ipotecar']['en'].')'] 	= "pages/special/credit-ipotecar";
$route['^('.$site_routers['add_ad']['ro'].'|ru/'.$site_routers['add_ad']['ru'].'|en/'.$site_routers['add_ad']['en'].')'] 	= "pages/add_ad";
$route['^('.$site_routers['unsubscribe']['ro'].'|ru/'.$site_routers['unsubscribe']['ru'].'|en/'.$site_routers['unsubscribe']['en'].')'] 	= "notify/unsubscribe/$1";
$route['^('.$site_routers['about_us']['ro'].'|ru/'.$site_routers['about_us']['ru'].'|en/'.$site_routers['about_us']['en'].')'] 	= "pages/about_us";
$route['^('.$site_routers['contacts']['ro'].'|ru/'.$site_routers['contacts']['ru'].'|en/'.$site_routers['contacts']['en'].')'] 	= "pages/contacts";
// END SPECIAL PAGES

// SERVICES
$route['^('.$site_routers['services']['ro'].'|ru/'.$site_routers['services']['ru'].'|en/'.$site_routers['services']['en'].')'] 	= "services/index";
foreach ($services_routers as $service_router) {
	$route['^('.$service_router['ro'].'|ru/'.$service_router['ru'].'|en/'.$service_router['en'].')'] 	= "services/detail/{$service_router['id_service']}";
}
// END SERVICES

// PLANING
$route['^('.$site_routers['planing']['ro'].'|ru/'.$site_routers['planing']['ru'].'|en/'.$site_routers['planing']['en'].')'] 	= "planing/index";
// END PLANING

// VACANCIES
$route['^('.$site_routers['favorite']['ro'].'|ru/'.$site_routers['favorite']['ru'].'|en/'.$site_routers['favorite']['en'].')'] 	= "favorite/index";
$route['^(ru/|en/|)favorite/ajax_operations/(:any)'] 	= "favorite/ajax_operations/$1";
// END VACANCIES
// VACANCIES
$route['^('.$site_routers['vacancies']['ro'].'|ru/'.$site_routers['vacancies']['ru'].'|en/'.$site_routers['vacancies']['en'].')'] 	= "vacancies/index";
$route['^(ru/|en/|)vacancies/ajax_operations/(:any)'] 	= "vacancies/ajax_operations/$1";
// END VACANCIES

// PARTNERS
$route['^('.$site_routers['partners']['ro'].'|ru/'.$site_routers['partners']['ru'].'|en/'.$site_routers['partners']['en'].')'] 	= "partners/index";
// END PARTNERS

// BLOG
$route['^('.$site_routers['blog']['ro'].'|ru/'.$site_routers['blog']['ru'].'|en/'.$site_routers['blog']['en'].')/(:any)'] 	= "blog/post/$2";
$route['^('.$site_routers['blog']['ro'].'|ru/'.$site_routers['blog']['ru'].'|en/'.$site_routers['blog']['en'].')'] 	= "blog/index";
$route['^('.$site_routers['blog']['ro'].'|ru/'.$site_routers['blog']['ru'].'|en/'.$site_routers['blog']['en'].')/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'blog/index/' . strtolower($categories);
};
// END BLOG

// MANAGER
$route['^('.$site_routers['manager']['ro'].'|ru/'.$site_routers['manager']['ru'].'|en/'.$site_routers['manager']['en'].')'] 	= "users/manager";
$route['^('.$site_routers['manager']['ro'].'|ru/'.$site_routers['manager']['ru'].'|en/'.$site_routers['manager']['en'].')/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'users/manager/' . strtolower($categories);
};
// END MANAGER

$route['^(ru/|en/|)notify/ajax_operations/(:any)'] 	= "notify/ajax_operations/$1";
$route['signout'] 	= "auth/signout";
$route['^(ru|en|)$'] = $route['default_controller'];
$route['^(ru|en|)/(.+)$'] = "$2";
$route['404_override'] = 'error/e404';

// ADMIN
$route['admin/([a-zA-Z0-9\/\-:._]+)'] = function ($module){
	$full_route = explode('/', $module);
	$module_name = array_shift($full_route);
	switch ($module_name) {
		case 'signin':
		case 'settings':
		case 'routings':
		case 'contacts':
		case 'translations':
		case 'ajax_operations':
		case 'popup_forms':
			return 'admin/'.$module;
		break;
		case 'edit_user':
		case 'managers':
		case 'groups':
		case 'groups_rights':
			$url[] = 'users/admin';
			$url[] = $module_name;
		break;
		default:
			$url[] = $module_name;
			$url[] = 'admin';			
		break;
	}
	
	if(!empty($full_route)){
		$url[] = implode('/', $full_route);	
	}
	
	return implode('/', $url);
};

//$route['translate_uri_dashes'] = FALSE;
$route['scaffolding_trigger'] = "";
