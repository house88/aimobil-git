<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = "home";

function get_static_uri($uri = '', $lang = false){
	$links = array(
		'contacts' => array(
			'ro' => 'contacte',
			'ru' => 'kontakty',
			'en' => 'contacts'
		),
		'about_us' => array(
			'ro' => 'companie-imobiliara',
			'ru' => 'kompanija-po-nedvizhimosti',
			'en' => 'real-estate-company'
		),
		'services' => array(
			'ro' => 'servicii-imobiliare',
			'ru' => 'rieltorskie-uslugi',
			'en' => 'real-estate-services'
		),
		'unsubscribe' => array(
			'ro' => 'unsubscribe',
			'ru' => 'unsubscribe',
			'en' => 'unsubscribe'
		),
		'add_ad' => array(
			'ro' => 'adauga-anunt',
			'ru' => 'podati-obijavlenie',
			'en' => 'add-a-listing'
		),
		'credit-ipotecar' => array(
			'ro' => 'credit-ipotecar',
			'ru' => 'ipotechnyj-kredit',
			'en' => 'mortgage'
		),
		'vacancies' => array(
			'ro' => 'posturi-vacante',
			'ru' => 'vacansii',
			'en' => 'job-vacancies'
		),
		'partners' => array(
			'ro' => 'companii-constructie',
			'ru' => 'stroitelinye-kompanii',
			'en' => 'construction-companies'
		),
		'planing' => array(
			'ro' => 'planificare-apartamente',
			'ru' => 'planirovki-kvartir',
			'en' => 'apartment-planning'
		),
		'residential-complexes' => array(
			'ro' => 'complexe-locative',
			'ru' => 'novostroiki-kishinieva',
			'en' => 'residential-complexes'
		),
		'discounts' => array(
			'ro' => 'imobile-pret-redus',
			'ru' => 'nedvizhimosti-nizkie-ceny',
			'en' => 'real-estate-discount'
		),
		'catalog/search' => array(
			'ro' => 'cautare-imobil',
			'ru' => 'poisk-nedvizhimosti',
			'en' => 'search-real-estate'
		),
		'favorite' => array(
			'ro' => 'favorite',
			'ru' => 'izbrannoye',
			'en' => 'favorites'
		),
	);

	if($lang === false){
		return (isset($links[$uri]))?$links[$uri]:$uri;
	}

	return (isset($links[$uri][$lang]))?$links[$uri][$lang]:$uri;
}

function get_dinamyc_uri($uri_key = '', $uri_dinamyc = false, $lang = false){
	$links = array(
		'services/detail/id' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'pages/special/id' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'residential-complexes' => array(
			'ro' => 'complexe-locative{url_ro}',
			'ru' => 'novostroiki-kishinieva{url_ru}',
			'en' => 'residential-complexes{url_en}'
		),
		'residential-complexes/detail/id' => array(
			'ro' => 'complexe-locative/{url_ro}',
			'ru' => 'novostroiki-kishinieva/{url_ru}',
			'en' => 'residential-complexes/{url_en}'
		),
		'discounts' => array(
			'ro' => 'imobile-pret-redus{url_ro}',
			'ru' => 'nedvizhimosti-nizkie-ceny{url_ru}',
			'en' => 'real-estate-discount{url_en}'
		),
		'catalog/search' => array(
			'ro' => 'cautare-imobil{url_ro}',
			'ru' => 'poisk-nedvizhimosti{url_ru}',
			'en' => 'search-real-estate{url_en}'
		),
		'catalog/type/category' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
		'manager' => array(
			'ro' => 'manager/{url_ro}',
			'ru' => 'manager/{url_ru}',
			'en' => 'manager/{url_en}'
		),
		'catalog/category/real_estate' => array(
			'ro' => '{url_ro}',
			'ru' => '{url_ru}',
			'en' => '{url_en}'
		),
	);
	
	if($lang === false){
		return array(
			'ro' => str_replace('{url_ro}', $uri_dinamyc['ro'], $links[$uri_key]['ro']),
			'ru' => str_replace('{url_ru}', $uri_dinamyc['ru'], $links[$uri_key]['ru']),
			'en' => str_replace('{url_en}', $uri_dinamyc['en'], $links[$uri_key]['en']),
		);
	}

	return str_replace('{url_'.$lang.'}', $uri_dinamyc, $links[$uri_key][$lang]);
}

// CATALOG SALE
$route['^(apartamente-vanzare|ru/prodazha-kvartir|en/apartments-for-sale)'] 	= "real_estate/index";
$route['^(apartamente-vanzare/page/(:any)|ru/prodazha-kvartir/page/(:any)|en/apartments-for-sale/page/(:any))'] 	= "real_estate/index/page/$1";
$route['^(case-vanzare|ru/prodazha-domov|en/houses-for-sale)'] 	= "real_estate/index";
$route['^(case-vanzare/page/(:any)|ru/prodazha-domov/page/(:any)|en/houses-for-sale/page/(:any))'] 	= "real_estate/index/page/$1";
$route['^(spatii-comerciale-vanzare|ru/prodazha-kommercheskaja-nedvizhimosti|en/commercial-spaces-for-sale)'] 	= "real_estate/index";
$route['^(spatii-comerciale-vanzare/page/(:any)|ru/prodazha-kommercheskaja-nedvizhimosti/page/(:any)|en/commercial-spaces-for-sale/page/(:any))'] 	= "real_estate/index/page/$1";
$route['^(terenuri-vanzare|ru/prodazha-zemelinye-uchastki|en/land-for-sale)'] 	= "real_estate/index";
$route['^(terenuri-vanzare/page/(:any)|ru/prodazha-zemelinye-uchastki/page/(:any)|en/land-for-sale/page/(:any))'] 	= "real_estate/index/page/$1";

// CATALOG RENT
$route['^(apartamente-chirie|ru/arenda-kvartir|en/apartments-for-rent)'] 	= "real_estate/index";
$route['^(apartamente-chirie/page/(:any)|ru/arenda-kvartir/page/(:any)|en/apartments-for-rent/page/(:any))'] 	= "real_estate/index/page/$1";
$route['^(case-chirie|ru/arenda-domov|en/houses-for-rent)'] 	= "real_estate/index";
$route['^(case-chirie/page/(:any)|ru/arenda-domov/page/(:any)|en/houses-for-rent/page/(:any))'] 	= "real_estate/index/page/$1";
$route['^(spatii-comerciale-chirie|ru/arenda-kommercheskaja-nedvizhimosti|en/commercial-spaces-for-rent)'] 	= "real_estate/index";
$route['^(spatii-comerciale-chirie/page/(:any)|ru/arenda-kommercheskaja-nedvizhimosti/page/(:any)|en/commercial-spaces-for-rent/page/(:any))'] 	= "real_estate/index/page/$1";

// CATALOG SALE ITEM
$route['^(apartamente-vanzare/(:any)|ru/prodazha-kvartir/(:any)|en/apartments-for-sale/(:any))'] 	= "real_estate/detail/$1";
$route['^(case-vanzare/(:any)|ru/prodazha-domov/(:any)|en/houses-for-sale/(:any))'] 	= "real_estate/detail/$1";
$route['^(spatii-comerciale-vanzare/(:any)|ru/prodazha-kommercheskaja-nedvizhimosti/(:any)|en/commercial-spaces-for-sale/(:any))'] 	= "real_estate/detail/$1";
$route['^(terenuri-vanzare/(:any)|ru/prodazha-zemelinye-uchastki/(:any)|en/land-for-sale/(:any))'] 	= "real_estate/detail/$1";

// CATALOG RENT ITEM
$route['^(apartamente-chirie/(:any)|ru/arenda-kvartir/(:any)|en/apartments-for-rent/(:any))'] 	= "real_estate/detail/$1";
$route['^(case-chirie/(:any)|ru/arenda-domov/(:any)|en/houses-for-rent/(:any))'] 	= "real_estate/detail/$1";
$route['^(spatii-comerciale-chirie/(:any)|ru/arenda-kommercheskaja-nedvizhimosti/(:any)|en/commercial-spaces-for-rent/(:any))'] 	= "real_estate/detail/$1";

// CATALOG SALE ITEM DELETED
$route['^(apartamente-vanzare/(:any)/page/(:num)|ru/prodazha-kvartir/(:any)/page/(:num)|en/apartments-for-sale/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
$route['^(case-vanzare/(:any)/page/(:num)|ru/prodazha-domov/(:any)/page/(:num)|en/houses-for-sale/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
$route['^(spatii-comerciale-vanzare/(:any)/page/(:num)|ru/prodazha-kommercheskaja-nedvizhimosti/(:any)/page/(:num)|en/commercial-spaces-for-sale/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
$route['^(terenuri-vanzare/(:any)/page/(:num)|ru/prodazha-zemelinye-uchastki/(:any)/page/(:num)|en/land-for-sale/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";

// CATALOG RENT ITEM DELETED
$route['^(apartamente-chirie/(:any)/page/(:num)|ru/arenda-kvartir/(:any)/page/(:num)|en/apartments-for-rent/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
$route['^(case-chirie/(:any)/page/(:num)|ru/arenda-domov/(:any)/page/(:num)|en/houses-for-rent/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";
$route['^(spatii-comerciale-chirie/(:any)/page/(:num)|ru/arenda-kommercheskaja-nedvizhimosti/(:any)/page/(:num)|en/commercial-spaces-for-rent/(:any)/page/(:num))'] 	= "real_estate/detail/$1/page/$2";

// DISCOUNTS
$route['^(imobile-pret-redus|ru/nedvizhimosti-nizkie-ceny|en/real-estate-discount)'] 	= "real_estate/discounts";
$route['^(imobile-pret-redus/page/(:any)|ru/nedvizhimosti-nizkie-ceny/page/(:any)|en/real-estate-discount/page/(:any))'] 	= "real_estate/discounts/page/$1";

// DISCOUNTS
$route['^(cautare-imobil|ru/poisk-nedvizhimosti|en/search-real-estate)'] 	= "real_estate/search";

// RESIDENTIAL COMPLEXES
$route['^(complexe-locative|ru/novostroiki-kishinieva|en/residential-complexes)'] 	= "residential_complexes/index";
$route['^(complexe-locative/page/(:any)|ru/novostroiki-kishinieva/page/(:any)|en/residential-complexes/page/(:any))'] 	= "residential_complexes/index/page/$1";
$route['^(complexe-locative/(:any)|ru/novostroiki-kishinieva/(:any)|en/residential-complexes/(:any))'] 	= "residential_complexes/detail/$1";

// SPECIAL PAGES
$route['^(credit-ipotecar|ru/ipotechnyj-kredit|en/mortgage)'] 	= "pages/special/credit-ipotecar";
$route['^(adauga-anunt|ru/podati-obijavlenie|en/add-a-listing)'] 	= "pages/add_ad";
$route['^(ru/|en/|)unsubscribe/(:any)'] 	= "notify/unsubscribe/$1";
$route['^(companie-imobiliara|ru/kompanija-po-nedvizhimosti|en/real-estate-company)'] 	= "pages/about_us";
$route['^(contacte|ru/kontakty|en/contacts)'] 	= "pages/contacts";
// END SPECIAL PAGES

// SERVICES
$route['^(servicii-imobiliare|ru/rieltorskie-uslugi|en/real-estate-services)'] 	= "services/index";
$route['^(reparatie-apartamente|ru/remont-kvartir|en/repair-of-apartments)'] 	= "services/detail/1";
$route['^(asistenta-juridica|ru/juridicheskaja-podderzhka|en/legal-assistance)'] 	= "services/detail/2";
$route['^(inchiriere-imobile|ru/arenda-nedvizhimosti|en/real-estate-renting-services)'] 	= "services/detail/3";
$route['^(vanzare-cumparare-bunuri-imobiliare|ru/kuplja-prodazha-nedvizhimosti|en/buying-selling-real-estate-services)'] 	= "services/detail/4";
// END SERVICES

// PLANING
$route['^(planificare-apartamente|ru/planirovki-kvartir|en/apartment-planning)'] 	= "planing/index";
// END PLANING

// VACANCIES
$route['^(favorite|ru/izbrannoye|en/favorites)'] 	= "favorite/index";
$route['^(ru/|en/|)favorite/ajax_operations/(:any)'] 	= "favorite/ajax_operations/$1";
// END VACANCIES
// VACANCIES
$route['^(posturi-vacante|ru/vacansii|en/job-vacancies)'] 	= "vacancies/index";
$route['^(ru/|en/|)vacancies/ajax_operations/(:any)'] 	= "vacancies/ajax_operations/$1";
// END VACANCIES

// PARTNERS
$route['^(companii-constructie|ru/stroitelinye-kompanii|en/construction-companies)'] 	= "partners/index";
// END PARTNERS

// BLOG
$route['^(ru/|en/|)blog/(:any)'] 	= "blog/post/$2";
// $route['^(ru/|en/|)blog/post/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
// 	return 'blog/post/' . strtolower($categories);
// };

$route['^(ru/|en/|)blog'] 	= "blog/index";
$route['^(ru/|en/|)blog/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'blog/index/' . strtolower($categories);
};
// END BLOG

// MANAGER
$route['^(ru/|en/|)manager'] 	= "users/manager";
$route['^(ru/|en/|)manager/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'users/manager/' . strtolower($categories);
};
// END MANAGER

$route['^(ru/|en/|)notify/ajax_operations/(:any)'] 	= "notify/ajax_operations/$1";
$route['signout'] 	= "auth/signout";
$route['^(ru|en|)$'] = $route['default_controller'];
$route['^(ru|en|)/(.+)$'] = "$2";
$route['404_override'] = 'error/e404';

// ADMIN
$route['admin/([a-zA-Z0-9\/\-:._]+)'] = function ($module){
	$full_route = explode('/', $module);
	$module_name = array_shift($full_route);
	switch ($module_name) {
		case 'signin':
		case 'settings':
		case 'contacts':
		case 'translations':
		case 'ajax_operations':
		case 'popup_forms':
			return 'admin/'.$module;
		break;
		case 'edit_user':
		case 'managers':
		case 'groups':
		case 'groups_rights':
			$url[] = 'users/admin';
			$url[] = $module_name;
		break;
		default:
			$url[] = $module_name;
			$url[] = 'admin';			
		break;
	}
	
	if(!empty($full_route)){
		$url[] = implode('/', $full_route);	
	}
	
	return implode('/', $url);
};

//$route['translate_uri_dashes'] = FALSE;
$route['scaffolding_trigger'] = "";
