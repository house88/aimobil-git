<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_plans')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Серий планировок';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Planing_model", "planing");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/planing/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/modules/planing/form';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_plan = (int)$this->uri->segment(4);
		$this->data['plan'] = $this->planing->handler_get($id_plan);

		if (empty($this->data['plan'])) {
			redirect('admin/planing');
		}

		$this->data['main_content'] = 'admin/modules/planing/form';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('default_photo', 'Фото серий', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$photos = ($this->input->post('photos'))?$this->input->post('photos'):array();
				if(empty($photos)){
					jsonResponse('Вы не добавили ни одну планировку к этой серий.');
				}

				// $photos_completed = true;
				// foreach ($photos as $key => $photo) {
				// 	if(empty($photo['title_ro']) || empty($photo['title_ru']) || empty($photo['title_en'])){
				// 		$photos_completed = false;
				// 		break;
				// 	}
				// }

				// if(!$photos_completed){
				// 	jsonResponse('Проверьте чтобы все поля для планировок были заполнены.');
				// }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/planing/'.$remove_photo);
						@unlink('files/planing/thumb_'.$remove_photo);
						@unlink('files/planing/thumb_h200_'.$remove_photo);
					}
				}

				$insert = array(
					'plan_name_ro' => $this->input->post('title_ro'),
					'plan_name_ru' => $this->input->post('title_ru'),
					'plan_name_en' => $this->input->post('title_en'),
					'plan_text_ro' => $this->input->post('text_ro'),
					'plan_text_ru' => $this->input->post('text_ru'),
					'plan_text_en' => $this->input->post('text_en'),
					'plan_photo' => $this->input->post('default_photo'),
					'plan_photos' => json_encode($photos)
				);

				$id_plan = $this->planing->handler_insert($insert);

				if(file_exists('files/'.$insert['plan_photo'])){
					compress_image('files/'.$insert['plan_photo']);
					$thumb = get_image_thumb('files/'.$insert['plan_photo'], 'h215', array('h' => 215));
				}

				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
				$this->form_validation->set_rules('id_plan', 'Серия планировок', 'required|xss_clean');
				$this->form_validation->set_rules('default_photo', 'Фото серий', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_plan = (int)$this->input->post('id_plan');
				$plan = $this->planing->handler_get($id_plan);
				if(empty($plan)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$photos = ($this->input->post('photos'))?$this->input->post('photos'):array();
				if(empty($photos)){
					jsonResponse('Вы не добавили ни одну планировку к этой серий.');
				}

				// $photos_completed = true;
				// foreach ($photos as $key => $photo) {
				// 	if(empty($photo['title_ro']) || empty($photo['title_ru']) || empty($photo['title_en'])){
				// 		$photos_completed = false;
				// 		break;
				// 	}
				// }

				// if(!$photos_completed){
				// 	jsonResponse('Проверьте чтобы все поля для планировок были заполнены.');
				// }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/planing/'.$remove_photo);
						@unlink('files/planing/thumb_'.$remove_photo);
						@unlink('files/planing/thumb_h200_'.$remove_photo);
					}
				}

				$update = array(
					'plan_name_ro' => $this->input->post('title_ro'),
					'plan_name_ru' => $this->input->post('title_ru'),
					'plan_name_en' => $this->input->post('title_en'),
					'plan_text_ro' => $this->input->post('text_ro'),
					'plan_text_ru' => $this->input->post('text_ru'),
					'plan_text_en' => $this->input->post('text_en'),
					'plan_date' => formatDate($this->input->post('plan_date'), 'Y-m-d H:i:s'),
					'plan_photo' => $this->input->post('default_photo'),
					'plan_photos' => json_encode($photos)
				);

				$this->planing->handler_update($id_plan, $update);

				if(file_exists('files/'.$update['plan_photo'])){
					compress_image('files/'.$update['plan_photo']);
					$thumb = get_image_thumb('files/'.$update['plan_photo'], 'h215', array('h' => 215));
				}

				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'delete':
				$id_plan = (int)$this->input->post('plan');
				$plan = $this->planing->handler_get($id_plan);
				if(empty($plan)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				@unlink('files/planing/'.$plan['plan_photo']);
				@unlink('files/planing/thumb_'.$plan['plan_photo']);
				$plan_photos = json_decode($plan['plan_photos'], true);
				if(!empty($plan_photos)){
					foreach ($plan_photos as $plan_photo) {
						@unlink('files/planing/'.$plan_photo['file']);
						@unlink('files/planing/Thumb_'.$plan_photo['file']);
					}
				}

				$this->planing->handler_delete($id_plan);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name': $params['sort_by'][] = 'plan_name_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->planing->handler_get_all($params);
				$records_total = $this->planing->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/planing/'.$record['plan_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  $record['plan_name_ru'],
						'dt_actions'	=>  '<a href="'.base_url('admin/planing/edit/'.$record['id_plan']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить серию?" data-callback="delete_action" data-plan="'.$record['id_plan'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/planing';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= 350;
				$config['max_size']	= 5000;
				$config['quality']	= 70;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$thumb = get_image_thumb('files/planing/'.$data['file_name'], 'h200', array('h' => 200));
				$info = new StdClass;
				$info->filename = $data['file_name'];
				jsonResponse('', 'success', array("file" => $info));
			break;
			// DONE
			case 'upload_photos':
				$path = 'files/planing';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '200';
				$config['min_height']	= '200';
				$config['max_size']	= '5000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => true,
					'thumb_marker'      => 'thumb_',
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 200,
					'height'            => 200
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$info->fileid = uniqid();
				jsonResponse('', 'success', array("file" => $info));
			break;
			// DONE
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	// DONE
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$this->data['categories'] = $this->partners->handler_get_categories(array('category_deleted' => 0));
				$content = $this->load->view('admin/modules/partners/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_partner = (int) $this->uri->segment(5);
				$this->data['partner'] = $this->partners->handler_get($id_partner);
				if(empty($this->data['partner'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->data['categories'] = $this->partners->handler_get_categories(array('category_deleted' => 0));
				$content = $this->load->view('admin/modules/partners/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}