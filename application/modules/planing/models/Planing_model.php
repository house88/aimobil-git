<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Planing_model extends CI_Model{
	var $apartments_plans = "apartments_plans";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->apartments_plans, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_plan = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_plan', $id_plan);
		$this->db->update($this->apartments_plans, $data);
	}

	function handler_delete($id_plan = 0){
		$this->db->where('id_plan', $id_plan);
		$this->db->delete($this->apartments_plans);
	}

	function handler_get($id_plan = 0){
		$this->db->where('id_plan', $id_plan);
		return $this->db->get($this->apartments_plans)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " plan_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->apartments_plans)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->apartments_plans);
	}
}