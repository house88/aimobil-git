<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_services')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Услуги';
		
		$this->load->model("Services_model", "services");
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/services/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		redirect('/admin');
		$this->data['main_content'] = 'admin/modules/services/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_service = (int)$this->uri->segment(4);
		$this->data['service'] = $this->services->handler_get($id_service);
		if(empty($this->data['service'])){
			$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
			redirect('admin/service');
		}

		$this->data['main_content'] = 'admin/modules/services/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('icon', 'Иконка', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ro', 'Текст, введение RO', 'required');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('special_url_ro', 'URL RO', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('special_url_ru', 'URL RU', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('special_url_en', 'URL En', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'service_icon' => $this->input->post('icon'),
					'service_poster' => (file_exists('files/'.$this->input->post('service_poster')))?$this->input->post('service_poster'):'',
					'service_name_ro' => $this->input->post('title_ro'),
					'service_name_ru' => $this->input->post('title_ru'),
					'service_name_en' => $this->input->post('title_en'),
					'service_stext_ro' => nl2br($this->input->post('stext_ro')),
					'service_stext_ru' => nl2br($this->input->post('stext_ru')),
					'service_stext_en' => nl2br($this->input->post('stext_en')),
					'service_text_ro' => $this->input->post('description_ro'),
					'service_text_ru' => $this->input->post('description_ru'),
					'service_text_en' => $this->input->post('description_en'),
					'special_url_ro' => $this->input->post('special_url_ro'),
					'special_url_ru' => $this->input->post('special_url_ru'),
					'special_url_en' => $this->input->post('special_url_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en')
				);

				$id_service = $this->services->handler_insert($insert);

				$this->_setup_services_routers();
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				$this->form_validation->set_rules('service', 'Услуга', 'required|xss_clean');
				$this->form_validation->set_rules('icon', 'Иконка', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ro', 'Текст, введение RO', 'required');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('special_url_ro', 'URL RO', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('special_url_ru', 'URL RU', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('special_url_en', 'URL En', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_service = (int)$this->input->post('service');
				$update = array(
					'service_icon' => $this->input->post('icon'),
					'service_poster' => (file_exists('files/'.$this->input->post('service_poster')))?$this->input->post('service_poster'):'',
					'service_name_ro' => $this->input->post('title_ro'),
					'service_name_ru' => $this->input->post('title_ru'),
					'service_name_en' => $this->input->post('title_en'),
					'service_stext_ro' => nl2br($this->input->post('stext_ro')),
					'service_stext_ru' => nl2br($this->input->post('stext_ru')),
					'service_stext_en' => nl2br($this->input->post('stext_en')),
					'service_text_ro' => $this->input->post('description_ro'),
					'service_text_ru' => $this->input->post('description_ru'),
					'service_text_en' => $this->input->post('description_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'special_url_ro' => $this->input->post('special_url_ro'),
					'special_url_ru' => $this->input->post('special_url_ru'),
					'special_url_en' => $this->input->post('special_url_en'),
					'service_date' => formatDate($this->input->post('service_date'), 'Y-m-d H:i:s')
				);

				$this->services->handler_update($id_service, $update);

				$this->_setup_services_routers();
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
				
				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_".$this->input->post("iSortCol_{$i}"))) {
							case 'dt_date': 
								$params['sort_by'][] = 'service_date-'.$this->input->post("sSortDir_{$i}");
							break;
						}
					}
				}

				$records = $this->services->handler_get_all($params);
				$records_total = $this->services->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$actions = array(
						'<a href="'.base_url('admin/services/edit/'.$record['id_service']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
					);
					// $status = '<a href="#" data-dtype="warning" data-message="Вы уверены что хотите сделать услугу неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-service="'.$record['id_service'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					// if($record['service_active'] == 0){
					// 	$status = '<a href="#" data-dtype="success" data-message="Вы уверены что хотите сделать услугу активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-service="'.$record['id_service'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					// }
					// $actions[] = $status;
					// $actions[] = '<a href="#" data-dtype="danger" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить услугу?" data-callback="delete_action" data-service="'.$record['id_service'].'"><i class="fa fa-trash"></i></a>';

					$output['aaData'][] = array(
						'dt_name'		=>  $record['service_name_ru'],
						'dt_date'		=>  formatDate($record['service_date'], 'd.m.Y H:i:s'),
						'dt_actions'	=>  implode(' ', $actions)
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'delete':
				$this->form_validation->set_rules('service', 'Услуга', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_service = (int)$this->input->post('service');
				$service = $this->services->handler_get($id_service);
				if(empty($service)){
					jsonResponse('Данные не верны.');
				}

				$this->services->handler_delete($id_service);

				$this->_setup_services_routers();
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('service', 'Услуга', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_service = (int)$this->input->post('service');
				$service = $this->services->handler_get($id_service);
				if(empty($service)){
					jsonResponse('Данные не верны.');
				}

				if($service['service_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->services->handler_update($id_service, array('service_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}

	function _setup_services_routers(){
		$services = $this->services->handler_get_all();
		$services_routers = array();
		foreach ($services as $service) {
			$services_routers[] = array(
				'id_service' => $service['id_service'],
				'ro' => $service['special_url_ro'],
				'ru' => $service['special_url_ru'],
				'en' => $service['special_url_en']
			);
		}

		$routers_file = APPPATH . 'config/routers_services.php';
		$f = fopen($routers_file, "w");
		fwrite($f, '<?php '."\r\n" . '$services_routers = '.var_export($services_routers, true).';');
		fclose($f);
	}
}