<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->data['active_menu'] = 'services';
		
		$this->load->model("Services_model", "services");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_static_uri('services');

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('servicii-imobiliare');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);

		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['services'] = $this->services->handler_get_all(array('service_active' => 1));
		$this->data['main_content'] = 'modules/services/index_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function detail(){
		if($this->lang->is_default()){
			$service_url = xss_clean($this->uri->segment(1));
		} else{
			$service_url = xss_clean($this->uri->segment(2));
		}

		$this->data['service_detail'] = $this->services->handler_get_by_url($service_url);
		if(empty($this->data['service_detail'])){
			return_404($this->data);
		}

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_dinamyc_uri('services/detail/id', array(
			'ro' => $this->data['service_detail']['special_url_ro'],
			'ru' => $this->data['service_detail']['special_url_ru'],
			'en' => $this->data['service_detail']['special_url_en']
		));

		$this->data['services'] = $this->services->handler_get_all(array('service_active' => 1));

		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['service_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['service_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['service_detail'][lang_column('md')],
			'page_image' => (!empty($this->data['service_detail']['service_poster']))?get_og_image('files/'.$this->data['service_detail']['service_poster']):false
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['main_content'] = 'modules/services/detail_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _get_services(){
		return $this->services->handler_get_all(array('service_active' => 1));
	}
}