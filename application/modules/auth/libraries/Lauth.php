<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lauth
{
	public function __construct(){
		$this->ci =& get_instance();
	}

	function set_sessiondata($user = array()){
		$session_data = array(
			'logged_in' => true,
			'id_user' => $user->id_user,
			'user_name_ro' => $user->user_name_ro,
			'user_name_ru' => $user->user_name_ru,
			'user_name_en' => $user->user_name_en,
			'user_999_api_key' => $user->user_999_api_key,
			'group_type' => $user->group_type,
			'rights' => $user->user_rights
		);
		
		$this->ci->session->set_userdata($session_data);
	}  
	
	function logged_in(){
		return (bool) $this->ci->session->userdata('logged_in');
	}
	
	function logout(){
		$this->ci->session->sess_destroy();
	}

	function id_user(){
		if($this->logged_in()){
			return (int) $this->ci->session->userdata('id_user');
		}else{
			return 0;
		}
	}

	function user_name($lang = 'ro'){
		if($this->logged_in()){
			return $this->ci->session->userdata("user_name_{$lang}");
		}else{
			return 0;
		}
	}

	function user_apy_key(){
		if($this->logged_in()){
			if(!empty($this->ci->session->userdata("user_999_api_key"))){
				return $this->ci->session->userdata("user_999_api_key");
			}

			return FALSE;
		}
		
		return FALSE;
	}
	
	function is_admin(){
		if(in_array($this->ci->session->userdata('group_type'), array('admin', 'manager'))){
			return true;
		}else{
			return false;
		}
	}
	
	function is_admin_only(){
		if($this->ci->session->userdata('group_type') == 'admin'){
			return true;
		}else{
			return false;
		}
	}
	
	function have_right($rights){
		$have_right = true;

		$search_rights = explode(',', $rights);
		foreach($search_rights as $right){
			if(!in_array(trim($right), $this->ci->session->userdata('rights'))){
				$have_right = false;
			}
		}

		return $have_right;
	}

	function have_right_or($rights){
		$search_rights = explode(',', $rights);
		foreach($search_rights as $right){
			if(in_array(trim($right), $this->ci->session->userdata('rights'))){
				return true;
				exit();
			}

		}

		return false;
	}

	function get_password_hash($password = ''){
		return password_hash($password, PASSWORD_BCRYPT);
	}

	function verify_password_hash($password = '', $hash_password = ''){
		return password_verify($password, $hash_password);
	}
}