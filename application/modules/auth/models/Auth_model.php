<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{
	
    public $return_object 	= true;
    private $users_table 	= 'users';
	private $users_groups_table = "users_groups";
	private $users_rights_table = "users_rights";
	private $users_groups_rights_table = "users_groups_rights";
	
	function __construct(){
		parent::__construct();
	}

	function handler_update($id_user = 0, $data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->where("id_user", $id_user);
		$this->db->update($this->users_table, $data);
	}

	function handler_get($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		$this->db->select(" * ");
		$this->db->from($this->users_table);
		$this->db->join($this->users_groups_table, " {$this->users_groups_table}.id_group = {$this->users_table}.id_group ", 'inner');

		extract($conditions);

		if(isset($id_user)){
			$this->db->where("{$this->users_table}.id_user", $id_user);
		}

		if(isset($email)){
			$this->db->where("{$this->users_table}.user_email", $email);
		}

        $query = $this->db->get();

		if($this->return_object){
            return $query->row();
        } else{
            return $query->row_array();            
        }
	}

	function handler_get_rights($id_group = 0){
		$this->db->select('*');
		$this->db->from($this->users_groups_rights_table);
		$this->db->where($this->users_groups_rights_table.'.id_group', $id_group);
		$this->db->join($this->users_rights_table, $this->users_rights_table.'.id_right = '.$this->users_groups_rights_table.'.id_right', 'inner');
		return $this->db->get()->result();
	}
}