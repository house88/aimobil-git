<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
        $this->data = array();
        $this->breadcrumbs = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	function index(){
		return_404($this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
            default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function signout(){
		$this->lauth->logout();
		redirect('');
	}
}