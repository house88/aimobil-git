<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lsettings
{
	private $settings = array();

	public function __construct(){
		$this->ci =& get_instance();
		
		$this->settings = arrayByKey($this->ci->db->get('settings')->result_array(), 'setting_alias');
	}

	public function item($alias = '', $lang = false){
		if(empty($alias)){
			return;
		}

		if(empty($this->settings[$alias])){
			return;
		}

		if($lang === false){
			return $this->settings[$alias][lang_column('setting_value')];
		} else{
			return $this->settings[$alias]["setting_value_{$lang}"];
		}
	}
}