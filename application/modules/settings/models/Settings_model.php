<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_Model{
	
	var $settings = "settings";
	var $settings_sections = "settings_sections";
	
	function __construct(){
		parent::__construct();
	}

	function handler_get_sections(){
		return $this->db->get($this->settings_sections)->result_array();
	}
	
	function set_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->settings, $data);
		return $this->db->insert_id();
	}
	
	function update_setting($id_setting = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_setting', $id_setting);
		$this->db->update($this->settings, $data);
	}
	
	function update_setting_by_alias($setting_alias = '', $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('setting_alias', $setting_alias);
		$this->db->update($this->settings, $data);
	}
	
	function update_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->settings, $data, 'setting_alias');
	}

	function get_setting($conditions = array()){
		if(empty($conditions)){
			return FALSE;
		}

        extract($conditions);

        if(isset($id_setting)){
			$this->db->where('id_setting', $id_setting);
		}

        if(isset($setting_alias)){
			$this->db->where('setting_alias', $setting_alias);
		}
		
		return $this->db->get($this->settings)->row_array();
	}
	
	function get_settings($conditions = array()){
        extract($conditions);

		if(isset($keywords)){
			$this->db->where("( setting_alias = '{$keywords}' OR setting_title LIKE '%{$keywords}%' OR setting_value_ro LIKE '%{$keywords}%' OR setting_value_ru LIKE '%{$keywords}%' OR setting_value_en LIKE '%{$keywords}%')");
		}
		
        if(isset($id_section)){
			$this->db->where_in('id_section', $id_section);
		}

		$this->db->order_by('id_setting', 'ASC');
		return $this->db->get($this->settings)->result_array();
	}
}