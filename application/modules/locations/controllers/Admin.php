<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_locations')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Местонахождения';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Locations_model", "locations");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['main_content'] = 'admin/modules/locations/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('id_parent', 'Тип', 'trim|required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]|is_unique['.$this->locations->locations.'.location_name_ro]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]|is_unique['.$this->locations->locations.'.location_name_ru]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|required|xss_clean|max_length[250]|is_unique['.$this->locations->locations.'.location_name_en]');
				if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_parent = (int)$this->input->post('id_parent');
				$parent = $this->locations->handler_get($id_parent);
				if(empty($parent)){
					jsonResponse('Данные не верны.');
				}

				$insert = array(
					'id_parent' 		=> $id_parent,
					'location_name_ro' 	=> $this->input->post('title_ro'),
					'location_name_ru' 	=> $this->input->post('title_ru'),
					'location_name_en' 	=> $this->input->post('title_en'),
					'location_active' 	=> ($this->input->post('location_active'))?1:0
				);

				$id_location = $this->locations->handler_insert($insert);

				$config = array(
					'table' => $this->locations->locations,
					'id' => 'id_location',
					'field' => 'url_ru',
					'title' => 'location_name_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ro' => $this->slug->create_slug(cut_str($insert['location_name_ro'], 250)),
					'url_ru' => $this->slug->create_slug(cut_str($insert['location_name_ru'], 250)),
					'url_en' => $this->slug->create_slug(cut_str($insert['location_name_en'], 250))
				);
				$this->locations->handler_update($id_location, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
				$id_location = (int)$this->input->post('id_location');
				$location = $this->locations->handler_get($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				if($location['id_parent'] != 0){
					$this->form_validation->set_rules('id_parent', 'Тип', 'trim|required|xss_clean');
				}

				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'trim|required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_parent = (int)$this->input->post('id_parent');
				if($location['id_parent'] != 0){
					$parent = $this->locations->handler_get($id_parent);
					if(empty($parent)){
						jsonResponse('Ошибка: Данные не верны.');
					}
				}

				$config = array(
					'table' => $this->locations->locations,
					'id' => 'id_location',
					'field' => 'url_ru',
					'title' => 'location_name_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'id_parent' 		=> $id_parent,
					'location_name_ro' 	=> $this->input->post('title_ro'),
					'location_name_ru' 	=> $this->input->post('title_ru'),
					'location_name_en' 	=> $this->input->post('title_en'),
					'location_active' 	=> ($this->input->post('location_active'))?1:0,
					'url_ro' => $this->slug->create_slug(cut_str($this->input->post('title_ro'), 250)),
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru'), 250)),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en'), 250))
				);

				$this->locations->handler_update($id_location, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($this->input->post('iDisplayLength')),
					'start' => intVal($this->input->post('iDisplayStart'))
				);

				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_" . intval($this->input->post('iSortCol_'.$i)))) {
							case 'dt_title': 
								$params['sort_by'][] = lang_column('location_name').'-'.$this->input->post('sSortDir_'.$i);
							break;
						}
					}
				}

				$records = $this->locations->handler_get_all($params);
				$records_total = $this->locations->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				$parent_locations = arrayByKey($this->locations->handler_get_all(array('id_parent' => 0)), 'id_location');
				foreach ($records as $record) {
					$status = '';
					$delete = '';
					if($record['id_parent'] > 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать местонахождение активной?" data-dtype="warning" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-location="'.$record['id_location'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
						if($record['location_active'] == 1){
							$status = '<a href="#" data-message="Вы уверены что хотите сделать местонахождение неактивной?" data-dtype="warning" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-location="'.$record['id_location'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
						}

						$delete = '<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить местонахождение?" data-callback="delete_action" data-location="'.$record['id_location'].'"><i class="fa fa-remove"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_title'		=>  $record[lang_column('location_name')],
						'dt_parent'		=>  ($record['id_parent'] == 0)?'&mdash;':$parent_locations[$record['id_parent']][lang_column('location_name')],
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/locations/popup_forms/edit/'.$record['id_location']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a> '
											.$status.' '.$delete
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete':
                $this->form_validation->set_rules('location', 'Местонахождение', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_location = (int)$this->input->post('location');
				$location = $this->locations->handler_get($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->locations->handler_update($id_location, array('location_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('location', 'Местонахождение', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_location = (int)$this->input->post('location');
				$location = $this->locations->handler_get($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($location['location_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->locations->handler_update($id_location, array('location_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}

	// DONE
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
				$content = $this->load->view('admin/modules/locations/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_location = (int) $this->uri->segment(5);
				$this->data['location'] = $this->locations->handler_get($id_location);
				if(empty($this->data['location'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
				$content = $this->load->view('admin/modules/locations/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function get_locations($params = array()){
		return $this->locations->handler_get_all($params);
	}
}