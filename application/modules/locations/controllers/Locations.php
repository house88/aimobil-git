<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->load->model("Locations_model", "locations");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function _get_tree($filters = array()){
		$params = array();

		extract($filters);

		if (isset($location_active)) {
			$params['location_active'] = 1;
		}

		$locations = $this->locations->handler_get_all($params);
		$locations_tree = array();
		foreach ($locations as $location) {
			if($location['id_parent'] == 0){
				if(!isset($locations_tree[$location['id_location']])){
					$locations_tree[$location['id_location']] = $location;
				} else{
					$locations_tree[$location['id_location']] = array_merge($location, $locations_tree[$location['id_location']]);
				}
			} else{
				$locations_tree[$location['id_parent']]['children'][] = $location;
			}
		}

		return $locations_tree;
	}
}