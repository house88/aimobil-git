<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations_model extends CI_Model{
	var $locations = "locations";
	var $regions_999 = "regions_999";
	var $region_raions_999 = "region_raions_999";
	var $region_raion_sectors_999 = "region_raion_sectors_999";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->locations, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_location = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_location', $id_location);
		$this->db->update($this->locations, $data);
	}

	function handler_delete($id_location = 0){
		$this->db->where('id_location', $id_location);
		$this->db->delete($this->locations);
	}

	function handler_get($id_location = 0){
		$this->db->where('id_location', $id_location);
		return $this->db->get($this->locations)->row_array();
	}

	function handler_get_full($id_location = 0){
		$this->db->select('l.*');
		$this->db->select('p.location_name_ro as plocation_name_ro, p.location_name_ru as plocation_name_ru, p.location_name_en as plocation_name_en');
		$this->db->select('p.url_ro as purl_ro, p.url_ru as purl_ru, p.url_en as purl_en');
		$this->db->from("{$this->locations} l");
		$this->db->join("{$this->locations} p", "l.id_parent = p.id_location");
		$this->db->where('l.id_location', $id_location);
		return $this->db->get()->row_array();
	}

	function handler_get_all_full($id_location = 0){
		$this->db->select('l.*');
		$this->db->select('p.location_name_ro as plocation_name_ro, p.location_name_ru as plocation_name_ru, p.location_name_en as plocation_name_en, p.location_in_title as plocation_in_title');
		$this->db->select('p.url_ro as purl_ro, p.url_ru as purl_ru, p.url_en as purl_en');
		$this->db->from("{$this->locations} l");
		$this->db->join("{$this->locations} p", "l.id_parent = p.id_location");
		$this->db->where('l.location_active', 1);
		$this->db->where('l.location_deleted', 0);
		return $this->db->get()->result_array();
	}

	function handler_get_all($conditions = array()){
		$order_column = lang_column('location_name');
        $order_by = " id_parent ASC, {$order_column} ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_location)){
			$this->db->where_in('id_location', $id_location);
        }

        if(isset($id_parent)){
			$this->db->where_in('id_parent', $id_parent);
        }

        if(isset($not_id_parent)){
			$this->db->where_not_in('id_parent', $not_id_parent);
        }

        if(isset($location_active)){
			$this->db->where('location_active', $location_active);
		}
		
		$this->db->where('location_deleted', 0);
		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->locations)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_location)){
			$this->db->where_in('id_location', $id_location);
        }

        if(isset($id_parent)){
			$this->db->where_in('id_parent', $id_parent);
        }

        if(isset($location_active)){
			$this->db->where('location_active', $location_active);
		}
		
		$this->db->where('location_deleted', 0);

		return $this->db->count_all_results($this->locations);
	}

	// 999.md API
	function handler_api_regions(){
		$this->db->order_by(" pos ASC ");
		return $this->db->get($this->regions_999)->result_array();
	}
	function handler_api_raions($id_region = 0){
		$this->db->where('region_id', $id_region);
		return $this->db->get($this->region_raions_999)->result_array();
	}
	function handler_api_sectors($id_raion = 0){
		$this->db->where('raion_id', $id_raion);
		return $this->db->get($this->region_raion_sectors_999)->result_array();
	}
}