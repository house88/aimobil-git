<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];

		$this->data['active_menu'] = 'blog';
		
		$this->load->model('Blog_model', 'blog');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(2);
		} else{
			$uri = $this->uri->uri_to_assoc(3);
		}

		check_uri($uri, array('category','archive','page'), $this->data);

		$page = 1;
		if(isset($uri['page'])){
			$page = (int)$uri['page'];
		}

		if($page < 1){
			return_404();
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('blog_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);
		
		$params = array(
			'limit' => $limit,
			'start' => $start,
			'blog_active' => 1
		);

		$categories_params = array(
			'category_active' => 1
		);
		$this->data['blog_categories'] = arrayByKey($this->blog->handler_get_all_categories($categories_params), 'id_category');
		if(isset($uri['category'])){
			$id_category = id_from_link($uri['category']);
			if(!array_key_exists($id_category, $this->data['blog_categories'])){
				return_404();
			}

			$current_category = $this->data['blog_categories'][$id_category];
			$this->data['selected_category'] = $params['id_category'] = $id_category;
		}

		$archive_params = array(
			'limit' => $this->lsettings->item('blog_archive_limit')
		);
		$this->data['blog_archives'] = arrayByKey($this->blog->handler_get_archives($archive_params), 'archive_year');
		if(isset($uri['archive'])){
			$archive_year = (int)$uri['archive'];
			if(!array_key_exists($archive_year, $this->data['blog_archives'])){
				return_404();
			}

			$this->data['selected_archive'] = $params['archive_year'] = $archive_year;
		}

		$links_map = array(
			'category' => array(
				'type' => 'uri',
				'deny' => array('page'),
			),
			'archive' => array(
				'type' => 'uri',
				'deny' => array('page'),
			),
			'page' => array(
				'type' => 'uri',
				'deny' => array(),
			)
		);

		$this->data['links_tpl'] = $this->uritpl->make_templates($links_map, $uri);

		$search_params_links_map = array(
			'category' => array(
				'type' => 'uri',
				'deny' => array('category','page'),
			),
			'archive' => array(
				'type' => 'uri',
				'deny' => array('archive','page'),
			),
			'page' => array(
				'type' => 'uri',
				'deny' => array('page'),
			)
		);

		$search_params_links_tpl = $this->uritpl->make_templates($search_params_links_map, $uri, true);

		if(!empty($current_category)){
			$this->data['langs_uri'] = array(
				'ro' => $this->uritpl->replace_dynamic_uri($current_category['url_ro'], $this->data['links_tpl']['category'], 'blog/'),
				'ru' => $this->uritpl->replace_dynamic_uri($current_category['url_ru'], $this->data['links_tpl']['category'], 'blog/'),
				'en' => $this->uritpl->replace_dynamic_uri($current_category['url_en'], $this->data['links_tpl']['category'], 'blog/')
			);
		}
		
		$this->data['records_total'] = $this->blog->handler_get_count($params);
		$this->data['blogs'] = $this->blog->handler_get_all($params);

		// PAGINATION
		if(isset($uri['page'])){
		    $page_segment = $this->uri->total_segments();
		} else{
		    $page_segment = $this->uri->total_segments()+1;
		}
		
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> $search_params_links_tpl['page'],
			'uri'		=> $uri,
			'lang' 		=> $this->ulang,
			'segment' 	=> $page_segment
		);
		$settings = $this->pagination_lib->get_settings('blog', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['langs_uri'] = get_dinamyc_uri('blog', array(
			'ro' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'ru' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'en' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):''
		));
		
		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('blog');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/blog/index_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function post(){
		if($this->lang->is_default()){
			$blog_url = xss_clean($this->uri->segment(2));
		} else{
			$blog_url = xss_clean($this->uri->segment(3));
		}

		$this->data['blog'] = $this->blog->handler_get_by_url($blog_url);
		if(empty($this->data['blog'])){
			return_404($this->data);
		}
		$id_blog = $this->data['blog']['id_blog'];

        if($this->data['blog']['blog_active'] == 0){
            return_404($this->data);
        }

		if($this->data['blog'][lang_column('url')] != $blog_url){
			redirect(site_url(get_dinamyc_uri('blog', '/'.$this->data['blog'][lang_column('url')], $this->ulang)));
		}

		$this->breadcrumbs[] = array(
			'title' => $this->data['blog']['blog_title_'.$this->ulang],
			'link' => base_url($this->ulang.'/blog/'.$this->data['blog']['url_'.$this->ulang])
		);

		$categories_params = array(
			'category_active' => 1
		);
		$this->data['blog_categories'] = arrayByKey($this->blog->handler_get_all_categories($categories_params), 'id_category');
		$this->data['selected_category'] = $this->data['blog']['id_category'];

		$archive_params = array(
			'limit' => $this->lsettings->item('blog_archive_limit')
		);
		$this->data['blog_archives'] = arrayByKey($this->blog->handler_get_archives($archive_params), 'archive_year');
		$this->data['selected_archive'] = formatDate($this->data['blog']['blog_date'], 'Y');

		$links_map = array(
			'category' => array(
				'type' => 'uri',
				'deny' => array('page'),
			),
			'archive' => array(
				'type' => 'uri',
				'deny' => array('page'),
			)
		);

		$this->data['links_tpl'] = $this->uritpl->make_templates($links_map);

		$same_blogs_params = array(
			'limit' => 3,
			'blog_active' => 1,
			'not_id_blog' => $this->data['blog']['id_blog'],
			'id_category' => $this->data['blog']['id_category']
		);
		$this->data['same_blogs'] = $this->blog->handler_get_all($same_blogs_params);

		$this->data['langs_uri'] = get_dinamyc_uri('blog', array(
			'ro' => "/{$this->data['blog']['url_ro']}",
			'ru' => "/{$this->data['blog']['url_ru']}",
			'en' => "/{$this->data['blog']['url_en']}"
		));

		$this->data['stitle'] = $this->data['blog']['mt_'.$this->ulang];
		$this->data['skeywords'] = $this->data['blog']['mk_'.$this->ulang];
		$this->data['sdescription'] = $this->data['blog']['md_'.$this->ulang];
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/blog/post_view';
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['blog'][lang_column('mt')],
			lang_column('page_mk') => $this->data['blog'][lang_column('mk')],
			lang_column('page_md') => $this->data['blog'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['blog']['blog_photo'])
		);

		$counters = file_get_contents('http://count-server.sharethis.com/v2.0/get_counts?url='.current_url());
		$counters = json_decode($counters, true);
		$this->data['blog_counters'] = array(
			'facebook' => (isset($counters['shares']['facebook']))?(int)$counters['shares']['facebook']:0,
			'googleplus' => (isset($counters['shares']['googleplus']))?(int)$counters['shares']['googleplus']:0,
			'twitter' => (isset($counters['shares']['twitter']))?(int)$counters['shares']['twitter']:0,
		);
		
		$this->load->view(get_theme_view('page_template'), $this->data);
	}
}