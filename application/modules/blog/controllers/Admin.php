<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_blogs')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Блог';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Blog_model", "blog");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/blog/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$params = array(
			'category_active' => 1
		);
		$this->data['categories'] = $this->blog->handler_get_all_categories($params);
		$this->data['main_content'] = 'admin/modules/blog/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_blog = (int)$this->uri->segment(4);
		$this->data['blog'] = $this->blog->handler_get($id_blog);
		$params = array(
			'category_active' => 1
		);
		$this->data['categories'] = $this->blog->handler_get_all_categories($params);
		$this->data['main_content'] = 'admin/modules/blog/edit';
		$this->load->view('admin/page', $this->data);
	}

	function categories(){
		$this->data['main_title'] = 'Категории блога';
		$this->data['main_content'] = 'admin/modules/blog/categories/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст статьи RO', 'required');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('url_ro', 'URL RO', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_ru', 'URL RU', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_en', 'URL En', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = $this->input->post('category');
				$category = $this->blog->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$insert = array(
					'id_category' => $id_category,
					'blog_title_ro' => $this->input->post('title_ro'),
					'blog_title_ru' => $this->input->post('title_ru'),
					'blog_title_en' => $this->input->post('title_en'),
					'blog_description_ro' => $this->input->post('description_ro'),
					'blog_description_ru' => $this->input->post('description_ru'),
					'blog_description_en' => $this->input->post('description_en'),
					'blog_small_description_ro' => $this->input->post('stext_ro'),
					'blog_small_description_ru' => $this->input->post('stext_ru'),
					'blog_small_description_en' => $this->input->post('stext_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'url_ro' => $this->input->post('url_ro'),
					'url_ru' => $this->input->post('url_ru'),
					'url_en' => $this->input->post('url_en'),
					'blog_photo' => $this->input->post('blog_image', true)
				);

				$id_blog = $this->blog->handler_insert($insert);

				if(file_exists('files/'.$insert['blog_photo'])){
					compress_image('files/'.$insert['blog_photo']);
					$thumb = get_image_thumb('files/'.$insert['blog_photo'], 'h215', array('h' => 215));
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				$this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст статьи RO', 'required');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('url_ro', 'URL RO', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_ru', 'URL RU', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_en', 'URL En', 'required|alpha_dash|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_blog = (int)$this->input->post('blog');

				$id_category = $this->input->post('category');
				$category = $this->blog->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$update = array(
					'id_category' => $id_category,
					'blog_title_ro' => $this->input->post('title_ro'),
					'blog_title_ru' => $this->input->post('title_ru'),
					'blog_title_en' => $this->input->post('title_en'),
					'blog_description_ro' => $this->input->post('description_ro'),
					'blog_description_ru' => $this->input->post('description_ru'),
					'blog_description_en' => $this->input->post('description_en'),
					'blog_small_description_ro' => $this->input->post('stext_ro'),
					'blog_small_description_ru' => $this->input->post('stext_ru'),
					'blog_small_description_en' => $this->input->post('stext_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'url_ro' => $this->input->post('url_ro'),
					'url_ru' => $this->input->post('url_ru'),
					'url_en' => $this->input->post('url_en'),
					'blog_photo' => $this->input->post('blog_image', true),
					'blog_date' => formatDate($this->input->post('blog_date'), 'Y-m-d H:i:s')
				);

				$this->blog->handler_update($id_blog, $update);

				if(file_exists('files/'.$update['blog_photo'])){
					compress_image('files/'.$update['blog_photo']);
					$thumb = get_image_thumb('files/'.$update['blog_photo'], 'h215', array('h' => 215));
				}
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_blog-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = 'blog_date-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'blog_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->blog->handler_get_all($params);
				$records_total = $this->blog->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать блог неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-blog="'.$record['id_blog'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['blog_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать блог активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-blog="'.$record['id_blog'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_blog'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/'.$record['blog_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  '<a href="'.base_url('admin/blog/edit/'.$record['id_blog']).'">'.$record['blog_title_ru'].'</a>',
						'dt_date'		=>  formatDate($record['blog_date'], 'd.m.Y H:i:s'),
						'dt_actions'	=>  $status
											.' <a href="'.base_url('admin/blog/edit/'.$record['id_blog']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить блог?" data-callback="delete_action" data-blog="'.$record['id_blog'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'delete':
				$this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_blog = (int)$this->input->post('blog');
				$blog = $this->blog->handler_get($id_blog);
				if(empty($blog)){
					jsonResponse('Блог не существует.');
				}

				$this->blog->handler_delete($id_blog);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_blog = (int)$this->input->post('blog');
				$blog = $this->blog->handler_get($id_blog);
				if(empty($blog)){
					jsonResponse('Блог не существует.');
				}
				if($blog['blog_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->blog->handler_update($id_blog, array('blog_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'add_category':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'category_active' => ($this->input->post('category_active'))?1:0
				);

				$id_category = $this->blog->handler_insert_category($insert);

				$config = array(
					'table' => 'blog_categories',
					'id' => 'id_category',
					'field' => 'url_ru',
					'title' => 'category_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ro' => $this->slug->create_slug(cut_str($insert['category_title_ro'], 200).'-'.$id_category),
					'url_ru' => $this->slug->create_slug(cut_str($insert['category_title_ru'], 200).'-'.$id_category),
					'url_en' => $this->slug->create_slug(cut_str($insert['category_title_en'], 200).'-'.$id_category)
				);
				$this->blog->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_category':
                $this->form_validation->set_rules('id_category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = (int)$this->input->post('id_category');
				$config = array(
					'table' => 'blog_categories',
					'id' => 'id_category',
					'field' => 'url_ru',
					'title' => 'category_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'category_active' => ($this->input->post('category_active'))?1:0,
					'url_ro' => $this->slug->create_slug(cut_str($this->input->post('title_ro'), 200).'-'.$id_category),
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru'), 200).'-'.$id_category),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en'), 200).'-'.$id_category)
				);

				$this->blog->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'categories_list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name_ru': $params['sort_by'][] = 'category_title_ru-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name_en': $params['sort_by'][] = 'category_title_en-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->blog->handler_get_all_categories($params);
				$records_total = $this->blog->handler_get_count_categories($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию активной?" data-dtype="warning" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					if($record['category_active'] == 1){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию неактивной?" data-dtype="warning" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['category_title_ru'],
						'dt_name_en'	=>  $record['category_title_en'],
						'dt_actions'	=>  $status
											.' <a href="#" data-href="'.base_url('admin/blog/popup_forms/edit_category/'.$record['id_category']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить категорию?" data-callback="delete_action" data-category="'.$record['id_category'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete_category':
                $this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = (int)$this->input->post('category');
				$category = $this->blog->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->blog->handler_update_category($id_category, array('category_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status_category':
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_category = (int)$this->input->post('category');
				$category = $this->blog->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($category['category_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->blog->handler_update_category($id_category, array('category_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add_category':
				$content = $this->load->view('admin/modules/blog/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit_category':
				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->blog->handler_get_category($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/blog/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}