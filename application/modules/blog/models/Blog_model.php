<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model{
	var $blog = "blog";
	var $blog_categories = "blog_categories";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->blog, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_blog, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_blog', $id_blog);
		$this->db->update($this->blog, $data);
	}

	function handler_delete($id_blog){
		$this->db->where('id_blog', $id_blog);
		$this->db->delete($this->blog);
	}

	function handler_get($id_blog){
		$this->db->where('id_blog', $id_blog);
		return $this->db->get($this->blog)->row_array();
	}

	function handler_get_by_url($url){
		$this->db->where('url_ru', $url);
		$this->db->or_where('url_ro', $url);
		$this->db->or_where('url_en', $url);
		$this->db->limit(1);
		return $this->db->get($this->blog)->row_array();
	}

	function handler_get_archives($conditions = array()){
		extract($conditions);

		$this->db->select("YEAR(blog_date) as archive_year");
		$this->db->from($this->blog);
		$this->db->group_by("archive_year");
		$this->db->order_by("archive_year DESC");

		if(isset($limit)){
			$this->db->limit($limit);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " blog_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_blog)){
			$this->db->where_in('id_blog', $id_blog);
        }

        if(isset($not_id_blog)){
			$this->db->where_not_in('id_blog', $not_id_blog);
        }

        if(isset($id_category)){
			$this->db->where('id_category', $id_category);
        }

        if(isset($blog_active)){
			$this->db->where('blog_active', $blog_active);
        }

        if(isset($archive_year)){
			$this->db->where('YEAR(blog_date)', $archive_year);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		} elseif(isset($limit)){
			$this->db->limit($limit);
		}

		return $this->db->get($this->blog)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_blog)){
			$this->db->where_in('id_blog', $id_blog);
        }

        if(isset($not_id_blog)){
			$this->db->where_not_in('id_blog', $not_id_blog);
        }

        if(isset($id_category)){
			$this->db->where('id_category', $id_category);
        }

        if(isset($blog_active)){
			$this->db->where('blog_active', $blog_active);
        }

        if(isset($archive_year)){
			$this->db->where('YEAR(blog_date)', $archive_year);
        }

		return $this->db->count_all_results($this->blog);
	}

	// CATEGORIES
	function handler_insert_category($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->blog_categories, $data);
		return $this->db->insert_id();
	}

	function handler_update_category($id_category = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_category', $id_category);
		$this->db->update($this->blog_categories, $data);
	}
	
	function handler_get_category($id_category = 0){
		$this->db->where('id_category', $id_category);
		return $this->db->get($this->blog_categories)->row_array();
	}
	
	function handler_get_all_categories($conditions = array()){
		$order_by = " id_category ASC ";
		$category_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->blog_categories)->result_array();
	}

	function handler_get_count_categories($conditions = array()){
		$category_deleted = 0;

		extract($conditions);
		
		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		return $this->db->count_all_results($this->blog_categories);
	}
}