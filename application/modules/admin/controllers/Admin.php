<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	// DONE
	function index(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}

		$this->data['main_title'] = 'Добрый день '.$this->lauth->user_name();
		$this->data['main_content'] = 'admin/modules/admin/dashboard_view';
		$this->load->view('admin/page', $this->data);
	}
	
	// DONE
	function settings(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->have_right('manage_settings')){
			redirect('/admin');
		}

		$this->data['settings_sections'] = $this->settings->handler_get_sections();
		$this->data['main_title'] = 'Настройки';
		$this->data['main_content'] = 'admin/modules/settings/list';
		$this->load->view('admin/page', $this->data);
	}
	
	// DONE
	function translations(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->have_right('manage_translations')){
			redirect('/admin');
		}

		$this->load->model('translations/Translations_model', 'translations');
		$this->data['translations_sections'] = $this->translations->handler_get_sections();
		$this->data['main_title'] = 'Переводы';
		$this->data['main_content'] = 'admin/modules/translations/list';
		$this->load->view('admin/page', $this->data);
	}
	
	// DONE
	function routings(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->have_right('manage_routings')){
			redirect('/admin');
		}

		$this->load->model('routers/Routers_model', 'routers');
		$this->data['routers'] = $this->routers->handler_get_all();
		$this->data['main_title'] = 'Ссылки';
		$this->data['main_content'] = 'admin/modules/routers/list';
		$this->load->view('admin/page', $this->data);
	}
	
	// DONE
	function signin(){
		if($this->lauth->logged_in()){
            redirect('admin');
        }
        
        $this->data['main_title'] = 'Авторизация';
        $this->load->view('admin/modules/auth/signin_view', $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}

		$option = $this->uri->segment(3);
		switch($option){
			// DONE
			case 'add_setting':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$this->form_validation->set_rules('title', 'Название', 'required|max_length[500]');
				$this->form_validation->set_rules('key', 'Ключ', 'trim|required|xss_clean|alpha_dash|max_length[200]|is_unique['.$this->settings->settings.'.setting_alias]', array(
					'is_unique' => 'Такой ключ уже используеться.'
				));
				$this->form_validation->set_rules('id_section', 'Модуль', 'required');

				if ($this->input->post('setting_different_values')) {
					$this->form_validation->set_rules('text_ro', 'Значение RO', 'required');
					$this->form_validation->set_rules('text_ru', 'Значение RU', 'required');
					$this->form_validation->set_rules('text_en', 'Значение En', 'required');
				} else{
					$this->form_validation->set_rules('text', 'Значение', 'required');					
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$setting_different_values = 0;
				if ($this->input->post('setting_different_values')) {
					$setting_different_values = 1;
					$text_ro = $this->input->post('text_ro');
					$text_ru = $this->input->post('text_ru');
					$text_en = $this->input->post('text_en');
				} else{
					$text_ro = $text_ru = $text_en = $this->input->post('text');
				}

				$insert = array(
					'id_section' => $this->input->post('id_section'),
					'setting_title' => $this->input->post('title'),
					'setting_alias' => $this->input->post('key'),
					'setting_value_ro' => $text_ro,
					'setting_value_ru' => $text_ru,
					'setting_value_en' => $text_en,
					'setting_different_values' => $setting_different_values
				);

				$this->settings->set_settings($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_setting':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->form_validation->set_rules('title', 'Название', 'required|max_length[500]');
				$this->form_validation->set_rules('id_setting', 'Ключ', 'required');
				$this->form_validation->set_rules('id_section', 'Модуль', 'required');

				if ($this->input->post('setting_different_values')) {
					$this->form_validation->set_rules('text_ro', 'Значение RO', 'required');
					$this->form_validation->set_rules('text_ru', 'Значение RU', 'required');
					$this->form_validation->set_rules('text_en', 'Значение En', 'required');
				} else{
					$this->form_validation->set_rules('text', 'Значение', 'required');					
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$setting_different_values = 0;
				if ($this->input->post('setting_different_values')) {
					$setting_different_values = 1;
					$text_ro = $this->input->post('text_ro');
					$text_ru = $this->input->post('text_ru');
					$text_en = $this->input->post('text_en');
				} else{
					$text_ro = $text_ru = $text_en = $this->input->post('text');
				}

				$id_setting = (int)$this->input->post('id_setting');
				$update = array(
					'id_section' => $this->input->post('id_section'),
					'setting_title' => $this->input->post('title'),
					'setting_value_ro' => $text_ro,
					'setting_value_ru' => $text_ru,
					'setting_value_en' => $text_en,
					'setting_different_values' => $setting_different_values
				);

				$this->settings->update_setting($id_setting, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'logo_1':
				$path = 'theme/access-imobil/css/images';
				create_dir($path);

				rename("{$path}/logo.svg", "{$path}/logo-delete.svg");

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'svg';
				$config['file_name'] = 'logo';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}
				
				unlink("{$path}/logo-delete.svg");
				jsonResponse('Логотип 1 изменен.', 'success');
			break;
			case 'logo_2':
				$path = 'theme/access-imobil/css/images';
				create_dir($path);

				rename("{$path}/vlogo.svg", "{$path}/vlogo-delete.svg");

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'svg';
				$config['file_name'] = 'vlogo';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}
				
				unlink("{$path}/vlogo-delete.svg");
				jsonResponse('Логотип 2 изменен.', 'success');
			break;
			// DONE
			case 'settings_list_dt':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$params = array();

				if ($this->input->post('keywords')) {
					$params['keywords'] = $this->input->post('keywords', true);
				}

				if ($this->input->post('id_section')) {
					$params['id_section'] = (int)$this->input->post('id_section');
				}
				
				$records = $this->settings->get_settings($params);
				$records_total = count($records);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					switch ($record['setting_alias']) {
						case 'logo_1':
							$setting_value = '<img style="width:170px;height:70;" src="'.base_url('theme/access-imobil/css/images/'.$record['setting_value_ro']).'">';
							$dt_actions = '<span class="btn btn-default btn-sm btn-file">
												<i class="fa fa-picture-o"></i>
												<input id="select_logo_1" type="file" name="userfile">
											</span>';
						break;
						case 'logo_2':
							$setting_value = '<div style="background:#3e3e3d;"><img style="width:115px;height:85;" src="'.base_url('theme/access-imobil/css/images/'.$record['setting_value_ro']).'"></div>';
							$dt_actions = '<span class="btn btn-default btn-sm btn-file">
												<i class="fa fa-picture-o"></i>
												<input id="select_logo_2" type="file" name="userfile">
											</span>';
						break;
						default:
							$setting_value = $record['setting_value_ro'];
							$dt_actions = '<a href="#" data-href="'.base_url('admin/popup_forms/edit_setting/'.$record['id_setting']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>';
						break;
					}
					$output['aaData'][] = array(
						'dt_title'		=>  '<strong>'.$record['setting_title'].'</strong>',
						'dt_text_ro'	=>  $setting_value,
						'dt_actions'	=>  $dt_actions
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'add_translation':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$this->form_validation->set_rules('id_section', 'Модуль', 'required');
				$this->form_validation->set_rules('key', 'Ключ', 'trim|required|xss_clean|alpha_dash|max_length[200]|is_unique['.$this->translations->translations.'.translation_key]', array(
					'is_unique' => 'Такой ключ уже используеться.'
				));
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_en', 'Текст En', 'required');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$insert = array(
					'id_section' => $this->input->post('id_section'),
					'translation_key' => $this->input->post('key'),
					'translation_text_ro' => $this->input->post('text_ro'),
					'translation_text_ru' => $this->input->post('text_ru'),
					'translation_text_en' => $this->input->post('text_en')
				);

				$this->translations->handler_insert_translations($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_translation':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$this->form_validation->set_rules('id_section', 'Модуль', 'required');
				$this->form_validation->set_rules('id_translation', 'Ключ', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_en', 'Текст En', 'required');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_translation = (int)$this->input->post('id_translation');
				$update = array(
					'id_section' => $this->input->post('id_section'),
					'translation_text_ro' => $this->input->post('text_ro'),
					'translation_text_ru' => $this->input->post('text_ru'),
					'translation_text_en' => $this->input->post('text_en')
				);

				$this->translations->handler_update_translation($id_translation, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'delete_translation':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}

				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->load->model('translations/Translations_model', 'translations');
				$this->form_validation->set_rules('id_translation', 'Ключ', 'required');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_translation = (int)$this->input->post('id_translation');
				$this->translations->handler_delete_translation($id_translation);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'translations_list_dt':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$params = array();

				if ($this->input->post('keywords')) {
					$params['keywords'] = $this->input->post('keywords', true);
				}

				if ($this->input->post('id_section')) {
					$params['id_section'] = (int)$this->input->post('id_section');
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$records = $this->translations->handler_get_translations($params);
				$records_total = count($records);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_key'		=>  $record['translation_key'],
						'dt_text_ro'	=>  $record['translation_text_ro'],
						'dt_text_ru'	=>  $record['translation_text_ru'],
						'dt_text_en'	=>  $record['translation_text_en'],
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/popup_forms/edit_translation/'.$record['id_translation']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
											<a href="#" data-dtype="danger" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить перевод?" data-callback="delete_action" data-translation="'.$record['id_translation'].'"><i class="fa fa-trash"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'update_translations_files':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$translations = $this->translations->handler_get_translations();

				$lang_ro = '';
				$lang_ru = '';
				$lang_en = '';
				foreach($translations as $translation){
					$lang_ro .= '$lang[\''.$translation['translation_key'].'\'] = \''.addslashes($translation['translation_text_ro']).'\';'."\r\n";
					$lang_ru .= '$lang[\''.$translation['translation_key'].'\'] = \''.addslashes($translation['translation_text_ru']).'\';'."\r\n";
					$lang_en .= '$lang[\''.$translation['translation_key'].'\'] = \''.addslashes($translation['translation_text_en']).'\';'."\r\n";
				}
				
				$lang_file = APPPATH . 'language/english/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n" . $lang_en);
				fclose($f);
				
				$lang_file = APPPATH . 'language/russian/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n" . $lang_ru);
				fclose($f);
				
				$lang_file = APPPATH . 'language/romanian/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n" . $lang_ro);
				fclose($f);
				
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'update_routers':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_routings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$routers = $this->input->post('routers');
				$this->load->model('routers/Routers_model', 'routers');
				$db_routers = $this->routers->handler_get_all();
				$update = array();
				$errors = array();
				
				foreach ($db_routers as $db_router) {
					$this->form_validation->reset_validation();
					$validate_route = array(
						'route_ro' => $routers[$db_router['route_key']]['ro'],
						'route_ru' => $routers[$db_router['route_key']]['ru'],
						'route_en' => $routers[$db_router['route_key']]['en']
					);					
					$this->form_validation->set_data($validate_route);
					$this->form_validation->set_rules('route_ro', "Ссылка RO для страницы: {$db_router['route_name']}", 'required|alpha_dash|xss_clean|max_length[250]');
					$this->form_validation->set_rules('route_ru', "Ссылка RU для страницы: {$db_router['route_name']}", 'required|alpha_dash|xss_clean|max_length[250]');
					$this->form_validation->set_rules('route_en', "Ссылка En для страницы: {$db_router['route_name']}", 'required|alpha_dash|xss_clean|max_length[250]');
					if ($this->form_validation->run() == false){
						$errors = array_merge($errors, $this->form_validation->error_array());
						continue;
					}
					
					$update[] = array(
						'route_key' => $db_router['route_key'],
						'route_ro' => $routers[$db_router['route_key']]['ro'],
						'route_ru' => $routers[$db_router['route_key']]['ru'],
						'route_en' => $routers[$db_router['route_key']]['en']
					);
				}

				if (!empty($errors)) {
					jsonResponse($errors);
				}

				$this->routers->handler_update($update);

				$routers_file = APPPATH . 'config/routers.php';
				$f = fopen($routers_file, "w");
				fwrite($f, '<?php '."\r\n" . '$site_routers = '.var_export($routers, true).';');
				fclose($f);

				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'signin':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'user_email',
						'label' => 'E-mail',
						'rules' => 'required|xss_clean|valid_email',
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$email = $this->input->post('user_email', true);
				$password 	= $this->input->post('password', true);
				$user_info 	= $this->mauth->handler_get(array('email' => $email));
				if(!$user_info){
					jsonResponse('Данные для входа не верны.');
				}

				if($this->lauth->verify_password_hash($password, $user_info->user_password)){
					$user_rights = $this->mauth->handler_get_rights($user_info->id_group);
					$rights = array();
					if($user_rights){
						foreach ($user_rights as $right) {
							$rights[] = $right->right_alias;
						}
					}

					$user_info->user_rights = $rights;
					$this->lauth->set_sessiondata($user_info);
				} else{
					jsonResponse('Неверный логин или пароль, попробуйте ещё раз.');
				}

				jsonResponse('', 'success', array('redirect' => 'admin'));
			break;
			// DONE			
			case 'edit':
				if(!$this->lauth->logged_in()){
					jsonResponse('Ошибка: Пройдите авторизацию.');
				}

				if(!$this->lauth->is_admin()){
					jsonResponse('Ошибка: Нет прав.');
				}

				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('user_password', 'Новый пароль', 'min_length[6]|max_length[50]');
				$password = $this->input->post('user_password');
				if(!empty($password)){
					$this->form_validation->set_rules('confirm_password', 'Повторите новый пароль', 'required|matches[user_password]');
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_info = $this->mauth->handler_get(array('id_user' => $this->lauth->id_user()));
				if(empty($user_info)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'user_name' => $this->input->post('user_name', true),
					'user_email' => $this->input->post('user_email', true)
				);

				$password = $this->input->post('user_password');
				if(!empty($password)){
					$update['user_password'] = $this->lauth->get_password_hash($password);
				}

				$this->session->set_userdata('user_name', $update['user_name']);
				$this->mauth->handler_update($this->lauth->id_user(), $update);
				jsonResponse('Сохранено.','success');
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'edit':
				if(!$this->lauth->logged_in()){
					jsonResponse('Ошибка: Пройдите авторизацию.');
				}

				if(!$this->lauth->is_admin()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->data['user'] = $this->mauth->handler_get(array('id_user' => $this->lauth->id_user()));
				if(empty($this->data['user'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/admin/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_setting':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->data['settings_sections'] = $this->settings->handler_get_sections();
				$content = $this->load->view('admin/modules/settings/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_setting':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$id_setting = (int)$this->uri->segment(4);
				$this->data['setting'] = $this->settings->get_setting(array('id_setting' => $id_setting));
				$this->data['settings_sections'] = $this->settings->handler_get_sections();
				$content = $this->load->view('admin/modules/settings/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_translation':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->load->model('translations/Translations_model', 'translations');
				$this->data['translations_sections'] = $this->translations->handler_get_sections();
				$content = $this->load->view('admin/modules/translations/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_translation':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_logged', false));
				}
				
				if(!$this->lauth->have_right('manage_translations')){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$this->load->model('translations/Translations_model', 'translations');
				$id_translation = (int)$this->uri->segment(4);
				$this->data['translation'] = $this->translations->handler_get_translation(array('id_translation' => $id_translation));
				$this->data['translations_sections'] = $this->translations->handler_get_sections();
				$content = $this->load->view('admin/modules/translations/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function signout(){
        $this->lauth->logout();
		redirect('');
	}
}