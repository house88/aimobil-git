<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vacancies extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->data['active_menu'] = 'about_us';
		
		$this->load->model('Vacancies_model', 'vacancies');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$params = array(
			'vacancy_active' => 1
		);
		$this->data['langs_uri'] = get_static_uri('vacancies');

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('posturi-vacante');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['og_meta'] = $this->data['og_meta'] = array(
			'page_title_'.$this->ulang => $this->data['page_detail'][lang_column('mt')],
			'page_mk_'.$this->ulang => $this->data['page_detail'][lang_column('mk')],
			'page_md_'.$this->ulang => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['records_total'] = $this->vacancies->handler_get_count($params);
		$this->data['vacancies'] = $this->vacancies->handler_get_all($params);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/vacancies/index_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		if($this->lang->is_default()){
			$action = $this->uri->segment(3);
		} else{
			$action = $this->uri->segment(4);
		}
		switch ($action) {
			case 'add':
				$this->form_validation->set_rules('id_vacancy', lang_line('vacancy_label', false), 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_vacancy = (int) $this->input->post('id_vacancy');
				$vacancy = $this->vacancies->handler_get($id_vacancy);

				if(empty($vacancy)){
					jsonResponse(lang_line('error_message_bad_action', false));
				}

				$cv_file = $this->session->userdata('cv_file');
				if(empty($cv_file)){
					jsonResponse(lang_line('cv_file_not_uploaded', false));
				}
				
				$path = 'files/cv/'.$cv_file;
				if(!file_exists($path)){
					jsonResponse(lang_line('cv_file_not_uploaded',false));
				}

				$email_data = array(
					'title' => $this->lsettings->item('cv_email_subject'),
					'vacancy' => $vacancy,
					'email_content' => 'vacancy_cv_tpl'
				);
				$this->email->from($this->lsettings->item('no_reply_email'), $this->lsettings->item('default_title'));
				$this->email->to($this->lsettings->item('cv_email_receiver'));					
				$this->email->subject($this->lsettings->item('cv_email_subject'));
				$this->email->message($this->load->view(get_theme_view('email_templates/email_tpl'), $email_data, true));
				$this->email->attach($path, 'attachment', name_file($cv_file, 'CV'));
				$this->email->set_mailtype("html");
				$this->email->send();

				jsonResponse(lang_line('cv_file_added', false),'success');
			break;
			case 'upload':
				$path = 'files/cv';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'pdf|doc|docx';
				$config['file_name'] = uniqid();
				$config['max_size']	= '10000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_custom_errors('',''),'error');
				}

				$upload_data = $this->upload->data();
				$this->session->set_userdata('cv_file', $upload_data['file_name']);

				$file = new StdClass;
				$file->name = $upload_data['client_name'];
				jsonResponse('', 'success', array("file" => $file,'file_name' => $this->session->userdata('cv_file')));
			break;
			default:
				jsonResponse(lang_line('error_message_bad_action', false));
			break;
		}
	}
}