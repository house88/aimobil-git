<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_vacancies')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Вакансии';
		
		$this->load->model("Vacancies_model", "vacancies");
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/vacancies/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/modules/vacancies/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_vacancy = (int)$this->uri->segment(4);
		$this->data['vacancy'] = $this->vacancies->handler_get($id_vacancy);
		if(empty($this->data['vacancy'])){
			$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
			redirect('admin/vacancies');
		}

		$this->data['main_content'] = 'admin/modules/vacancies/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('description_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('description_en', 'Текст En', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'vacancy_name_ro' => $this->input->post('title_ro'),
					'vacancy_name_ru' => $this->input->post('title_ru'),
					'vacancy_name_en' => $this->input->post('title_en'),
					'vacancy_text_ro' => $this->input->post('description_ro'),
					'vacancy_text_ru' => $this->input->post('description_ru'),
					'vacancy_text_en' => $this->input->post('description_en')
				);

				$id_vacancy = $this->vacancies->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				$this->form_validation->set_rules('vacancy', 'Вакансия', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('description_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('description_en', 'Текст En', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_vacancy = (int)$this->input->post('vacancy');
				$update = array(
					'vacancy_name_ro' => $this->input->post('title_ro'),
					'vacancy_name_ru' => $this->input->post('title_ru'),
					'vacancy_name_en' => $this->input->post('title_en'),
					'vacancy_text_ro' => $this->input->post('description_ro'),
					'vacancy_text_ru' => $this->input->post('description_ru'),
					'vacancy_text_en' => $this->input->post('description_en')
				);

				$this->vacancies->handler_update($id_vacancy, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);

				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_".$this->input->post("iSortCol_{$i}"))) {
							case 'dt_date': 
								$params['sort_by'][] = 'vacancy_date-'.$this->input->post("sSortDir_{$i}");
							break;
						}
					}
				}

				$records = $this->vacancies->handler_get_all($params);
				$records_total = $this->vacancies->handler_get_count($params);

				$output = array(
					"sEcho" => (int)$this->input->post('sEcho'),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-dtype="warning" data-message="Вы уверены что хотите сделать вакансию неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-vacancy="'.$record['id_vacancy'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['vacancy_active'] == 0){
						$status = '<a href="#" data-dtype="success" data-message="Вы уверены что хотите сделать вакансию активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-vacancy="'.$record['id_vacancy'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name'		=>  $record['vacancy_name_ru'],
						'dt_date'		=>  formatDate($record['vacancy_date'], 'd.m.Y H:i:s'),
						'dt_actions'	=>  $status
											.' <a href="'.base_url('admin/vacancies/edit/'.$record['id_vacancy']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" data-dtype="danger" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить вакансию?" data-callback="delete_action" data-vacancy="'.$record['id_vacancy'].'"><i class="fa fa-trash"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'delete':
				$this->form_validation->set_rules('vacancy', 'Вакансия', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_vacancy = (int)$this->input->post('vacancy');
				$vacancy = $this->vacancies->handler_get($id_vacancy);
				if(empty($vacancy)){
					jsonResponse('Данные не верны.');
				}

				$this->vacancies->handler_delete($id_vacancy);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('vacancy', 'Вакансия', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_vacancy = (int)$this->input->post('vacancy');
				$vacancy = $this->vacancies->handler_get($id_vacancy);
				if(empty($vacancy)){
					jsonResponse('Данные не верны.');
				}

				if($vacancy['vacancy_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->vacancies->handler_update($id_vacancy, array('vacancy_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}