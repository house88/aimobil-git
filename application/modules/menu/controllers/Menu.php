<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model("Menu_model", 'menu');
        $this->data = array();
	}

	function _get_menu_hash($menu_params = array()){
		extract($menu_params);

		if(!isset($menu_hash)){
			return FALSE;
		}

		$menu = $this->menu->handler_get_by_hash($menu_hash);
		if(empty($menu)){
			return FALSE;
		}

		$menu['elements'] =json_decode($menu['menu_elements'], true);

		return $menu;
	}

	function _get_dashboard_menu($menu_params = array()){
		return $this->menu->handler_dasboard();
	}
}
