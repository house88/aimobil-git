<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{    
    function __construct(){
		parent::__construct();
        $this->config->set_item('language', 'russian');
        
        if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_site_menu')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

        $this->load->model("Menu_model", "menu");

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

    function index(){        
        $this->data['main_title'] = 'Меню';
		$this->data['main_content'] = 'admin/modules/menu/list';
		$this->load->view('admin/page', $this->data);
    }

    function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $option = $this->uri->segment(4);
        switch($option){
            // DONE
            case 'add':
                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$menu_values = $this->input->post('menu_values');
				$valid_menu_values = array();
				if(!empty($menu_values)){
					foreach($menu_values['links'] as $menu_key => $menu_link){
						if(empty($menu_values['title_ro'][$menu_key]) || empty($menu_values['title_ru'][$menu_key]) || empty($menu_values['title_en'][$menu_key])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
                        }
                        
						if(empty($menu_link)){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
                        }
                        
						$valid_menu_values[] = array(
                            'link' => $menu_link,
                            'title_ro' => $menu_values['title_ro'][$menu_key],
                            'title_ru' => $menu_values['title_ru'][$menu_key],
                            'title_en' => $menu_values['title_en'][$menu_key]
                        );
					}
				}
                $insert = array(
                    'menu_title'    => $this->input->post('title'),
                    'menu_hash'     => md5(uniqid().$this->input->post('title')),
					'menu_elements' => json_encode($valid_menu_values)
                );
                $this->menu->handler_insert($insert);
                jsonResponse('Сохранено.', 'success');
            break;
            // DONE
            case 'edit':
                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int)$this->input->post('menu');
				$menu_values = $this->input->post('menu_values');
				$valid_menu_values = array();
				if(!empty($menu_values)){
					foreach($menu_values['links'] as $menu_key => $menu_link){
						if(empty($menu_values['title_ro'][$menu_key]) || empty($menu_values['title_ru'][$menu_key]) || empty($menu_values['title_en'][$menu_key])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
                        }
                        
						if(empty($menu_link)){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
                        }
                        
						$valid_menu_values[] = array(
                            'link' => $menu_link,
                            'title_ro' => $menu_values['title_ro'][$menu_key],
                            'title_ru' => $menu_values['title_ru'][$menu_key],
                            'title_en' => $menu_values['title_en'][$menu_key]
                        );
					}
				}
                $update = array(
                    'menu_title' => $this->input->post('title'),
					'menu_elements' => json_encode($valid_menu_values)
                );
                $this->menu->handler_update($id_menu, $update);
                jsonResponse('Сохранено.', 'success');
            break;
            // DONE
			case 'list_dt':
                $records = $this->menu->handler_get_all();
                $records_total = count($records);

                $output = array(
                    "sEcho" => intval($_POST['sEcho']),
                    "iTotalRecords" => $records_total,
                    "iTotalDisplayRecords" => $records_total,
                    "aaData" => array()
                );

                foreach ($records as $record) {
                    $output['aaData'][] = array(
                        'dt_name'		=>  $record['menu_title'],
                        'dt_hash'		=>  $record['menu_hash'],
                        'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/menu/popup_forms/edit/'.$record['id_menu']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
                                            <a href="#" data-dtype="danger" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить меню?" data-callback="delete_action" data-menu="'.$record['id_menu'].'"><i class="fa fa-trash"></i></a>'
                    );
                }
                jsonResponse('', 'success', $output);
            break;
            // DONE
            case 'delete':
                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int)$this->input->post('menu');
                $this->menu->handler_delete($id_menu);
                jsonResponse('Операция прошла успешно.', 'success');
            break;
        }
    }

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add':
				$content = $this->load->view('admin/modules/menu/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit':
				$id_menu = (int)$this->uri->segment(5);
				$this->data['menu'] = $this->menu->handler_get($id_menu);
				if(empty($this->data['menu'])){
					jsonResponse('Данные не верны.');
				}

				$content = $this->load->view('admin/modules/menu/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}
