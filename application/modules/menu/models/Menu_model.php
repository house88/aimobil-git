<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model{
    var $menu = "menu";

    function __construct(){
        parent::__construct();
    }
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->menu, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_menu = 0, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_menu', $id_menu);
        $this->db->update($this->menu, $data);
	}
	
	function handler_delete($id_menu = 0){
        $this->db->where('id_menu', $id_menu);
        $this->db->delete($this->menu);
	}

	function handler_get($id_menu = 0){
        $this->db->where('id_menu', $id_menu);
        return $this->db->get($this->menu)->row_array();
	}	

	function handler_get_by_hash($menu_hash = ''){
        if(empty($menu_hash)){
            return FALSE;
        }

        $this->db->where('menu_hash', $menu_hash);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_menu ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->menu)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		return $this->db->count_all_results($this->menu);
    }
    
    function handler_dasboard(){
		$menu = array();
		$menu['cpanel'] = new stdClass();
		$menu['cpanel']->url = "";
		$menu['cpanel']->name = '<i class="fa fa-dashboard"></i> <span>Панель управления</span>';
		$menu['cpanel']->rights = 'manage_settings,manage_translations,manage_site_menu,manage_locations,manage_pages_meta,manage_currency';
		$menu['cpanel']->submenu = array(
			array(
				'url' => 'admin/settings',
				'name' => '<i class="fa fa-cogs"></i> <span>Настройки</span>',
				'rights' => 'manage_settings'
			),
			array(
				'url' => 'admin/translations',
				'name' => '<i class="fa fa-globe"></i> <span>Переводы</span>',
				'rights' => 'manage_translations'
			),
			// array(
			// 	'url' => 'admin/menu',
			// 	'name' => '<i class="fa fa-th-large"></i> <span>Меню</span>',
			// 	'rights' => 'manage_site_menu'
			// ),
			array(
				'url' => 'admin/routings',
				'name' => '<i class="fa fa-th-large"></i> <span>Ссылки сайта</span>',
				'rights' => 'manage_routings'
			),
			array(
				'url' => 'admin/locations',
				'name' => '<i class="fa fa-map-marker"></i> <span>Местонахождения</span>',
				'rights' => 'manage_locations'
			),
			array(
				'url' => 'admin/currency',
				'name' => '<i class="fa fa-money"></i> <span>Валюты сайта</span>',
				'rights' => 'manage_currency'
			),
			array(
				'url' => 'admin/notify',
				'name' => '<i class="fa fa-envelope"></i> <span>Email шаблоны</span>',
				'rights' => 'manage_email_templates'
			)
		);

		$menu['real_estate'] = new stdClass();
		$menu['real_estate']->url = "";
		$menu['real_estate']->name = '<i class="fa fa-building"></i> <span>Недвижимость</span>';
		$menu['real_estate']->rights = 'manage_real_estate,manage_re_categories,manage_re_properties,manage_re_icons';
		$menu['real_estate']->submenu = array(
			array(
				'url' => 'admin/real_estate',
				'name' => '<i class="fa fa-building"></i> <span>Обьекты</span>',
				'rights' => 'manage_real_estate'
			),
			array(
				'url' => 'admin/real_estate/icons',
				'name' => '<i class="fa fa-eye"></i> <span>Иконки</span>',
				'rights' => 'manage_re_categories'
			),
			array(
				'url' => 'admin/real_estate/categories',
				'name' => '<i class="fa fa-list"></i> <span>Категории</span>',
				'rights' => 'manage_re_properties'
			),
			array(
				'url' => 'admin/real_estate/properties',
				'name' => '<i class="fa fa-list"></i> <span>Свойства объектов</span>',
				'rights' => 'manage_re_icons'
			)
		);

		$menu['residential_complexes'] = new stdClass();
		$menu['residential_complexes']->url = "";
		$menu['residential_complexes']->name = '<i class="fa fa-building"></i> <span>Жилые комплексы</span>';
		$menu['residential_complexes']->rights = 'manage_residential_complexes,manage_pages';
		$menu['residential_complexes']->submenu = array(
			array(
				'url' => 'admin/residential_complexes',
				'name' => '<i class="fa fa-building"></i> <span>Жилые комплексы</span>',
				'rights' => 'manage_residential_complexes'
			),
			array(
				'url' => 'admin/pages/edit/complexe-locative',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			)
		);

		$menu['planing'] = new stdClass();
		$menu['planing']->url = "";
		$menu['planing']->name = '<i class="fa fa-building"></i> <span>Планировки квартир</span>';
		$menu['planing']->rights = 'manage_plans,manage_pages';
		$menu['planing']->submenu = array(
			array(
				'url' => 'admin/planing',
				'name' => '<i class="fa fa-building"></i> <span>Планировки квартир</span>',
				'rights' => 'manage_plans'
			),
			array(
				'url' => 'admin/pages/edit/apartment-planning',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			),
		);

		$menu['blog'] = new stdClass();
		$menu['blog']->url = "";
		$menu['blog']->name = '<i class="fa fa-newspaper-o"></i> <span>Блог</span>';
		$menu['blog']->rights = 'manage_blogs,manage_blogs_categories,manage_pages';
		$menu['blog']->submenu = array(
			array(
				'url' => 'admin/blog',
				'name' => '<i class="fa fa-th-large"></i> <span>Блог</span>',
				'rights' => 'manage_blogs'
			),
			array(
				'url' => 'admin/blog/categories',
				'name' => '<i class="fa fa-cogs"></i> <span>Категории</span>',
				'rights' => 'manage_blogs_categories'
			),
			array(
				'url' => 'admin/pages/edit/blog',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			)
		);
		$menu['services'] = new stdClass();
		$menu['services']->url = "";
		$menu['services']->name = '<i class="fa fa-list"></i> <span>Услуги</span>';
		$menu['services']->rights = 'manage_services,manage_pages';
		$menu['services']->submenu = array(
			array(
				'url' => 'admin/services',
				'name' => '<i class="fa fa-list"></i> <span>Услуги</span>',
				'rights' => 'manage_services'
			),
			array(
				'url' => 'admin/pages/edit/servicii-imobiliare',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			),
		);

		$menu['banners'] = new stdClass();
		$menu['banners']->url = "admin/banner";
		$menu['banners']->name = '<i class="fa fa-object-group"></i> <span>Баннеры</span>';
		$menu['banners']->rights = 'manage_banners';
		$menu['banners']->submenu = array();

		$menu['pages'] = new stdClass();
		$menu['pages']->url = "";
		$menu['pages']->name = '<i class="fa fa-list"></i> <span>Страницы</span>';
		$menu['pages']->rights = 'manage_pages';
		$menu['pages']->submenu = array(
			array(
				'url' => 'admin/pages/edit/home',
				'name' => '<i class="fa fa-home"></i> <span>Главная</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/add_ad',
				'name' => '<i class="fa fa-file-text"></i> <span>Добавить объявление</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/credit-ipotecar',
				'name' => '<i class="fa fa-file-text"></i> <span>Ипотека</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/companie-imobiliara',
				'name' => '<i class="fa fa-file-text"></i> <span>О Acces Imobil</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/contacts',
				'name' => '<i class="fa fa-file-text"></i> <span>Контакты</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/discounts',
				'name' => '<i class="fa fa-file-text"></i> <span>Низкие цены</span>',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'admin/pages/edit/real_estate_deleted',
				'name' => '<i class="fa fa-file-text"></i> <span>Объект удален</span>',
				'rights' => 'manage_pages'
			),
		);

		$menu['vacancies'] = new stdClass();
		$menu['vacancies']->url = "admin/vacancies";
		$menu['vacancies']->name = '<i class="fa fa-outdent"></i> <span>Вакансии</span>';
		$menu['vacancies']->rights = 'manage_vacancies,manage_pages';
		$menu['vacancies']->submenu = array(
			array(
				'url' => 'admin/vacancies',
				'name' => '<i class="fa fa-outdent"></i> <span>Вакансии</span>',
				'rights' => 'manage_vacancies'
			),
			array(
				'url' => 'admin/pages/edit/posturi-vacante',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			),
		);

		$menu['partners'] = new stdClass();
		$menu['partners']->url = "";
		$menu['partners']->name = '<i class="fa fa-globe"></i> <span>Партнеры</span>';
		$menu['partners']->rights = 'manage_partners,manage_pages';
		$menu['partners']->submenu = array(
			array(
				'url' => 'admin/partners',
				'name' => '<i class="fa fa-globe"></i> <span>Партнеры</span>',
				'rights' => 'manage_partners'
			),
			array(
				'url' => 'admin/partners/categories',
				'name' => '<i class="fa fa-list"></i> <span>Типы</span>',
				'rights' => 'manage_partners'
			),
			array(
				'url' => 'admin/pages/edit/partners',
				'name' => '<i class="fa fa-file-text"></i> <span>Meta страницы</span>',
				'rights' => 'manage_pages'
			)
		);

		$menu['users'] = new stdClass();
		$menu['users']->url = "";
		$menu['users']->name = '<i class="fa fa-users"></i> <span>Менеджеры</span>';
		$menu['users']->rights = 'manage_users,manage_groups,manage_group_rights';
		$menu['users']->submenu = array(
			array(
				'url' => 'admin/users/managers',
				'name' => '<i class="fa fa-users"></i> <span>Менеджеры</span>',
				'rights' => 'manage_users'
			),
			array(
				'url' => 'admin/users/positions',
				'name' => '<i class="fa fa-th-large"></i> <span>Должности</span>',
				'rights' => 'manage_users'
			),
			array(
				'url' => 'admin/users/groups',
				'name' => '<i class="fa fa-cogs"></i> <span>Группы</span>',
				'rights' => 'manage_groups'
			),
			// array(
			// 	'url' => 'admin/users/groups_rights',
			// 	'name' => '<i class="fa fa-cogs"></i> <span>Права по группам</span>',
			// 	'rights' => 'manage_group_rights'
			// )
		);

		return $menu;
	}
}
