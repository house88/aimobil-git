<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notify_model extends CI_Model{
	private $email_templates = "email_templates";

	public $secret_key = '@imob!l(&%#!@$^*)';
	function __construct(){
		parent::__construct();
	}

	function handler_update($id_template = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_template', $id_template);
		$this->db->update($this->email_templates, $data);
	}

	function handler_get($id_template = 0){
		$this->db->where('id_template', $id_template);
		return $this->db->get($this->email_templates)->row_array();
	}

	function handler_get_by_alias($template_alias = ''){
		if(empty($template_alias)){
			return false;
		}

		$this->db->where('template_alias', $template_alias);
		return $this->db->get($this->email_templates)->row_array();
	}

	function handler_get_all(){
		return $this->db->get($this->email_templates)->result_array();
	}

	function handler_get_count(){
		return $this->db->count_all_results($this->email_templates);
	}
}