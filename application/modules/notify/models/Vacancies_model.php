<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vacancies_model extends CI_Model{
	private $vacancies = "vacancies";
	private $vacancies_cv = "vacancies_cv";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->vacancies, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_vacancy = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_vacancy', $id_vacancy);
		$this->db->update($this->vacancies, $data);
	}

	function handler_delete($id_vacancy = 0){
		$this->db->where('id_vacancy', $id_vacancy);
		$this->db->delete($this->vacancies);
	}

	function handler_get($id_vacancy = 0){
		$this->db->where('id_vacancy', $id_vacancy);
		return $this->db->get($this->vacancies)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " vacancy_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_vacancy)){
			$this->db->where_in('id_vacancy', $id_vacancy);
        }

        if(isset($vacancy_active)){
			$this->db->where('vacancy_active', $vacancy_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->vacancies)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_vacancy)){
			$this->db->where_in('id_vacancy', $id_vacancy);
        }

        if(isset($vacancy_active)){
			$this->db->where('vacancy_active', $vacancy_active);
        }

		return $this->db->count_all_results($this->vacancies);
	}

	// CV HANDLERS
	function handler_cv_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->vacancies_cv, $data);
		return $this->db->insert_id();
	}

	function handler_cv_update($id_cv = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_cv', $id_cv);
		$this->db->update($this->vacancies_cv, $data);
	}

	function handler_cv_delete($id_cv = 0){
		$this->db->where('id_cv', $id_cv);
		$this->db->delete($this->vacancies_cv);
	}

	function handler_cv_get($id_cv = 0){
		$this->db->where('id_cv', $id_cv);
		return $this->db->get($this->vacancies_cv)->row_array();
	}

	function handler_cv_get_all($conditions = array()){
        $order_by = " cv.cv_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("*");
		$this->db->from("{$this->vacancies_cv} cv");
		$this->db->join("{$this->vacancies} v", "cv.id_vacancy = v.id_vacancy", "inner");

        if(isset($id_cv)){
			$this->db->where_in('cv.id_cv', $id_cv);
        }

        if(isset($id_vacancy)){
			$this->db->where_in('v.id_vacancy', $id_vacancy);
        }

        if(isset($vacancy_active)){
			$this->db->where('v.vacancy_active', $vacancy_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_cv_get_count($conditions = array()){
        extract($conditions);

        $this->db->select("*");
		$this->db->from("{$this->vacancies_cv} cv");
		$this->db->join("{$this->vacancies} v", "cv.id_vacancy = v.id_vacancy", "inner");

        if(isset($id_cv)){
			$this->db->where_in('cv.id_cv', $id_cv);
        }

        if(isset($id_vacancy)){
			$this->db->where_in('v.id_vacancy', $id_vacancy);
        }

        if(isset($vacancy_active)){
			$this->db->where('v.vacancy_active', $vacancy_active);
        }

		return $this->db->count_all_results();
	}
}