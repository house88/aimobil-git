<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notify extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];

		$this->load->model("Notify_model", "notify");
		
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		return_404();
	}

	function unsubscribe(){
		if($this->lang->is_default()){
			$token = xss_clean($this->uri->segment(2));
		} else{
			$token = xss_clean($this->uri->segment(3));
		}

		$this->load->model('subscribers/Subscribers_model', 'subscribers');
		$this->subscribers->handler_update_token($token, array('subscribe_active' => 0));

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('unsubscribe');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['langs_uri'] = get_static_uri('unsubscribe');
		$this->data['active_menu'] = 'unsubscribe';
		
		$this->data['main_content'] = 'modules/pages/unsubscribe_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		if($this->lang->is_default()){
			$action = $this->uri->segment(3);
		} else{
			$action = $this->uri->segment(4);
		}

		switch ($action) {
			case 'add_ad':
				$this->form_validation->set_rules('category', lang_line('notify_add_ad_label_category', false), 'required|xss_clean');
				$this->form_validation->set_rules('name', lang_line('notify_add_ad_label_name', false), 'required|xss_clean');
				$this->form_validation->set_rules('phone', lang_line('notify_add_ad_label_phone', false), 'required|numeric|xss_clean');
				$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$response = $this->recaptcha->verifyResponse($this->input->post('g-recaptcha-response'));
				if (!isset($response['success']) || $response['success'] !== true) {
					jsonResponse(lang_line('error_message_invalid_captcha', false));
				}
				
				$this->load->model('real_estate/Real_estate_model', 'real_estate');
				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$email_data = array(
					'title' => $this->lsettings->item('add_ad_email_subject'),
					'ad' => array(
						'category' => $category['category_title_ru'],
						'name' => $this->input->post('name', true),
						'phone' => $this->input->post('phone', true)
					),
					'email_content' => 'add_ad_tpl'
				);
				$this->email->from($this->lsettings->item('no_reply_email'), $this->lsettings->item('default_title'));
				$this->email->to($this->lsettings->item('add_ad_email_receiver'));					
				$this->email->subject($this->lsettings->item('add_ad_email_subject'));
				$this->email->message($this->load->view(get_theme_view('email_templates/email_tpl'), $email_data, true));
				$this->email->set_mailtype("html");
				$this->email->send();

				jsonResponse(lang_line('notify_feedback_success_sent', false),'success');
			break;
			case 'feedback':
				$this->form_validation->set_rules('name', lang_line('notify_feedback_label_name', false), 'required|xss_clean');
				$this->form_validation->set_rules('email', lang_line('notify_feedback_label_email', false), 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('phone', lang_line('notify_feedback_label_phone', false), 'required|numeric|xss_clean');
				$this->form_validation->set_rules('message', lang_line('notify_feedback_label_message', false), 'required|xss_clean');
				$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$response = $this->recaptcha->verifyResponse($this->input->post('g-recaptcha-response'));
				if (!isset($response['success']) || $response['success'] !== true) {
					jsonResponse(lang_line('error_message_invalid_captcha', false));
				}

				$email_data = array(
					'title' => $this->lsettings->item('feedback_email_subject'),
					'feedback' => array(
						'name' => $this->input->post('name', true),
						'email' => $this->input->post('email', true),
						'phone' => $this->input->post('phone', true),
						'message' => $this->input->post('message', true)
					),
					'email_content' => 'feedback_tpl'
				);
				$this->email->from($email_data['feedback']['email'], $email_data['feedback']['name']);
				$this->email->to($this->lsettings->item('feedback_email_receiver'));					
				$this->email->subject($this->lsettings->item('feedback_email_subject'));
				$this->email->message($this->load->view(get_theme_view('email_templates/email_tpl'), $email_data, true));
				$this->email->set_mailtype("html");
				$this->email->send();
				
				jsonResponse(lang_line('notify_feedback_success_sent', false),'success');
			break;
			case 'send_offer':
				$this->form_validation->set_rules('name', lang_line('notify_feedback_label_name', false), 'required|xss_clean');
				$this->form_validation->set_rules('offer_price', lang_line('form_label_offer_price', false), 'required|xss_clean');
				$this->form_validation->set_rules('phone', lang_line('notify_feedback_label_phone', false), 'required|numeric|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_real_estate = (int)$this->input->post('id_real_estate');
				$_real_estate = Modules::run('real_estate/_get', $id_real_estate);
				if(empty($_real_estate) || $_real_estate['real_estate_active'] != 1 || $_real_estate['real_estate_deleted'] == 1){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$_re_manager = Modules::run('users/_get_user', $_real_estate['id_manager']);
				if(empty($_re_manager)){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$email_config = array(
					'from' => $this->lsettings->item('no_reply_email'),
					'to' => $_re_manager->user_email,
					'subject' => 'Предложение по цене для обьекта',
					'template' => 'send_offer_tpl',
					'template_replace' => array(
						'{{title}}' => 'Предложение по цене для обьекта',
						'{{client_name}}' => $this->input->post('name', true),
						'{{client_phone}}' => formatPhone($this->input->post('phone', true)),
						'{{client_price}}' => $this->input->post('offer_price', true),
						'{{link}}' => site_url(get_dinamyc_uri('catalog/category/real_estate', $_real_estate[lang_column('full_url')], $this->ulang))
					)
				);
				$this->emailsender->send_email($email_config);
				
				jsonResponse(lang_line('notify_offer_success_sent', false),'success');
			break;
			case 'send_subscribe':
				$this->form_validation->set_rules('email', lang_line('notify_feedback_label_email', false), 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('subscribe_period', lang_line('label_subscribe_select_period', false), 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_real_estate = (int)$this->input->post('id_real_estate');
				$_real_estate = Modules::run('real_estate/_get', $id_real_estate);
				if(empty($_real_estate) || $_real_estate['real_estate_active'] != 1 || $_real_estate['real_estate_deleted'] == 1){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$this->data['template'] = $this->notify->handler_get_by_alias('user_subscribe');

				$email_config = array(
					'from' => $this->lsettings->item('no_reply_email'),
					'to' => $this->input->post('email'),
					'subject' => $this->data['template']["template_subject_{$this->data['lang']}"],
					'template' => 'send_subscribe_tpl',
					'template_replace' => array(
						'{{title}}' => $this->data['template']["template_subject_{$this->data['lang']}"],
						'{{text}}' => $this->data['template']["template_text_{$this->data['lang']}"]
					)
				);
				$this->emailsender->send_email($email_config);

				$subscribe_settings = Modules::run('real_estate/_get_same_params', $id_real_estate);
				$insert = array(
					'subscribe_lang' => $this->ulang,
					'subscribe_email' => $this->input->post('email', true),
					'subscribe_token' => md5($this->input->post('email', true).$this->notify->secret_key),
					'subscribe_months' => (int)$this->input->post('subscribe_period', true),
					'subscribe_settings' => json_encode($subscribe_settings)
				);
				$this->load->model('subscribers/Subscribers_model', 'subscribers');
				$this->subscribers->handler_insert($insert);
				
				jsonResponse(lang_line('notify_subscribe_success_sent', false),'success');
			break;
			default:
				jsonResponse(lang_line('error_message_bad_action', false));
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		if($this->lang->is_default()){
			$action = $this->uri->segment(3);
		} else{
			$action = $this->uri->segment(4);
		}
		
		switch ($action) {
			case 'send_offer':
				if($this->lang->is_default()){
					$id_real_estate = (int)$this->uri->segment(4);
				} else{
					$id_real_estate = (int)$this->uri->segment(5);
				}

				$_real_estate = Modules::run('real_estate/_get', $id_real_estate);
				if(empty($_real_estate) || $_real_estate['real_estate_active'] != 1 || $_real_estate['real_estate_deleted'] == 1){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$this->data['id_real_estate'] = $id_real_estate;

				$content = $this->load->view(get_theme_view('modules/real_estate/form_offer'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'send_subscribe':
				if($this->lang->is_default()){
					$id_real_estate = (int)$this->uri->segment(4);
				} else{
					$id_real_estate = (int)$this->uri->segment(5);
				}

				$_real_estate = Modules::run('real_estate/_get', $id_real_estate);
				if(empty($_real_estate) || $_real_estate['real_estate_active'] != 1 || $_real_estate['real_estate_deleted'] == 1){
					jsonResponse(lang_line('error_message_bad_data', false));
				}

				$this->data['id_real_estate'] = $id_real_estate;

				$content = $this->load->view(get_theme_view('modules/real_estate/form_subscribe'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse(lang_line('error_message_bad_action', false));
			break;
		}
	}
}