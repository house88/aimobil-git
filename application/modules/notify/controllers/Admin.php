<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{    
    function __construct(){
		parent::__construct();
        $this->config->set_item('language', 'russian');
        
        if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_site_menu')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

        $this->load->model("Notify_model", "notify");

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

    function index(){        
        $this->data['main_title'] = 'Email шаблоны';
		$this->data['main_content'] = 'admin/modules/notify/templates_list';
		$this->load->view('admin/page', $this->data);
    }

    function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $option = $this->uri->segment(4);
        switch($option){
            // DONE
            case 'edit':
                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                $this->form_validation->set_rules('template', 'Шаблон', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_template = (int)$this->input->post('template');
                $update = array(
                    'template_name' => $this->input->post('title'),
                    'template_subject_ro' => $this->input->post('subject_ro'),
                    'template_subject_ru' => $this->input->post('subject_ru'),
                    'template_subject_en' => $this->input->post('subject_en'),
                    'template_text_ro' => $this->input->post('text_ro'),
                    'template_text_ru' => $this->input->post('text_ru'),
                    'template_text_en' => $this->input->post('text_en')
                );
                $this->notify->handler_update($id_template, $update);
                jsonResponse('Сохранено.', 'success');
            break;
            // DONE
			case 'list_templates_dt':
                $records = $this->notify->handler_get_all();
                $records_total = $this->notify->handler_get_count();

                $output = array(
                    "sEcho" => intval($_POST['sEcho']),
                    "iTotalRecords" => $records_total,
                    "iTotalDisplayRecords" => $records_total,
                    "aaData" => array()
                );

                foreach ($records as $record) {
                    $output['aaData'][] = array(
                        'dt_name'		=>  $record['template_name'],
                        'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/notify/popup_forms/edit/'.$record['id_template']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
                    );
                }
                jsonResponse('', 'success', $output);
            break;
        }
    }

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'edit':
				$id_template = (int)$this->uri->segment(5);
				$this->data['template'] = $this->notify->handler_get($id_template);
				if(empty($this->data['template'])){
					jsonResponse('Данные не верны.');
				}

				$content = $this->load->view('admin/modules/notify/form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}
