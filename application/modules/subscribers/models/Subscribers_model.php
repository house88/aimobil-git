<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subscribers_model extends CI_Model{
	
	var $subscribers = "subscribers";
	
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->subscribers, $data);
		return $this->db->insert_id();
	}
	
	function handler_update($id_subscriber = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_subscriber', $id_subscriber);
		$this->db->update($this->subscribers, $data);
	}
	
	function handler_update_token($token = '', $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('subscribe_token', $token);
		$this->db->update($this->subscribers, $data);
	}
	
	function handler_delete($id_subscriber = 0){
		$this->db->where('id_subscriber', $id_subscriber);
		$this->db->delete($this->subscribers);
	}

	function handler_get_all($conditions = array()){
        extract($conditions);

		if (isset($subscribe_active)) {
			$this->db->where('subscribe_active', $subscribe_active);
		}
		
		return $this->db->get($this->subscribers)->result_array();
	}
}