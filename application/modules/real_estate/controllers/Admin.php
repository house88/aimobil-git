<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_real_estate')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Недвижимость';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Real_estate_model", "real_estate");
		$this->load->model("locations/Locations_model", "locations");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$params = array(
			'category_active' => 1
		);
		$this->data['categories'] = $this->real_estate->handler_get_categories($params);
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0)), 'id_parent', true);

		$this->data['default_currency'] = Modules::run('currency/_get_default_currency');
		$this->data['main_title'] = 'Объекты недвижимости';
		$this->data['main_content'] = 'admin/modules/real_estate/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$params = array(
			'category_active' => 1
		);
		$this->data['categories'] = $this->real_estate->handler_get_categories($params);
		
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0)), 'id_parent', true);
		
		$this->data['regions_999'] = $this->locations->handler_api_regions();

		$this->data['main_title'] = 'Добавить объект недвижимости';
		$this->data['main_content'] = 'admin/modules/real_estate/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_real_estate = (int)$this->uri->segment(4);
		$this->data['real_estate'] = $this->real_estate->handler_get($id_real_estate);

		if (empty($this->data['real_estate'])) {
			redirect('admin/real_estate');
		}

		$params = array(
			'category_active' => 1
		);
		$this->data['categories'] = $this->real_estate->handler_get_categories($params);

		$this->data['category'] = $this->real_estate->handler_get_category($this->data['real_estate']['id_category']);

		$properties_params = array(
			'id_category' => $this->data['real_estate']['id_category'],
			'property_type' => array('multiselect','range')
		);
		$this->data['properties'] = $this->real_estate->handler_get_properties($properties_params);
		$this->data['selected_properties'] = arrayByKey($this->real_estate->handler_get_real_estate_properties_values($id_real_estate), 'id_property');

		if($this->data['category']['category_has_icons'] == 1){
			$this->data['icons'] = $this->real_estate->handler_get_icons();
			$this->data['icons_selected'] = json_decode($this->data['real_estate']['real_estate_icons'], true);
		}
		
		$this->data['location'] = $this->locations->handler_get_full($this->data['real_estate']['id_location']);
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0)), 'id_parent', true);
		
		$this->data['regions_999'] = $this->locations->handler_api_regions();
		if($this->data['real_estate']['id_region_999'] > 0){
			$this->data['raions_999'] = $this->locations->handler_api_raions($this->data['real_estate']['id_region_999']);
		}

		if($this->data['real_estate']['id_raion_999'] > 0){
			$this->data['sectors_999'] = $this->locations->handler_api_sectors($this->data['real_estate']['id_raion_999']);
		}

		$this->data['main_content'] = 'admin/modules/real_estate/edit';
		$this->load->view('admin/page', $this->data);
	}

	// DONE
	function categories(){
		$this->data['main_title'] = 'Категории недвижимости';
		$this->data['main_content'] = 'admin/modules/real_estate/categories/list';
		$this->load->view('admin/page', $this->data);
	}

	// DONE
	function properties(){
		$this->data['main_title'] = 'Свойства объектов недвижимости';
		$this->data['main_content'] = 'admin/modules/real_estate/properties/list';
		$this->load->view('admin/page', $this->data);
	}

	// DONE
	function icons(){
		$this->data['main_title'] = 'Иконки';
		$this->data['main_content'] = 'admin/modules/real_estate/icons/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				if ($this->lauth->have_right('manage_real_estate_manager')) {
					$this->form_validation->set_rules('manager', 'Менеджер', 'required|xss_clean');
				}

				$this->form_validation->set_rules('title_ro', 'Краткое название RO', 'required|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Краткое название RU', 'max_length[250]');
				$this->form_validation->set_rules('title_en', 'Краткое название En', 'max_length[250]');
				$this->form_validation->set_rules('full_title_ro', 'Полное название RO', 'required|max_length[250]');
				$this->form_validation->set_rules('full_title_ru', 'Полное название RU', 'max_length[250]');
				$this->form_validation->set_rules('full_title_en', 'Полное название En', 'max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('owner_name', 'Имя владельца', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('owner_phone', 'Телефон владельца', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('owner_email', 'Email владельца', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('owner_address', 'Адрес владельца', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('category', 'Тип недвижимости', 'required|xss_clean');
				$this->form_validation->set_rules('offer_type', 'Тип предложения', 'required|xss_clean');
				$this->form_validation->set_rules('price', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('address', 'Адрес', 'xss_clean');

				// 999.md FIELDS
				if ($this->input->post('advert_api')) {
					$this->form_validation->set_rules('advert_status', 'Статус на 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('region_999', 'Регион 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('raion_999', 'Населенный пункт 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('sector_999', 'Сектор 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('title_999', 'Заголовок объявления 999.md', 'required|max_length[250]');
					$this->form_validation->set_rules('text_999', 'Текст 999.md', 'required|max_length[2000]');
				}

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$uploaded_files = $this->input->post('uploaded_files');
				if(empty($uploaded_files)){
					jsonResponse('Ошибка: Нужно добавить как минимум одну фотографию.');
				}
				
				$main_photo = $this->input->post('main_photo');
				if(empty($main_photo) || !in_array($main_photo, $uploaded_files)){
					jsonResponse('Ошибка: Нужно выбрать главную фотографию.');
				}

				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$id_location = (int)$this->input->post('location_child');
				$location = $this->locations->handler_get_full($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Нужно выбрать местоположение.');
				}

				$id_location_aditional = $this->input->post('location_child_additional');
				$aditional_locations = array();
				if(!empty($id_location_aditional)){
					$id_location_aditional = array_filter($id_location_aditional);
					if(!empty($id_location_aditional)){
						$locations_aditional = $this->locations->handler_get_all(array('id_location' => $id_location_aditional));
						if(!empty($locations_aditional)){
							foreach ($locations_aditional as $location_aditional) {
								$aditional_locations[] = $location_aditional['id_location'];
							}
						}
					}
				}
				
				if (!($this->input->post('latitude') && $this->input->post('longitude'))) {
					jsonResponse('Ошибка: Нужно выбрать местоположение на карте.');
				}

				$discount = $this->input->post('discount');
				$discount_type = 'no';
				$discount_value = 0;
				if(!empty($discount)){
					switch ($discount['type']) {
						case 'simple':
							$discount_type = 'simple';
						break;
						case 'new_price':
							$discount_type = 'new_price';
							$discount_value = (int) $discount['value'];
						break;
						case 'by_m2':
							$discount_type = 'by_m2';
							$discount_value = (int) $discount['value'];
						break;
					}
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/real_estate/temp/'.$remove_photo);
						@unlink('files/real_estate/temp/thumb_'.$remove_photo);
					}
				}

				$filtered_icons = array();
				$insert = array(
					'id_category' => $id_category,
					'id_region_999' => (int)$this->input->post('region_999'),
					'id_raion_999' => (int)$this->input->post('raion_999'),
					'id_sector_999' => $this->input->post('sector_999'),
					'real_estate_title_999' => $this->input->post('title_999'),
					'real_estate_text_999' => $this->input->post('text_999'),
					'advert_status_999' => $this->input->post('advert_status'),
					'advert_on_999' => ($this->input->post('advert_api'))?1:0,
					'id_manager' => ($this->lauth->have_right('manage_real_estate_manager'))?(int)$this->input->post('manager'):$this->lauth->id_user(),
					'id_manager_created' => $this->lauth->id_user(),
					'real_estate_title_ro' => $this->input->post('title_ro'),
					'real_estate_title_ru' => $this->input->post('title_ru'),
					'real_estate_title_en' => $this->input->post('title_en'),
					'real_estate_full_title_ro' => $this->input->post('full_title_ro'),
					'real_estate_full_title_ru' => $this->input->post('full_title_ru'),
					'real_estate_full_title_en' => $this->input->post('full_title_en'),
					'real_estate_text_ro' => $this->input->post('description_ro'),
					'real_estate_text_ru' => $this->input->post('description_ru'),
					'real_estate_text_en' => $this->input->post('description_en'),
					'real_estate_text_clean_ro' => strip_tags_with_whitespace($this->input->post('description_ro')),
					'real_estate_text_clean_ru' => strip_tags_with_whitespace($this->input->post('description_ru')),
					'real_estate_text_clean_en' => strip_tags_with_whitespace($this->input->post('description_en')),
					'real_estate_owner_name' => $this->input->post('owner_name'),
					'real_estate_owner_phone' => $this->input->post('owner_phone'),
					'real_estate_owner_email' => $this->input->post('owner_email'),
					'real_estate_owner_address' => $this->input->post('owner_address'),
					'mt_ro' => $this->input->post('full_title_ro'),
					'mt_ru' => $this->input->post('full_title_ru'),
					'mt_en' => $this->input->post('full_title_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'real_estate_price' => (float) $this->input->post('price'),
					'real_estate_discount_type' => $discount_type,
					'real_estate_discount_value' => $discount_value,
					'real_estate_photo' => $main_photo,
					'real_estate_photos' => json_encode($uploaded_files),
					'real_estate_offer_type' => $this->input->post('offer_type'),
					'id_location' => $id_location,
					'id_location_aditional' => implode(',', $aditional_locations),
					'real_estate_address' => $this->input->post('address'),
					'real_estate_lat' => (float)$this->input->post('latitude'),
					'real_estate_lng' => (float)$this->input->post('longitude'),
					'real_estate_icons' => json_encode($filtered_icons),
					'real_estate_exclusive' => (int)$this->input->post('real_estate_exclusive'),
					'real_estate_date_created' => $this->real_estate->db_date()
				);

				$id_real_estate = $this->real_estate->handler_insert($insert);

				if($category['category_has_icons'] == 1 && is_array($this->input->post('icons'))){
					$category_icons = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');

					$icons = array_filter($this->input->post('icons'));
					foreach ($icons as $key => $icon) {
						if (!array_key_exists($key, $category_icons)) {
							continue;
						}

						if(!isset($icon['is_checked'])){
							continue;
						}

						$filtered_icons[$key] = array(
							'id_icon' => $key,
							'id_real_estate' => $id_real_estate,
							'icon_name_ro' => (empty($icon['name_ro']))?$category_icons[$key]['icon_name_ro']:$icon['name_ro'],
							'icon_name_ru' => (empty($icon['name_ru']))?$category_icons[$key]['icon_name_ru']:$icon['name_ru'],
							'icon_name_en' => (empty($icon['name_en']))?$category_icons[$key]['icon_name_en']:$icon['name_en']
						);
					}
					
					if(!empty($filtered_icons)){
						$this->real_estate->handler_insert_icons_relation($filtered_icons);
					}
				}

				$path = 'files/real_estate/'.$id_real_estate;
				create_dir($path);
				foreach ($uploaded_files as $uploaded_file) {
					copy('files/real_estate/temp/'.$uploaded_file, $path.'/'.$uploaded_file);
					copy('files/real_estate/temp/thumb_'.$uploaded_file, $path.'/thumb_'.$uploaded_file);
					compress_image($path.'/'.$uploaded_file);
					compress_image($path.'/thumb_'.$uploaded_file);
					@unlink('files/real_estate/temp/'.$uploaded_file);
					@unlink('files/real_estate/temp/thumb_'.$uploaded_file);
				}
				
				$post_properties = $this->input->post('properties');
				$properties_on_item = array();
				$real_estate_last_floor = 0;
				if(!empty($post_properties) && is_array($post_properties)){
					$params = array(
						'id_category' => $id_category,
						'property_type' => array('multiselect','range')
					);
					$properties = $this->real_estate->handler_get_properties($params);					
					
					$insert_properties = array();
					foreach ($properties as $property) {
						if(!array_key_exists($property['id_property'], $post_properties)){
							continue;
						}
						
						$insert_properties[] = array(
							'id_real_estate' => $id_real_estate,
							'id_property' => $property['id_property'],
							'property_value' => $post_properties[$property['id_property']]['value'],
							'property_value_max' => (isset($post_properties[$property['id_property']]['value_max']))?$post_properties[$property['id_property']]['value_max']:'',
							'property_value_sufix' => (isset($post_properties[$property['id_property']]['sufix']))?1:0
						);

						if($property['property_alias'] == 'floor' && isset($post_properties[$property['id_property']]['value_max']) && $post_properties[$property['id_property']]['value'] == $post_properties[$property['id_property']]['value_max']){
							$real_estate_last_floor = 1;
						}
					}
					
					$this->real_estate->handler_insert_real_estate_properties_values($insert_properties);
				}

				$config = array(
					'table' => $this->real_estate->real_estate,
					'id' => 'id_real_estate',
					'field' => 'url_ro',
					'title' => 'real_estate_title_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ro' => $this->slug->create_slug(cut_str($insert['real_estate_title_ro'], 200).'-'.$id_real_estate),
					'url_ru' => $this->slug->create_slug(cut_str($insert['real_estate_title_ru'], 200).'-'.$id_real_estate),
					'url_en' => $this->slug->create_slug(cut_str($insert['real_estate_title_en'], 200).'-'.$id_real_estate),
					'real_estate_search_info' => $insert['real_estate_full_title_ro'].' '.$insert['real_estate_full_title_ru'].' '.$insert['real_estate_full_title_en'].' '.$insert['real_estate_title_ro'].' '.$insert['real_estate_title_ru'].' '.$insert['real_estate_title_en'].' '.orderNumberOnly($id_real_estate),
					'real_estate_last_floor' => $real_estate_last_floor,
					'real_estate_icons' => json_encode($filtered_icons)
				);
				$this->real_estate->handler_update($id_real_estate, $update);
				Modules::run('real_estate/update_search_info', array('id_real_estate' => $id_real_estate));

				if ($this->input->post('advert_api')) {
					$this->_add_advert($id_real_estate);
				}

				jsonResponse('Объект добавлен.', 'success', array('insert' => $insert));
			break;
			// DONE
			case 'edit':
				if ($this->lauth->have_right('manage_real_estate_manager')) {
					$this->form_validation->set_rules('manager', 'Менеджер', 'required|xss_clean');
				}
				
				$this->form_validation->set_rules('id_real_estate', 'Объект', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Краткое название RO', 'required|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Краткое название RU', 'max_length[250]');
				$this->form_validation->set_rules('title_en', 'Краткое название En', 'max_length[250]');
				$this->form_validation->set_rules('full_title_ro', 'Полное название RO', 'required|max_length[250]');
				$this->form_validation->set_rules('full_title_ru', 'Полное название RU', 'max_length[250]');
				$this->form_validation->set_rules('full_title_en', 'Полное название En', 'max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
				if ($this->lauth->is_admin_only()) {
					$this->form_validation->set_rules('owner_name', 'Имя владельца', 'required|xss_clean|max_length[250]');
					$this->form_validation->set_rules('owner_phone', 'Телефон владельца', 'required|xss_clean|max_length[250]');
					$this->form_validation->set_rules('owner_email', 'Email владельца', 'xss_clean|max_length[250]');
					$this->form_validation->set_rules('owner_address', 'Адрес владельца', 'xss_clean|max_length[250]');
				}
				$this->form_validation->set_rules('category', 'Тип недвижимости', 'required|xss_clean');
				$this->form_validation->set_rules('offer_type', 'Тип предложения', 'required|xss_clean');
				$this->form_validation->set_rules('price', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('address', 'Адрес', 'xss_clean');

				// 999.md FIELDS
				if ($this->input->post('advert_api')) {
					$this->form_validation->set_rules('advert_status', 'Статус на 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('region_999', 'Регион 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('raion_999', 'Населенный пункт 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('sector_999', 'Сектор 999.md', 'required|xss_clean');
					$this->form_validation->set_rules('title_999', 'Заголовок объявления 999.md', 'required|max_length[250]');
					$this->form_validation->set_rules('text_999', 'Текст 999.md', 'required|max_length[2000]');
				}

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}

				$id_real_estate = (int)$this->input->post('id_real_estate');
				$real_estate = $this->real_estate->handler_get($id_real_estate);
				if(empty($real_estate)){
					jsonResponse('Ошибка: Объект не найден.');
				}
				
				$uploaded_files = $this->input->post('uploaded_files');
				if(empty($uploaded_files)){
					jsonResponse('Ошибка: Нужно добавить как минимум одну фотографию.');
				}
				
				$main_photo = $this->input->post('main_photo');
				if(empty($main_photo) || !in_array($main_photo, $uploaded_files)){
					jsonResponse('Ошибка: Нужно выбрать главную фотографию.');
				}

				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$id_location = (int)$this->input->post('location_child');
				$location = $this->locations->handler_get_full($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Нужно выбрать местоположение.');
				}

				$id_location_aditional = $this->input->post('location_child_additional');
				$aditional_locations = array();
				if(!empty($id_location_aditional)){
					$id_location_aditional = array_filter($id_location_aditional);
					if(!empty($id_location_aditional)){
						$locations_aditional = $this->locations->handler_get_all(array('id_location' => $id_location_aditional));
						if(!empty($locations_aditional)){
							foreach ($locations_aditional as $location_aditional) {
								$aditional_locations[] = $location_aditional['id_location'];
							}
						}
					}
				}
				
				if (!($this->input->post('latitude') && $this->input->post('longitude'))) {
					jsonResponse('Ошибка: Нужно выбрать местоположение на карте.');
				}

				$discount = $this->input->post('discount');
				$discount_type = 'no';
				$discount_value = 0;
				if(!empty($discount)){
					switch ($discount['type']) {
						case 'simple':
							$discount_type = 'simple';
						break;
						case 'new_price':
							$discount_type = 'new_price';
							$discount_value = (int) $discount['value'];
						break;
						case 'by_m2':
							$discount_type = 'by_m2';
							$discount_value = (int) $discount['value'];
						break;
					}
				}

				$path = 'files/real_estate/'.$id_real_estate;
				create_dir($path);

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/real_estate/temp/'.$remove_photo);
						@unlink('files/real_estate/temp/thumb_'.$remove_photo);
					}
				}

				$remove_existent_photos = $this->input->post('remove_existent_photos');
				if(!empty($remove_existent_photos)){
					foreach($remove_existent_photos as $remove_existent_photo){
						@unlink($path.'/'.$remove_existent_photo);
						@unlink($path.'/thumb_'.$remove_existent_photo);
					}
				}

				$filtered_icons = array();
				$this->real_estate->handler_delete_icons_relation(array('id_real_estate' => $id_real_estate));
				if($category['category_has_icons'] == 1 && is_array($this->input->post('icons'))){
					$category_icons = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');

					$icons = array_filter($this->input->post('icons'));
					foreach ($icons as $key => $icon) {
						if (!array_key_exists($key, $category_icons)) {
							continue;
						}

						if(!isset($icon['is_checked'])){
							continue;
						}

						$filtered_icons[$key] = array(
							'id_icon' => $key,
							'id_real_estate' => $id_real_estate,
							'icon_name_ro' => (empty($icon['name_ro']))?$category_icons[$key]['icon_name_ro']:$icon['name_ro'],
							'icon_name_ru' => (empty($icon['name_ru']))?$category_icons[$key]['icon_name_ru']:$icon['name_ru'],
							'icon_name_en' => (empty($icon['name_en']))?$category_icons[$key]['icon_name_en']:$icon['name_en']
						);
					}
					
					if(!empty($filtered_icons)){
						$this->real_estate->handler_insert_icons_relation($filtered_icons);
					}
				}

				$real_estate_photos = json_decode($real_estate['real_estate_photos'], true);
				$filtered_photos = array();
				foreach ($uploaded_files as $uploaded_file) {
					$filtered_photos[] = $uploaded_file;

					if(!in_array($uploaded_file, $real_estate_photos)){
						copy('files/real_estate/temp/'.$uploaded_file, $path.'/'.$uploaded_file);
						copy('files/real_estate/temp/thumb_'.$uploaded_file, $path.'/thumb_'.$uploaded_file);
						compress_image($path.'/'.$uploaded_file);
						compress_image($path.'/thumb_'.$uploaded_file);
						@unlink('files/real_estate/temp/'.$uploaded_file);
						@unlink('files/real_estate/temp/thumb_'.$uploaded_file);
					}
				}

				foreach ($filtered_photos as $key => $filtered_photo) {
					compress_image($path.'/'.$filtered_photo);
					compress_image($path.'/thumb_'.$filtered_photo);
				}

				$post_properties = $this->input->post('properties');
				$real_estate_last_floor = 0;
				$this->real_estate->handler_delete_real_estate_properties_values($id_real_estate);
				if(!empty($post_properties) && is_array($post_properties)){
					$properties_params = array(
						'id_category' => $id_category,
						'property_type' => array('multiselect','range')
					);
					$properties = $this->real_estate->handler_get_properties($properties_params);					
					
					$insert_properties = array();
					foreach ($properties as $property) {
						if(!array_key_exists($property['id_property'], $post_properties)){
							continue;
						}

						if($post_properties[$property['id_property']]['value'] == ''){
							continue;
						}
						
						$insert_properties[] = array(
							'id_real_estate' => $id_real_estate,
							'id_property' => $property['id_property'],
							'property_value' => $post_properties[$property['id_property']]['value'],
							'property_value_max' => (isset($post_properties[$property['id_property']]['value_max']))?$post_properties[$property['id_property']]['value_max']:'',
							'property_value_sufix' => (isset($post_properties[$property['id_property']]['sufix']))?1:0
						);

						if($property['property_alias'] == 'floor' && isset($post_properties[$property['id_property']]['value_max']) && $post_properties[$property['id_property']]['value'] == $post_properties[$property['id_property']]['value_max']){
							$real_estate_last_floor = 1;
						}
					}
					
					$this->real_estate->handler_insert_real_estate_properties_values($insert_properties);
				}

				$config = array(
					'table' => $this->real_estate->real_estate,
					'id' => 'id_real_estate',
					'field' => 'url_ro',
					'title' => 'real_estate_title_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'id_category' => $id_category,
					'id_region_999' => (int)$this->input->post('region_999'),
					'id_raion_999' => (int)$this->input->post('raion_999'),
					'id_sector_999' => $this->input->post('sector_999'),
					'real_estate_title_999' => $this->input->post('title_999'),
					'real_estate_text_999' => $this->input->post('text_999'),
					'advert_status_999' => $this->input->post('advert_status'),
					'advert_on_999' => ($this->input->post('advert_api'))?1:0,
					'id_manager' => $real_estate['id_manager'],
					'id_manager_created' => ($real_estate['id_manager_created'] == 0)?$this->lauth->id_user():$real_estate['id_manager_created'],
					'real_estate_title_ro' => $this->input->post('title_ro'),
					'real_estate_title_ru' => $this->input->post('title_ru'),
					'real_estate_title_en' => $this->input->post('title_en'),
					'real_estate_full_title_ro' => $this->input->post('full_title_ro'),
					'real_estate_full_title_ru' => $this->input->post('full_title_ru'),
					'real_estate_full_title_en' => $this->input->post('full_title_en'),
					'real_estate_text_ro' => $this->input->post('description_ro'),
					'real_estate_text_ru' => $this->input->post('description_ru'),
					'real_estate_text_en' => $this->input->post('description_en'),
					'real_estate_text_clean_ro' => strip_tags_with_whitespace($this->input->post('description_ro')),
					'real_estate_text_clean_ru' => strip_tags_with_whitespace($this->input->post('description_ru')),
					'real_estate_text_clean_en' => strip_tags_with_whitespace($this->input->post('description_en')),
					'mt_ro' => $this->input->post('full_title_ro'),
					'mt_ru' => $this->input->post('full_title_ru'),
					'mt_en' => $this->input->post('full_title_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'real_estate_price' => (float) $this->input->post('price'),
					'real_estate_discount_type' => $discount_type,
					'real_estate_discount_value' => $discount_value,
					'real_estate_photo' => $main_photo,
					'real_estate_photos' => json_encode($filtered_photos),
					'real_estate_offer_type' => $this->input->post('offer_type'),
					'id_location' => $id_location,
					'id_location_aditional' => implode(',', $aditional_locations),
					'real_estate_address' => $this->input->post('address'),
					'real_estate_lat' => (float)$this->input->post('latitude'),
					'real_estate_lng' => (float)$this->input->post('longitude'),
					'real_estate_icons' => json_encode($filtered_icons),
					'url_ro' => $this->slug->create_slug(cut_str($this->input->post('title_ro', true), 200).'-'.$id_real_estate),
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru', true), 200).'-'.$id_real_estate),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en', true), 200).'-'.$id_real_estate),
					'real_estate_last_floor' => $real_estate_last_floor,
					'real_estate_exclusive' => (int)$this->input->post('real_estate_exclusive'),
					'real_estate_search_info' => $this->input->post('full_title_ro', true).' '.$this->input->post('full_title_ru', true).' '.$this->input->post('full_title_en', true).' '.$this->input->post('title_ro', true).' '.$this->input->post('title_ru', true).' '.$this->input->post('title_en', true).' '.orderNumberOnly($id_real_estate)
				);

				if ($this->lauth->is_admin_only()) {
					$update['real_estate_owner_name'] = (int)$this->input->post('owner_name');
					$update['real_estate_owner_phone'] = (int)$this->input->post('owner_phone');
					$update['real_estate_owner_email'] = (int)$this->input->post('owner_email');
					$update['real_estate_owner_address'] = (int)$this->input->post('owner_address');
				}
				
				if($this->lauth->have_right('manage_real_estate_manager')){
					$update['id_manager'] = (int)$this->input->post('manager');
				}

				if($real_estate['id_category'] != $update['id_category'] || $real_estate['id_manager'] != $update['id_manager']){
					$this->real_estate->handler_update($id_real_estate, array('advert_status_999' => 'private'));
					
					if($real_estate['id_999'] > 0){
						$this->_advert_status($id_real_estate);
						$update['id_999'] = 0;
					}
				}

				$this->real_estate->handler_update($id_real_estate, $update);
				Modules::run('real_estate/update_search_info', array('id_real_estate' => $id_real_estate));

				if ($this->input->post('advert_api')) {
					$this->_add_advert($id_real_estate);
				}

				jsonResponse('Данные сохранены.', 'success', array('update' => $update));
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 're.id_real_estate-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = 're.real_estate_date_created-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 're.real_estate_title_ru-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_price': $params['sort_by'][] = 're.real_estate_price-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
				
				if($this->input->post('category')){
					$params['id_category'] = (int)$this->input->post('category');
				}
				
				if($this->input->post('manager')){
					$params['id_manager'] = (int)$this->input->post('manager');
				}
				
				if($this->input->post('manager_created')){
					$params['id_manager_created'] = (int)$this->input->post('manager_created');
				}

				$locations = array();
				if($this->input->post('locations_1')){
					$locations = array_merge($locations, array_filter(explode(',', $this->input->post('locations_1'))));
				}

				if($this->input->post('locations_2')){
					$locations = array_merge($locations, array_filter(explode(',', $this->input->post('locations_2'))));
				}

				if(!empty($locations)){
					$params['id_location'] = $locations;
				}
				
				if(isset($_POST['show_on_home'])){
					$params['show_on_home'] = (int)$this->input->post('show_on_home');
				}
				
				if(isset($_POST['best'])){
					$params['best'] = (int)$this->input->post('best');
				}
				
				if(isset($_POST['mortgage'])){
					$params['mortgage'] = (int)$this->input->post('mortgage');
				}
				
				if(isset($_POST['re_active'])){
					$params['real_estate_active'] = (int)$this->input->post('re_active');
					$params['sort_by'][] = 're.real_estate_active-DESC';
					$params['sort_by'][] = 're.real_estate_active_date-DESC';
				}
				
				if($this->input->post('offer_type')){
					$params['offer_type'] = $this->input->post('offer_type');
				}

				if ($this->input->post('keywords')) {
					$params['keywords'] = $this->input->post('keywords', true);
				}
				
				if($this->input->post('price_from')){
					$params['price_from'] = (int)$this->input->post('price_from');
				}

				if ($this->input->post('price_to')) {
					$params['price_to'] = (int)$this->input->post('price_to');
				}

				if($this->input->post('discount_type')){
					$discount_type = $this->input->post('discount_type', true);
					switch ($discount_type) {
						case 'all':
							$params['discount_type'] = array('simple', 'new_price', 'by_m2');
						break;
						case 'no':
						case 'simple':
						case 'new_price':
						case 'by_m2':
							$params['discount_type'] = $discount_type;
						break;
					}
				}

				$records = $this->real_estate->handler_get_all($params);
				$records_total = $this->real_estate->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-href="'.base_url('admin/real_estate/popup_forms/change_status/'.$record['id_real_estate']).'" title="Сделать объект неактивным" class="btn btn-success btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-eye"></i></a>';
					if($record['real_estate_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать объект активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-real-estate="'.$record['id_real_estate'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$options = array();
					if ($record['real_estate_offer_type'] == 'sale') {
						if($record['real_estate_mortgage'] == 0){
							$options[] = '<span class="btn btn-default btn-xs confirm-dialog" data-message="Вы действительно хотите включить признак Показать сумму ипотеки в месяц?" data-callback="change_marketing" data-marketing="mortgage" data-real-estate="'.$record['id_real_estate'].'" data-title="Включить признак Показать сумму ипотеки в месяц"><i class="fa fa-eur"></i></span>';
						} else{
							$options[] = '<span class="btn btn-warning btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Показать сумму ипотеки в месяц?" data-callback="change_marketing" data-marketing="mortgage" data-real-estate="'.$record['id_real_estate'].'" data-title="Убрать признак Показать сумму ипотеки в месяц"><i class="fa fa-eur"></i></span>';
						}
					}

					if($record['real_estate_discount_type'] != 'no'){
						if($record['real_estate_show_on_home'] == 0){
							$options[] = '<span class="btn btn-default btn-xs confirm-dialog" data-message="Вы действительно хотите включить признак Показать на главной?" data-callback="change_marketing" data-marketing="show_on_home" data-real-estate="'.$record['id_real_estate'].'" data-title="Включить признак Показать на главной"><i class="fa fa-home"></i></span>';
						} else{
							$options[] = '<span class="btn btn-success btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Показать на главной?" data-callback="change_marketing" data-marketing="show_on_home" data-real-estate="'.$record['id_real_estate'].'" data-title="Убрать признак Показать на главной"><i class="fa fa-home"></i></span>';
						}
					}

					if($record['real_estate_best'] == 0){
						$options[] = '<span class="btn btn-default btn-xs confirm-dialog" data-message="Вы действительно хотите включить признак Самый лучший объект?" data-callback="change_marketing" data-marketing="best" data-real-estate="'.$record['id_real_estate'].'" data-title="Включить признак Самый лучший объект"><i class="fa fa-star"></i></span>';
					} else{
						$options[] = '<span class="btn btn-primary btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Самый лучший объект?" data-callback="change_marketing" data-marketing="best" data-real-estate="'.$record['id_real_estate'].'" data-title="Убрать признак Самый лучший объект"><i class="fa fa-star"></i></span>';
					}

					if($record['real_estate_reserved'] == 0){
						$options[] = '<span class="btn btn-default btn-xs confirm-dialog" data-message="Вы действительно хотите включить признак Зарезервированный?" data-callback="change_marketing" data-marketing="reserved" data-real-estate="'.$record['id_real_estate'].'" data-title="Включить признак Зарезервированный"><i class="fai fai-booked"></i></span>';
					} else{
						$options[] = '<span class="btn btn-warning btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Зарезервированный?" data-callback="change_marketing" data-marketing="reserved" data-real-estate="'.$record['id_real_estate'].'" data-title="Убрать признак Зарезервированный"><i class="fai fai-booked"></i></span>';
					}

					$sold_status = '';
					if ($record['real_estate_offer_type'] == 'sale') {
						if($record['real_estate_sold'] == 0){
							$sold_status = '<a href="#" data-href="'.base_url('admin/real_estate/popup_forms/change_sold/'.$record['id_real_estate']).'" title="Включить признак Продан" class="btn btn-default btn-xs call-popup" data-popup="#general_popup_form"><i class="fai fai-sold"></i></a>';
						} else{
							$sold_status = '<span class="btn btn-info btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Продан?" data-callback="change_sold" data-real-estate="'.$record['id_real_estate'].'" data-title="Убрать признак Продан"><i class="fai fai-sold"></i></span> ';
						}
					}

					$delete_btn = '';
					if ($this->lauth->have_right('delete_real_estate')) {
						$delete_btn = '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-title="Удалить" data-message="Вы уверены что хотите удалить объект?" data-callback="delete_action" data-real-estate="'.$record['id_real_estate'].'"><i class="fa fa-remove"></i></a>';
					}

					$_record_price = getPrice($record, true);
					$output['aaData'][] = array(
						'dt_id'			=> orderNumberOnly($record['id_real_estate']),
						'dt_options'	=> implode(' ', $options),
						'dt_price'		=> formatNumber($record['real_estate_price']),
						'dt_photo'		=> '<img src="'.base_url(getImage('files/real_estate/'.$record['id_real_estate'].'/thumb_'.$record['real_estate_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=> '<a href="'.base_url('admin/real_estate/edit/'.$record['id_real_estate']).'" target="_blank">'.$record['real_estate_full_title_ro'].'</a>',
						'dt_date'		=> formatDate($record['real_estate_date_created'], 'd.m.Y H:i:s'),
						'dt_actions'	=> $sold_status.$status
										   .' <a href="'.base_url('admin/real_estate/edit/'.$record['id_real_estate']).'" data-title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a> ' 
										   .$delete_btn
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/real_estate/temp';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '30000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				
				$this->load->library('image_lib');
				$image_info = getimagesize($path.'/'.$data['file_name']);
				if($image_info[0] > 1200 && $image_info[1] > 768 || $image_info[0] > 768 && $image_info[1] > 1200){
					$config = array(
						'source_image'      => $path.'/'.$data['file_name'],
						'create_thumb'   	=> FALSE,
						'maintain_ratio'   	=> TRUE
					);

					if($image_info[0] > $image_info[1]){
						$config['width'] = 1200;
					} else{
						$config['height'] = 768;
					}

					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}

				if($this->lsettings->item('upload_photo_filters')){
					$config = array(
						'source_image' => $path.'/'.$data['file_name']
					);
					$this->image_lib->initialize($config);
					$this->image_lib->brightnes();
					$this->image_lib->contrast();
				}

				if ($this->lsettings->item('upload_photo_watermark') == 1) {
					$config = array(
						'source_image'      => $path.'/'.$data['file_name'],
						'wm_type'   		=> 'overlay',
						'wm_overlay_path'   => 'files/watermark/watermark.png'
					);
					$this->image_lib->initialize($config);
					$this->image_lib->watermark();
					$this->image_lib->clear();
				}

				$config = array(
					'source_image'      => $path.'/'.$data['file_name'],
					'create_thumb'   	=> TRUE,
					'maintain_ratio'   	=> TRUE,
					'thumb_marker'		=> 'thumb_',
					'quality' 			=> 70
				);
				
				if($image_info[0] > $image_info[1]){
					$config['height'] = 370;
				} else{
					$config['width'] = 370;
				}

				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				$config = array(
					'source_image'      => $path.'/'.$data['file_name'],
					'create_thumb'   	=> TRUE,
					'maintain_ratio'   	=> TRUE,
					'thumb_marker'		=> 'thumb_180_',
					'quality' 			=> 70,
					'height'			=> 180
				);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				$config = array(
					'source_image'      => $path.'/'.$data['file_name'],
					'create_thumb'   	=> TRUE,
					'maintain_ratio'   	=> TRUE,
					'thumb_marker'		=> 'thumb_220_',
					'quality' 			=> 70,
					'height'			=> 220
				);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				compress_image($path.'/'.$data['file_name']);
				compress_image($path.'/thumb_'.$data['file_name']);
				compress_image($path.'/thumb_180_'.$data['file_name']);
				compress_image($path.'/thumb_220_'.$data['file_name']);

				$file = new StdClass;
				$file->filename = $data['file_name'];
				jsonResponse('', 'success', array("file" => $file, 'wconfig' =>$config, 'upload_data' => $data));
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('real_estate', 'Объект недвижимости', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_real_estate = (int)$this->input->post('real_estate');
				$real_estate = $this->real_estate->handler_get($id_real_estate);
				if(empty($real_estate)){
					jsonResponse('Объект недвижимости не существует.');
				}

				$this->real_estate->handler_update($id_real_estate, array('advert_status_999' => 'private'));
				$this->_advert_status($id_real_estate);

				remove_dir("files/real_estate/{$id_real_estate}");

				$_same_params = Modules::run('real_estate/_get_same_params', $id_real_estate);
				$insert_deleted = array(
					'id_real_estate' => $id_real_estate, 
					'full_url_ro' => $real_estate['full_url_ro'], 
					'full_url_ru' => $real_estate['full_url_ru'], 
					'full_url_en' => $real_estate['full_url_en'], 
					'real_estate_same_params' => json_encode($_same_params)
				);
				$this->real_estate->handler_insert_deleted($insert_deleted);

				$this->real_estate->handler_delete($id_real_estate);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$id_real_estate = (int)$this->input->post('real_estate');
				$real_estate = $this->real_estate->handler_get($id_real_estate);
				if(empty($real_estate)){
					jsonResponse('Объект недвижимости не существует.');
				}

				$update = array();
				if($real_estate['real_estate_active']){				
					$this->form_validation->set_rules('reason', 'Укажите причину', 'required|xss_clean');
					if ($this->form_validation->run() == false){
						jsonResponse($this->form_validation->error_array());
					}

					$update['real_estate_active_date'] = $this->real_estate->db_date();
					$update['real_estate_not_active_reason'] = $this->input->post('reason', true);
					$update['real_estate_active'] = 0;
					$update['advert_status_999'] = 'private';
				} else{
					$update['real_estate_active_date'] = NULL;
					$update['real_estate_not_active_reason'] = NULL;
					$update['real_estate_active'] = 1;
					$update['advert_status_999'] = 'public';
				}

				$this->real_estate->handler_update($id_real_estate, $update);
				$this->_advert_status($id_real_estate);
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $update['real_estate_active']));
			break;
			// DONE
			case 'change_sold':
				$id_real_estate = (int)$this->input->post('real_estate');
				$real_estate = $this->real_estate->handler_get($id_real_estate);
				if(empty($real_estate)){
					jsonResponse('Объект недвижимости не существует.');
				}

				$update = array();
				if($real_estate['real_estate_sold'] == 1){
					$update = array(
						'real_estate_sold' => 0,
						'real_estate_sold_date' => '0000-00-00 00:00:00',
						'real_estate_active_date' => NULL,
						'real_estate_not_active_reason' => NULL,
						'real_estate_active' => 1,
						'advert_status_999' => 'public'
					);
				} else{			
					$this->form_validation->set_rules('reason', 'Укажите причину', 'required|xss_clean');
					if ($this->form_validation->run() == false){
						jsonResponse($this->form_validation->error_array());
					}

					$update = array(
						'real_estate_sold' => 1,
						'real_estate_sold_date' => date('Y-m-d H:i:s'),
						'real_estate_not_active_reason' => $this->input->post('reason', true)
					);
				}

				$this->real_estate->handler_update($id_real_estate, $update);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_marketing':
				$this->form_validation->set_rules('real_estate', 'Объект недвижимости', 'required|xss_clean');
				$this->form_validation->set_rules('marketing', 'Опция', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_real_estate = (int)$this->input->post('real_estate');
				$marketing = $this->input->post('marketing', true);
				$real_estate = $this->real_estate->handler_get($id_real_estate);
				if(empty($real_estate)){
					jsonResponse('Объект недвижимости не существует.');
				}

				if(!isset($real_estate['real_estate_'.$marketing])){
					jsonResponse('Данные не верны.');
				}

				if($real_estate['real_estate_'.$marketing] == 1){
					$status = 0;
				} else{
					$status = 1;
				}

				switch ($marketing) {
					case 'best':
						$this->real_estate->handler_update_all(array('real_estate_best' => 0), array('real_estate_offer_type' => $real_estate['real_estate_offer_type']));
					break;
				}
				
				$this->real_estate->handler_update($id_real_estate, array('real_estate_'.$marketing => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'add_category':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_ro', 'Название, в единственном числе RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_ru', 'Название, в единственном числе RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_en', 'Название, в единственном числе En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('h1_ro', 'H1 для каталога RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('h1_ru', 'H1 для каталога RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('h1_en', 'H1 для каталога En', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_ro', 'Ссылка RO', 'trim|required|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_ro]');
				$this->form_validation->set_rules('url_ru', 'Ссылка RU', 'trim|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_ru]');
				$this->form_validation->set_rules('url_en', 'Ссылка En', 'trim|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_en]');
				$this->form_validation->set_rules('category_icon', 'Иконка', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('category_type', 'Тип', 'trim|required|xss_clean');
				$this->form_validation->set_rules('category_weight', 'Порядковый номер', 'trim|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_category_999 = (int) $this->input->post('category_999');
				if($id_category_999 > 0 && !array_key_exists($id_category_999, $this->real_estate->api_categories)){
					jsonResponse('Не верный идентификатор категории от 999.md');
				}

				$id_subcategory_999 = (int) $this->input->post('subcategory_999');
				if($id_subcategory_999 > 0 && !array_key_exists($id_subcategory_999, $this->real_estate->api_subcategories)){
					jsonResponse('Не верный идентификатор подкатегории от 999.md');
				}

				$id_offer_type_999 = (int) $this->input->post('offer_type_999');
				if($id_offer_type_999 > 0 && !array_key_exists($id_offer_type_999, $this->real_estate->api_offers_type)){
					jsonResponse('Не верный идентификатор Тип предложения от 999.md');
				}

				$insert = array(
					'id_category_999' => $id_category_999,
					'id_subcategory_999' => $id_subcategory_999,
					'id_offer_type_999' => $id_offer_type_999,
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'category_title_single_ro' => $this->input->post('title_single_ro'),
					'category_title_single_ru' => $this->input->post('title_single_ru'),
					'category_title_single_en' => $this->input->post('title_single_en'),
					'category_h1_ro' => $this->input->post('h1_ro'),
					'category_h1_ru' => $this->input->post('h1_ru'),
					'category_h1_en' => $this->input->post('h1_en'),
					'category_image' => (file_exists('files/'.$this->input->post('category_image')))?$this->input->post('category_image'):'',
					'category_text_counter_ro' => $this->input->post('counter_ro'),
					'category_text_counter_ru' => $this->input->post('counter_ru'),
					'category_text_counter_en' => $this->input->post('counter_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'seo_ro' => $this->input->post('seo_ro'),
					'seo_ru' => $this->input->post('seo_ru'),
					'seo_en' => $this->input->post('seo_en'),
					'category_icon' => $this->input->post('category_icon'),
					'category_type' => $this->input->post('category_type'),
					'category_weight' => (int)$this->input->post('category_weight'),
					'category_active' => ($this->input->post('category_active'))?1:0,
					'category_has_icons' => ($this->input->post('category_has_icons'))?1:0,
					'url_ro' => $this->input->post('url_ro'),
					'url_ru' => $this->input->post('url_ru'),
					'url_en' => $this->input->post('url_en')
				);

				$category_menu = $this->input->post('category_menu');
				$filtered_menu = array();
				if (!empty($category_menu['url'])) {
					foreach ($category_menu['url'] as $key => $category_menu_url) {
						if(empty($category_menu_url) || empty($category_menu['name_ro'][$key])){
							continue;
						}

						$filtered_menu[] = array(
							'url' => $category_menu_url,
							'name_ro' => $category_menu['name_ro'][$key],
							'name_ru' => (!empty($category_menu['name_ru'][$key]))?$category_menu['name_ru'][$key]:$category_menu['name_ro'][$key],
							'name_en' => (!empty($category_menu['name_en'][$key]))?$category_menu['name_en'][$key]:$category_menu['name_ro'][$key]
						);
					}
				}

				$insert['category_menu'] = json_encode($filtered_menu);

				$id_category = $this->real_estate->handler_insert_category($insert);

				$this->_setup_categories_routers();
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_category':
				$id_category = (int)$this->input->post('id_category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_ro', 'Название, в единственном числе RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_ru', 'Название, в единственном числе RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_single_en', 'Название, в единственном числе En', 'trim|xss_clean|max_length[250]');
								
				if($category['url_ro'] != trim($this->input->post('url_ro'))){
					$this->form_validation->set_rules('url_ro', 'Ссылка RO', 'trim|required|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_ro]');
				} else{
					$this->form_validation->set_rules('url_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				}

				if($category['url_ru'] != trim($this->input->post('url_ru'))){
					$this->form_validation->set_rules('url_ru', 'Ссылка RU', 'trim|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_ru]');
				} else{
					$this->form_validation->set_rules('url_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				}

				if($category['url_en'] != trim($this->input->post('url_en'))){
					$this->form_validation->set_rules('url_en', 'Ссылка En', 'trim|xss_clean|max_length[250]|alpha_dash|is_unique['.$this->real_estate->real_estate_categories.'.url_en]');
				} else{
					$this->form_validation->set_rules('url_en', 'Название En', 'trim|required|xss_clean|max_length[250]');
				}

				$this->form_validation->set_rules('h1_ro', 'H1 для каталога RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('h1_ru', 'H1 для каталога RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('h1_en', 'H1 для каталога En', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('category_icon', 'Иконка', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('category_type', 'Тип', 'trim|required|xss_clean');
				$this->form_validation->set_rules('category_weight', 'Порядковый номер', 'trim|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$id_category_999 = (int) $this->input->post('category_999');
				if($id_category_999 > 0 && !array_key_exists($id_category_999, $this->real_estate->api_categories)){
					jsonResponse('Не верный идентификатор категории от 999.md');
				}

				$id_subcategory_999 = (int) $this->input->post('subcategory_999');
				if($id_subcategory_999 > 0 && !array_key_exists($id_subcategory_999, $this->real_estate->api_subcategories)){
					jsonResponse('Не верный идентификатор подкатегории от 999.md');
				}

				$id_offer_type_999 = (int) $this->input->post('offer_type_999');
				if($id_offer_type_999 > 0 && !array_key_exists($id_offer_type_999, $this->real_estate->api_offers_type)){
					jsonResponse('Не верный идентификатор Тип предложения от 999.md');
				}

				$update = array(
					'id_category_999' => $id_category_999,
					'id_subcategory_999' => $id_subcategory_999,
					'id_offer_type_999' => $id_offer_type_999,
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'category_title_single_ro' => $this->input->post('title_single_ro'),
					'category_title_single_ru' => $this->input->post('title_single_ru'),
					'category_title_single_en' => $this->input->post('title_single_en'),
					'category_h1_ro' => $this->input->post('h1_ro'),
					'category_h1_ru' => $this->input->post('h1_ru'),
					'category_h1_en' => $this->input->post('h1_en'),
					'category_image' => (file_exists('files/'.$this->input->post('category_image')))?$this->input->post('category_image'):'',
					'category_text_counter_ro' => $this->input->post('counter_ro'),
					'category_text_counter_ru' => $this->input->post('counter_ru'),
					'category_text_counter_en' => $this->input->post('counter_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'seo_ro' => $this->input->post('seo_ro'),
					'seo_ru' => $this->input->post('seo_ru'),
					'seo_en' => $this->input->post('seo_en'),
					'category_icon' => $this->input->post('category_icon'),
					'category_type' => $this->input->post('category_type'),
					'category_weight' => (int)$this->input->post('category_weight'),
					'category_active' => ($this->input->post('category_active'))?1:0,
					'url_ro' => $this->input->post('url_ro'),
					'url_ru' => $this->input->post('url_ru'),
					'url_en' => $this->input->post('url_en'),
					'category_has_icons' => ($this->input->post('category_has_icons'))?1:0
				);

				$category_menu = $this->input->post('category_menu');
				$filtered_menu = array();
				if (!empty($category_menu['url'])) {
					foreach ($category_menu['url'] as $key => $category_menu_url) {
						if(empty($category_menu_url) || empty($category_menu['name_ro'][$key])){
							continue;
						}

						$filtered_menu[] = array(
							'url' => $category_menu_url,
							'name_ro' => $category_menu['name_ro'][$key],
							'name_ru' => (!empty($category_menu['name_ru'][$key]))?$category_menu['name_ru'][$key]:$category_menu['name_ro'][$key],
							'name_en' => (!empty($category_menu['name_en'][$key]))?$category_menu['name_en'][$key]:$category_menu['name_ro'][$key]
						);
					}
				}
				$update['category_menu'] = json_encode($filtered_menu);

				$this->real_estate->handler_update_category($id_category, $update);

				$this->_setup_categories_routers();
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'get_api_subcategories':
				$api_subcategories = $this->real_estate->api_subcategories;
				jsonResponse('', 'success', array('subcategories' => $api_subcategories, 'subcategories_count' => count($api_subcategories)));
			break;
			// DONE
			case 'get_api_raions':
				$id_region = (int)$this->input->post('region');
				$raions_999 = $this->locations->handler_api_raions($id_region);
				jsonResponse('', 'success', array('raions_999' => $raions_999, 'raions_999_count' => count($raions_999)));
			break;
			// DONE
			case 'get_api_sectors':
				$id_raion = (int)$this->input->post('raion');
				$sectors_999 = $this->locations->handler_api_sectors($id_raion);
				jsonResponse('', 'success', array('sectors_999' => $sectors_999, 'sectors_999_count' => count($sectors_999)));
			break;
			// DONE
			case 'get_api_offer_types':
				if($this->lauth->user_apy_key() === FALSE){
					jsonResponse('В настройках для вашего акаунта не указан API key.');
				}

				$api_offer_types = $this->real_estate->api_offers_type;

				jsonResponse('', 'success', array('offer_types' => $api_offer_types, 'offer_types_count' => count($api_offer_types)));
			break;
			// DONE
			case 'categories_list_dt':
				$params = array(
					'limit' => intVal($this->input->post('iDisplayLength')),
					'start' => intVal($this->input->post('iDisplayStart'))
				);

				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_" . intval($this->input->post('iSortCol_'.$i)))) {
							case 'dt_name_ru': 
								$params['sort_by'][] = 'category_title_ru-' . $this->input->post('sSortDir_'.$i);
							break;
							case 'dt_type': 
								$params['sort_by'][] = 'category_type-' . $this->input->post('sSortDir_'.$i);
							break;
						}
					}
				}

				$records = $this->real_estate->handler_get_categories($params);
				$records_total = $this->real_estate->handler_count_categories($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию активной?" data-dtype="warning" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					if($record['category_active'] == 1){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию неактивной?" data-dtype="warning" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['category_title_ru'],
						'dt_type'		=>  ($record['category_type'] == 'rent')?'Аренда':'Продажа',
						'dt_actions'	=>  $status
											.' <a href="#" data-href="'.base_url('admin/real_estate/popup_forms/edit_category/'.$record['id_category']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить категорию?" data-callback="delete_action" data-category="'.$record['id_category'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete_category':
                $this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->real_estate->handler_update_category($id_category, array('category_deleted' => 1));

				$this->_setup_categories_routers();
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status_category':
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($category['category_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->real_estate->handler_update_category($id_category, array('category_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'add_property':
				$this->form_validation->set_rules('categories[]', 'Категорий', 'trim|required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('type_property', 'Тип значений', 'trim|required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'property_title_ro' => $this->input->post('title_ro'),
					'property_title_ru' => $this->input->post('title_ru'),
					'property_title_en' => $this->input->post('title_en'),
					'property_in_filter' => ($this->input->post('property_in_filter'))?1:0,
					'property_on_item' => ($this->input->post('property_on_item'))?1:0,
					'property_values_has_suffix' => 0,
					'property_weight' => (int) $this->input->post('property_weight', true),
					'property_values_cols' => (int) $this->input->post('property_values_cols', true)
				);

				$categories = array_filter($this->input->post('categories[]'));
				if (empty($categories)) {
					jsonResponse('Выберите категорию.');
				}

				$valid_categories = $this->real_estate->handler_get_categories(array('id_category' => $categories));
				if(empty($categories)){
					jsonResponse('Выберите категорию.');
				}

				$filtered_categories = array();
				foreach ($valid_categories as $category) {
					$filtered_categories[] = $category['id_category'];
				}

				$insert['property_categories'] = implode(',', $filtered_categories);

				$property_values = $this->input->post('property_values');
				$property_type = $this->input->post('type_property');
				$insert['property_type'] = $property_type;
				$property_settings = array(
					'unit' => array(
						'title_ro' => '',
						'title_ru' => '',
						'title_en' => '',
						'title_plural_ro' => '',
						'title_plural_ru' => array(),
						'title_plural_en' => '',
						'show' => false
					),
					'repeat_values' => false
				);
				switch ($property_type) {
					case 'multiselect':
						if($this->input->post('property_floating_values')){
							$insert['property_values_has_suffix'] = 1;
						}

						if(!isset($property_values['ro'], $property_values['ru'], $property_values['en'])){
							jsonResponse('Добавьте значение или проверьте чтобы значения были на всех языках.');
						}

						$filtered_values = array();
						foreach ($property_values['ro'] as $value_key => $value) {
							if(!isset($property_values['ru'][$value_key], $property_values['en'][$value_key])){
								jsonResponse('Проверьте чтобы значения были на всех языках.');
							}

							$value_ro = trim($value);
							$value_ru = trim($property_values['ru'][$value_key]);
							$value_en = trim($property_values['en'][$value_key]);
							if(empty($value_ro) || empty($value_ru) || empty($value_en)){
								jsonResponse('Проверьте чтобы значения были на всех языках.');
							}

							$value_id = $value_key + 1;
							$filtered_values[] = array(
								'id_value' => 'v'.$value_id,
								'title_ro' => $value_ro,
								'title_ru' => $value_ru,
								'title_en' => $value_en
							);
						}
						
						$property_units = array_filter($this->input->post('property_unit'));
						$property_settings['unit']['title_ro'] = (isset($property_units['ro']))?$property_units['ro']:'';
						$property_settings['unit']['title_ru'] = (isset($property_units['ru']))?$property_units['ru']:'';
						$property_settings['unit']['title_en'] = (isset($property_units['en']))?$property_units['en']:'';
						$property_settings['unit']['title_plural_ro'] = (isset($property_units['plural_ro']))?$property_units['plural_ro']:'';
						$property_settings['unit']['title_plural_ru'] = (isset($property_units['plural_ru']))?$property_units['plural_ru']:array();
						$property_settings['unit']['title_plural_en'] = (isset($property_units['plural_en']))?$property_units['plural_en']:'';
						
						$property_settings['unit']['show'] = (isset($property_units['show']) && $property_units['show'] == 'on')?true:false;
					break;
					case 'range':
						$values = array_filter($property_values);
						if(empty($values)){
							jsonResponse('Добавьте значение.');
						}

						$property_units = array_filter($this->input->post('property_unit'));
						$property_settings['unit']['title_ro'] = (isset($property_units['ro']))?$property_units['ro']:'';
						$property_settings['unit']['title_plural_ro'] = (isset($property_units['plural_ro']))?$property_units['plural_ro']:'';
						$property_settings['unit']['title_ru'] = (isset($property_units['ru']))?$property_units['ru']:'';
						$property_settings['unit']['title_plural_ru'] = (isset($property_units['plural_ru']))?$property_units['plural_ru']:array();
						$property_settings['unit']['title_en'] = (isset($property_units['en']))?$property_units['en']:'';
						$property_settings['unit']['title_plural_en'] = (isset($property_units['plural_en']))?$property_units['plural_en']:'';
						$property_settings['unit']['show'] = (isset($property_units['show']) && $property_units['show'] == 'on')?true:false;

						if($this->input->post('property_repeat_values')){
							$property_settings['repeat_values'] = true;
						}

						$filtered_values = array();
						foreach ($values as $value_key => $value) {
							$filtered_values[] = $value;
						}
					break;
					case 'price':
						$this->load->module('currency');
						$currencies = $this->currency->_get_currencies(array('active' => 1));
						$default_currency = $this->currency->_get_default_currency();

						$filtered_values = array();
						$ccodes = array_map(function($currency){
							return $currency['currency_code'];
						}, $currencies);

						if (!isset($property_values[$default_currency['currency_code']])) {
							jsonResponse('Проверьте чтобы значения были для всех типов валют.');
						}
						
						foreach ($property_values[$default_currency['currency_code']] as $value_key => $value) {
							foreach ($currencies as $currency) {
								if (!isset($property_values[$currency['currency_code']])) {
									jsonResponse('Проверьте чтобы значения были для всех типов валют.');
								}

								$filtered_values[$currency['currency_code']][] = (int) $property_values[$currency['currency_code']][$value_key];
							}
						}
					break;
					default:
						jsonResponse('Не верный тип значений.');
					break;
				}

				$insert['property_settings'] = json_encode($property_settings);
				$insert['property_values'] = json_encode($filtered_values);

				$id_property = $this->real_estate->handler_insert_property($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_property':
				$this->form_validation->set_rules('id_property', 'Свойство', 'required|xss_clean');
				$this->form_validation->set_rules('categories[]', 'Категорий', 'trim|required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('type_property', 'Тип значений', 'trim|required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_property = (int)$this->input->post('id_property');
				$property = $this->real_estate->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'property_title_ro' => $this->input->post('title_ro'),
					'property_title_ru' => $this->input->post('title_ru'),
					'property_title_en' => $this->input->post('title_en'),
					'property_in_filter' => ($this->input->post('property_in_filter'))?1:0,
					'property_on_item' => ($this->input->post('property_on_item'))?1:0,
					'property_values_has_suffix' => 0,
					'property_weight' => (int) $this->input->post('property_weight', true),
					'property_values_cols' => (int) $this->input->post('property_values_cols', true)
				);

				$categories = array_filter($this->input->post('categories[]'));
				if (empty($categories)) {
					jsonResponse('Выберите категорию.');
				}

				$valid_categories = $this->real_estate->handler_get_categories(array('id_category' => $categories));
				if(empty($categories)){
					jsonResponse('Выберите категорию.');
				}

				$filtered_categories = array();
				foreach ($valid_categories as $category) {
					$filtered_categories[] = $category['id_category'];
				}

				$update['property_categories'] = implode(',', $filtered_categories);

				$property_values = $this->input->post('property_values');
				$property_settings = array(
					'unit' => array(
						'title_ro' => '',
						'title_ru' => '',
						'title_en' => '',
						'title_plural_ro' => '',
						'title_plural_ru' => array(),
						'title_plural_en' => '',
						'show' => false
					),
					'repeat_values' => false
				);
				switch ($property['property_type']) {
					case 'multiselect':
						if($this->input->post('property_floating_values')){
							$update['property_values_has_suffix'] = 1;
						}

						if(!isset($property_values['ro'], $property_values['ru'], $property_values['en'], $property_values['id'])){
							jsonResponse('Добавьте значение или проверьте чтобы значения были на всех языках.');
						}

						$current_property_values = json_decode($property['property_values'], true);
						$current_property_values = arrayByKey($current_property_values, 'id_value');
						$current_property_values_keys = array_keys($current_property_values);
						$max_key = (int) substr(max($current_property_values_keys), 1);
						$filtered_values = array();
						foreach ($property_values['id'] as $value_key => $value_id) {
							if(!isset($property_values['ro'][$value_key], $property_values['ru'][$value_key], $property_values['en'][$value_key])){
								jsonResponse('Проверьте чтобы значения были на всех языках.');
							}

							if($value_id == 'new'){
								$max_key++;
								$id_value = 'v'.$max_key; 
							} else{
								$id_value = $value_id; 
							}

							$value_ro = trim($property_values['ro'][$value_key]);
							$value_ru = trim($property_values['ru'][$value_key]);
							$value_en = trim($property_values['en'][$value_key]);
							if(empty($value_ro) || empty($value_ru) || empty($value_en)){
								jsonResponse('Проверьте чтобы значения были на всех языках.');
							}

							$filtered_values[] = array(
								'id_value' => $id_value,
								'title_ro' => $value_ro,
								'title_ru' => $value_ru,
								'title_en' => $value_en
							);
						}

						$property_units = array_filter($this->input->post('property_unit'));
						$property_settings['unit']['title_ro'] = (isset($property_units['ro']))?$property_units['ro']:'';
						$property_settings['unit']['title_plural_ro'] = (isset($property_units['plural_ro']))?$property_units['plural_ro']:'';
						$property_settings['unit']['title_ru'] = (isset($property_units['ru']))?$property_units['ru']:'';
						$property_settings['unit']['title_plural_ru'] = (isset($property_units['plural_ru']))?$property_units['plural_ru']:array();
						$property_settings['unit']['title_en'] = (isset($property_units['en']))?$property_units['en']:'';
						$property_settings['unit']['title_plural_en'] = (isset($property_units['plural_en']))?$property_units['plural_en']:'';
						$property_settings['unit']['show'] = (isset($property_units['show']) && $property_units['show'] == 'on')?true:false;
					break;
					case 'range':
						$values = array_filter($property_values);
						if(empty($values)){
							jsonResponse('Добавьте значение.');
						}

						$property_units = array_filter($this->input->post('property_unit'));
						$property_settings['unit']['title_ro'] = (isset($property_units['ro']))?$property_units['ro']:'';
						$property_settings['unit']['title_plural_ro'] = (isset($property_units['plural_ro']))?$property_units['plural_ro']:'';
						$property_settings['unit']['title_ru'] = (isset($property_units['ru']))?$property_units['ru']:'';
						$property_settings['unit']['title_plural_ru'] = (isset($property_units['plural_ru']))?$property_units['plural_ru']:array();
						$property_settings['unit']['title_en'] = (isset($property_units['en']))?$property_units['en']:'';
						$property_settings['unit']['title_plural_en'] = (isset($property_units['plural_en']))?$property_units['plural_en']:'';
						$property_settings['unit']['show'] = (isset($property_units['show']) && $property_units['show'] == 'on')?true:false;

						if($this->input->post('property_repeat_values')){
							$property_settings['repeat_values'] = true;
						}

						$filtered_values = array();
						foreach ($values as $value_key => $value) {
							$filtered_values[] = $value;
						}
					break;
					case 'price':
						$this->load->module('currency');
						$currencies = $this->currency->_get_currencies(array('active' => 1));
						$default_currency = $this->currency->_get_default_currency();

						$filtered_values = array();
						$ccodes = array_map(function($currency){
							return $currency['currency_code'];
						}, $currencies);

						if (!isset($property_values[$default_currency['currency_code']])) {
							jsonResponse('Проверьте чтобы значения были для всех типов валют.');
						}
						
						foreach ($property_values[$default_currency['currency_code']] as $value_key => $value) {
							foreach ($currencies as $currency) {
								if (!isset($property_values[$currency['currency_code']])) {
									jsonResponse('Проверьте чтобы значения были для всех типов валют.');
								}

								$filtered_values[$currency['currency_code']][] = (int) $property_values[$currency['currency_code']][$value_key];
							}
						}
					break;
					default:
						jsonResponse('Не верный тип значений.');
					break;
				}

				$update['property_settings'] = json_encode($property_settings);
				$update['property_values'] = json_encode($filtered_values);

				$this->real_estate->handler_update_property($id_property, $update);
				jsonResponse('Сохранено.', 'success', array());
			break;
			// DONE
			case 'delete_property':
				$validator_rules = array(
					array(
						'field' => 'property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$id_property = (int)$this->input->post('property');
				$this->real_estate->handler_delete_property($id_property);
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status_property':
				$this->form_validation->set_rules('property', 'Свойство', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int)$this->input->post('property');
				$property = $this->real_estate->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($property['property_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->real_estate->handler_update_property($id_property, array('property_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'properties_list_dt':
				$params = array(
					'limit' => intVal($this->input->post('iDisplayLength')),
					'start' => intVal($this->input->post('iDisplayStart'))
				);

				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_" . intval($this->input->post('iSortCol_'.$i)))) {
							case 'dt_name_ru': 
								$params['sort_by'][] = 'property_title_ru-' . $this->input->post('sSortDir_'.$i);
							break;
						}
					}
				}

				$records = $this->real_estate->handler_get_properties($params);
				$records_total = $this->real_estate->handler_count_properties($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				$categories = arrayByKey($this->real_estate->handler_get_categories(), 'id_category');
				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать свойство активной?" data-dtype="warning" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-property="'.$record['id_property'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					if($record['property_active'] == 1){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать свойство неактивной?" data-dtype="warning" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-property="'.$record['id_property'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					}

					$property_categories = explode(',', $record['property_categories']);
					$categories_list = array();
					foreach ($property_categories as $property_category) {
						$categories_list[] = '<span>'.$categories[$property_category]['category_title_ru'].' <small class="text-muted">'.(($categories[$property_category]['category_type'] == 'rent')?'Аренда':'Продажа').'</small></span>';
					}

					$property_in_filter = '<span class="fa fa-check-circle text-success fs-18"></span>';
					if($record['property_in_filter'] == 0){
						$property_in_filter = '<span class="fa fa-minus-circle text-danger fs-18"></span>';
					}
					
					$property_on_item = '<span class="fa fa-check-circle text-success fs-18"></span>';
					if($record['property_on_item'] == 0){
						$property_on_item = '<span class="fa fa-minus-circle text-danger fs-18"></span>';
					}

					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['property_title_ru'],
						'dt_cname'		=>  implode(', ', $categories_list),
						'dt_in_filter'	=>  $property_in_filter,
						'dt_on_item'	=>  $property_on_item,
						'dt_actions'	=>  $status
											.' <a href="#" data-href="'.base_url('admin/real_estate/popup_forms/edit_property/'.$record['id_property']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить свойство?" data-callback="delete_action" data-property="'.$record['id_property'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'add_icon':
				$this->form_validation->set_rules('icon_class', 'Css Class', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_ro', 'Название RO', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_ru', 'Название RU', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_en', 'Название En', 'trim|required|xss_clean|max_length[50]');
				if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'icon_name_ro' => $this->input->post('icon_name_ro'),
					'icon_name_ru' => $this->input->post('icon_name_ru'),
					'icon_name_en' => $this->input->post('icon_name_en'),
					'icon_class' => $this->input->post('icon_class')
				);

				$id_icon = $this->real_estate->handler_insert_icon($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_icon':
				$id_icon = (int)$this->input->post('id_icon');
				$icon = $this->real_estate->handler_get_icon($id_icon);
				if(empty($icon)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$this->form_validation->set_rules('icon_class', 'Css Class', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_ro', 'Название RO', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_ru', 'Название RU', 'trim|required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('icon_name_en', 'Название En', 'trim|required|xss_clean|max_length[50]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$update = array(
					'icon_name_ro' => $this->input->post('icon_name_ro'),
					'icon_name_ru' => $this->input->post('icon_name_ru'),
					'icon_name_en' => $this->input->post('icon_name_en'),
					'icon_class' => $this->input->post('icon_class')
				);

				$this->real_estate->handler_update_icon($id_icon, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'delete_icon':
				$validator_rules = array(
					array(
						'field' => 'icon',
						'label' => 'Иконка',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$id_icon = (int)$this->input->post('icon');
				$this->real_estate->handler_delete_icon($id_icon);
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_icon_weight':
				$validator_rules = array(
					array(
						'field' => 'id_icon',
						'label' => 'Иконка',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$id_icon = (int)$this->input->post('id_icon');
				$icon = $this->real_estate->handler_get_icon($id_icon);
				if(empty($icon)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$params = array(
					'icon_weight' => $icon['icon_weight']
				);

				if($this->input->post('direction') != 'down'){
					$params['direction'] = 'up';
				}
				
				$last_icon_weight = $this->real_estate->handler_get_icon_weight($params);
				if(!empty($last_icon_weight)){
					$icon_weight = $last_icon_weight['icon_weight'];
					$update = array(
						'icon_weight' => $icon_weight
					);
					$this->real_estate->handler_update_icon($id_icon, $update);
					$update = array(
						'icon_weight' => $icon['icon_weight']
					);
					$this->real_estate->handler_update_icon($last_icon_weight['id_icon'], $update);
				}

				jsonResponse('Сохранено.','success');
			break;
			// DONE
			case 'icons_list_dt':
				$params = array(
					'limit' => intVal($this->input->post('iDisplayLength')),
					'start' => intVal($this->input->post('iDisplayStart'))
				);

				$records = $this->real_estate->handler_get_icons();
				$records_total = $this->real_estate->handler_count_icons();

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {					
					$weight = ' <a class="btn btn-default btn-xs call-function" href="#" title="Поднять" data-callback="change_weight" data-icon="'.$record['id_icon'].'" data-direction="up"><span class="fa fa-arrow-up"></span></a>
								<a class="btn btn-default btn-xs call-function" title="Отпустить" data-callback="change_weight" data-icon="'.$record['id_icon'].'" data-direction="down"><span class="fa fa-arrow-down"></span></a>';
					$output['aaData'][] = array(
						'dt_icon'		=>  '<span class="fs-32 '.$record['icon_class'].'"></span>',
						'dt_name_ru'	=>  $record['icon_name_ru'],
						'dt_weight'		=>  $weight,
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/real_estate/popup_forms/edit_icon/'.$record['id_icon']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
											 <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить иконку?" data-callback="delete_action" data-icon="'.$record['id_icon'].'"><i class="fa fa-trash"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'get_category_settings':
				$id_category = (int)$this->input->post('category');
				$category = $this->real_estate->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$params = array(
					'id_category' => $id_category,
					'property_type' => array('multiselect','range')
				);

				$this->data['properties'] = $this->real_estate->handler_get_properties($params);

				if($category['category_has_icons'] == 1){
					$this->data['icons'] = $this->real_estate->handler_get_icons();
				}
				$content = $this->load->view('admin/modules/real_estate/category_settings_view', $this->data, true);
				jsonResponse('', 'success', array('settings' => $content));
			break;
		}
	}

	// DONE
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'change_status':
				$id_real_estate = (int)$this->uri->segment(5);
				$this->data['real_estate'] = $this->real_estate->handler_get($id_real_estate);
		
				if (empty($this->data['real_estate'])) {
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/real_estate/change_status_form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'change_sold':
				$id_real_estate = (int)$this->uri->segment(5);
				$this->data['real_estate'] = $this->real_estate->handler_get($id_real_estate);
		
				if (empty($this->data['real_estate'])) {
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/real_estate/change_sold_form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_category':
				$this->data['api_categories'] = $this->real_estate->api_categories;

				$content = $this->load->view('admin/modules/real_estate/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_category':
				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->real_estate->handler_get_category($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->data['api_categories'] = $this->real_estate->api_categories;
				if($this->data['category']['id_category_999'] > 0){
					$this->data['api_subcategories'] = $this->real_estate->api_subcategories;
					
					if($this->data['category']['id_subcategory_999'] > 0){
						$this->data['api_offer_types'] = $this->real_estate->api_offers_type;
					}
				}
				
				$content = $this->load->view('admin/modules/real_estate/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_property':
				$params = array(
					'category_active' => 1
				);
				$this->data['categories'] = $this->real_estate->handler_get_categories($params);

				$content = $this->load->view('admin/modules/real_estate/properties/add_form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content, 'features' => $output_features));
			break;
			// DONE
			case 'edit_property':
				$id_property = (int) $this->uri->segment(5);
				$this->data['property'] = $this->real_estate->handler_get_property($id_property);
				if(empty($this->data['property'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$params = array(
					'category_active' => 1
				);
				$this->data['categories'] = $this->real_estate->handler_get_categories($params);
				$content = $this->load->view('admin/modules/real_estate/properties/edit_form', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_icon':
				$content = $this->load->view('admin/modules/real_estate/icons/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_icon':
				$id_icon = (int) $this->uri->segment(5);
				$this->data['icon'] = $this->real_estate->handler_get_icon($id_icon);
				if(empty($this->data['icon'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/real_estate/icons/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function _setup_categories_routers(){
		$categories = $this->real_estate->handler_get_categories();
		$categories_routers = array();
		foreach ($categories as $category) {
			$categories_routers[] = array(
				'ro' => $category['url_ro'],
				'ru' => $category['url_ru'],
				'en' => $category['url_en']
			);
		}

		$routers_file = APPPATH . 'config/routers_categories.php';
		$f = fopen($routers_file, "w");
		fwrite($f, '<?php '."\r\n" . '$categories_routers = '.var_export($categories_routers, true).';');
		fclose($f);
	}

	// 999.md API
	function _add_advert($id_real_estate = 0){
		$real_estate = $this->real_estate->handler_get($id_real_estate);
		$category = $this->real_estate->handler_get_category($real_estate['id_category']);
		$manager = Modules::run('users/_get_user', $real_estate['id_manager']);
		
		if(empty($manager->user_999_api_key)){
			jsonResponse('Для менеджера не установлен АПИ ключ.');
		}

		$properties_params = array(
			'id_category' => $real_estate['id_category'],
			'property_type' => array('multiselect','range')
		);
		$this->data['properties'] = $this->real_estate->handler_get_properties($properties_params);
		$this->data['selected_properties'] = arrayByKey($this->real_estate->handler_get_real_estate_properties_values($id_real_estate), 'id_property');

		$_999_required_completed = true;
		$_999_category_key = "{$category['id_subcategory_999']}_{$category['id_offer_type_999']}";
		$api_properties = $this->real_estate->handler_get_api_properties($_999_category_key);

		$http_basic_auth_credentials = "{$manager->user_999_api_key}:password";
		$authorization_header = base64_encode($http_basic_auth_credentials);
		$phone_numbers = file_get_contents("https://{$http_basic_auth_credentials}@partners-api.999.md/phone_numbers?lang=ru");
		$phone_numbers = json_decode($phone_numbers, true);
		$phones = array();
		foreach ($phone_numbers['phone_numbers'] as $phone_number) {
			$phones[] = $phone_number['phone_number'];
		}

		if(empty($phones)){
			jsonResponse('Подтвердите хотябы один номер телефона на сайте 999.md');
		}

		$re_images = json_decode($real_estate['real_estate_photos'], true);
		$re_images_999 = json_decode($real_estate['real_estate_photos_999'], true);
		$re_images_update = array();
		$images = array();
		$images_count = 0;
		foreach ($re_images as $re_image){
			if ($images_count >= 10){
				break;
			}

			$image = null;

			if(is_array($re_images_999) && array_key_exists($re_image, $re_images_999)){
				$image = $re_images_999[$re_image];
			} else{
				$header = "Content-Type: multipart/form-data; boundary=-----BOUNDARY\r\nAuthorization: Basic {$authorization_header}";
				$filename = FCPATH."files/real_estate/{$id_real_estate}/{$re_image}";
				if (!is_file($filename)){
					continue;
				}
	
				$file_contents = file_get_contents($filename);
				$form_filename = basename($filename);
				$file_content_type = mime_content_type($filename);
				$content =  "-------BOUNDARY\r\nContent-Disposition: form-data; name=\"file\"; filename=\"{$form_filename}\"\r\nContent-Type: {$file_content_type}\r\n\r\n{$file_contents}\r\n-------BOUNDARY--\r\n";
				
				$context = stream_context_create(array(
					'http' => array(
						  'method' => 'POST',
						  'header' => $header,
						  'content' => $content,
					)
				));
				
				$res = file_get_contents('https://partners-api.999.md/images', false, $context);
				$result_images = json_decode($res, true);
				if ($result_images['image_id'] != ''){
					$image = $result_images['image_id'];
				}
			}
			
			if($image){
				$images[] = $image;
				$re_images_update[$re_image] = $image;
				$images_count++;
			}
		}
		
		$_price = getPrice($real_estate, true);

		$add_advert = array(
			'offer_type' => $category['id_offer_type_999'],
			'features' => array(
				array(
					'id' => '2',
					'value' => (string)ceil($_price['display_price_db']),
					'unit' => strtolower($_price['currency_code'])
				),
				array(
					'id' => '16', 
					'value' => $phones
				),
				array(
					'id' => '7', 
					'value' => $real_estate['id_region_999']
				),
				array(
					'id' => '8', 
					'value' => $real_estate['id_raion_999']
				),
				array(
					'id' => '9', 
					'value' => $real_estate['id_sector_999']
				),
				array(
					'id' => '12', 
					'value' => $real_estate['real_estate_title_999']
				),
				array(
					'id' => '13', 
					'value' => $real_estate['real_estate_text_999']
				),
			)
		);

		$update_advert = true;
		if($real_estate['id_999'] == 0){
			$add_advert['category_id'] = $category['id_category_999'];
			$add_advert['subcategory_id'] = $category['id_subcategory_999'];
			$update_advert = false;
		}

		if(!empty($images)){
			$add_advert['features'][] = array(
				'id' => '14',
				'value' => $images
			);
		}

		$required_features = array(2,16,7,8,9,12,13,795);

		if(!empty($api_properties)){
			if(in_array($_999_category_key, array('1404_776', '1404_912'))){
				$add_advert['features'][] = array(
					'id' => '795',
					'value' => '18894'
				);
			}

			$real_estate_properties = arrayByKey($this->real_estate->handler_get_real_estate_properties_values($id_real_estate), 'id_property');
			foreach ($api_properties as $property_id => $_999_property) {
				if($_999_property['required'] == true && empty($real_estate_properties[$property_id])){
					$_999_required_completed = false;
				}

				if(empty($real_estate_properties[$property_id])){
					continue;
				}

				if($_999_property['required'] == true){
					$required_features[] = $_999_property['id_property_999'];
				}

				$feature_value_sufix = '';
				if(in_array($property_id, array(1,18)) && $real_estate_properties[$property_id]['property_value_sufix'] == 1){
					$feature_value_sufix = '+';
				}

				$feature = array(
					'id' => (string)$_999_property['id_property_999'],
					'value' => (!empty($_999_property['options']))?(isset($_999_property['options'][$real_estate_properties[$property_id]['property_value'].$feature_value_sufix]))?(string)$_999_property['options'][$real_estate_properties[$property_id]['property_value'].$feature_value_sufix]:null:(string)$real_estate_properties[$property_id]['property_value'],
				);

				if($_999_property['unit'] != null){
					$feature['unit'] = (string)$_999_property['unit'];
				}

				$add_advert['features'][] = $feature;

				if($real_estate_properties[$property_id]['property_value_max'] > 0){
					$add_advert['features'][] = array(
						'id' => (string)$_999_property['id_property_999_max'],
						'value' => (string)$_999_property['options_max'][$real_estate_properties[$property_id]['property_value_max']]
					);
				}
			}

			if($category['category_has_icons'] == 1){
				$category_icons = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');
				$icons_selected = json_decode($real_estate['real_estate_icons'], true);
				foreach ($icons_selected as $icons) {
					if($category_icons[$icons['id_icon']]['id_999'] > 0){
						$feature = array(
							'id' => (string)$category_icons[$icons['id_icon']]['id_999'],
							'value' => true
						);
		
						$add_advert['features'][] = $feature;
					}
				}
			}
		}

		if($_999_required_completed){
			$http_basic_auth_credentials = "{$manager->user_999_api_key}:password";
			$authorization_header = base64_encode($http_basic_auth_credentials);

			$advert_features_999 = json_decode($real_estate['advert_features_999'], true);
			if(!empty($advert_features_999) && $update_advert){
				foreach ($advert_features_999 as $key => $advert_feature_999) {
					if(!in_array($advert_feature_999['id'], $required_features)){
						$advert_features_999[$key]['value'] = null;
					}
				}
				
				$result = $this->_curl_999($http_basic_auth_credentials, 'https://partners-api.999.md/adverts/'.$real_estate['id_999'], 'PATCH', json_encode(array('features' => $advert_features_999)));
			}

			if($update_advert){
				$url = 'https://partners-api.999.md/adverts/'.$real_estate['id_999'];
				$method = 'PATCH';
			} else{
				$url = 'https://partners-api.999.md/adverts';
				$method = 'POST';
			}

			$result = $this->_curl_999($http_basic_auth_credentials, $url, $method, json_encode($add_advert));

			$update = array(
				'advert_features_999' => json_encode($add_advert['features']), 
				'real_estate_photos_999' => json_encode($re_images_update)
			);

			if(!$update_advert && !empty($result['advert']['id'])){
				$update['id_999'] = $result['advert']['id'];
			}
			$this->real_estate->handler_update($id_real_estate, $update);

			$this->_advert_status($id_real_estate);
			jsonResponse('Данные сохранены.', 'success', array('add_advert' => $add_advert,'result' => $result));
		}
	}

	function _advert_status($id_real_estate = 0){
		$real_estate = $this->real_estate->handler_get($id_real_estate);

		if($real_estate['id_999'] > 0){
			$manager = Modules::run('users/_get_user', $real_estate['id_manager']);
	
			$http_basic_auth_credentials = "{$manager->user_999_api_key}:password";
			$url = 'https://partners-api.999.md/adverts/'.$real_estate['id_999'].'/access_policy';
			$post_content = json_encode(array('access_policy' => $real_estate['advert_status_999']));
			$result = $this->_curl_999($http_basic_auth_credentials, $url, 'PUT', $post_content);

			// if($real_estate['advert_status_999'] == 'private'){
			// 	$url = 'https://partners-api.999.md/adverts/'.$real_estate['id_999'].'/autorepublisher';
			// 	$result = $this->_curl_999($http_basic_auth_credentials, $url, 'GET');

			// 	if(isset($result['enabled']) && $result['enabled'] != 0){
			// 		unset($result['info']);
			// 		$result['enabled'] = false;
			// 		$url = 'https://partners-api.999.md/adverts/'.$real_estate['id_999'].'/autorepublisher';
			// 		$post_content = json_encode($result);
			// 		$result = $this->_curl_999($http_basic_auth_credentials, $url, 'PUT', $post_content);
			// 	}
			// }
		}
	}

	function _curl_999($http_basic_auth_credentials = '', $url = '', $method = 'POST', $content = array(), $headers = array("Content-Type: application/json")){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "{$method}");
		curl_setopt($ch, CURLOPT_USERPWD, $http_basic_auth_credentials);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if(curl_errno($ch)) {
			jsonResponse('Ошибка отправки данных на 999.md. Попробуйте поже.');
		}
		curl_close($ch);
		
		$result = json_decode($result, true);
		return $result;
	}

	function test(){
		echo phpinfo();
	}
}