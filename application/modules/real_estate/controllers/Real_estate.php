<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Real_estate extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		$this->data['_init_google_maps'] = true;
		
		$this->load->model('Real_estate_model', 'real_estate');
		$this->load->model("locations/Locations_model", "locations");
		$this->load->model('users/Users_model', 'users');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$segments = $this->uri->segment_array();
		if(!$this->lang->is_default()){
			$uri_lang = array_shift($segments);
			$temp = array();
			foreach ($segments as $key => $value) {
				$temp[$key+1] = $value;
			}

			$segments = $temp;
			unset($temp);
		}

		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(2);
		} else{
			$uri = $this->uri->uri_to_assoc(3);
		}

		if(isset($uri['page'])){
			$category_url = array_slice($segments, 0, -2, true);
		} else{
			$category_url = $segments;
		}

		$category_url = implode('/', $category_url);
		$this->data['category'] = $this->real_estate->handler_get_category_url($category_url, $this->ulang);
		if(empty($this->data['category'])){
			if (!$this->input->is_ajax_request()) {
				return_404($this->data);
			} else{
				jsonResponse('', 'error');
			}
		}

		$category_properties_params = array(
			'property_in_filter' => 1,
			'id_category' => array($this->data['category']['id_category'])
		);
		$this->data['category_properties'] = arrayByKey($this->real_estate->handler_get_properties($category_properties_params), 'id_property');
		
		$this->data['currencies'] = arrayByKey(Modules::run('currency/_get_currencies', array('active' => 1)), 'currency_code');
		$this->data['curent_currency'] = Modules::run('currency/_get_current_code', array('active' => 1));

		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$category_locations = $this->real_estate->handler_get_category_locations($this->data['category']['id_category']);
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0, 'id_location' => array_merge(array(0), $category_locations))), 'id_parent', true);
		
		$catalog_params = array(
			'id_category' => $this->data['category']['id_category'],
			'real_estate_active' => 1
		);

		$active_filters = array();
		$this->data['selected_filters'] = array();
		$this->data['selected_filters_price'] = array();
		$this->data['selected_filters_range'] = array();
		$property_select = array();
		$property_range = array();
		$id_items = array();
		$use_items = false;
		if ($this->input->is_ajax_request()) {
			$post_properties = $this->input->post('property');
		} elseif ($this->input->get('filters')) {
			$post_properties = json_decode(base64_decode($this->input->get('filters')), true);
			// if(!empty($post_properties['fp'])){
			// 	if(empty($post_properties['fp']['currency'])){
			// 		unset($post_properties['fp']);
			// 	} else{
			// 		$_currency_view = Modules::run('currency/_get_by_code', $post_properties['fp']['currency']);
			// 		if(empty($_currency_view)){
			// 			unset($post_properties['fp']);
			// 		} elseif($this->data['curent_currency'] != $_currency_view['currency_code']){
			// 			set_cookie('_ai_currency', $_currency_view['currency_code'], 3600, getDomain('.'), '/');
			// 			redirect($_SERVER['REQUEST_URI'], 'refresh');
			// 		}
			// 	}
			// }
		}
		
		$_available_post_properties = array();
		if(!empty($post_properties)){
			foreach ($post_properties as $property_type => $post_property) {
				switch ($property_type) {
					case 'fm':
						foreach ($post_property as $property_id => $property_values) {
							if(array_key_exists($property_id, $this->data['category_properties']) && $this->data['category_properties'][$property_id]['property_in_filter'] == 1){
								$db_values = arrayByKey(json_decode($this->data['category_properties'][$property_id]['property_values'], true), 'id_value');
								$valid_values = array();
								$valid_values_sufix = array();
								foreach (array_keys($property_values) as $property_value) {
									if(array_key_exists($property_value, $db_values)){
										$valid_values[] = $property_value;
										$active_filters[] = array(
											'id_property' => $property_id,
											'property_name' => $this->data['category_properties'][$property_id][lang_column('property_title')],
											'property_value' => $db_values[$property_value][lang_column('title')],
											'target_type' => 'checkbox',
											'target' => "property[fm][{$property_id}][{$property_value}]"
										);

										$this->data['selected_filters'][] = "fm_{$property_id}_{$property_value}";
										$_available_post_properties['fm'][$property_id][$property_value] = 'on';

										if ($this->data['category_properties'][$property_id]['property_values_has_suffix'] == 1) {
											$v_sufix = (int)substr($property_value, 1) - 1;
											if($v_sufix > 0){
												$valid_values_sufix[] = "v{$v_sufix}";
											}
										}
									}
								}

								if(!empty($valid_values)){
									$select_params = array(
										'id_property' => $property_id, 
										'property_values' => $valid_values
									);

									if ($use_items == true) {
										if(empty($id_items)){
											$id_items = array(0);
										}

										$select_params['id_real_estate'] = $id_items;
									}

									$id_items = $this->real_estate->handler_get_real_estate_by_property_values($select_params);

									if(!empty($valid_values_sufix)){
										$select_params_sufix = array(
											'id_property' => $property_id, 
											'property_values' => $valid_values_sufix,
											'property_value_sufix' => 1
										);

										if(isset($select_params['id_real_estate'])){
											$select_params_sufix['id_real_estate'] = $select_params['id_real_estate'];
										}
										$id_items = array_merge($id_items, $this->real_estate->handler_get_real_estate_by_property_values($select_params_sufix));
									}
								}
							}

							$use_items = true;
						}
					break;
					case 'fp':
						if(empty($post_properties['fp']['from']) && empty($post_properties['fp']['to']) && $post_property['currency']){
							break;
						}

						$price_property = $this->real_estate->handler_get_property_by_alias('price', array('id_category' => $this->data['category']['id_category']));
						$price_values = json_decode($price_property['property_values'], true);
						$_available_post_properties['fp']['currency'] = $post_property['currency'];

						if(!empty($post_property['from'])){
							$post_property['from'] = (int)$post_property['from'];
							$_price_from = convertPrice($post_property['from'], $post_property['currency'], $this->data['curent_currency']);
							
							$catalog_params['price_from'] = $_price_from['amount_default'];
							$active_filters[] = array(
								'id_property' => 'price',
								'property_name' => $price_property[lang_column('property_title')] . ' ' . lang_line('label_property_min', false),
								'property_value' => $_price_from['amount_out_display'] .' '. $_price_from['currency_symbol_out'],
								'target_type' => 'select',
								'target' => "property[fp][from]"
							);
							$this->data['selected_filters_price']['from'] = $_price_from['amount_out'];
							$_available_post_properties['fp']['from'] = $post_property['from'];
						}

						if(!empty($post_property['to'])){
							$post_property['to'] = (int)$post_property['to'];
							$_price_to = convertPrice($post_property['to'], $post_property['currency'], $this->data['curent_currency']);

							$catalog_params['price_to'] = $_price_to['amount_default'];
							$active_filters[] = array(
								'id_property' => 'price',
								'property_name' => $price_property[lang_column('property_title')] . ' ' . lang_line('label_property_max', false),
								'property_value' => $_price_to['amount_out_display'] .' '. $_price_to['currency_symbol_out'],
								'target_type' => 'select',
								'target' => "property[fp][to]"
							);
							$this->data['selected_filters_price']['to'] = $_price_to['amount_out'];
							$_available_post_properties['fp']['to'] = $post_property['to'];
						}
					break;
					case 'fr':
						foreach ($post_property as $property_id => $property_values) {
							if(empty($property_values['from']) && empty($property_values['to'])){
								continue;
							}

							if(array_key_exists($property_id, $this->data['category_properties']) && $this->data['category_properties'][$property_id]['property_in_filter'] == 1){
								$range_params = array(
									'id_property' => $property_id, 
								);
								$category_property_settings = json_decode($this->data['category_properties'][$property_id]['property_settings'], true);
								if(!empty($property_values['from'])){
									$range_params['property_range_from'] = (int)$property_values['from'];
									$active_filters[] = array(
										'id_property' => $property_id,
										'property_name' => $this->data['category_properties'][$property_id][lang_column('property_title')] . ' ' . lang_line('label_property_min', false),
										'property_value' => $range_params['property_range_from'].(($category_property_settings['unit']['show'])?' '.$category_property_settings['unit'][lang_column('title')]:''),
										'target_type' => 'select',
										'target' => "property[fr][{$property_id}][from]"
									);

									$this->data['selected_filters_range']["fr_{$property_id}_from"] = $range_params['property_range_from'];
									$_available_post_properties['fr'][$property_id]['from'] = $range_params['property_range_from'];
								}
		
								if(!empty($property_values['to'])){
									$range_params['property_range_to'] = (int)$property_values['to'];
									$active_filters[] = array(
										'id_property' => $property_id,
										'property_name' => $this->data['category_properties'][$property_id][lang_column('property_title')] . ' ' . lang_line('label_property_max', false),
										'property_value' => $range_params['property_range_to'].(($category_property_settings['unit']['show'])?' '.$category_property_settings['unit'][lang_column('title')]:''),
										'target_type' => 'select',
										'target' => "property[fr][{$property_id}][to]"
									);

									$this->data['selected_filters_range']["fr_{$property_id}_to"] = $range_params['property_range_to'];
									$_available_post_properties['fr'][$property_id]['to'] = $range_params['property_range_to'];
								}

								if ($use_items == true) {
									if(empty($id_items)){
										$id_items = array(0);
									}

									$range_params['id_real_estate'] = $id_items;
								}

								$id_items = $this->real_estate->handler_get_real_estate_by_property_values($range_params);
							}
							$use_items = true;
						}
					break;
					case 'fl':
						foreach ($post_property as $id_parent => $children_locations) {
							$filter_locations = array();
							if(array_key_exists($id_parent, $this->data['children_locations']) && !empty($children_locations)){
								$db_children_locations = arrayByKey($this->data['children_locations'][$id_parent], 'id_location');

								if(!is_array($children_locations)){
									$children_locations = array($children_locations);
								}

								foreach ($children_locations as $child_location) {
									if(array_key_exists($child_location, $db_children_locations)){
										$id_location = (int) $child_location;
										$catalog_params['id_location'][] = $id_location;
										$active_filters[] = array(
											'id_property' => 'location',
											'property_name' => 'Locatie',
											'property_value' => $db_children_locations[$id_location][lang_column('location_name')],
											'target_type' => 'selectmultiple',
											'target' => "property[fl][{$id_parent}][]",
											'target_option' => $id_location
										);
	
										$this->data['selected_filters'][] = "fl_{$id_parent}_{$id_location}";
										$_available_post_properties['fl'][$id_parent][] = $id_location;
									}
								}
							}
						}
					break;
					case 'exclude_last_floor':
						$catalog_params['exclude_last_floor'] = 1;
						$this->data['selected_filters'][] = "exclude_last_floor";
						$_available_post_properties['exclude_last_floor'] = 'on';
					break;
				}
			}

			if($use_items == true){
				if(empty($id_items)){
					$id_items = array(0);
				}

				$catalog_params['id_real_estate'] = $id_items;
			}
		}

		if($this->input->is_ajax_request()){
			$this->data['active_filters'] = $this->load->view(get_theme_view('modules/real_estate/active_filters_list_view'), array('active_filters' => $active_filters), true);
		} else{
			$this->data['active_filters'] = $this->load->view(get_theme_view('modules/real_estate/active_filters_view'), array('active_filters' => $active_filters), true);
		}

		if(!empty($_available_post_properties)){
			$catalog_params['order_by'] = 'has_discount DESC, real_estate_exclusive DESC, real_estate_date_created DESC';
		}

		$catalog_view = get_cookie('_ai_ccatalog_view');
		if(!in_array($catalog_view, array('grid', 'list'))){
			$catalog_view = 'grid';
		}

		$page = 1;
		if(isset($uri['page']) && (int)$uri['page'] > 0){
			$page = (int)$uri['page'];
		} elseif($this->input->post('page')){
			$page = (int) $this->input->post('page');
			
			if($page > 1){
				$uri['page'] = $page;
			}
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;
		switch ($catalog_view) {
			default:
			case 'grid':
				$catalog_params['limit'] = $this->data['limit'] = $limit = (int) $this->lsettings->item('catalog_limit');
			break;
			case 'list':
				$catalog_params['limit'] = $this->data['limit'] = $limit = (int) $this->lsettings->item('catalog_limit_list');
			break;
			
		}
		$catalog_params['start'] = $start = ($page <= 1) ? 0 : (($page * $limit) - $limit);

		$this->data['records_total'] = $this->real_estate->handler_get_count($catalog_params);
		$this->data['real_estates'] = $this->real_estate->handler_get_all($catalog_params);

		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}

							$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name
						);
					break;
				}
			}
		}

		// PAGINATION
		$page_segment = $this->uri->total_segments()+1;
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> $this->data['category'][lang_column('url')],
			'lang' 		=> $this->ulang,
			'cur_page' 	=> $page,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('catalog/type/category', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['catalog_view'] = $catalog_view;
		$this->data['re_managers'] = arrayByKey(Modules::run('users/_get_users'), 'id_user');

		$filters_query = (!empty($_available_post_properties))?base64_encode(json_encode($_available_post_properties)):'';
		$this->data['langs_uri'] = get_dinamyc_uri('catalog/type/category', array(
			'ro' => $this->data['category']['url_ro'].((!empty($filters_query))?'?filters='.$filters_query:''),
			'ru' => $this->data['category']['url_ru'].((!empty($filters_query))?'?filters='.$filters_query:''),
			'en' => $this->data['category']['url_en'].((!empty($filters_query))?'?filters='.$filters_query:''),
		));

		if ($this->input->is_ajax_request()) {
			$dropdown_languages = $this->config->item('supported_languages');
			$_dropdown_items = array();
			unset($dropdown_languages[$this->ulang]);
			foreach($dropdown_languages as $lang_key => $dropdown_language){
				$_dropdown_items[] = '<a class="dropdown-item" href="'.$this->lang->switch_uri($lang_key, (!empty($this->data['langs_uri']))?$this->data['langs_uri'][$lang_key]:'').'">'.$dropdown_language['name'].'</a>';
			}
			$result = array(
				'content' => $this->load->view(get_theme_view('modules/real_estate/catalog_'.$catalog_view.'_view'), $this->data, true),
				'pagination' => $this->data['pagination'],
				'active_filters' => $this->data['active_filters'],
				'records_total' => $this->data['records_total'],
				'catalog_view' => $catalog_view,
				'dropdown_languages' => implode('', $_dropdown_items),
				'filters_query' => $filters_query
			);

			jsonResponse('', 'success', $result);
		}

		$categories_params = array(
			'category_active' => 1,
			'category_type' => $this->data['category']['category_type'],
			'category_deleted' => 0
		);
		$this->data['filter_categories'] = $this->real_estate->handler_get_categories($categories_params);

		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['category'][lang_column('mt')],
			lang_column('page_mk') => $this->data['category'][lang_column('mk')],
			lang_column('page_md') => $this->data['category'][lang_column('md')],
			'page_image' => (!empty($this->data['category']['category_image']))?get_og_image('files/'.$this->data['category']['category_image']):false
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['active_menu'] = 'catalog/'.$this->data['category']['category_type'];
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/real_estate/catalog_view';
		$this->data['page_seo'] = $this->data['category'][lang_column('seo')];
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if($this->lang->is_default()){
			$option = $this->uri->segment(3);
		} else{
			$option = $this->uri->segment(4);
		}
		
		switch($option){
			case 'set_catalog_view':
				$catalog_view = $this->input->post('view');
				if(!in_array($catalog_view, array('grid', 'list'))){
					$catalog_view = 'grid';
				}

				set_cookie('_ai_ccatalog_view', $catalog_view, 3600, getDomain('.'), '/');

				jsonResponse('', 'success');
			break;
			case 'get_home_items':
				$real_estate_params = array(
					'show_on_home' => 1,
					'discount_type' => array('simple','new_price','by_m2'),
					'real_estate_active' => 1
				);
				$this->data['real_estates'] = $this->real_estate->handler_get_all($real_estate_params);
		
				$properties_params = array(
					'property_type' => array('multiselect','range'),
					'property_on_item' => 1
				);
				$properties = $this->real_estate->handler_get_properties_values($properties_params);
				$this->data['re_properties'] = array();
				if(!empty($properties)){
					foreach ($properties as $property) {
						$property_settings = json_decode($property['property_settings'], true);
		
						switch ($property['property_type']) {
							case 'multiselect':
								$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
								$unit_name = $property_settings['unit'][lang_column('title')];
								if(isset($property_values[$property['property_value']])){
									$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
									if($property['property_value_sufix'] == 1){
										$unit_name = lang_line('label_living', false);
									}
									$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
										'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
										'unit_name' => $unit_name
									);
								}
							break;
							case 'range':
								$property_value = array($property['property_value']);
								
								if($property_settings['repeat_values'] == true){
									$property_value[] = $property['property_value_max'];
								}
								$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
										
								$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => implode('/', $property_value),
									'unit_name' => $unit_name
								);
							break;
						}
					}
				}
				
				$content = $this->load->view(get_theme_view('modules/home/home_items_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
		}
	}

	function discounts(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(2);
		} else{
			$uri = $this->uri->uri_to_assoc(3);
		}

		$page = 1;
		if($this->input->post('page')){
			$page = (int)$this->input->post('page');
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('discounts_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);

		$real_estate_params = array(
			'discount_type' => array('simple','by_m2'),
			'real_estate_active' => 1,
			'start' => $start,
			'limit' => $limit
		);

		if($this->input->post('id_category')){
			$id_category = (int)$this->input->post('id_category');
			if($id_category > 0){
				$real_estate_params['id_category'] = $id_category;
			}
		}
		
		$this->data['real_estates'] = $this->real_estate->handler_get_all($real_estate_params);
		$this->data['records_total'] = $this->real_estate->handler_get_count($real_estate_params);

		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}

							$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name
						);
					break;
				}
			}
		}

		// PAGINATION
		$page_segment = $this->uri->total_segments()+1;
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> '',
			'lang' 		=> $this->ulang,
			'cur_page' 	=> $page,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('discounts', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		if ($this->input->is_ajax_request()) {
			$result = array(
				'content' => $this->load->view(get_theme_view('modules/real_estate/discounts_list_view'), $this->data, true),
				'pagination' => $this->data['pagination']
			);

			jsonResponse('', 'success', $result);
		}

		$categories_params = array(
			'category_active' => 1,
			'category_deleted' => 0,
			'category_type' => 'sale'
		);
		$categories = $this->real_estate->handler_get_categories($categories_params);
		$real_estate_categories_params = array(
			'discount_type' => array('simple','new_price','by_m2'),
			'real_estate_active' => 1
		);
		$this->data['categories'] = array();
		foreach ($categories as $key => $category) {
			$real_estate_categories_params['id_category'] = $category['id_category'];
			if($this->real_estate->handler_get_count($real_estate_categories_params) > 0){
				$this->data['categories'][] = $category;
			}
		}

		$this->data['langs_uri'] = get_dinamyc_uri('discounts', array(
			'ro' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'ru' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'en' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):''
		));
		
		$this->data['active_menu'] = 'discounts';
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('discounts');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];


		$this->data['main_content'] = 'modules/real_estate/discounts_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function search(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(2);
		} else{
			$uri = $this->uri->uri_to_assoc(3);
		}

		$page = 1;
		if($this->input->get('page')){
			$page = (int)$this->input->get('page');
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('search_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);

		$this->data['get_keywords_text'] = $this->data['get_keywords'] = $get_keywords = $keywords = trim($this->input->get('q', true));
		if(mb_strlen($keywords) < 3){
			$keywords = '{{empty_keywords}}';
			$this->data['get_keywords_text'] = lang_line('label_no_search_query', false);
		}

		$real_estate_params = array(
			'keywords' => $keywords,
			'real_estate_active' => 1,
			'start' => $start,
			'limit' => $limit
		);
		
		$this->data['real_estates'] = $this->real_estate->handler_get_all($real_estate_params);
		$this->data['records_total'] = $this->real_estate->handler_get_count($real_estate_params);

		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}

							$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name
						);
					break;
				}
			}
		}

		// PAGINATION
		$page_segment = $this->uri->total_segments()+1;
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> '',
			'lang' 		=> $this->ulang,
			'cur_page' 	=> $page,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('catalog/search', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		if ($this->input->is_ajax_request()) {
			$result = array(
				'content' => $this->load->view(get_theme_view('modules/real_estate/search_list_view'), $this->data, true),
				'pagination' => $this->data['pagination'],
				'records_total' => $this->data['records_total'],
				'get_keywords_text' => clean_output($this->data['get_keywords_text'])
			);

			jsonResponse('', 'success', $result);
		}

		$this->data['langs_uri'] = get_dinamyc_uri('catalog/search', array(
			'ro' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri)."?q={$get_keywords}":"?q={$get_keywords}",
			'ru' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri)."?q={$get_keywords}":"?q={$get_keywords}",
			'en' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri)."?q={$get_keywords}":"?q={$get_keywords}"
		));
		
		$this->data['active_menu'] = false;
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		// $this->load->model("pages/Pages_model", "pages");
		// $this->data['page_detail'] = $this->pages->handler_get_by_alias('discounts');
		// $this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		// $this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		// $this->data['og_meta'] = array(
		// 	lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
		// 	lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
		// 	lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
		// 	'page_image' => get_og_image($this->data['page_detail']['page_poster'])
		// );
		// $this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		// $this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		// $this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];


		$this->data['main_content'] = 'modules/real_estate/search_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function detail(){
		if($this->lang->is_default()){
			$real_estate_url = $this->uri->segment(2);
			$re_category_url = $this->uri->segment(1);
		} else{
			$real_estate_url = $this->uri->segment(3);
			$re_category_url = $this->uri->segment(2);
		}
		
		$id_real_estate = id_from_link($real_estate_url);

		if($this->input->get('t') == 'pdf'){
			return $this->_dpdf($id_real_estate);
		}

		$this->data['real_estate'] = $this->real_estate->handler_get($id_real_estate);
		if(empty($this->data['real_estate'])){
			$this->_detail_deleted($id_real_estate);
			return false;
		}

        if($this->data['real_estate']['real_estate_active'] == 0 || $this->data['real_estate']['real_estate_deleted'] == 1){
            return_404($this->data);
		}

		if($re_category_url.'/'.$real_estate_url != $this->data['real_estate'][lang_column('full_url')]){
			redirect($this->ulang.'/'.get_dinamyc_uri('catalog/category/real_estate', $this->data['real_estate'][lang_column('full_url')], $this->ulang));
		}
		
		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'id_real_estate' => $id_real_estate
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['real_estate']['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}
							$this->data['real_estate']['re_properties'][$property['id_property']] = array(
								'property_name' => $property[lang_column('property_title')],
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name,
								'show_unit' => ($property['property_value_sufix'] == 1)?true:$property_settings['unit']['show']
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						$this->data['real_estate']['re_properties'][$property['id_property']] = array(
							'property_name' => $property[lang_column('property_title')],
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name,
							'show_unit' => $property_settings['unit']['show']
						);
					break;
				}
			}
		}

		$this->users->return_object = false;
		$this->data['re_manager'] = $this->users->handler_get($this->data['real_estate']['id_manager']);

		$this->data['icons'] = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');

		$properties_on_item_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties_on_items = $this->real_estate->handler_get_properties_values($properties_on_item_params);

		$last_viewed = get_cookie('_ai_re_last_viewed');
		$_cookie_last_viewed = array();
		$this->data['last_viewed'] = array();
		if(!empty($last_viewed)){
			$last_viewed = base64_decode($last_viewed);
			$_cookie_last_viewed = json_decode($last_viewed, true);

			if(isset($_cookie_last_viewed[$id_real_estate])){
				unset($_cookie_last_viewed[$id_real_estate]);
			}

			$_cookie_last_viewed_limit = array_slice($_cookie_last_viewed, -$this->lsettings->item('last_viewed_limit'), $this->lsettings->item('last_viewed_limit'), true);
			if(!empty($_cookie_last_viewed_limit)){
				foreach ($_cookie_last_viewed_limit as $key => $value) {
					$_cookie_last_viewed_limit[$key] = (int)$value;
				}
				$real_estate_params = array(
					'id_real_estate' => $_cookie_last_viewed_limit,
					'real_estate_active' => 1
				);
				$this->data['last_viewed'] = $this->real_estate->handler_get_all($real_estate_params);
		
				
				$this->data['lv_re_properties'] = array();
				if(!empty($properties_on_items)){
					foreach ($properties_on_items as $property) {
						$property_settings = json_decode($property['property_settings'], true);
		
						switch ($property['property_type']) {
							case 'multiselect':
								$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
								$unit_name = $property_settings['unit'][lang_column('title')];
								if(isset($property_values[$property['property_value']])){
									$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
									if($property['property_value_sufix'] == 1){
										$unit_name = lang_line('label_living', false);
									}
									$this->data['lv_re_properties'][$property['id_real_estate']][$property['id_property']] = array(
										'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
										'unit_name' => $unit_name
									);
								}
							break;
							case 'range':
								$property_value = array($property['property_value']);
								
								if($property_settings['repeat_values'] == true){
									$property_value[] = $property['property_value_max'];
								}
								$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
								$this->data['lv_re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => implode('/', $property_value),
									'unit_name' => $unit_name
								);
							break;
						}
					}
				}
			}
		}

		$_cookie_last_viewed = array_slice($_cookie_last_viewed, -$this->lsettings->item('last_viewed_limit')-3, $this->lsettings->item('last_viewed_limit')+3, true);
		$_cookie_last_viewed[$id_real_estate] = $id_real_estate;
		set_cookie('_ai_re_last_viewed', base64_encode(json_encode($_cookie_last_viewed)), 3600, getDomain('.'), '/');

		$_price = getPrice($this->data['real_estate']);
		$real_estate_same_price_percent = (int)$this->lsettings->item('real_estate_same_price_percent')/100;
		$same_params = array(
			'price_from' => floor($_price['display_price_db'] - $_price['display_price_db']*$real_estate_same_price_percent),
			'price_to' => ceil($_price['display_price_db'] + $_price['display_price_db']*$real_estate_same_price_percent),
			'real_estate_active' => 1,
			'not_id_real_estate' => $this->data['real_estate']['id_real_estate'],
			'id_real_estate' => array(0),
			'limit' => (int)$this->lsettings->item('real_estate_same_limit')
		);

		$property_area_m2 = $this->real_estate->handler_get_property_by_alias('area_m2');
		if(!empty($property_area_m2)){
			$real_estate_area = $this->real_estate->handler_get_real_estate_property_value($id_real_estate, $property_area_m2['id_property']);
			if(!empty($real_estate_area)){
				$real_estate_same_area_percent = (int)$this->lsettings->item('real_estate_same_area_percent')/100;
				$area_from = floor($real_estate_area['property_value'] - $real_estate_area['property_value'] * $real_estate_same_area_percent);
				$area_to = ceil($real_estate_area['property_value'] + $real_estate_area['property_value'] * $real_estate_same_area_percent);
				$area_params = array(
					'property_range_from' => $area_from,
					'property_range_to' => $area_to
				);
				$re_by_area = $this->real_estate->handler_get_properties_values($area_params);

				if(!empty($re_by_area)){
					foreach ($re_by_area as $re_by_area_item) {
						$same_params['id_real_estate'][] = $re_by_area_item['id_real_estate'];
					}
				}
			}
		}
		
		$this->data['re_same'] = $this->real_estate->handler_get_all($same_params);
		$this->data['same_re_properties'] = array();
		if(!empty($this->data['re_same'])){
			if(!empty($properties_on_items)){
				foreach ($properties_on_items as $property) {
					$property_settings = json_decode($property['property_settings'], true);
	
					switch ($property['property_type']) {
						case 'multiselect':
							$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
							$unit_name = $property_settings['unit'][lang_column('title')];
							if(isset($property_values[$property['property_value']])){
								$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
								if($property['property_value_sufix'] == 1){
									$unit_name = lang_line('label_living', false);
								}
								$this->data['same_re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
									'unit_name' => $unit_name
								);
							}
						break;
						case 'range':
							$property_value = array($property['property_value']);
							
							if($property_settings['repeat_values'] == true){
								$property_value[] = $property['property_value_max'];
							}
							$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							$this->data['same_re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => implode('/', $property_value),
								'unit_name' => $unit_name
							);
						break;
					}
				}
			}
		}

		$this->data['langs_uri'] = get_dinamyc_uri('catalog/category/real_estate', array(
			'ro' => $this->data['real_estate']['full_url_ro'],
			'ru' => $this->data['real_estate']['full_url_ru'],
			'en' => $this->data['real_estate']['full_url_en']
		));

		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['real_estate'][lang_column('real_estate_full_title')],
			lang_column('page_mk') => $this->data['real_estate'][lang_column('mk')],
			lang_column('page_md') => $this->data['real_estate'][lang_column('md')],
			'page_image' => get_og_image('files/real_estate/'.$this->data['real_estate']['id_real_estate'].'/'.$this->data['real_estate']['real_estate_photo'])
		);
		$this->data['stitle'] = $this->data['real_estate'][lang_column('mt')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		if($this->_update_viewed($id_real_estate, $this->data['real_estate']['real_estate_viewed'])){
			$real_estate_viewed = $this->data['real_estate']['real_estate_viewed'];
			if($real_estate_viewed >= 1000){
				$real_estate_viewed = 500;
			} else{
				$real_estate_viewed = $real_estate_viewed + 1;
			}

			$this->data['real_estate']['real_estate_viewed'] = $real_estate_viewed;
		}
		
		$this->data['remove_wrapper_bg'] = TRUE;
		$this->data['active_menu'] = 'catalog/'.$this->data['real_estate']['category_type'];
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/real_estate/detail_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _dpdf($id_real_estate = 0){
		$this->data['real_estate'] = $this->real_estate->handler_get($id_real_estate);
		if(empty($this->data['real_estate'])){
			return_404($this->data);
		}

        if($this->data['real_estate']['real_estate_active'] == 0 || $this->data['real_estate']['real_estate_deleted'] == 1){
            return_404($this->data);
		}
		
		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'id_real_estate' => $id_real_estate
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['real_estate']['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}
							$this->data['real_estate']['re_properties'][$property['id_property']] = array(
								'property_name' => $property[lang_column('property_title')],
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name,
								'show_unit' => $property_settings['unit']['show']
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						$this->data['real_estate']['re_properties'][$property['id_property']] = array(
							'property_name' => $property[lang_column('property_title')],
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name,
							'show_unit' => $property_settings['unit']['show']
						);
					break;
				}
			}
		}

		$this->users->return_object = false;
		$this->data['re_manager'] = $this->users->handler_get($this->data['real_estate']['id_manager']);

		$this->data['icons'] = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');

		$this->load->library('Pdflib','pdflib');
		$pdflib = $this->pdflib->new_pdf();
		$content = $this->load->view('admin/pdf_template/real_estate_view', $this->data, true);
		$pdflib->defaultfooterline = 0;
        $pdflib->WriteHTML($content);
        $pdflib->Output($file, "I");
	}

	private function _detail_deleted($id_real_estate = 0){
		$_real_estate_deleted = $this->real_estate->handler_get_deleted($id_real_estate);
		if(empty($_real_estate_deleted)){
			return_404($this->data);
		}

		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(1);
		} else{
			$uri = $this->uri->uri_to_assoc(2);
		}

		$page = 1;
		if(isset($uri['page'])){
			$page = (int)$uri['page'];
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('real_estate_deleted_same_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);

		$_real_estate_same_params = json_decode($_real_estate_deleted['real_estate_same_params'], true);
		$same_params = array(
			'id_category' => $_real_estate_same_params['id_category'],
			'price_from' => $_real_estate_same_params['price_from'],
			'price_to' => $_real_estate_same_params['price_to'],
			'real_estate_active' => 1,
			'id_real_estate' => array(0),
			'start' => $start,
			'limit' => $limit
		);

		$properties_on_item_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties_on_items = $this->real_estate->handler_get_properties_values($properties_on_item_params);
		
		$area_params = array(
			'property_range_from' => $_real_estate_same_params['property_range_from'],
			'property_range_to' => $_real_estate_same_params['property_range_to']
		);
		$re_by_area = $this->real_estate->handler_get_properties_values($area_params);

		if(!empty($re_by_area)){
			foreach ($re_by_area as $re_by_area_item) {
				$same_params['id_real_estate'][] = $re_by_area_item['id_real_estate'];
			}
		}
		
		$this->data['real_estates'] = $this->real_estate->handler_get_all($same_params);
		$this->data['records_total'] = $this->real_estate->handler_get_count($same_params);

		$this->data['re_properties'] = array();
		if(!empty($this->data['real_estates'])){
			if(!empty($properties_on_items)){
				foreach ($properties_on_items as $property) {
					$property_settings = json_decode($property['property_settings'], true);
	
					switch ($property['property_type']) {
						case 'multiselect':
							$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
							$unit_name = $property_settings['unit'][lang_column('title')];
							if(isset($property_values[$property['property_value']])){
								$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
								if($property['property_value_sufix'] == 1){
									$unit_name = lang_line('label_living', false);
								}
								$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
									'unit_name' => $unit_name
								);
							}
						break;
						case 'range':
							$property_value = array($property['property_value']);
							
							if($property_settings['repeat_values'] == true){
								$property_value[] = $property['property_value_max'];
							}
							$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => implode('/', $property_value),
								'unit_name' => $unit_name
							);
						break;
					}
				}
			}
		}

		$this->data['langs_uri'] = get_dinamyc_uri('catalog/category/real_estate', array(
			'ro' => $_real_estate_deleted['full_url_ro'].(($page > 1)?'/page/'.$page:''),
			'ru' => $_real_estate_deleted['full_url_ru'].(($page > 1)?'/page/'.$page:''),
			'en' => $_real_estate_deleted['full_url_en'].(($page > 1)?'/page/'.$page:'')
		));

		// PAGINATION
		$page_segment = $this->uri->total_segments()+1;
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> $_real_estate_deleted[lang_column('full_url')],
			'lang' 		=> $this->ulang,
			'cur_page' 	=> $page,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('re_same', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('real_estate_deleted');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/real_estate/deleted_page_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _update_viewed($id_real_estate, $real_estate_viewed){
		$check_visitor = $this->input->cookie('_re_'.$id_real_estate, FALSE);
		$ip = $this->input->ip_address();
		if ($check_visitor == false) {
			$cookie = array(
				"name"   => '_re_'.$id_real_estate,
				"value"  => "$ip",
				"expire" =>  time() + 86400,
				"secure" => false
			);
			$this->input->set_cookie($cookie);
			$this->real_estate->handler_update_viewed($id_real_estate, $real_estate_viewed);
			return TRUE;
		}

		return FALSE;
	}

	function _get_categories($params = array()){
		return $this->real_estate->handler_get_categories($params);
	}

	function _get_category($id_category = 0){
		return $this->real_estate->handler_get_category($id_category);
	}

	function _get($id_real_estate = 0){
		return $this->real_estate->handler_get($id_real_estate);
	}

	function _get_count($params = array()){
		return $this->real_estate->handler_get_count($params);
	}

	function _get_same_params($id_real_estate = 0){
		$return_params = array(
			'id_category' => 0,
			'id_location' => 0,
			'price_from' => 0,
			'price_to' => 0,
			'property_range_from' => 0,
			'property_range_to' => 0
		);
		$real_estate = $this->real_estate->handler_get($id_real_estate);

		if(empty($real_estate)){
			return $return_params;
		}

		$return_params['id_category'] = $real_estate['id_category'];
		$return_params['id_location'] = $real_estate['id_location'];

		$_price = getPrice($real_estate);
		$real_estate_same_price_percent = (int)$this->lsettings->item('real_estate_same_price_percent')/100;
		$return_params['price_from'] = floor($_price['display_price_db'] - $_price['display_price_db']*$real_estate_same_price_percent);
		$return_params['price_to'] = ceil($_price['display_price_db'] + $_price['display_price_db']*$real_estate_same_price_percent);

		$property_area_m2 = $this->real_estate->handler_get_property_by_alias('area_m2');
		if(!empty($property_area_m2)){
			$real_estate_area = $this->real_estate->handler_get_real_estate_property_value($id_real_estate, $property_area_m2['id_property']);
			if(!empty($real_estate_area)){
				$real_estate_same_area_percent = (int)$this->lsettings->item('real_estate_same_area_percent')/100;
				$return_params['property_range_from'] = floor($real_estate_area['property_value'] - $real_estate_area['property_value'] * $real_estate_same_area_percent);
				$return_params['property_range_to'] = ceil($real_estate_area['property_value'] + $real_estate_area['property_value'] * $real_estate_same_area_percent);
			}
		}

		return $return_params;
	}

	function _get_best($params = array()){
		$real_estate = $this->real_estate->handler_get_best($params);
		if(!empty($real_estate)){
			$properties_params = array(
				'property_type' => array('multiselect','range'),
				'property_on_item' => 1,
				'id_real_estate' => $real_estate['id_real_estate']
			);
			$properties = $this->real_estate->handler_get_properties_values($properties_params);
			$real_estate['re_properties'] = array();
			if(!empty($properties)){
				foreach ($properties as $property) {
					$property_settings = json_decode($property['property_settings'], true);
	
					switch ($property['property_type']) {
						case 'multiselect':
							$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
							if(isset($property_values[$property['property_value']])){
								$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
								$real_estate['re_properties'][$property['id_property']] = array(
									'property_value' => $property_values[$property['property_value']][lang_column('title')],
									'unit_name' => $unit_name
								);
							}
						break;
						case 'range':
							$property_value = array($property['property_value']);
							
							if($property_settings['repeat_values'] == true){
								$property_value[] = $property['property_value_max'];
							}
							$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							$real_estate['re_properties'][$property['id_property']] = array(
								'property_value' => implode('/', $property_value),
								'unit_name' => $unit_name
							);
						break;
					}
				}
			}
		}
		return $real_estate;
	}

	function update_search_info($params = array()){
		ini_set('max_execution_time', 0);
		$records = $this->real_estate->handler_get_all($params);
		foreach ($records as $record) {
			$search_info = array(
				$record['real_estate_full_title_ro'],
				$record['real_estate_full_title_ru'],
				$record['real_estate_full_title_en'],
				$record['real_estate_title_ro'],
				$record['real_estate_title_ru'],
				$record['real_estate_title_en'],
				orderNumberOnly($record['id_real_estate'])
			);

			$manager = Modules::run('users/_get_user', $record['id_manager']);
			if(!empty($manager)){
				$search_info[] = $manager->user_name_ro;
				$search_info[] = $manager->user_name_ru;
				$search_info[] = $manager->user_name_en;
				$search_info[] = formatPhone($manager->user_phone);
			}
			$update = array(
				'real_estate_search_info' => implode(' ', $search_info)
			);
			
			$this->real_estate->handler_update($record['id_real_estate'], $update);
		}
	}
}