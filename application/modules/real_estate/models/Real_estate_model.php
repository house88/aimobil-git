<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Real_estate_model extends CI_Model{
	var $real_estate = "real_estate";
	var $real_estate_deleted = "real_estate_deleted";
	var $real_estate_columns = "re.*, re_c.category_type, re_c.url_ro as category_url_ro, re_c.url_ru as category_url_ru, re_c.url_en as category_url_en, CONCAT(re_c.url_ro,'/',re.url_ro) as full_url_ro, CONCAT(re_c.url_ru,'/',re.url_ru) as full_url_ru, CONCAT(re_c.url_en,'/',re.url_en) as full_url_en";
	var $real_estate_icons = "real_estate_icons";
	var $real_estate_icons_relation = "real_estate_icons_relation";
	var $real_estate_categories = "real_estate_categories";
	var $real_estate_properties = "real_estate_properties";
	var $real_estate_properties_values = "real_estate_properties_values";
	var $locations = "locations";
	var $users = "users";

	var $api_categories = array(
		270 => array(
			'id' => 270,
			'title' => 'Недвижимость'
		),
	);

	var $api_subcategories = array(
		1404 => array(
			'id' => 1404,
			'title' => 'Квартиры'
		),
		1406 => array(
			'id' => 1406,
			'title' => 'Дома'
		),
		1407 => array(
			'id' => 1407,
			'title' => 'Земельные участки'
		),
		1405 => array(
			'id' => 1405,
			'title' => 'Коммерческая недвижимость'
		)
	);

	var $api_offers_type = array(
		776 => array(
			'id' => 776,
			'type' => 'sell',
			'title' => 'Продажа'
		),
		912 => array(
			'id' => 912,
			'type' => 'rent',
			'title' => 'Аренда'
		)
	);

	var $api_properties = array(
		'1404_776' => array(
			1 => array( // Кол. комнат
				'id_property' => 1,
				'id_property_999' => 241,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 893,
					'v1+' => 894,
					'v2' => 894,
					'v2+' => 902,
					'v3' => 902,
					'v3+' => 904,
					'v4' => 904,
					'v4+' => 20442,
				)
			),
			2 => array( // Тип конструкции
				'id_property' => 2,
				'id_property_999' => 852,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 19108,
					'v2' => 19109
				)
			),
			3 => array( // Состояние
				'id_property' => 3,
				'id_property_999' => 253,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 916,
					'v2' => 931,
					'v3' => 925
				)
			),
			5 => array( // Площадь
				'id_property' => 5,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
			7 => array( // Этаж
				'id_property' => 7,
				'id_property_999' => 248,
				'required' => true,
				'unit' => null,
				'options' => array(
					1 => 918,
					2 => 935,
					3 => 905,
					4 => 929,
					5 => 909,
					6 => 955,
					7 => 895,
					8 => 921,
					9 => 934,
					10 => 947,
					11 => 970,
					12 => 965,
					13 => 958,
					14 => 913,
					15 => 1016,
					16 => 1019,
					17 => 940,
					18 => 1021,
					19 => 1015,
					20 => 1681,
					21 => 1679,
					22 => 12484,
					23 => 12485,
					24 => 1661,
					25 => 1014
				),
				'id_property_999_max' => 249,
				'options_max' => array(
					1 => 956,
					2 => 964,
					3 => 906,
					4 => 936,
					5 => 910,
					6 => 919,
					7 => 971,
					8 => 975,
					9 => 896,
					10 => 951,
					11 => 948,
					12 => 954,
					13 => 966,
					14 => 959,
					15 => 979,
					16 => 914,
					17 => 1018,
					18 => 1017,
					19 => 982,
					20 => 972,
					21 => 963,
					22 => 1020,
					23 => 1680,
					24 => 941,
					25 => 1668
				),
			),
			8 => array( // Ванные комнаты
				'id_property' => 8,
				'id_property_999' => 252,
				'required' => false,
				'unit' => null,
				'options' => array(
					1 => 900,
					2 => 950,
					3 => 967,
					4 => 968
				)
			),
			46 => array( // Отопление
				'id_property' => 46,
				'id_property_999' => 154,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => true,
					'v2' => false,
				)
			),
			47 => array( // Планировка
				'id_property' => 47,
				'id_property_999' => 246,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 915,
					'v2' => 943,
					'v3' => 898,
					'v4' => 930,
					'v5' => 953,
					'v6' => 969,
					'v7' => 978,
					'v10' => 938,
					'v11' => 961,
					'v12' => 960,
				)
			),
		),
		'1404_912' => array(
			18 => array( // Кол. комнат
				'id_property' => 18,
				'id_property_999' => 241,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 893,
					'v1+' => 894,
					'v2' => 894,
					'v2+' => 902,
					'v3' => 902,
					'v3+' => 904,
					'v4' => 904,
					'v4+' => 20442,
				)
			),
			22 => array( // Тип конструкции
				'id_property' => 2,
				'id_property_999' => 852,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 19108,
					'v2' => 19109
				)
			),
			26 => array( // Состояние
				'id_property' => 26,
				'id_property_999' => 253,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 916,
					'v2' => 931,
					'v3' => 925
				)
			),
			36 => array( // Площадь
				'id_property' => 36,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
			41 => array( // Этаж
				'id_property' => 41,
				'id_property_999' => 248,
				'required' => true,
				'unit' => null,
				'options' => array(
					1 => 918,
					2 => 935,
					3 => 905,
					4 => 929,
					5 => 909,
					6 => 955,
					7 => 895,
					8 => 921,
					9 => 934,
					10 => 947,
					11 => 970,
					12 => 965,
					13 => 958,
					14 => 913,
					15 => 1016,
					16 => 1019,
					17 => 940,
					18 => 1021,
					19 => 1015,
					20 => 1681,
					21 => 1679,
					22 => 12484,
					23 => 12485,
					24 => 1661,
					25 => 1014
				),
				'id_property_999_max' => 249,
				'options_max' => array(
					1 => 956,
					2 => 964,
					3 => 906,
					4 => 936,
					5 => 910,
					6 => 919,
					7 => 971,
					8 => 975,
					9 => 896,
					10 => 951,
					11 => 948,
					12 => 954,
					13 => 966,
					14 => 959,
					15 => 979,
					16 => 914,
					17 => 1018,
					18 => 1017,
					19 => 982,
					20 => 972,
					21 => 963,
					22 => 1020,
					23 => 1680,
					24 => 941,
					25 => 1668
				),
			),
			45 => array( // Ванные комнаты
				'id_property' => 45,
				'id_property_999' => 252,
				'required' => false,
				'unit' => null,
				'options' => array(
					1 => 900,
					2 => 950,
					3 => 967,
					4 => 968
				)
			),
			46 => array( // Отопление
				'id_property' => 46,
				'id_property_999' => 154,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => true,
					'v2' => false,
				)
			),
			47 => array( // Планировка
				'id_property' => 47,
				'id_property_999' => 246,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 915,
					'v2' => 943,
					'v3' => 898,
					'v4' => 930,
					'v5' => 953,
					'v6' => 969,
					'v7' => 978,
					'v10' => 938,
					'v11' => 961,
					'v12' => 960,
				)
			),
		),
		'1406_776' => array(
			17 => array( // Кол. комнат
				'id_property' => 17,
				'id_property_999' => 588,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1,
					'v1+' => 2,
					'v2' => 2,
					'v2+' => 3,
					'v3' => 3,
					'v3+' => 4,
					'v4' => 4,
					'v4+' => 5,
				)
			),
			10 => array( // Кол. этажей
				'id_property' => 10,
				'id_property_999' => 249,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 1641,
					'v2' => 1643,
					'v3' => 1644,
					'v4' => 1652
				)
			),
			24 => array( // Состояние
				'id_property' => 24,
				'id_property_999' => 254,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1649,
					'v2' => 1640,
					'v3' => 1648
				)
			),
			34 => array( // Площадь
				'id_property' => 34,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
			6 => array( // Площадь земли
				'id_property' => 6,
				'id_property_999' => 245,
				'required' => true,
				'unit' => 'ar',
				'options' => null
			),
			48 => array( // Отопление
				'id_property' => 48,
				'id_property_999' => 154,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => true,
					'v2' => false,
				)
			),
		),
		'1406_912' => array(
			19 => array( // Кол. комнат
				'id_property' => 19,
				'id_property_999' => 588,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1,
					'v1+' => 2,
					'v2' => 2,
					'v2+' => 3,
					'v3' => 3,
					'v3+' => 4,
					'v4' => 4,
					'v4+' => 5,
				)
			),
			20 => array( // Кол. этажей
				'id_property' => 20,
				'id_property_999' => 249,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 1641,
					'v2' => 1643,
					'v3' => 1644,
					'v4' => 1652
				)
			),
			27 => array( // Состояние
				'id_property' => 27,
				'id_property_999' => 254,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1649,
					'v2' => 1640,
					'v3' => 1648
				)
			),
			37 => array( // Площадь
				'id_property' => 37,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
			40 => array( // Площадь земли
				'id_property' => 40,
				'id_property_999' => 245,
				'required' => true,
				'unit' => 'ar',
				'options' => null
			)
		),
		'1407_776' => array(
			14 => array( // Предназначение
				'id_property' => 14,
				'id_property_999' => 258,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 1039,
					'v2' => 1040,
					'v3' => 1040
				)
			),
			39 => array( // Площадь земли
				'id_property' => 39,
				'id_property_999' => 245,
				'required' => true,
				'unit' => 'ar',
				'options' => null
			)
		),
		'1405_776' => array(
			13 => array( // Предназначение
				'id_property' => 13,
				'id_property_999' => 257,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 1030,
					'v2' => 1026,
					'v3' => 1027,
					'v4' => 1023
				)
			),
			42 => array( // Этаж
				'id_property' => 42,
				'id_property_999' => 248,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 918,
					'v2' => 935,
					'v3' => 905,
					'v4' => 929,
					'v5' => 909,
					'v6' => 955,
					'v7' => 895,
					'v8' => 921,
					'v9' => 934,
					'v10' => 947,
				)
			),
			25 => array( // Состояние
				'id_property' => 25,
				'id_property_999' => 255,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1024,
					'v2' => 1037,
					'v3' => 1038
				)
			),
			35 => array( // Площадь
				'id_property' => 35,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
		),
		'1405_912' => array(
			21 => array( // Предназначение
				'id_property' => 21,
				'id_property_999' => 257,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 1030,
					'v2' => 1026,
					'v3' => 1027,
					'v4' => 1023
				)
			),
			44 => array( // Этаж
				'id_property' => 44,
				'id_property_999' => 248,
				'required' => true,
				'unit' => null,
				'options' => array(
					'v1' => 918,
					'v2' => 935,
					'v3' => 905,
					'v4' => 929,
					'v5' => 909,
					'v6' => 955,
					'v7' => 895,
					'v8' => 921,
					'v9' => 934,
					'v10' => 947,
				)
			),
			28 => array( // Состояние
				'id_property' => 28,
				'id_property_999' => 255,
				'required' => false,
				'unit' => null,
				'options' => array(
					'v1' => 1024,
					'v2' => 1037,
					'v3' => 1038
				)
			),
			38 => array( // Площадь
				'id_property' => 38,
				'id_property_999' => 244,
				'required' => true,
				'unit' => 'm2',
				'options' => null
			),
		)
	);	

	function __construct(){
		parent::__construct();
	}

	function db_date(){
		return $this->db->select('NOW() as db_date')->get()->row()->db_date;
	}

	function handler_get_api_properties($key = null){
		if ($key && !empty($this->api_properties[$key])) {
			return $this->api_properties[$key];
		}
		
		return $this->api_properties;
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->real_estate, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_real_estate = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_real_estate', $id_real_estate);
		$this->db->update($this->real_estate, $data);
	}

	function handler_update_viewed($id_real_estate = 0, $real_estate_viewed = 0){
		if($real_estate_viewed >= 1000){
			$real_estate_viewed = 500;
		} else{
			$real_estate_viewed = $real_estate_viewed + 1;
		}

		$this->db->where('id_real_estate', $id_real_estate);
		$this->db->update($this->real_estate, array('real_estate_viewed' => $real_estate_viewed));
	}

	function handler_update_all($data = array(), $params = array()){
		if(empty($data)){
			return false;
		}

		extract($params);

		if(isset($real_estate_offer_type)){
			$this->db->where('real_estate_offer_type', $real_estate_offer_type);
		}

		if(isset($id_real_estate)){
			$this->db->where_in('id_real_estate', $id_real_estate);
		}
		
		$this->db->update($this->real_estate, $data);
	}

	function handler_delete($id_real_estate = 0){
		$this->db->where('id_real_estate', $id_real_estate);
		$this->db->delete($this->real_estate);
	}

	function handler_get($id_real_estate = 0){
		$this->db->select($this->real_estate_columns);
		$this->db->select(" IF(re.real_estate_discount_type = 'no', 0, 1) has_discount ");
		$this->db->from("{$this->real_estate} re");
		$this->db->join("{$this->real_estate_categories} re_c", "re.id_category = re_c.id_category", "inner");

		$this->db->where('id_real_estate', $id_real_estate);
		return $this->db->get()->row_array();
	}

	function handler_get_best($conditions = array()){
		extract($conditions);

		$this->db->select($this->real_estate_columns);
		$this->db->select(" IF(re.real_estate_discount_type = 'no', 0, 1) has_discount ");
		$this->db->from("{$this->real_estate} re");
		$this->db->join("{$this->real_estate_categories} re_c", "re.id_category = re_c.id_category", "inner");

		$this->db->where('real_estate_best', 1);
		
		if(isset($real_estate_type)){
			$this->db->where('real_estate_offer_type', $real_estate_type);
		}

		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	function handler_get_search($keywords = ''){
		$keywords = trim($keywords);
		$result = array();
		if(empty($keywords)){
			return $result;
		}
		
		$where = array();
		$where_title = array();
		$words = array_filter(explode(' ', $keywords));
		$filtered_words = array();
		foreach($words as $word){
			$temp_word = trim($word);
			if(!empty($temp_word)){
				$filtered_words[] = $temp_word;
			}
		}

		if(empty($filtered_words)){
			return $result[] = 0;
		}
		
		foreach ($filtered_words as $key_word => $word) {
			if(mb_strlen($word) >= 1){
				$where_title[] = " real_estate_search_info LIKE '%".$this->db->escape_like_str($word)."%' ESCAPE '!' ";
			}
		}

		if(!empty($where_title)){
			$where[] = implode(" AND ", $where_title);
		} else{
			return array(0);
		}
			
		$sql = "SELECT id_real_estate
				FROM {$this->real_estate} 
				WHERE " . implode(" OR ", $where);

		$records = $this->db->query($sql)->result();
		if(!empty($records)){
			foreach ($records as $record) {
				$result[] = $record->id_real_estate;
			}
		} else{
			$result[] = 0;
		}
		
		return $result;
	}

	function handler_get_all($conditions = array()){
		$order_by = " re.real_estate_date_created DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if (isset($keywords)) {
			$search = get_keywords_combination($keywords);
			if(!empty($search)){
				$order_by .= " , search_relevance DESC ";
				$this->real_estate_columns .= " , IF(re.real_estate_search_info LIKE '%".$this->db->escape_like_str($keywords)."%' ESCAPE '!', (MATCH (re.real_estate_search_info) AGAINST ('".$this->db->escape_like_str($search)."') + 100), MATCH (re.real_estate_search_info) AGAINST ('".$this->db->escape_like_str($search)."')) as search_relevance ";
			}
		}

		$this->db->select($this->real_estate_columns);
		$this->db->select(" IF(re.real_estate_discount_type = 'no', 0, 1) has_discount ");
		$this->db->from("{$this->real_estate} re");
		$this->db->join("{$this->real_estate_categories} re_c", "re.id_category = re_c.id_category", "inner");

		$this->db->where('re.real_estate_deleted', 0);

        if(isset($id_real_estate)){
			$this->db->where_in('re.id_real_estate', $id_real_estate);
        }

        if(isset($images_actualized)){
			$this->db->where('re.images_actualized', $images_actualized);
        }

        if(isset($not_id_real_estate)){
			$this->db->where_not_in('re.id_real_estate', $not_id_real_estate);
        }

        if(isset($id_manager)){
			$this->db->where_in('re.id_manager', $id_manager);
        }

        if(isset($id_manager_created)){
			$this->db->where_in('re.id_manager_created', $id_manager_created);
        }

        if(isset($id_location)){
			if(is_array($id_location)){
				$location_where = array(
					' re.id_location IN ('.implode(',', $id_location).')'
				);
				foreach ($id_location as $_location) {
					$location_where[] = ' FIND_IN_SET('.$_location.', re.id_location_aditional) ';
				}
			} else{
				$this->db->where_in('re.id_location', $id_location);
				$location_where = array(
					' re.id_location = '. $id_location,
					'FIND_IN_SET('.$id_location.', re.id_location_aditional)'
				);
			}

			$this->db->where('('.implode(' OR ', $location_where).')');
		}

        if(isset($id_category)){
			$this->db->where_in('re.id_category', $id_category);
		}

        if(isset($category_group)){
			$this->db->where('re_c.category_group', $category_group);
		}
		
        if(isset($price_from)){
			$this->db->where('re.real_estate_price >=', $price_from);
        }
		
        if(isset($price_to)){
			$this->db->where('re.real_estate_price <=', $price_to);
        }
		
        if(isset($offer_type)){
			$this->db->where('re.real_estate_offer_type', $offer_type);
        }
		
        if(isset($show_on_home)){
			$this->db->where('re.real_estate_show_on_home', $show_on_home);
        }
		
        if(isset($best)){
			$this->db->where('re.real_estate_best', $best);
        }
		
        if(isset($exclude_last_floor)){
			$this->db->where('re.real_estate_last_floor', 0);
        }
		
        if(isset($mortgage)){
			$this->db->where('re.real_estate_mortgage', $mortgage);
        }
		
        if(isset($discount_type)){
			$this->db->where_in('re.real_estate_discount_type', $discount_type);
        }

        if(isset($real_estate_active)){
			$this->db->where('re.real_estate_active', $real_estate_active);
		}

		if(isset($created_from)){
			$this->db->where("DATE(re.real_estate_date_created) >= DATE('{$created_from}')");
		}

		if(isset($real_estate_sold_date)){
			$this->db->where("DATE(re.real_estate_sold_date) <= DATE('{$real_estate_sold_date}')");
			$this->db->where("re.real_estate_sold", 1);
			$this->db->where("re.real_estate_sold_date != '0000-00-00 00:00:00'");
		} elseif(isset($real_estate_sold)){
			$this->db->where("re.real_estate_sold", 1);
        }
		
		if (isset($keywords)) {			
			$search_in = $this->handler_get_search($keywords);
			if(!empty($search_in)){
				$this->db->where_in('re.id_real_estate', $search_in);
			}
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		} elseif (isset($limit)) {
			$this->db->limit($limit);
		}

		// echo $this->db->get_compiled_select();
		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
		extract($conditions);
		
		$this->db->select("*");
		$this->db->from("{$this->real_estate} re");
		$this->db->join("{$this->real_estate_categories} re_c", "re.id_category = re_c.id_category", "inner");

		$this->db->where('re.real_estate_deleted', 0);

        if(isset($id_real_estate)){
			$this->db->where_in('re.id_real_estate', $id_real_estate);
        }

        if(isset($not_id_real_estate)){
			$this->db->where_not_in('re.id_real_estate', $not_id_real_estate);
        }

        if(isset($id_manager)){
			$this->db->where_in('re.id_manager', $id_manager);
        }

        if(isset($id_manager_created)){
			$this->db->where_in('re.id_manager_created', $id_manager_created);
        }

        if(isset($id_location)){
			if(is_array($id_location)){
				$location_where = array(
					' re.id_location IN ('.implode(',', $id_location).')'
				);
				foreach ($id_location as $_location) {
					$location_where[] = ' FIND_IN_SET('.$_location.', re.id_location_aditional) ';
				}
			} else{
				$this->db->where_in('re.id_location', $id_location);
				$location_where = array(
					' re.id_location = '. $id_location,
					'FIND_IN_SET('.$id_location.', re.id_location_aditional)'
				);
			}

			$this->db->where('('.implode(' OR ', $location_where).')');
		}

        if(isset($id_category)){
			$this->db->where_in('re.id_category', $id_category);
		}

        if(isset($category_group)){
			$this->db->where('re_c.category_group', $category_group);
		}
		
        if(isset($price_from)){
			$this->db->where('re.real_estate_price >=', $price_from);
        }
		
        if(isset($price_to)){
			$this->db->where('re.real_estate_price <=', $price_to);
        }
		
        if(isset($offer_type)){
			$this->db->where('re.real_estate_offer_type', $offer_type);
        }
		
        if(isset($show_on_home)){
			$this->db->where('re.real_estate_show_on_home', $show_on_home);
        }
		
        if(isset($best)){
			$this->db->where('re.real_estate_best', $best);
        }
		
        if(isset($exclude_last_floor)){
			$this->db->where('re.real_estate_last_floor', 0);
        }
		
        if(isset($mortgage)){
			$this->db->where('re.real_estate_mortgage', $mortgage);
        }
		
        if(isset($discount_type)){
			$this->db->where_in('re.real_estate_discount_type', $discount_type);
        }

        if(isset($real_estate_active)){
			$this->db->where('re.real_estate_active', $real_estate_active);
		}

		if(isset($created_from)){
			$this->db->where("DATE(re.real_estate_date_created) >= DATE('{$created_from}')");
		}

		if(isset($real_estate_sold_date)){
			$this->db->where("DATE(re.real_estate_sold_date) <= DATE('{$real_estate_sold_date}')");
			$this->db->where("re.real_estate_sold", 1);
			$this->db->where("re.real_estate_sold_date != '0000-00-00 00:00:00'");
		} elseif(isset($real_estate_sold)){
			$this->db->where("re.real_estate_sold", 1);
        }
		
		if (isset($keywords)) {			
			$search_in = $this->handler_get_search($keywords);
			if(!empty($search_in)){
				$this->db->where_in('re.id_real_estate', $search_in);
			}
		}

		return $this->db->count_all_results();
	}

	public function handler_get_category_locations($id_category = 0){
		if(!$id_category){
			return false;
		}

		$id_category = (int)$id_category;
		$sql = "SELECT l.*
				FROM {$this->locations} l
				INNER JOIN {$this->real_estate} re on l.id_location = re.id_location
				WHERE re.id_category = {$id_category}
				GROUP BY l.id_location";

		$records = $this->db->query($sql)->result_array();
		$result = array();
		if(!empty($records)){
			foreach ($records as $record) {
				$result[] = $record['id_location'];
			}
		}

		return $result;
	}

	// CATEGORIES
	function handler_insert_category($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->real_estate_categories, $data);
		return $this->db->insert_id();
	}

	function handler_update_category($id_category = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_category', $id_category);
		$this->db->update($this->real_estate_categories, $data);
	}
	
	function handler_get_category($id_category = 0){
		$this->db->where('id_category', $id_category);
		return $this->db->get($this->real_estate_categories)->row_array();
	}
	
	function handler_get_category_url($url_category = '', $lang = false){
		if(empty($url_category)){
			return FALSE;
		}

		if($lang === false){
			$this->db->where('url_ro', $url_category);
			$this->db->or_where('url_ru', $url_category);
			$this->db->or_where('url_en', $url_category);
		} else{
			$this->db->where("url_{$lang}", $url_category);
		}

		return $this->db->get($this->real_estate_categories)->row_array();
	}
	
	function handler_get_categories($conditions = array()){
		$order_by = " category_type ASC, category_weight ASC ";
		$category_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		if(isset($category_type)){
			$this->db->where('category_type', $category_type);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->real_estate_categories)->result_array();
	}

	function handler_count_categories($conditions = array()){
		$category_deleted = 0;

		extract($conditions);
		
		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		if(isset($category_type)){
			$this->db->where('category_type', $category_type);
		}

		return $this->db->count_all_results($this->real_estate_categories);
	}

	function handler_get_manager_categories($id_manager = 0, $conditions = array()){
		$this->db->select('rc.*, COUNT(re.id_real_estate) as total_adds');
		$this->db->from("{$this->real_estate_categories} rc");
		$this->db->join("{$this->real_estate} re", "rc.id_category = re.id_category", 'inner');
		$this->db->join("{$this->users} u", "re.id_manager = u.id_user", 'inner');
		
		extract($conditions);

		$this->db->where('u.id_user', $id_manager);
		
		if(isset($real_estate_active)){
			$this->db->where('re.real_estate_active', $real_estate_active);
		}

		if(isset($category_active)){
			$this->db->where('rc.category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('rc.category_deleted', $category_deleted);
		}

		$this->db->group_by('rc.id_category');
		$this->db->order_by('rc.category_weight');

		return $this->db->get()->result_array();
	}

	// PROPERTIES
	function handler_insert_property($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->real_estate_properties, $data);
		return $this->db->insert_id();
	}

	function handler_update_property($id_property = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_property', $id_property);
		$this->db->update($this->real_estate_properties, $data);
	}
	
	function handler_get_property($id_property = 0){
		$this->db->where('id_property', $id_property);
		return $this->db->get($this->real_estate_properties)->row_array();
	}
	
	function handler_get_property_by_alias($property_alias = false, $conditions = array()){
		if(!$property_alias || empty($property_alias)){
			return false;
		}

		extract($conditions);

		if(isset($id_category)){
			$this->db->where("FIND_IN_SET({$id_category}, property_categories)");
		}

		$this->db->where('property_alias', $property_alias);
		$this->db->limit(1);
		return $this->db->get($this->real_estate_properties)->row_array();
	}

	function handler_delete_property($id_property = 0){
		$this->db->where('id_property', $id_property);
		$this->db->delete($this->real_estate_properties);
	}
	
	function handler_get_properties($conditions = array()){
		$order_by = " property_weight ASC ";
		$category_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(",", $multi_order_by);
		}

		if(isset($id_property)){
			$this->db->where_in("id_property", $id_property);
		}

		if(isset($property_on_item)){
			$this->db->where("property_on_item", $property_on_item);
		}

		if(isset($property_in_filter)){
			$this->db->where("property_in_filter", $property_in_filter);
		}

		if(isset($property_type)){
			$this->db->where_in("property_type", $property_type);
		}

		if(isset($id_category)){
			if(!is_array($id_category)){
				$id_category = explode(',', $id_category);
			}

			if(!empty($id_category)){
				$find_in_set = array();
				foreach ($id_category as $category) {
					$find_in_set[] = "FIND_IN_SET({$category}, property_categories)";
				}
				$this->db->where("(".implode(" OR ", $find_in_set).")");
			}
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->real_estate_properties)->result_array();
	}

	function handler_count_properties($conditions = array()){
		extract($conditions);
		
		if(isset($id_property)){
			$this->db->where_in("id_property", $id_property);
		}

		if(isset($property_on_item)){
			$this->db->where("property_on_item", $property_on_item);
		}

		if(isset($property_in_filter)){
			$this->db->where("property_in_filter", $property_in_filter);
		}

		if(isset($property_type)){
			$this->db->where_in("property_type", $property_type);
		}

		if(isset($id_category)){
			if(!is_array($id_category)){
				$id_category = explode(',', $id_category);
			}

			if(!empty($id_category)){
				$find_in_set = array();
				foreach ($id_category as $category) {
					$find_in_set[] = "FIND_IN_SET({$category}, property_categories)";
				}
				$this->db->where("(".implode(" OR ", $find_in_set).")");
			}
		}

		return $this->db->count_all_results($this->real_estate_properties);
	}

	// PROPERTIES VALUES
	function handler_insert_real_estate_properties_values($data = array()){
		if(empty($data)){
			return;
		}

		return $this->db->insert_batch($this->real_estate_properties_values, $data);
	}
	
	function handler_get_real_estate_properties_values($id_real_estate = 0){
		$this->db->where("id_real_estate", $id_real_estate);

		return $this->db->get($this->real_estate_properties_values)->result_array();
	}
	
	function handler_get_real_estate_property_value($id_real_estate = 0, $id_property = 0){
		$this->db->where("id_real_estate", $id_real_estate);
		$this->db->where("id_property", $id_property);
		$this->db->limit(1);

		return $this->db->get($this->real_estate_properties_values)->row_array();
	}
	
	function handler_delete_real_estate_properties_values($id_real_estate = 0){
		$this->db->where("id_real_estate", $id_real_estate);

		return $this->db->delete($this->real_estate_properties_values);
	}
	
	function handler_get_properties_values($conditions = array()){
		$order_by = " p.property_weight ASC ";

		extract($conditions);

		$this->db->select("p.*, pv.*");
		$this->db->from("{$this->real_estate_properties_values} pv");
		$this->db->join("{$this->real_estate_properties} p", "pv.id_property = p.id_property", "INNER");

		if(isset($id_real_estate)){
			$this->db->where_in("pv.id_real_estate", $id_real_estate);
		}

		if(isset($property_on_item)){
			$this->db->where("p.property_on_item", $property_on_item);
		}

		if(isset($property_in_filter)){
			$this->db->where("p.property_in_filter", $property_in_filter);
		}

		if(isset($property_type)){
			$this->db->where_in("p.property_type", $property_type);
		}

		if(isset($property_range_from) || isset($property_range_to)){
			$this->db->where("p.property_type", "range");

			if(isset($property_range_from)){
				$this->db->where("pv.property_value >= ", $property_range_from);
			}
	
			if(isset($property_range_to)){
				$this->db->where("pv.property_value <= ", $property_range_to);
			}
		}

		if(isset($id_category)){
			if(!is_array($id_category)){
				$id_category = explode(',', $id_category);
			}

			if(!empty($id_category)){
				$find_in_set = array();
				foreach ($id_category as $category) {
					$find_in_set[] = "FIND_IN_SET({$category}, p.property_categories)";
				}
				$this->db->where("(".implode(" OR ", $find_in_set).")");
			}
		}

		return $this->db->get()->result_array();
	}

	function handler_get_real_estate_by_property_values($conditions = array()){
		extract($conditions);

		if(isset($id_real_estate)){
			$this->db->where_in("id_real_estate", $id_real_estate);
		}

		if(isset($id_property)){
			$this->db->where("id_property", $id_property);
		}

		if(isset($property_values)){
			$this->db->where_in("property_value", $property_values);
		}

		if(isset($property_value_sufix)){
			$this->db->where_in("property_value_sufix", $property_value_sufix);
		}

		if(isset($property_range_from) || isset($property_range_to)){
			if(isset($property_range_from)){
				$this->db->where("property_value >= ", $property_range_from);
			}
	
			if(isset($property_range_to)){
				$this->db->where("property_value <= ", $property_range_to);
			}
		}

		$records = $this->db->get($this->real_estate_properties_values)->result_array();

		$re_list = array();
		foreach ($records as $record) {
			$re_list[$record['id_real_estate']] = $record['id_real_estate'];
		}

		return $re_list;
	}

	// ICONS
	function handler_insert_icon($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->real_estate_icons, $data);
		return $this->db->insert_id();
	}

	function handler_update_icon($id_icon = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_icon', $id_icon);
		$this->db->update($this->real_estate_icons, $data);
	}

	function handler_delete_icon($id_icon = 0){
		$this->db->where('id_icon', $id_icon);
		$this->db->delete($this->real_estate_icons);
	}
	
	function handler_get_icon($id_icon = 0){
		$this->db->where('id_icon', $id_icon);
		return $this->db->get($this->real_estate_icons)->row_array();
	}

	function handler_get_icons($conditions = array()){
		$order_by = 'icon_weight ASC';
		extract($conditions);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		$this->db->order_by($order_by);
		return $this->db->get($this->real_estate_icons)->result_array();
	}

	function handler_count_icons($conditions = array()){
		extract($conditions);
		
		return $this->db->count_all_results($this->real_estate_icons);
	}

	function handler_insert_icons_relation($data = array()){
		if(empty($data)){
			return;
		}

		return $this->db->insert_batch($this->real_estate_icons_relation, $data);
	}

	function handler_delete_icons_relation($conditions = array()){
		extract($conditions);

		if(isset($id_icon)){
			$this->db->where('id_icon', $id_icon);
		}

		if(isset($id_real_estate)){
			$this->db->where('id_real_estate', $id_real_estate);
		}
		
		$this->db->delete($this->real_estate_icons_relation);
	}
	
	function handler_get_icon_weight($conditions = array()){
		$direction = 'down';
		$order_by = 'icon_weight DESC';
        extract($conditions);

		$this->db->select('*'); // set selected columns
		$this->db->from($this->real_estate_icons);  // set from what table(s)
        if(isset($icon_weight)){
            if($direction == 'down'){
                $this->db->where('icon_weight >= ', $icon_weight + 1);
				$order_by = 'icon_weight ASC';
            } else{
                $this->db->where('icon_weight <= ', $icon_weight - 1);
            }
        }
		
		$this->db->order_by($order_by);
		$this->db->limit(1);
		
        return $this->db->get()->row_array();
	}

	// DELETED REAL ESTATE SAME PARAMS
	function handler_insert_deleted($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->real_estate_deleted, $data);
	}

	function handler_get_deleted($id_real_estate = 0){
		$this->db->where('id_real_estate', $id_real_estate);
		return $this->db->get($this->real_estate_deleted)->row_array();
	}
}