<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Calculator extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
	}

	function _show_body($params = array()){
		$currency_symbol = '&euro;';
		$price_min = $price_default = $this->lsettings->item('calc_price_min');
		$price_max = $this->lsettings->item('calc_price_max');
		$interest_rate_min = (int)$this->lsettings->item('calc_interest_rate_min');
		$interest_rate_max = (int)$this->lsettings->item('calc_interest_rate_max');
		$interest_rate_default = (int)$this->lsettings->item('calc_interest_rate_default');
		$first_rate_min = (int)$this->lsettings->item('calc_first_rate_min');
		$first_rate_max = (int)$this->lsettings->item('calc_first_rate_max');
		$first_rate_default = (int)$this->lsettings->item('calc_first_rate_default');
		$period_min = (int)$this->lsettings->item('calc_period_min');
		$period_max = (int)$this->lsettings->item('calc_period_max');
		$period_default = (int)$this->lsettings->item('calc_period_default');
		$calc_on_item = false;

		extract($params);

		$this->data = array(
			'currency_symbol' => $currency_symbol,
			'price_min' => (int)preg_replace('/[^0-9]/', '', $price_min),
			'price_max' => (int)preg_replace('/[^0-9]/', '', $price_max),
			'real_price' => $price_default,
			'price_default' => (int)preg_replace('/[^0-9]/', '', $price_default),
			'interest_rate_min' => $interest_rate_min,
			'interest_rate_max' => $interest_rate_max,
			'interest_rate_default' => $interest_rate_default,
			'first_rate_min' => $first_rate_min,
			'first_rate_max' => $first_rate_max,
			'first_rate_default' => $first_rate_default,
			'period_min' => $period_min,
			'period_max' => $period_max,
			'period_default' => $period_default,
			'calc_on_item' => $calc_on_item
		);

		return $this->load->view(get_theme_view('modules/calculator/body_view'), $this->data, true);
	}
}