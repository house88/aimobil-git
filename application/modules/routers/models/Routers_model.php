<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Routers_model extends CI_Model{
	
	var $routings = "routings";
	
	function __construct(){
		parent::__construct();
	}
	
	function handler_get($route_key = ''){
        if(empty($route_key)){
			return false;
		}

		$this->db->where('route_key', $route_key);		
		return $this->db->get($this->routings)->row_array();
	}
	
	function handler_update($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->routings, $data, 'route_key');
	}
	
	function handler_get_all($conditions = array()){
        extract($conditions);
		
		return $this->db->get($this->routings)->result_array();
	}

	function handler_count_all($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->routings);
	}
}