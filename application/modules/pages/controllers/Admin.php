<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_pages')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Страницы';
		
		$this->load->model("Pages_model", "pages");
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/pages/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/modules/pages/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$page = $this->uri->segment(4);
		switch ($page) {
			case 'credit-ipotecar':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Ипотека';
				$this->data['main_content'] = 'admin/modules/pages/special/credit-ipotecar_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'add_ad':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Добавить объявление';
				$this->data['main_content'] = 'admin/modules/pages/special/add_ad_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'partners':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Партнеры';
				$this->data['main_content'] = 'admin/modules/pages/special/partners_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'blog':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Все блоги';
				$this->data['main_content'] = 'admin/modules/pages/special/blog_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'complexe-locative':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Жилые комплексы';
				$this->data['main_content'] = 'admin/modules/pages/special/complexe-locative_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'discounts':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Низкие цены';
				$this->data['main_content'] = 'admin/modules/pages/special/discounts_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'servicii-imobiliare':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Услуги';
				$this->data['main_content'] = 'admin/modules/pages/special/servicii-imobiliare_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'companie-imobiliara':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - О Acces Imobil';
				$this->data['main_content'] = 'admin/modules/pages/special/companie-imobiliara_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'posturi-vacante':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Вакансии';
				$this->data['main_content'] = 'admin/modules/pages/special/posturi-vacante_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'contacts':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Контакты';
				$this->data['main_content'] = 'admin/modules/pages/special/contacts_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'apartment-planning':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Планировки квартир';
				$this->data['main_content'] = 'admin/modules/pages/special/apartment-planning_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'home':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}

				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Главная';
				$this->data['main_content'] = 'admin/modules/pages/special/home_view';
				$this->load->view('admin/page', $this->data);
			break;
			case 'real_estate_deleted':
				$this->data['page'] = $this->pages->handler_get_by_alias($page);
				if(empty($this->data['page'])){
					$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
					redirect('admin');
				}
		
				$this->data['page']['page_blocks'] = json_decode($this->data['page']['page_blocks'], true);
				$this->data['page']['video'] = json_decode($this->data['page']['video'], true);
				$this->data['main_title'] = 'Страница - Объект был удален';
				$this->data['main_content'] = 'admin/modules/pages/special/real_estate_deleted_view';
				$this->load->view('admin/page', $this->data);
			break;
			default:
				redirect('admin');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_ro', 'Спец. ссылка RO', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_ru', 'Спец. ссылка RU', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url_en', 'Спец. ссылка En', 'trim|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}

				$insert = array(
					'page_image_ro' => (file_exists('files/'.$this->input->post('sidebar_image_ro')))?$this->input->post('sidebar_image_ro'):'',
					'page_image_ru' => (file_exists('files/'.$this->input->post('sidebar_image_ru')))?$this->input->post('sidebar_image_ru'):'',
					'page_image_en' => (file_exists('files/'.$this->input->post('sidebar_image_en')))?$this->input->post('sidebar_image_en'):'',
					'page_name_ro' => $this->input->post('title_ro'),
					'page_name_ru' => $this->input->post('title_ru'),
					'page_name_en' => $this->input->post('title_en'),
					'page_text_ro' => $this->input->post('description_ro'),
					'page_text_ru' => $this->input->post('description_ru'),
					'page_text_en' => $this->input->post('description_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'show_calc' => ($this->input->post('show_calc'))?1:0,
					'url_special' => ($this->input->post('url_ro'))?1:0
				);

				$id_page = $this->pages->handler_insert($insert);

				$config = array(
					'table' => 'pages',
					'id' => 'id_page',
					'field' => 'url_ru',
					'title' => 'page_name_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ro' => ($this->input->post('url_ro'))?$this->slug->create_slug($this->input->post('url_ro')):$this->slug->create_slug(cut_str($insert['page_name_ro'], 200).'-'.$id_page),
					'url_ru' => ($this->input->post('url_ru'))?$this->slug->create_slug($this->input->post('url_ru')):$this->slug->create_slug(cut_str($insert['page_name_ru'], 200).'-'.$id_page),
					'url_en' => ($this->input->post('url_en'))?$this->slug->create_slug($this->input->post('url_en')):$this->slug->create_slug(cut_str($insert['page_name_en'], 200).'-'.$id_page)
				);
				$this->pages->handler_update($id_page, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				$this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_page = (int)$this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				$update = array(
					'page_image_ro' => (file_exists('files/'.$this->input->post('page_image_ro')))?$this->input->post('page_image_ro'):'',
					'page_image_ru' => (file_exists('files/'.$this->input->post('page_image_ru')))?$this->input->post('page_image_ru'):'',
					'page_image_en' => (file_exists('files/'.$this->input->post('page_image_en')))?$this->input->post('page_image_en'):'',
					'page_name_ro' => $this->input->post('title_ro'),
					'page_name_ru' => $this->input->post('title_ru'),
					'page_name_en' => $this->input->post('title_en'),
					'page_text_ro' => $this->input->post('description_ro'),
					'page_text_ru' => $this->input->post('description_ru'),
					'page_text_en' => $this->input->post('description_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'page_poster' => '',
					'page_blocks' => '',
					'video' => ''
				);

				if($this->input->post('page_poster') && file_exists('files/'.$this->input->post('page_poster'))){
					$update['page_poster'] = $this->input->post('page_poster');
				}

				if($this->input->post('page_blocks')){
					$update['page_blocks'] = json_encode($this->input->post('page_blocks'));
				}

				if($this->input->post('video')){
					$update['video'] = json_encode($this->input->post('video'));
				}

				$this->pages->handler_update($id_page, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);

				$records = $this->pages->handler_get_all($params);
				$records_total = $this->pages->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-dtype="warning" data-message="Вы уверены что хотите сделать страницу неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-page="'.$record['id_page'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['page_active'] == 0){
						$status = '<a href="#" data-dtype="success" data-message="Вы уверены что хотите сделать страницу активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-page="'.$record['id_page'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name'		=>  $record['page_name_ru'],
						'dt_actions'	=>  $status
											.' <a href="'.base_url('admin/pages/edit/'.$record['id_page']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить страницу?" data-callback="delete_action" data-page="'.$record['id_page'].'"><i class="fa fa-trash"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'delete':
				$this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_page = (int)$this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				if(empty($page)){
					jsonResponse('Данные не верны.');
				}

				$this->pages->handler_delete($id_page);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_page = (int)$this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				if(empty($page)){
					jsonResponse('Данные не верны.');
				}

				if($page['page_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->pages->handler_update($id_page, array('page_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}