<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->load->model("Pages_model", "pages");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function detail(){
		if($this->lang->is_default()){
			$url_page = $this->uri->segment(3);
		} else{
			$url_page = $this->uri->segment(4);
		}

		$id_page = id_from_link($url_page);
		$this->data['page_detail'] = $this->pages->handler_get($id_page);
		if(empty($this->data['page_detail'])){
			return_404($this->data);
		}

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_dinamyc_uri('pages/detail/id', array(
			'ro' => $this->data['page_detail']['url_ro'],
			'ru' => $this->data['page_detail']['url_ru'],
			'en' => $this->data['page_detail']['url_en']
		));

		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);

		$this->data['og_meta'] = array(
			'page_title_'.$this->ulang => $this->data['page_detail'][lang_column('page_name')],
			'page_mk_'.$this->ulang => $this->data['page_detail'][lang_column('mk')],
			'page_md_'.$this->ulang => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['main_content'] = 'modules/pages/detail_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function special(){
		if($this->lang->is_default()){
			$url_page = $this->uri->segment(1);
		} else{
			$url_page = $this->uri->segment(2);
		}

		$this->data['page_detail'] = $this->pages->handler_get_url($url_page);
		if(empty($this->data['page_detail'])){
			return_404($this->data);
		}

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_dinamyc_uri('pages/special/id', array(
			'ro' => $this->data['page_detail']['url_ro'],
			'ru' => $this->data['page_detail']['url_ru'],
			'en' => $this->data['page_detail']['url_en']
		));

		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);

		$this->data['og_meta'] = array(
			'page_title_'.$this->ulang => $this->data['page_detail'][lang_column('mt')],
			'page_mk_'.$this->ulang => $this->data['page_detail'][lang_column('mk')],
			'page_md_'.$this->ulang => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['active_menu'] = $this->data['page_detail']['page_alias'];

		$this->data['main_content'] = 'modules/pages/detail_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	// DONE
	function contacts(){
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['_init_google_maps'] = true;

		$this->data['langs_uri'] = get_static_uri('contacts');
		
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('contacts');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['rating_w'] = 20*(float)$this->lsettings->item('google_rating');
		$this->data['rating'] = (float)$this->lsettings->item('google_rating');
		$this->data['active_menu'] = 'contacts';
		
		$this->data['main_content'] = 'modules/pages/contacts_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	// DONE
	function about_us(){
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_static_uri('about_us');
		
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('companie-imobiliara');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['active_menu'] = 'about_us';

		$this->data['main_content'] = 'modules/pages/about_us_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	// DONE
	function add_ad(){
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['langs_uri'] = get_static_uri('add_ad');

		$this->data['page_detail'] = $this->pages->handler_get_by_alias('add_ad');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['active_menu'] = 'add_ad';

		$this->data['main_content'] = 'modules/pages/add_ad_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _get_captcha(){
		return array(
			'recaptcha_widget' => $this->recaptcha->getWidget(),
			'recaptcha_script' => $this->recaptcha->getScriptTag()
		);
	}
}