<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{    
    function __construct(){
		parent::__construct();
        $this->config->set_item('language', 'russian');
        
        if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        $this->load->model("Migrate_model", "migrate");
        
		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

    function index(){        
        $this->migrate->handler_migrate();
    }
}
