<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migrate_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function handler_migrate(){
		$queries = array(
			'ALTER TABLE `real_estate_categories` ADD COLUMN `category_image` VARCHAR(1000) NOT NULL AFTER `category_h1_en`;',
			'ALTER TABLE `services` DROP COLUMN `service_image_ro`, DROP COLUMN `service_image_ru`, DROP COLUMN `service_image_en`;',
			'ALTER TABLE `services` ADD COLUMN `service_poster` TEXT NOT NULL AFTER `service_icon`;',
			'ALTER TABLE `real_estate` ADD COLUMN `id_location_aditional` TEXT NOT NULL AFTER `id_location`;',
			"ALTER TABLE `users` ADD COLUMN `user_on_about` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `user_on_site`;",
			'ALTER TABLE `real_estate_categories` ADD COLUMN `category_group` VARCHAR(50) NOT NULL AFTER `id_offer_type_999`;'
		);

		if(!empty($queries)){
			$this->db->query(implode('', $queries));
		}
	}
}