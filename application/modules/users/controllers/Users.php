<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->load->model('Users_model', 'users');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function manager(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(1);
		} else{
			$uri = $this->uri->uri_to_assoc(2);
		}

		$manager_route = $this->routers->handler_get('manager');
		if(empty($manager_route)){
			if ($this->input->is_ajax_request()) {
				jsonResponse('Error: 404 Bad request.', 'error');
			} else{
				return_404($this->data);
			}
		}

		$id_user = id_from_link($uri[$manager_route[lang_column('route')]]);
		$this->users->return_object = false;
		$this->data['manager'] = $this->users->handler_get($id_user);
		if(empty($this->data['manager'])){
			if ($this->input->is_ajax_request()) {
				jsonResponse('Error: 404 Bad request.', 'error');
			} else{
				return_404($this->data);
			}
		}
		
		if($this->data['manager']['user_banned'] == 1){
			if ($this->input->is_ajax_request()) {
				jsonResponse('Error: 404 Bad request.', 'error');
			} else{
				return_404($this->data);
			}
		}

		$page = 1;
		if($this->input->post('page')){
			$page = (int)$this->input->post('page');
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('manager_catalo_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);
		
		$this->load->model('real_estate/Real_estate_model', 'real_estate');
		
		$categories_params = array(
			'real_estate_active' => 1,
			'category_active' => 1,
			'category_deleted' => 0
		);
		$categories = $this->real_estate->handler_get_manager_categories($id_user, $categories_params);
		$groupped_categories = array();
		if(!empty($categories)){
			foreach ($categories as $category) {
				if(!isset($groupped_categories[$category['category_group']])){
					$groupped_categories[$category['category_group']] = $category;
					$groupped_categories[$category['category_group']]['categories'] = array($category['id_category']);
				} else{
					$groupped_categories[$category['category_group']]['categories'][] = $category['id_category'];
				}
			}
		}

		if(!empty($groupped_categories) && count($groupped_categories) > 1){
			$this->data['categories'] = $groupped_categories;
		}

		$real_estate_params = array(
			'id_manager' => $id_user,
			'real_estate_active' => 1,
			'start' => $start,
			'limit' => $limit,
			'sort_by' => array('re_c.category_weight-ASC','re.real_estate_date_created-DESC')
		);

		if($this->input->post('id_category')){
			$category_group = $this->input->post('id_category', true);
			if(!empty($this->data['categories']) && array_key_exists($category_group, $this->data['categories'])){
				$real_estate_params['category_group'] = $category_group;
			}
		}
		
		$this->data['real_estates'] = $this->real_estate->handler_get_all($real_estate_params);
		$this->data['records_total'] = $this->real_estate->handler_get_count($real_estate_params);

		$properties_params = array(
			'property_type' => array('multiselect','range'),
			'property_on_item' => 1
		);
		$properties = $this->real_estate->handler_get_properties_values($properties_params);
		$this->data['re_properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$property_settings = json_decode($property['property_settings'], true);

				switch ($property['property_type']) {
					case 'multiselect':
						$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
						$unit_name = $property_settings['unit'][lang_column('title')];
						if(isset($property_values[$property['property_value']])){
							$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
							if($property['property_value_sufix'] == 1){
								$unit_name = lang_line('label_living', false);
							}

							$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
								'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
								'unit_name' => $unit_name
							);
						}
					break;
					case 'range':
						$property_value = array($property['property_value']);
						
						if($property_settings['repeat_values'] == true){
							$property_value[] = $property['property_value_max'];
						}
						
						$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
						
						$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
							'property_value' => implode('/', $property_value),
							'unit_name' => $unit_name
						);
					break;
				}
			}
		}

		// PAGINATION
		$page_segment = $this->uri->total_segments()+1;
		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'link'		=> $this->data['manager'][lang_column('user_url')],
			'lang' 		=> $this->ulang,
			'cur_page' 	=> $page,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('manager', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		if ($this->input->is_ajax_request()) {
			$result = array(
				'content' => $this->load->view(get_theme_view('modules/users/user_catalog_view'), $this->data, true),
				'pagination' => $this->data['pagination']
			);

			jsonResponse('', 'success', $result);
		}

		$this->data['langs_uri'] = get_dinamyc_uri('manager', array(
			'ro' => $this->data['manager']['user_url_ro'],
			'ru' => $this->data['manager']['user_url_ru'],
			'en' => $this->data['manager']['user_url_en']
		));

		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['manager'][lang_column('mt')],
			lang_column('page_mk') => $this->data['manager'][lang_column('mk')],
			lang_column('page_md') => $this->data['manager'][lang_column('md')],
			'page_image' => get_og_image('files/users/'.$this->data['manager']['user_photo'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['main_content'] = 'modules/users/user_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _get_user($id_user = 0){
		return $this->users->handler_get($id_user);
	}

	function _get_users($params = array()){
		return $this->users->handler_get_all($params);
	}
}