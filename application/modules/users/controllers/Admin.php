<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

		$this->load->model("Users_model", "users");

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){		
		redirect('/admin');
	}

	function managers(){
		if(!$this->lauth->have_right('manage_users')){
			redirect('/admin');
		}

        $this->data['groups'] = arrayByKey($this->users->handler_get_groups_all(), 'id_group');
		$this->data['main_title'] = 'Менеджеры';
		$this->data['main_content'] = 'admin/modules/users/list_view';
		$this->load->view('admin/page', $this->data);
	}

	function add_user(){
		if(!$this->lauth->have_right('manage_users')){
			redirect('/admin');
		}

        $this->data['groups'] = arrayByKey($this->users->handler_get_groups_all(), 'id_group');
        $this->data['positions'] = arrayByKey($this->users->handler_get_positions(), 'id_position');
		$this->data['main_title'] = 'Добавить профиль';
		$this->data['main_content'] = 'admin/modules/users/add_view';
		$this->load->view('admin/page', $this->data);
	}

	function edit_user(){
		if(!$this->lauth->have_right_or('manage_users,manage_personal_profile')){
			redirect('/admin');
		}
		
		$id_user = (int) $this->uri->segment(4);
		if($this->lauth->have_right('manage_personal_profile') && !$id_user){
			$id_user = $this->lauth->id_user();
		}

		$this->users->return_object = false;
		$this->data['user'] = $this->users->handler_get($id_user);
		if(empty($this->data['user'])){
			redirect('/admin');
		}

        $this->data['groups'] = arrayByKey($this->users->handler_get_groups_all(), 'id_group');
        $this->data['positions'] = arrayByKey($this->users->handler_get_positions(), 'id_position');
		$this->data['main_title'] = 'Редактировать профиль';
		$this->data['main_content'] = 'admin/modules/users/edit_view';
		$this->load->view('admin/page', $this->data);
	}

	function groups(){
		if(!$this->lauth->have_right('manage_groups')){
			redirect('/admin');
		}

		$this->data['main_title'] = 'Группы';
		$this->data['main_content'] = 'admin/modules/users/groups/list_view';
		$this->load->view('admin/page', $this->data);
	}

	function groups_rights(){
		if(!$this->lauth->have_right('manage_group_rights')){
			redirect('/admin');
		}

        $this->data['groups'] = $this->users->handler_get_groups_all();
        $this->data['rights'] = $this->users->handler_get_rights_all();
        $this->data['groups_rights'] = arrayByKey($this->users->handler_get_groups_rights_all(), 'gr_key');

		$this->data['main_title'] = 'Права по группам';
		$this->data['main_content'] = 'admin/modules/users/groups/groups_rights_view';
		$this->load->view('admin/page', $this->data);
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add_group':
				if(!$this->lauth->have_right('manage_groups')){
					jsonResponse('Ошибка: Нет прав.');
				}

				$content = $this->load->view('admin/modules/users/groups/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit_group':
				if(!$this->lauth->have_right('manage_groups')){
					jsonResponse('Ошибка: Нет прав.');
				}

				$id_group = (int) $this->uri->segment(5);
				$this->data['group'] = $this->users->handler_get_group($id_group);
				if(empty($this->data['group'])){
					jsonResponse('Данные не верны.');
				}

				$content = $this->load->view('admin/modules/users/groups/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'add_position':
				$content = $this->load->view('admin/modules/users/positions/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit_position':
				$id_position = (int) $this->uri->segment(5);
				$this->data['position'] = $this->users->handler_get_position($id_position);
				if(empty($this->data['position'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/users/positions/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function positions(){
		$this->data['main_title'] = 'Должности';
		$this->data['main_content'] = 'admin/modules/users/positions/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add':
				if(!$this->lauth->have_right('manage_users')){
					jsonDTResponse('Ошибка: Нет прав!');
				}

				$on_site = ($this->input->post('user_on_site'))?1:0;
				$required = '';
				if($on_site){
					$required = 'required|';
				}

				$this->form_validation->set_rules('user_photo', 'Фото', $required.'xss_clean');
				$this->form_validation->set_rules('location', 'Рабочий регион', $required.'xss_clean');
				$this->form_validation->set_rules('user_name_ro', 'Имя RO', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_name_ru', 'Имя RU', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_name_en', 'Имя En', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('group', 'Группа', 'required');
				$this->form_validation->set_rules('position', 'Должность', $required.'xss_clean');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('user_phone', 'Телефон', $required.'xss_clean');
				$this->form_validation->set_rules('user_password', 'Пароль', 'min_length[6]|max_length[50]');
				$this->form_validation->set_rules('confirm_password', 'Повторите пароль', 'required|matches[user_password]');

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$password = $this->input->post('user_password');
				$check_user = $this->users->handler_get_by_conditions(array('email' => $this->input->post('user_email', true)));
				if(!empty($check_user)){
					jsonResponse('Ошибка: Email уже используеться.');
				}

				$id_group = (int)$this->input->post('group', true);
				$group = $this->users->handler_get_group($id_group);
				if(empty($group)){
					jsonResponse('Данные не верны.');
				}

				$id_location = (int)$this->input->post('location', true);
				if($on_site){
					$location = $this->locations->handler_get($id_location);
					if(empty($location)){
						jsonResponse('Данные не верны.');
					}
				}

				$id_position = (int)$this->input->post('position', true);
				if($on_site){
					$position = $this->users->handler_get_position($id_position);
					if(empty($position)){
						jsonResponse('Данные не верны.');
					}
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/users/'.$remove_photo);
					}
				}

				$insert = array(
					'user_photo' => ($this->input->post('user_photo', true))?$this->input->post('user_photo'):'',
					'user_name_ro' => $this->input->post('user_name_ro', true),
					'user_name_ru' => $this->input->post('user_name_ru', true),
					'user_name_en' => $this->input->post('user_name_en', true),
					'user_description_ro' => nl2br($this->input->post('user_description_ro', true)),
					'user_description_ru' => nl2br($this->input->post('user_description_ru', true)),
					'user_description_en' => nl2br($this->input->post('user_description_en', true)),
					'user_phone' => $this->input->post('user_phone', true),
					'user_email' => $this->input->post('user_email', true),
					'registered_date' => date('Y-m-d H:i:s'),
					'id_location' => $id_location,
					'id_group' => $id_group,
					'id_position' => $id_position,
					'user_on_site' => ($this->input->post('user_on_site'))?1:0,
					'mt_ro' => $this->input->post('mt_ro', true),
					'mt_ru' => $this->input->post('mt_ru', true),
					'mt_en' => $this->input->post('mt_en', true),
					'mk_ro' => $this->input->post('mk_ro', true),
					'mk_ru' => $this->input->post('mk_ru', true),
					'mk_en' => $this->input->post('mk_en', true),
					'md_ro' => $this->input->post('md_ro', true),
					'md_ru' => $this->input->post('md_ru', true),
					'md_en' => $this->input->post('md_en', true),
				);

				$password = $this->input->post('user_password');
				if(!empty($password)){
					$insert['user_password'] = $this->lauth->get_password_hash($password);
				}

				$last_user_weight = $this->users->handler_get_weight();
				$user_weight = 1;
				if(!empty($last_user_weight)){
					$user_weight = $last_user_weight['user_weight'] + 1;
				}
				$insert['user_weight'] = $user_weight;

				$id_user = $this->users->handler_insert($insert);

				$config = array(
					'table' => 'users',
					'id' => 'id_user',
					'field' => 'user_url_ro',
					'title' => 'user_name_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'user_url_ro' => $this->slug->create_slug(cut_str($insert['user_name_ro'], 80).'-'.$id_user),
					'user_url_ru' => $this->slug->create_slug(cut_str($insert['user_name_ru'], 80).'-'.$id_user),
					'user_url_en' => $this->slug->create_slug(cut_str($insert['user_name_en'], 80).'-'.$id_user)
				);
				$this->users->handler_update($id_user, $update);
				Modules::run('real_estate/update_search_info', array('id_manager' => $id_user));
				jsonResponse('Сохранено.','success');
			break;
			case 'edit':
				if(!$this->lauth->have_right_or('manage_users,manage_personal_profile')){
					jsonDTResponse('Ошибка: Нет прав!');
				}

				$on_site = ($this->input->post('user_on_site'))?1:0;
				$required = '';
				if($on_site){
					$required = 'required|';
				}

				$this->form_validation->set_rules('user_photo', 'Фото', $required.'xss_clean');
				$this->form_validation->set_rules('location', 'Рабочий регион', $required.'xss_clean');
				$this->form_validation->set_rules('user_name_ro', 'Имя RO', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_name_ru', 'Имя RU', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_name_en', 'Имя En', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('id_user', 'Пользователь', 'required');
				$this->form_validation->set_rules('user_phone', 'Телефон', $required.'xss_clean');
				if($this->lauth->have_right('manage_users')){
					$this->form_validation->set_rules('group', 'Группа', 'required');
					$this->form_validation->set_rules('position', 'Должность', $required.'xss_clean');
				}
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('user_password', 'Новый пароль', 'min_length[6]|max_length[50]');
				$password = $this->input->post('user_password');
				if(!empty($password)){
					$this->form_validation->set_rules('confirm_password', 'Повторите новый пароль', 'required|matches[user_password]');
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int) $this->input->post('id_user');
				if(!$this->lauth->have_right('manage_users')){
					$id_user = $this->lauth->id_user();
				}

				$user_info = $this->users->handler_get($id_user);
				if(empty($user_info)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/users/'.$remove_photo);
					}
				}

				$config = array(
					'table' => 'users',
					'id' => 'id_user',
					'field' => 'user_url_ro',
					'title' => 'user_name_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);

				$update = array(
					'user_photo' => ($this->input->post('user_photo', true))?$this->input->post('user_photo'):'',
					'user_name_ro' => $this->input->post('user_name_ro', true),
					'user_name_ru' => $this->input->post('user_name_ru', true),
					'user_name_en' => $this->input->post('user_name_en', true),
					'user_url_ro' => $this->slug->create_slug(cut_str($this->input->post('user_name_ro', true), 80).'-'.$id_user),
					'user_url_ru' => $this->slug->create_slug(cut_str($this->input->post('user_name_ru', true), 80).'-'.$id_user),
					'user_url_en' => $this->slug->create_slug(cut_str($this->input->post('user_name_en', true), 80).'-'.$id_user),
					'user_description_ro' => nl2br($this->input->post('user_description_ro', true)),
					'user_description_ru' => nl2br($this->input->post('user_description_ru', true)),
					'user_description_en' => nl2br($this->input->post('user_description_en', true)),
					'user_phone' => $this->input->post('user_phone', true),
					'mt_ro' => $this->input->post('mt_ro', true),
					'mt_ru' => $this->input->post('mt_ru', true),
					'mt_en' => $this->input->post('mt_en', true),
					'mk_ro' => $this->input->post('mk_ro', true),
					'mk_ru' => $this->input->post('mk_ru', true),
					'mk_en' => $this->input->post('mk_en', true),
					'md_ro' => $this->input->post('md_ro', true),
					'md_ru' => $this->input->post('md_ru', true),
					'md_en' => $this->input->post('md_en', true),
					'user_999_api_key' => $this->input->post('user_api_key', true),
				);

				if($this->lauth->have_right('manage_users')){
					$id_group = (int)$this->input->post('group', true);
					$group = $this->users->handler_get_group($id_group);
					if(!empty($group)){
						$update['id_group'] = $id_group;
					}

					$id_location = (int)$this->input->post('location', true);
					if($on_site){
						$location = $this->locations->handler_get($id_location);
						if(!empty($location)){
							$update['id_location'] = $id_location;
						}
					}

					$id_position = (int)$this->input->post('position', true);
					if($on_site){
						$position = $this->users->handler_get_position($id_position);
						if(!empty($position)){
							$update['id_position'] = $id_position;
						}
					}

					$update['user_on_site'] = ($this->input->post('user_on_site'))?1:0;
				}

				if($user_info->user_email != $this->input->post('user_email', true)){
					$check_user = $this->users->handler_get_by_conditions(array('email' => $this->input->post('user_email', true)));
					if(!empty($check_user)){
						jsonResponse('Ошибка: Email уже используеться.');
					}
				}

				$update['user_email'] = $this->input->post('user_email', true);

				$password = $this->input->post('user_password');
				if(!empty($password)){
					$update['user_password'] = $this->lauth->get_password_hash($password);
				}

				if(!$this->lauth->have_right('manage_users')){
					$this->session->set_userdata('user_name_ro', $update['user_name_ro']);
					$this->session->set_userdata('user_name_ru', $update['user_name_ru']);
					$this->session->set_userdata('user_name_en', $update['user_name_en']);
					$this->session->set_userdata('user_999_api_key', $update['user_999_api_key']);
				}

				$this->users->handler_update($id_user, $update);
				Modules::run('real_estate/update_search_info', array('id_manager' => $id_user));
				jsonResponse('Сохранено.','success');
			break;
			case 'upload_photo':
				$path = 'files/users';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '10000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 800
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				jsonResponse('', 'success', array("file" => $info));
			break;
			case 'change_on_about':
                if(!$this->lauth->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int)$this->input->post('user');
				$user = $this->users->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Данные не верны.');
				}

				if($user->user_on_about == 1){
					$on_about = 0;
				} else{
					$on_about = 1;
				}

				$this->users->handler_update($id_user, array('user_on_about' => $on_about));
				jsonResponse('Операция прошла успешно.', 'success', array('on_about' => $on_about));
			break;
			case 'change_on_site':
                if(!$this->lauth->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int)$this->input->post('user');
				$user = $this->users->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Данные не верны.');
				}

				if($user->user_on_site == 1){
					$on_site = 0;
				} else{
					$on_site = 1;
				}

				$this->users->handler_update($id_user, array('user_on_site' => $on_site));
				jsonResponse('Операция прошла успешно.', 'success', array('on_site' => $on_site));
			break;
			case 'change_weight':
				if(!$this->lauth->have_right('manage_users')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('id_user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int)$this->input->post('id_user');
				$user = $this->users->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Данные не верны.');
				}

				$params = array(
					'user_weight' => $user->user_weight
				);

				if($this->input->post('direction') != 'down'){
					$params['direction'] = 'up';
				}
				
				$last_user_weight = $this->users->handler_get_weight($params);
				if(!empty($last_user_weight)){
					$user_weight = $last_user_weight['user_weight'];
					$update = array(
						'user_weight' => $user_weight
					);
					$this->users->handler_update($id_user, $update);
					$update = array(
						'user_weight' => $user->user_weight
					);
					$this->users->handler_update($last_user_weight['id_user'], $update);
				}

				jsonResponse('Сохранено.','success');
			break;
			case 'change_banned_status':
                if(!$this->lauth->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int)$this->input->post('user');

				if($id_user == $this->lauth->id_user()){
					jsonResponse('Ошибка: Вы не можете заблокировать свой профиль!');
				}

				$user = $this->users->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Данные не верны.');
				}

				if($user->user_banned == 1){
					$status = 0;
				} else{
					$status = 1;
				}

				$this->users->handler_update($id_user, array('user_banned' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
            case 'delete':
                if(!$this->lauth->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Менеджер', 'required|xss_clean');                
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_user = (int)$this->input->post('user');
				$user = $this->users->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Данные не верны.');
				}

				if($user->group_can_delete_user != 1){
					jsonResponse('Данные не верны.');
				}

				$this->users->handler_delete($id_user);
				jsonResponse('Удалено.', 'success');
            break;
			case 'users_list_dt':
				if(!$this->lauth->have_right('manage_users')){
					jsonDTResponse('Ошибка: Нет прав!');
				}
		
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'group_type' => array('admin','manager')
				);
		
				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_".$this->input->post("iSortCol_{$i}"))) {
							case 'dt_type': 
								$params['sort_by'][] = 'group_type-'.$this->input->post("sSortDir_{$i}");
							break;
							case 'dt_email': 
								$params['sort_by'][] = 'user_email-'.$this->input->post("sSortDir_{$i}");
							break;
							case 'dt_regdate': 
								$params['sort_by'][] = 'registered_date-'.$this->input->post("sSortDir_{$i}");
							break;
						}
					}
				}

				if ($this->input->post('id_group')) {
					$params['id_group'] = (int)$this->input->post('id_group');
				}
		
				$records = $this->users->handler_get_all($params);
				$records_total = $this->users->handler_get_count($params);
		
				$output = array(
					"sEcho" => (int)$this->input->post('sEcho'),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$status = '';
					if ($record['id_user'] != $this->lauth->id_user()) {
						$status = '<a href="#" data-dtype="danger" data-message="Вы уверены что хотите заблокировать пользователя?" title="Заблокировать" data-title="Заблокировать пользователя" data-callback="change_banned_status" data-user="'.$record['id_user'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
						if($record['user_banned'] == 1){
							$status = '<a href="#" data-dtype="success" data-message="Вы уверены что хотите разаблокировать пользователя?" title="Разаблокировать" data-title="Разаблокировать пользователя" data-callback="change_banned_status" data-user="'.$record['id_user'].'" class="btn btn-danger btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
						}
					}

					$on_site = '<a href="#" data-dtype="primary" data-message="Вы уверены что хотите включить показ пользователя на странице О нас?" title="Включить показ пользователя на странице О нас" data-title="Включить показ пользователя на странице О нас" data-callback="change_on_about" data-user="'.$record['id_user'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-minus"></i></a>';
					if($record['user_on_about'] == 1){
						$on_site = '<a href="#" data-dtype="danger" data-message="Вы уверены что хотите выключить показ пользователя на  странице О нас?" title="Выключить показ пользователя на странице О нас" data-title="Выключить показ пользователя на странице О нас" data-callback="change_on_about" data-user="'.$record['id_user'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-check"></i></a>';
					}

					if($record['user_on_site'] == 1){
						$on_site .= ' <a href="#" data-dtype="danger" data-message="Вы уверены что хотите выключить показ пользователя на странице Контакты?" title="Выключить показ пользователя на странице Контакты" data-title="Выключить показ пользователя на странице Контакты" data-callback="change_on_site" data-user="'.$record['id_user'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-check"></i></a>';
					} else{
						$on_site .= ' <a href="#" data-dtype="primary" data-message="Вы уверены что хотите включить показ пользователя на странице Контакты?" title="Включить показ пользователя на странице Контакты" data-title="Включить показ пользователя на странице Контакты" data-callback="change_on_site" data-user="'.$record['id_user'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-minus"></i></a>';
					}

					$on_site .= ' <a class="btn btn-default btn-xs call-function" href="#" title="Поднять" data-callback="change_weight" data-user="'.$record['id_user'].'" data-direction="up"><span class="fa fa-arrow-up"></span></a>
								<a class="btn btn-default btn-xs call-function" title="Отпустить" data-callback="change_weight" data-user="'.$record['id_user'].'" data-direction="down"><span class="fa fa-arrow-down"></span></a>';

					$delete_btn = '';
					if($record['group_can_delete_user'] == 1){
						$delete_btn = '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-title="Удалить" data-message="Вы уверены что хотите удалить менеджера?" data-callback="delete_action" data-user="'.$record['id_user'].'"><i class="fa fa-remove"></i></a>';
					}

					$edit_btn = '';
					if($record['group_type'] != 'admin'){
						$edit_btn = '<a href="'.base_url('admin/users/edit_user/'.$record['id_user']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
					}
					// change_weight

					$output['aaData'][] = array(
						'dt_name'		=>  $record['user_name_ro'],
						'dt_email'		=>  $record['user_email'],
						'dt_phone'		=>  $record['user_phone'],
						'dt_type'		=>  $record['group_name'],
						'dt_on_site'	=>  $on_site,
						'dt_regdate'	=>  formatDate($record['registered_date'], 'd/m/Y H:i:s'),
						'dt_actions'	=>  '<div class="btn-toolbar">
												<div class="btn-group mb-5">
													'.$status.'
													'.$edit_btn.'
													'.$delete_btn.'
												</div>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
            case 'add_group':
                if(!$this->lauth->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('group_type', 'Тип', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type')
                );

				$this->users->handler_insert_group($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit_group':
                if(!$this->lauth->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('group_type', 'Тип', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_group = (int)$this->input->post('group');
				$update = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type')
                );
				$this->users->handler_update_group($id_group, $update);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'delete_group':
                if(!$this->lauth->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');                

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_group = (int)$this->input->post('group');
                if($this->users->handler_get_group_rights($id_group)){
				    jsonResponse('Удалите все связи с правами для данной группы.');
                }
                
                if($this->users->handler_get_group_users($id_group)){
				    jsonResponse('Удалите все связи с пользователями для данной группы.');
                }

				$this->users->handler_delete_group($id_group);
				jsonResponse('Удалено.', 'success');
            break;
			case 'list_groups_dt':
				if(!$this->lauth->have_right('manage_groups')){
					jsonResponse('Ошибка: Нет прав.');
				}

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
		
				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_".$this->input->post("iSortCol_{$i}"))) {
							case 'dt_name': $params['sort_by'][] = 'group_name-'.$this->input->post("sSortDir_{$i}");
							break;
						}
					}
				}
		
				$records = $this->users->handler_get_groups_all($params);
				$records_total = $this->users->handler_get_groups_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				$group_types = array(
					'admin' => array(
						'title' => 'Администратор'
					),
					'manager' => array(
						'title' => 'Менеджер'
					)
				);
		
				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_name'		=>  $record['group_name'],
						'dt_type'		=>  $group_types[$record['group_type']]['title'],
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/users/popup_forms/edit_group/'.$record['id_group']).'" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить группу?" data-callback="delete_action" data-group="'.$record['id_group'].'"><i class="fa fa-trash"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
            case 'change_groups_right':
                if(!$this->lauth->have_right('manage_group_rights')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('right', 'Право', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $group = (int) $this->input->post('group');
                $right = (int) $this->input->post('right');
                if($this->users->handler_get_group_right($group, $right)){
                    $this->users->handler_delete_group_right($group, $right);
                } else{
                    $insert = array(
                        'id_group' => $group,
                        'id_right' => $right
                    );
                    $this->users->handler_insert_group_right($insert);
                }
				jsonResponse('Сохранено.', 'success');
            break;
			// DONE
			case 'add_position':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'position_name_ro' => $this->input->post('title_ro'),
					'position_name_ru' => $this->input->post('title_ru'),
					'position_name_en' => $this->input->post('title_en')
				);

				$this->users->handler_insert_position($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_position':
                $this->form_validation->set_rules('id_position', 'Должность', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_position = (int)$this->input->post('id_position');
				$update = array(
					'position_name_ro' => $this->input->post('title_ro'),
					'position_name_ru' => $this->input->post('title_ru'),
					'position_name_en' => $this->input->post('title_en')
				);

				$this->users->handler_update_position($id_position, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'positions_list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				$records = $this->users->handler_get_positions($params);
				$records_total = $this->users->handler_count_positions($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['position_name_ru'],
						'dt_actions'	=>  ' <a href="#" data-href="'.base_url('admin/users/popup_forms/edit_position/'.$record['id_position']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить должность?" data-callback="delete_action" data-position="'.$record['id_position'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete_position':
                $this->form_validation->set_rules('position', 'Должность', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_position = (int)$this->input->post('position');
				$position = $this->users->handler_get_position($id_position);
				if(empty($position)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->users->handler_update_position($id_position, array('position_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}