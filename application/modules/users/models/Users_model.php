<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model{
	
    public $return_object 	= true;
    private $locations_table = 'locations';
    private $users_table = 'users';
	private $users_groups_table = "users_groups";
	private $users_rights_table = "users_rights";
	private $users_groups_rights_table = "users_groups_rights";
	private $users_positions_table = "users_positions";
    private $ci   = null;
	
	function __construct(){
        $this->ci =& get_instance();
		parent::__construct();
	}

    // USERS FUNCTIONS
    function handler_insert($data = array()){
        if(empty($data)){
            return false;
        }

        $this->db->insert($this->users_table, $data);
		return $this->db->insert_id();
    }

    function handler_update($id_user = 0, $data = array()){
        if(empty($data)){
            return false;
        }

        $this->db->where('id_user', $id_user);
        return $this->db->update($this->users_table, $data);
    }

    function handler_get($id_user = 0){
		$this->db->select("u.*, ug.*, up.*, ul.*, ulp.location_name_ro as plocation_name_ro, ulp.location_name_ru as plocation_name_ru, ulp.location_name_en as plocation_name_en, ulp.location_in_title as plocation_in_title");
		$this->db->from("{$this->users_table} u");
		$this->db->join("{$this->users_groups_table} ug", "u.id_group = ug.id_group", "inner");
		$this->db->join("{$this->users_positions_table} up", "u.id_position = up.id_position", "left");
		$this->db->join("{$this->locations_table} ul", "u.id_location = ul.id_location", "left");
		$this->db->join("{$this->locations_table} ulp", "ul.id_parent = ulp.id_location", "left");
        $this->db->where('id_user', $id_user);
        $query = $this->db->get();

        if($this->return_object){
            return $query->row();
        } else{
            return $query->row_array();            
        }
    }

	function handler_get_by_conditions($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		$this->db->select("u.*, ug.*, up.*, ul.*, ulp.location_name_ro as plocation_name_ro, , ulp.location_name_ru as plocation_name_ru, ulp.location_name_en as plocation_name_en, ulp.location_in_title as plocation_in_title");
		$this->db->from("{$this->users_table} u");
		$this->db->join("{$this->users_groups_table} ug", "u.id_group = ug.id_group", "inner");
		$this->db->join("{$this->users_positions_table} up", "u.id_position = up.id_position", "left");
		$this->db->join("{$this->locations_table} ul", "u.id_location = ul.id_location", "left");
		$this->db->join("{$this->locations_table} ulp", "ul.id_parent = ulp.id_location", "left");

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}

		if(isset($email)){
			$this->db->where('user_email', $email);
		}
		
        $query = $this->db->get();

		if($this->return_object){
            return $query->row();
        } else{
            return $query->row_array();            
        }
	}

    function handler_delete($id_user = 0){
        $this->db->where('id_user', $id_user);
        $this->db->delete($this->users_table);
    }

    function handler_get_all($conditions = array()){
        $order_by = " user_weight ASC, id_user DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("u.*, ug.*, up.*, ul.*, ulp.location_name_ro as plocation_name_ro, , ulp.location_name_ru as plocation_name_ru, ulp.location_name_en as plocation_name_en, ulp.location_in_title as plocation_in_title");
		$this->db->from("{$this->users_table} u");
		$this->db->join("{$this->users_groups_table} ug", "u.id_group = ug.id_group", "inner");
		$this->db->join("{$this->users_positions_table} up", "u.id_position = up.id_position", "left");
		$this->db->join("{$this->locations_table} ul", "u.id_location = ul.id_location", "left");
		$this->db->join("{$this->locations_table} ulp", "ul.id_parent = ulp.id_location", "left");

        if(isset($id_user)){
			$this->db->where_in("u.id_user", $id_user);
        }

        if(isset($id_group)){
			$this->db->where_in("u.id_group", $id_group);
        }

        if(isset($user_banned)){
			$this->db->where("u.user_banned", $user_banned);
        }

        if(isset($on_site)){
			$this->db->where("u.user_on_site", $on_site);
        }

        if(isset($on_about)){
			$this->db->where("u.user_on_about", $on_about);
        }

        if(isset($group_type)){
			$this->db->where_in("ug.group_type", $group_type);
        }

        if(isset($group_alias)){
			$this->db->where_in("ug.group_alias", $group_alias);
        }

        if(isset($registered_date_from)){
			$this->db->where("DATE(u.registered_date) >= DATE('{$registered_date_from}')");
        }

        if(isset($registered_date_to)){
			$this->db->where("DATE(u.registered_date) <= DATE('{$registered_date_to}')");
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
		extract($conditions);
		
		$this->db->from("{$this->users_table} u");
		$this->db->join("{$this->users_groups_table} ug", "u.id_group = ug.id_group", "inner");
		$this->db->join("{$this->locations_table} ul", "u.id_location = ul.id_location", "left");
		$this->db->join("{$this->locations_table} ulp", "ul.id_parent = ulp.id_location", "left");

        if(isset($id_user)){
			$this->db->where_in("u.id_user", $id_user);
        }

        if(isset($id_group)){
			$this->db->where_in("u.id_group", $id_group);
        }

        if(isset($user_banned)){
			$this->db->where("u.user_banned", $user_banned);
        }

        if(isset($on_site)){
			$this->db->where("u.user_on_site", $on_site);
        }

        if(isset($on_about)){
			$this->db->where("u.user_on_about", $on_about);
        }

        if(isset($group_type)){
			$this->db->where_in("ug.group_type", $group_type);
        }

        if(isset($group_alias)){
			$this->db->where_in("ug.group_alias", $group_alias);
        }

        if(isset($registered_date_from)){
			$this->db->where("DATE(u.registered_date) >= DATE('{$registered_date_from}')");
        }

        if(isset($registered_date_to)){
			$this->db->where("DATE(u.registered_date) <= DATE('{$registered_date_to}')");
        }

		return $this->db->count_all_results();
	}
	
	function handler_get_weight($conditions = array()){
		$direction = 'down';
		$order_by = 'user_weight DESC';
        extract($conditions);

		$this->db->select('*'); // set selected columns
		$this->db->from($this->users_table);  // set from what table(s)
        if(isset($user_weight)){
            if($direction == 'down'){
                $this->db->where('user_weight >= ', $user_weight + 1);
				$order_by = 'user_weight ASC';
            } else{
                $this->db->where('user_weight <= ', $user_weight - 1);
            }
        }
		
		$this->db->order_by($order_by);
		$this->db->limit(1);
		
        return $this->db->get()->row_array();
	}
    
	// GROUPS FUNCTIONS
	function handler_insert_group($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->users_groups_table, $data);
		return $this->db->insert_id();
	}

	function handler_update_group($id_group, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where("id_group", $id_group);
		return $this->db->update($this->users_groups_table, $data);
	}

	function handler_delete_group($id_group){
		$this->db->where('id_group', $id_group);			
		return $this->db->delete($this->users_groups_table);
	}

	function handler_get_group($id_group){
		$this->db->where("id_group", $id_group);
		return $this->db->get($this->users_groups_table)->row_array();
	}

	function handler_get_groups_all($conditions = array()){
        $order_by = " id_group ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($group_type)){
			$this->db->where("group_type", $group_type);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->users_groups_table)->result_array();
	}

	function handler_get_groups_count($conditions = array()){
        extract($conditions);

        if(isset($group_type)){
			$this->db->where("$this->users_groups_table.group_type", $group_type);
        }

		return $this->db->count_all_results($this->users_groups_table);
	}

	// RIGHTS FUNCTIONS
	function handler_get_rights_all($conditions = array()){
        $order_by = " id_right ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->users_rights_table)->result_array();
	}

	// GROUP RIGHTS FUNCTIONS
	function handler_insert_group_right($data = array()){
		if(empty($data)){
			return;
		}
		
		return $this->db->insert($this->users_groups_rights_table, $data);
	}

	function handler_delete_group_right($group, $right){
		$this->db->where('id_group', $group);
		$this->db->where('id_right', $right);				
		return $this->db->delete($this->users_groups_rights_table);
	}

	function handler_get_group_right($group, $right){
		$this->db->where('id_group', $group);
		$this->db->where('id_right', $right);				
		return $this->db->get($this->users_groups_rights_table)->row();
	}

	function handler_get_groups_rights_all(){
		$sql = "SELECT CONCAT_WS('_', id_group, id_right) as gr_key, id_group, id_right
				FROM {$this->users_groups_rights_table}";
				
		return $this->db->query($sql)->result_array();
	}

	function handler_get_group_rights($group){
		$this->db->where('id_group', $group);				
		return $this->db->get($this->users_groups_rights_table)->result();
	}

	function handler_get_group_users($group){
		$this->db->where('id_group', $group);				
		return $this->db->get($this->users_table)->result();
	}

	function handler_get_right_groups($right){
		$this->db->where('id_right', $right);				
		return $this->db->get($this->users_groups_rights_table)->result();
	}

	// POSITIONS
	function handler_insert_position($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->users_positions_table, $data);
		return $this->db->insert_id();
	}

	function handler_update_position($id_position = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_position', $id_position);
		$this->db->update($this->users_positions_table, $data);
	}
	
	function handler_get_position($id_position = 0){
		$this->db->where('id_position', $id_position);
		return $this->db->get($this->users_positions_table)->row_array();
	}
	
	function handler_get_positions($conditions = array()){
		$order_by = " id_position ASC ";
		$position_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_position)){
			$this->db->where_in('id_position', $id_position);
		}

		$this->db->where('position_deleted', $position_deleted);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->users_positions_table)->result_array();
	}

	function handler_count_positions($conditions = array()){
		$position_deleted = 0;

		extract($conditions);
		
		if(isset($id_position)){
			$this->db->where_in('id_position', $id_position);
		}

		$this->db->where('position_deleted', $position_deleted);

		return $this->db->count_all_results($this->users_positions_table);
	}
}