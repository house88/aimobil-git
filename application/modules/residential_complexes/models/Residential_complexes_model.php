<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Residential_complexes_model extends CI_Model{
	var $residential_complexes = "residential_complexes";
	var $complexes_icons_relation = "residential_complexes_icons";
	
	function __construct(){
		parent::__construct();
	}

	function db_date(){
		return $this->db->select('NOW() as db_date')->get()->row()->db_date;
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->residential_complexes, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_complex = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_complex', $id_complex);
		$this->db->update($this->residential_complexes, $data);
	}

	function handler_delete($id_complex = 0){
		$this->db->where('id_complex', $id_complex);
		$this->db->delete($this->residential_complexes);
	}

	function handler_get($id_complex = 0){
		$this->db->where('id_complex', $id_complex);
		return $this->db->get($this->residential_complexes)->row_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " complex_date_created DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->where('complex_deleted', 0);

        if(isset($id_complex)){
			$this->db->where_in('id_complex', $id_complex);
        }

        if(isset($complex_active)){
			$this->db->where('complex_active', $complex_active);
		}

        if(isset($complex_on_home)){
			$this->db->where('complex_on_home', $complex_on_home);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}
		
		return $this->db->get($this->residential_complexes)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		$this->db->where('complex_deleted', 0);

        if(isset($id_complex)){
			$this->db->where_in('id_complex', $id_complex);
        }

        if(isset($complex_active)){
			$this->db->where('complex_active', $complex_active);
		}

        if(isset($complex_on_home)){
			$this->db->where('complex_on_home', $complex_on_home);
		}

		return $this->db->count_all_results($this->residential_complexes);
	}

	function handler_insert_icons_relation($data = array()){
		if(empty($data)){
			return;
		}

		return $this->db->insert_batch($this->complexes_icons_relation, $data);
	}

	function handler_delete_icons_relation($conditions = array()){
		extract($conditions);

		if(isset($id_icon)){
			$this->db->where('id_icon', $id_icon);
		}

		if(isset($id_complex)){
			$this->db->where('id_complex', $id_complex);
		}
		
		$this->db->delete($this->complexes_icons_relation);
	}
}