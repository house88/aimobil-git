<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Residential_complexes extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];

		$this->data['active_menu'] = 'residential_complexes';
		$this->data['_init_google_maps'] = true;
		
		$this->load->model("Residential_complexes_model", "complexes");
		$this->load->model("real_estate/Real_estate_model", "real_estate");
		$this->load->model("locations/Locations_model", "locations");
		$this->load->model('users/Users_model', 'users');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(2);
		} else{
			$uri = $this->uri->uri_to_assoc(3);
		}
		
		check_uri($uri, array('page'), $this->data);

		$links_map = array(
			'page' => array(
				'type' => 'uri',
				'deny' => array(),
			)
		);

		$this->data['links_tpl'] = $this->uritpl->make_templates($links_map, $uri);

		$search_params_links_map = array(
			'page' => array(
				'type' => 'uri',
				'deny' => array('page'),
			)
		);

		$search_params_links_tpl = $this->uritpl->make_templates($search_params_links_map, $uri, true);

		$page = 1;
		if(isset($uri['page'])){
			$page = (int)$uri['page'];
		}

		if($page < 1){
			return_404();
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->lsettings->item('residential_complexes_limit');
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);
		
		$params = array(
			'limit' => $limit,
			'start' => $start,
			'complex_active' => 1,
			'complex_deleted' => 0
		);

		$this->data['records_total'] = $this->complexes->handler_get_count($params);
		$this->data['residential_complexes'] = $this->complexes->handler_get_all($params);
		$this->data['locations'] = arrayByKey($this->locations->handler_get_all_full(), 'id_location');
		$this->data['managers'] = arrayByKey($this->users->handler_get_all(), 'id_user');

		$this->data['langs_uri'] = get_dinamyc_uri('residential-complexes', array(
			'ro' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'ru' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'',
			'en' => (!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):''
		));

		// PAGINATION
		if(isset($uri['page'])){
		    $page_segment = $this->uri->total_segments();
		} else{
		    $page_segment = $this->uri->total_segments()+1;
		}

		$pagination_params = array(
			'total' 	=> $this->data['records_total'],
			'limit' 	=> $limit,
			'uri'		=> $uri,
			'link'		=> $search_params_links_tpl['page'],
			'lang' 		=> $this->ulang,
			'segment' 	=> $page_segment
		);
		
		$settings = $this->pagination_lib->get_settings('residential-complexes', $pagination_params);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('complexe-locative');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/residential_complexes/list_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function detail(){
		if($this->lang->is_default()){
			$uri = $this->uri->uri_to_assoc(1);
		} else{
			$uri = $this->uri->uri_to_assoc(2);
		}

		$lang_uri = get_static_uri('residential-complexes', $this->ulang);
		check_uri($uri, array($lang_uri), $this->data);
		
		$id_complex = id_from_link($uri[$lang_uri]);
		$this->data['complex'] = $this->complexes->handler_get($id_complex);
		if(empty($this->data['complex'])){
			return_404($this->data);
		}

        if($this->data['complex']['complex_active'] == 0 || $this->data['complex']['complex_deleted'] == 1){
            return_404($this->data);
        }

		if($this->data['complex'][lang_column('url')] != $uri[$lang_uri]){
			redirect($this->ulang.'/'.get_dinamyc_uri('residential-complexes/detail/id', $this->data['complex'][lang_column('url')], $this->ulang));
		}

		$this->users->return_object = false;
		$this->data['complex_manager'] = $this->users->handler_get($this->data['complex']['id_manager']);

		$this->data['icons'] = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');

		$this->data['langs_uri'] = get_dinamyc_uri('residential-complexes/detail/id', array(
			'ro' => $this->data['complex']['url_ro'],
			'ru' => $this->data['complex']['url_ru'],
			'en' => $this->data['complex']['url_en']
		));

		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['complex'][lang_column('mt')],
			lang_column('page_mk') => $this->data['complex'][lang_column('mk')],
			lang_column('page_md') => $this->data['complex'][lang_column('md')],
			'page_image' => get_og_image('files/residential_complexes/'.$this->data['complex']['id_complex'].'/'.$this->data['complex']['complex_photo'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/residential_complexes/detail_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function _get($id_complex = 0){
		return $this->complexes->handler_get($id_complex);
	}

	function _get_categories($params = array()){
		return $this->real_estate->handler_get_categories($params);
	}

	function _get_category($id_category = 0){
		return $this->real_estate->handler_get_category($id_category);
	}
}