<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_residential_complexes')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Жилые комплексы';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Residential_complexes_model", "complexes");
		$this->load->model("real_estate/Real_estate_model", "real_estate");
		$this->load->model("locations/Locations_model", "locations");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['default_currency'] = Modules::run('currency/_get_default_currency');
		$this->data['main_content'] = 'admin/modules/residential_complexes/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['default_currency'] = Modules::run('currency/_get_default_currency');
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0)), 'id_parent', true);
		$this->data['icons'] = $this->real_estate->handler_get_icons();
		
		$this->data['main_title'] = 'Добавить жилой комплекс';
		$this->data['main_content'] = 'admin/modules/residential_complexes/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_complex = (int)$this->uri->segment(4);
		$this->data['complex'] = $this->complexes->handler_get($id_complex);

		if(empty($this->data['complex'])){
			redirect('admin/residential_complexes');
		}

		$this->data['default_currency'] = Modules::run('currency/_get_default_currency');
		$this->data['location'] = $this->locations->handler_get_full($this->data['complex']['id_location']);
		$this->data['parent_locations'] = $this->locations->handler_get_all(array('id_parent' => 0));
		$this->data['children_locations'] = arrayByKey($this->locations->handler_get_all(array('not_id_parent' => 0)), 'id_parent', true);
		$this->data['icons'] = $this->real_estate->handler_get_icons();
		$this->data['icons_selected'] = json_decode($this->data['complex']['complex_icons'], true);

		$this->data['main_title'] = 'Редактировать жилой комплекс';
		$this->data['main_content'] = 'admin/modules/residential_complexes/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('price_from', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_ro', 'Текст блока с ценой RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_ru', 'Текст блока с ценой RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_en', 'Текст блока с ценой En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('complex_address_ro', 'Адрес RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_address_ru', 'Адрес RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_address_en', 'Адрес En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('address', 'Адрес', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$uploaded_files = $this->input->post('uploaded_files');
				if(empty($uploaded_files)){
					jsonResponse('Ошибка: Нужно добавить как минимум одну фотографию.');
				}
				
				$main_photo = $this->input->post('main_photo');
				if(empty($main_photo) || !in_array($main_photo, $uploaded_files)){
					jsonResponse('Ошибка: Нужно выбрать главную фотографию.');
				}

				$id_location = (int)$this->input->post('location_child');
				$location = $this->locations->handler_get_full($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Нужно выбрать местоположение.');
				}
				
				if (!($this->input->post('latitude') && $this->input->post('longitude'))) {
					jsonResponse('Ошибка: Нужно выбрать местоположение на карте.');
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/residential_complexes/temp/'.$remove_photo);
						@unlink('files/residential_complexes/temp/thumb_'.$remove_photo);
					}
				}

				$complex_active = ($this->input->post('complex_active'))?1:0;

				$filtered_plans = array();
				$filtered_icons = array();
				$insert = array(
					'id_manager' => ($this->lauth->is_admin_only())?(int)$this->input->post('manager'):$this->lauth->id_user(),
					'complex_price_from' => formatNumber((float) $this->input->post('price_from')),
					'complex_title_ro' => $this->input->post('title_ro'),
					'complex_title_ru' => $this->input->post('title_ru'),
					'complex_title_en' => $this->input->post('title_en'),
					'complex_head_ro' => $this->input->post('complex_head_ro'),
					'complex_head_ru' => $this->input->post('complex_head_ru'),
					'complex_head_en' => $this->input->post('complex_head_en'),
					'complex_address_ro' => $this->input->post('complex_address_ro'),
					'complex_address_ru' => $this->input->post('complex_address_ru'),
					'complex_address_en' => $this->input->post('complex_address_en'),
					'complex_stext_ro' => $this->input->post('stext_ro'),
					'complex_stext_ru' => $this->input->post('stext_ru'),
					'complex_stext_en' => $this->input->post('stext_en'),
					'complex_text_ro' => $this->input->post('description_ro'),
					'complex_text_ru' => $this->input->post('description_ru'),
					'complex_text_en' => $this->input->post('description_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'complex_photo' => $main_photo,
					'complex_photos' => json_encode($uploaded_files),
					'id_location' => $id_location,
					'complex_address' => $this->input->post('address', true),
					'complex_lat' => (float)$this->input->post('latitude'),
					'complex_lng' => (float)$this->input->post('longitude'),
					'complex_icons' => json_encode($filtered_icons),
					'complex_plans' => json_encode($filtered_plans),
					'complex_active' => $complex_active,
					'complex_date_created' => $this->complexes->db_date()
				);

				$id_complex = $this->complexes->handler_insert($insert);

				$icons = array_filter($this->input->post('icons'));
				$full_icons = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');
				foreach ($icons as $key => $icon) {
					if (!array_key_exists($key, $full_icons)) {
						continue;
					}

					if(!isset($icon['is_checked'])){
						continue;
					}

					$filtered_icons[$key] = array(
						'id_icon' => $key,
						'id_complex' => $id_complex,
						'icon_name_ro' => (empty($icon['name_ro']))?$full_icons[$key]['icon_name_ro']:$icon['name_ro'],
						'icon_name_ru' => (empty($icon['name_ru']))?$full_icons[$key]['icon_name_ru']:$icon['name_ru'],
						'icon_name_en' => (empty($icon['name_en']))?$full_icons[$key]['icon_name_en']:$icon['name_en']
					);
				}
				
				if(!empty($filtered_icons)){
					$this->complexes->handler_insert_icons_relation($filtered_icons);
				}

				$path = 'files/residential_complexes/'.$id_complex;
				create_dir($path);
				
				$plans = $this->input->post('plans');
				if(!empty($plans) && is_array($plans)){
					foreach ($plans as $key => $plan) {
						$filtered_plans[$key] = array(
							'title_ro' => $plan['title_ro'],
							'title_ru' => $plan['title_ru'],
							'title_en' => $plan['title_en'],
							'price_from' => formatNumber((float) $plan['price_from']),
							'file' => $plan['file'],
							'floors' => $plan['floors'],
							'block' => $plan['block']
						);

						copy('files/residential_complexes/temp/'.$plan['file'], $path.'/'.$plan['file']);
						copy('files/residential_complexes/temp/thumb_'.$plan['file'], $path.'/thumb_'.$plan['file']);
						@unlink('files/residential_complexes/temp/'.$plan['file']);
						@unlink('files/residential_complexes/temp/thumb_'.$plan['file']);
					}
				}

				foreach ($uploaded_files as $uploaded_file) {
					copy('files/residential_complexes/temp/'.$uploaded_file, $path.'/'.$uploaded_file);
					copy('files/residential_complexes/temp/thumb_'.$uploaded_file, $path.'/thumb_'.$uploaded_file);
					@unlink('files/residential_complexes/temp/'.$uploaded_file);
					@unlink('files/residential_complexes/temp/thumb_'.$uploaded_file);
				}

				$config = array(
					'table' => $this->complexes->residential_complexes,
					'id' => 'id_complex',
					'field' => 'url_ro',
					'title' => 'complex_title_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ro' => $this->slug->create_slug(cut_str($insert['complex_title_ro'], 200).'-'.$id_complex),
					'url_ru' => $this->slug->create_slug(cut_str($insert['complex_title_ru'], 200).'-'.$id_complex),
					'url_en' => $this->slug->create_slug(cut_str($insert['complex_title_en'], 200).'-'.$id_complex),
					'complex_plans' => json_encode($filtered_plans),
					'complex_icons' => json_encode($filtered_icons)
				);
				$this->complexes->handler_update($id_complex, $update);

				jsonResponse('Комплекс добавлен.', 'success', array('insert' => $insert));
			break;
			// DONE
			case 'edit':
				$this->form_validation->set_rules('id_complex', 'Комплекс', 'required|xss_clean');
				$this->form_validation->set_rules('price_from', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_ro', 'Текст блока с ценой RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_ru', 'Текст блока с ценой RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_head_en', 'Текст блока с ценой En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_address_ro', 'Адрес RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_address_ru', 'Адрес RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('complex_address_en', 'Адрес En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('description_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('mt_ro', 'Meta title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_ru', 'Meta title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mt_en', 'Meta title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('address', 'Адрес', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_complex = (int)$this->input->post('id_complex');
				$complex = $this->complexes->handler_get($id_complex);
				if(empty($complex)){
					jsonResponse('Ошибка: Комплекс не найден.');
				}

				$uploaded_files = $this->input->post('uploaded_files');
				if(empty($uploaded_files)){
					jsonResponse('Ошибка: Нужно добавить как минимум одну фотографию.');
				}
				
				$main_photo = $this->input->post('main_photo');
				if(empty($main_photo) || !in_array($main_photo, $uploaded_files)){
					jsonResponse('Ошибка: Нужно выбрать главную фотографию.');
				}

				$id_location = (int)$this->input->post('location_child');
				$location = $this->locations->handler_get_full($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Нужно выбрать местоположение.');
				}
				
				if (!($this->input->post('latitude') && $this->input->post('longitude'))) {
					jsonResponse('Ошибка: Нужно выбрать местоположение на карте.');
				}
				
				$path = 'files/residential_complexes/'.$id_complex;
				create_dir($path);

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/residential_complexes/temp/'.$remove_photo);
						@unlink('files/residential_complexes/temp/thumb_'.$remove_photo);
					}
				}

				$remove_existent_photos = $this->input->post('remove_existent_photos');
				if(!empty($remove_existent_photos)){
					foreach($remove_existent_photos as $remove_existent_photo){
						@unlink($path.'/'.$remove_existent_photo);
						@unlink($path.'/thumb_'.$remove_existent_photo);
					}
				}

				$complex_active = ($this->input->post('complex_active'))?1:0;

				$filtered_plans = array();
				$plans = $this->input->post('plans');
				if(!empty($plans) && is_array($plans)){
					$complex_plans = json_decode($complex['complex_plans'], true);
					foreach ($plans as $key => $plan) {
						$filtered_plans[$key] = array(
							'title_ro' => $plan['title_ro'],
							'title_ru' => $plan['title_ru'],
							'title_en' => $plan['title_en'],
							'price_from' => formatNumber((float) $plan['price_from']),
							'file' => $plan['file'],
							'floors' => $plan['floors'],
							'block' => $plan['block']
						);

						if(!array_key_exists($key, $complex_plans)){
							copy('files/residential_complexes/temp/'.$plan['file'], $path.'/'.$plan['file']);
							copy('files/residential_complexes/temp/thumb_'.$plan['file'], $path.'/thumb_'.$plan['file']);
							@unlink('files/residential_complexes/temp/'.$plan['file']);
							@unlink('files/residential_complexes/temp/thumb_'.$plan['file']);
						}
					}
				}
				
				$complex_photos = json_decode($complex['complex_photos'], true);
				$filtered_photos = array();
				foreach ($uploaded_files as $uploaded_file) {
					$filtered_photos[] = $uploaded_file;

					if(!in_array($uploaded_file, $complex_photos)){
						copy('files/residential_complexes/temp/'.$uploaded_file, $path.'/'.$uploaded_file);
						copy('files/residential_complexes/temp/thumb_'.$uploaded_file, $path.'/thumb_'.$uploaded_file);
						@unlink('files/residential_complexes/temp/'.$uploaded_file);
						@unlink('files/residential_complexes/temp/thumb_'.$uploaded_file);
					}
				}

				$filtered_icons = array();
				$icons = array_filter($this->input->post('icons'));
				$full_icons = arrayByKey($this->real_estate->handler_get_icons(), 'id_icon');
				foreach ($icons as $key => $icon) {
					if (!array_key_exists($key, $full_icons)) {
						continue;
					}

					if(!isset($icon['is_checked'])){
						continue;
					}

					$filtered_icons[$key] = array(
						'id_icon' => $key,
						'id_complex' => $id_complex,
						'icon_name_ro' => (empty($icon['name_ro']))?$full_icons[$key]['icon_name_ro']:$icon['name_ro'],
						'icon_name_ru' => (empty($icon['name_ru']))?$full_icons[$key]['icon_name_ru']:$icon['name_ru'],
						'icon_name_en' => (empty($icon['name_en']))?$full_icons[$key]['icon_name_en']:$icon['name_en']
					);
				}
				
				$this->complexes->handler_delete_icons_relation(array('id_complex' => $id_complex));
				if(!empty($filtered_icons)){
					$this->complexes->handler_insert_icons_relation($filtered_icons);
				}

				$config = array(
					'table' => $this->complexes->residential_complexes,
					'id' => 'id_complex',
					'field' => 'url_ro',
					'title' => 'complex_title_ro',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'complex_price_from' => formatNumber((float) $this->input->post('price_from')),
					'complex_title_ro' => $this->input->post('title_ro'),
					'complex_title_ru' => $this->input->post('title_ru'),
					'complex_title_en' => $this->input->post('title_en'),
					'complex_head_ro' => $this->input->post('complex_head_ro'),
					'complex_head_ru' => $this->input->post('complex_head_ru'),
					'complex_head_en' => $this->input->post('complex_head_en'),
					'complex_address_ro' => $this->input->post('complex_address_ro'),
					'complex_address_ru' => $this->input->post('complex_address_ru'),
					'complex_address_en' => $this->input->post('complex_address_en'),
					'complex_stext_ro' => $this->input->post('stext_ro'),
					'complex_stext_ru' => $this->input->post('stext_ru'),
					'complex_stext_en' => $this->input->post('stext_en'),
					'complex_text_ro' => $this->input->post('description_ro'),
					'complex_text_ru' => $this->input->post('description_ru'),
					'complex_text_en' => $this->input->post('description_en'),
					'mt_ro' => $this->input->post('mt_ro'),
					'mt_ru' => $this->input->post('mt_ru'),
					'mt_en' => $this->input->post('mt_en'),
					'mk_ro' => $this->input->post('mk_ro'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ro' => $this->input->post('md_ro'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'url_ro' => $this->slug->create_slug(cut_str($this->input->post('title_ro'), 200).'-'.$id_complex),
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru'), 200).'-'.$id_complex),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en'), 200).'-'.$id_complex),
					'complex_photo' => $main_photo,
					'complex_photos' => json_encode($filtered_photos),
					'id_location' => $id_location,
					'complex_address' => $this->input->post('address', true),
					'complex_lat' => (float)$this->input->post('latitude'),
					'complex_lng' => (float)$this->input->post('longitude'),
					'complex_icons' => json_encode($filtered_icons),
					'complex_plans' => json_encode($filtered_plans),
					'complex_active' => $complex_active
				);

				if($this->lauth->is_admin_only()){
					$update['id_manager'] = (int)$this->input->post('manager');
				}
				$this->complexes->handler_update($id_complex, $update);

				jsonResponse('Данные сохранены.', 'success', array('update' => $update));
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_complex-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = 'complex_date_created-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'complex_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
								
				if(isset($_POST['show_on_home'])){
					$params['show_on_home'] = (int)$this->input->post('show_on_home');
				}

				$records = $this->complexes->handler_get_all($params);
				$records_total = $this->complexes->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать жилой комплекс неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-complex="'.$record['id_complex'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['complex_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать жилой комплекс активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-complex="'.$record['id_complex'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$options = array();
					if($record['complex_on_home'] == 0){
						$options[] = '<span class="btn btn-default btn-xs confirm-dialog" data-message="Вы действительно хотите включить признак Показать на главной?" data-callback="change_marketing" data-marketing="on_home" data-complex="'.$record['id_complex'].'" data-title="Включить признак Показать на главной"><i class="fa fa-home"></i></span>';
					} else{
						$options[] = '<span class="btn btn-success btn-xs confirm-dialog" data-message="Вы действительно хотите убрать признак Показать на главной?" data-callback="change_marketing" data-marketing="on_home" data-complex="'.$record['id_complex'].'" data-title="Убрать признак Показать на главной"><i class="fa fa-home"></i></span>';
					}

					$output['aaData'][] = array(
						'dt_id'			=> orderNumberOnly($record['id_complex']),
						'dt_photo'		=> '<img src="'.base_url(getImage('files/residential_complexes/'.$record['id_complex'].'/thumb_'.$record['complex_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=> '<a href="'.base_url('admin/residential_complexes/edit/'.$record['id_complex']).'" target="_blank">'.$record['complex_title_ru'].'</a>',
						'dt_options'	=> implode(' ', $options),
						'dt_date'		=> formatDate($record['complex_date_created'], 'd.m.Y H:i:s'),
						'dt_actions'	=> $status
										   .' <a href="'.base_url('admin/residential_complexes/edit/'.$record['id_complex']).'" data-title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a> 
										   <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-title="Удалить" data-message="Вы уверены что хотите удалить жилой комплекс?" data-callback="delete_action" data-complex="'.$record['id_complex'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/residential_complexes/temp';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '10000';
				$config['quality']	= 70;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				
				$this->load->library('image_lib');
				$image_info = getimagesize($path.'/'.$data['file_name']);
				if($image_info[0] > 770 || $image_info[1] > 515){
					$config = array(
						'source_image'      => $path.'/'.$data['file_name'],
						'create_thumb'   	=> FALSE,
						'maintain_ratio'   	=> TRUE
					);

					if($image_info[0] > $image_info[1]){
						$config['width'] = 770;
					} else{
						$config['height'] = 515;
					}

					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}

				if($this->lsettings->item('upload_photo_filters')){
					$config = array(
						'source_image'      => $path.'/'.$data['file_name']
					);
					$this->image_lib->initialize($config);
					$this->image_lib->brightnes();
					$this->image_lib->contrast();
					$this->image_lib->clear();
				}

				$config = array(
					'source_image'      => $path.'/'.$data['file_name'],
					'create_thumb'   	=> TRUE,
					'maintain_ratio'   	=> TRUE,
					'thumb_marker'		=> 'thumb_',
					'quality' 			=> 70,
					'height'			=> 220
				);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				
				compress_image($path.'/'.$data['file_name']);
				compress_image($path.'/thumb_'.$data['file_name']);

				$file = new StdClass;
				$file->filename = $data['file_name'];
				jsonResponse('', 'success', array("file" => $file, 'wconfig' =>$config, 'upload_data' => $data));
			break;
			// DONE
			case 'upload_photo_plan':
				$path = 'files/residential_complexes/temp';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '10000';
				$config['quality']	= 70;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$this->load->library('image_lib');
				$image_info = getimagesize($path.'/'.$data['file_name']);
				if($image_info[0] > 1200 && $image_info[1] > 768 || $image_info[0] > 768 && $image_info[1] > 1200){
					$config = array(
						'source_image'      => $path.'/'.$data['file_name'],
						'create_thumb'   	=> FALSE,
						'maintain_ratio'   	=> TRUE
					);

					if($image_info[0] > $image_info[1]){
						$config['width'] = 1200;
					} else{
						$config['height'] = 768;
					}

					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}

				$config = array(
					'source_image'      => $path.'/'.$data['file_name'],
					'create_thumb'   	=> TRUE,
					'maintain_ratio'   	=> TRUE,
					'thumb_marker'		=> 'thumb_',
					'quality' 			=> 70,
					'height'			=> 270
				);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				
				compress_image($path.'/'.$data['file_name']);
				compress_image($path.'/thumb_'.$data['file_name']);

				$file = new StdClass;
				$file->filename = $data['file_name'];
				$file->fileid = uniqid();
				jsonResponse('', 'success', array("file" => $file, 'wconfig' =>$config, 'upload_data' => $data));
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('complex', 'Жилой комплекс', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_complex = (int)$this->input->post('complex');
				$complex = $this->complexes->handler_get($id_complex);
				if(empty($complex)){
					jsonResponse('Жилой комплекс не существует.');
				}

				$this->complexes->handler_update($id_complex, array('complex_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('complex', 'Жилой комплекс', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_complex = (int)$this->input->post('complex');
				$complex = $this->complexes->handler_get($id_complex);
				if(empty($complex)){
					jsonResponse('Жилой комплекс не существует.');
				}

				if($complex['complex_active']){
					$status = 0;
				} else{
					$status = 1;
				}

				$this->complexes->handler_update($id_complex, array('complex_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'change_marketing':
				$this->form_validation->set_rules('complex', 'Жилой комплекс', 'required|xss_clean');
				$this->form_validation->set_rules('marketing', 'Опция', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_complex = (int)$this->input->post('complex');
				$marketing = $this->input->post('marketing', true);
				$complex = $this->complexes->handler_get($id_complex);
				if(empty($complex)){
					jsonResponse('Жилой комплекс не существует.');
				}

				if(!isset($complex['complex_'.$marketing])){
					jsonResponse('Данные не верны.');
				}

				if($complex['complex_'.$marketing] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				
				$this->complexes->handler_update($id_complex, array('complex_'.$marketing => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
		}
	}
}