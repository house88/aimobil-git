<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Favorite extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$_cookie_favorite = get_cookie('_ai_favorite');
		if(!empty($_cookie_favorite)){
			$_cookie_favorite = json_decode(base64_decode($_cookie_favorite), true);
		}

		if(!empty($_cookie_favorite['imobil'])){
			$id_real_estate = array_filter($_cookie_favorite['imobil']);
			if(!empty($id_real_estate)){
				$this->load->model("real_estate/Real_estate_model", "real_estate");
				$real_estate_params = array(
					'real_estate_active' => 1,
					'real_estate_deleted' => 1,
					'id_real_estate' => $id_real_estate
				);
				$this->data['real_estates'] = $this->real_estate->handler_get_all($real_estate_params);
		
				$properties_params = array(
					'property_type' => array('multiselect','range'),
					'property_on_item' => 1
				);
				$properties = $this->real_estate->handler_get_properties_values($properties_params);
				$this->data['re_properties'] = array();
				if(!empty($properties)){
					foreach ($properties as $property) {
						$property_settings = json_decode($property['property_settings'], true);
		
						switch ($property['property_type']) {
							case 'multiselect':
								$property_values = arrayByKey(json_decode($property['property_values'], true), 'id_value');
								$unit_name = $property_settings['unit'][lang_column('title')];
								if(isset($property_values[$property['property_value']])){
									$unit_name = plural_numword((int)clean_plused($property_values[$property['property_value']][lang_column('title')]), $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
									if($property['property_value_sufix'] == 1){
										$unit_name = lang_line('label_living', false);
									}
									$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
										'property_value' => clean_plused($property_values[$property['property_value']][lang_column('title')]),
										'unit_name' => $unit_name
									);
								}
							break;
							case 'range':
								$property_value = array($property['property_value']);
								
								if($property_settings['repeat_values'] == true){
									$property_value[] = $property['property_value_max'];
								}
								$unit_name = plural_numword((int)$property['property_value'], $property_settings['unit'][lang_column('title')], @$property_settings['unit'][lang_column('title_plural')], $this->ulang);
								
								$this->data['re_properties'][$property['id_real_estate']][$property['id_property']] = array(
									'property_value' => implode('/', $property_value),
									'unit_name' => $unit_name
								);
							break;
						}
					}
				}
			}
		}

		if(!empty($_cookie_favorite['complex'])){
			$id_complex = array_filter($_cookie_favorite['complex']);
			if(!empty($id_complex)){
				$this->load->model("residential_complexes/Residential_complexes_model", "complexes");
				$this->load->model("locations/Locations_model", "locations");
				$this->load->model('users/Users_model', 'users');
				
				$this->data['residential_complexes'] = $this->complexes->handler_get_all(array('complex_active' => 1, 'id_complex' => $id_complex, 'complex_deleted' => 0));
				$this->data['locations'] = arrayByKey($this->locations->handler_get_all_full(), 'id_location');
				$this->data['managers'] = arrayByKey($this->users->handler_get_all(), 'id_user');
				$this->data['_init_google_maps'] = true;
			}
		}

		$this->data['langs_uri'] = get_static_uri('favorite');

		// $this->data['og_meta'] = array(
		// 	lang_column('page_title') => $this->data['category'][lang_column('mt')],
		// 	lang_column('page_mk') => $this->data['category'][lang_column('mk')],
		// 	lang_column('page_md') => $this->data['category'][lang_column('md')],
		// 	'page_image' => get_og_image()
		// );
		// $this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		// $this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		// $this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'modules/favorite/index_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if($this->lang->is_default()){
			$option = $this->uri->segment(3);
		} else{
			$option = $this->uri->segment(4);
		}
		
		switch($option){
			case 'set_favorite':
				$item = (int)$this->input->post('item');
				$type = $this->input->post('type', true);
				switch ($type) {
					case 'imobil':
						$real_estate = Modules::run('real_estate/_get', $item);
						if(empty($real_estate)){
							jsonResponse(lang_line('error_message_bad_data', false));
						}
				
						if($real_estate['real_estate_active'] == 0 || $real_estate['real_estate_deleted'] == 1){
							jsonResponse(lang_line('error_message_bad_data', false));
						}
					break;
					case 'complex':
						$complex = Modules::run('residential_complexes/_get', $item);
						if(empty($complex)){
							jsonResponse(lang_line('error_message_bad_data', false));
						}
				
						if($complex['complex_active'] == 0 || $complex['complex_deleted'] == 1){
							jsonResponse(lang_line('error_message_bad_data', false));
						}
					break;
					default:
						jsonResponse(lang_line('error_message_bad_data', false));
					break;
				}

				$_cookie_favorite = get_cookie('_ai_favorite');
				if(!empty($_cookie_favorite)){
					$_cookie_favorite = json_decode(base64_decode($_cookie_favorite), true);
				}

				if(!empty($_cookie_favorite[$type])){
					if(!in_array($item, $_cookie_favorite[$type])){
						$_cookie_favorite[$type][] = $item;
					} else{
						$_cookie_favorite[$type] = array_diff($_cookie_favorite[$type], array($item));
					}
				} else{
					$_cookie_favorite[$type][] = $item;
				}

				if(!empty($_cookie_favorite)){
					set_cookie('_ai_favorite', base64_encode(json_encode($_cookie_favorite)), 7776000, getDomain('.'), '/');
				}

				jsonResponse('', 'success', array('total_records' => count(@$_cookie_favorite[$type]), 'empty_message' => lang_line('no_data', false)));
			break;
			default:
				jsonResponse(lang_line('error_message_bad_action', false));
			break;
		}
	}
}