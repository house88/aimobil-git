<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Translations_model extends CI_Model{
	
	var $translations = "translations";
	var $translations_sections = "translations_sections";
	
	function __construct(){
		parent::__construct();
	}

	function handler_insert_section($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->translations_sections, $data);
		return $this->db->insert_id();
	}

	function handler_get_sections(){
		return $this->db->get($this->translations_sections)->result_array();
	}
	
	function handler_insert_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->translations, $data);
		return $this->db->insert_id();
	}
	
	function handler_update_translation($id_translation = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_translation', $id_translation);
		$this->db->update($this->translations, $data);
	}
	
	function handler_delete_translation($id_translation = 0){
		$this->db->where('id_translation', $id_translation);
		$this->db->delete($this->translations);
	}
	
	function handler_get_translation($conditions = array()){
		if(empty($conditions)){
			return FALSE;
		}

        extract($conditions);

        if(isset($id_translation)){
			$this->db->where('id_translation', $id_translation);
		}

        if(isset($translation_key)){
			$this->db->where('translation_key', $translation_key);
		}
		
		return $this->db->get($this->translations)->row_array();
	}
	
	function update_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->translations, $data, 'translation_key');
	}
	
	function handler_get_translations($conditions = array()){
        extract($conditions);

        if(isset($id_section)){
			$this->db->where_in('id_section', $id_section);
		}

        if(isset($keywords)){
			$this->db->where("( translation_key = '{$keywords}' OR translation_text_ro LIKE '%{$keywords}%' OR  translation_text_ru LIKE '%{$keywords}%' OR  translation_text_en LIKE '%{$keywords}%')");
		}
		
		return $this->db->get($this->translations)->result_array();
	}

	function handler_get_translations_count($conditions = array()){
        extract($conditions);

        if(isset($id_section)){
			$this->db->where_in('id_section', $id_section);
        }

        if(isset($keywords)){
			$this->db->where("( translation_key = '{$keywords}' OR translation_text_ro LIKE '%{$keywords}%' OR  translation_text_ru LIKE '%{$keywords}%' OR  translation_text_en LIKE '%{$keywords}%')");
		}

		return $this->db->count_all_results($this->translations);
	}
}