<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');
		
		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_pages_meta')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Мета страниц';
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/page_meta/list';
		$this->load->view('admin/page', $this->data);
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'edit':
				$page_key = $this->uri->segment(5);
				$this->data['page_meta'] = $this->page_meta->handler_get($page_key);
				if(empty($this->data['page_meta'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/page_meta/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content, 'page_key' => $page_key));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
                $this->form_validation->set_rules('page_key', 'Страница', 'required|xss_clean');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ro', 'Meta keywords RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ro', 'Meta description RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('page_image', 'Постер страницы', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$page_key = $this->input->post('page_key', true);
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/page_meta/'.$remove_photo);
					}
				}

				$update = array(
					'page_title_ro' => $this->input->post('title_ro'),
					'page_title_ru' => $this->input->post('title_ru'),
					'page_title_en' => $this->input->post('title_en'),
					'page_mk_ro' => $this->input->post('mk_ro'),
					'page_mk_ru' => $this->input->post('mk_ru'),
					'page_mk_en' => $this->input->post('mk_en'),
					'page_md_ro' => $this->input->post('md_ro'),
					'page_md_ru' => $this->input->post('md_ru'),
					'page_md_en' => $this->input->post('md_en'),
					'page_seo_ro' => $this->input->post('seo_ro'),
					'page_seo_ru' => $this->input->post('seo_ru'),
					'page_seo_en' => $this->input->post('seo_en'),
					'page_image' => $this->input->post('page_image', true)
				);

				$this->page_meta->handler_update($page_key, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$records = $this->page_meta->handler_get_all();
				$records_total = $this->page_meta->handler_get_count();

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_name'		=>  $record['page_name'],
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/page_meta/popup_forms/edit/'.$record['page_key']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'upload_photo':
				$path = 'files/page_meta';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '1200';
				$config['min_height']	= '100';
				$config['max_size']	= '5000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 1200
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
		}
	}
}