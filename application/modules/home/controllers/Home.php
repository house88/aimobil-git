<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$real_estate_params = array(
			'show_on_home' => 1,
			'discount_type' => array('simple','new_price','by_m2'),
			'real_estate_active' => 1
		);
		$this->data['real_estates_count'] = Modules::run('real_estate/_get_count', $real_estate_params);

		if($this->lsettings->item('home_residential_complexes_show') == 1){
			$this->load->model("residential_complexes/Residential_complexes_model", "complexes");
			$this->data['complexes'] = $this->complexes->handler_get_all(array('complex_active' => 1, 'complex_on_home' => 1, 'complex_deleted' => 0));
		}
		
		$this->load->model("services/Services_model", "services");
		$this->data['services'] = $this->services->handler_get_all(array('service_active' => 1));
		
		$this->load->model("blog/Blog_model", "blog");
		$blogs_params = array(
			'limit' => 3,
			'blog_active' => 1
		);
		$this->data['blogs'] = $this->blog->handler_get_all($blogs_params);

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('home');
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['has_paralax'] = true;
		$this->data['main_content'] = 'modules/home/home_view';
		$this->load->view(get_theme_view('page_aditional_template'), $this->data);
	}
}