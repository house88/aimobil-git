<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Partners_model extends CI_Model{
	var $partners = "partners";
	var $partners_categories = "partners_categories";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->partners, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_partner = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_partner', $id_partner);
		$this->db->update($this->partners, $data);
	}

	function handler_delete($id_partner = 0){
		$this->db->where('id_partner', $id_partner);
		$this->db->delete($this->partners);
	}

	function handler_get($id_partner = 0){
		$this->db->where('id_partner', $id_partner);
		return $this->db->get($this->partners)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_partner ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_partner)){
			$this->db->where_in('id_partner', $id_partner);
        }

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
        }

        if(isset($partner_active)){
			$this->db->where('partner_active', $partner_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->partners)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_partner)){
			$this->db->where_in('id_partner', $id_partner);
        }

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
        }

        if(isset($partner_active)){
			$this->db->where('partner_active', $partner_active);
        }

		return $this->db->count_all_results($this->partners);
	}

	// CATEGORIES
	function handler_insert_category($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->partners_categories, $data);
		return $this->db->insert_id();
	}

	function handler_update_category($id_category = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_category', $id_category);
		$this->db->update($this->partners_categories, $data);
	}
	
	function handler_get_category($id_category = 0){
		$this->db->where('id_category', $id_category);
		return $this->db->get($this->partners_categories)->row_array();
	}
	
	function handler_get_categories($conditions = array()){
		$order_by = " id_category ASC ";
		$category_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->partners_categories)->result_array();
	}

	function handler_count_categories($conditions = array()){
		$category_deleted = 0;

		extract($conditions);
		
		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		return $this->db->count_all_results($this->partners_categories);
	}
}