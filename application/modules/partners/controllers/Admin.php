<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_partners')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Партнеры';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Partners_model", "partners");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['categories'] = $this->partners->handler_get_categories(array('category_deleted' => 0));
		$this->data['main_content'] = 'admin/modules/partners/list';
		$this->load->view('admin/page', $this->data);
	}

	// DONE
	function categories(){
		$this->data['main_title'] = 'Типы партнеров';
		$this->data['main_content'] = 'admin/modules/partners/categories/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('category', 'Тип', 'required|xss_clean');
				$this->form_validation->set_rules('partner_logo', 'Лого', 'required');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('link', 'Ссылка', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/partners/'.$remove_photo);
					}
				}

				$id_category = $this->input->post('category');
				$category = $this->partners->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$insert = array(
					'id_category' => $id_category,
					'partner_logo' => $this->input->post('partner_logo'),
					'partner_name' => $this->input->post('title'),
					'partner_link' => $this->input->post('link')
				);

				$id_partner = $this->partners->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
				$this->form_validation->set_rules('id_partner', 'Партнер', 'required|xss_clean');
				$this->form_validation->set_rules('category', 'Тип', 'required|xss_clean');
				$this->form_validation->set_rules('partner_logo', 'Лого', 'required');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('link', 'Ссылка', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_partner = (int)$this->input->post('id_partner');

				$id_category = $this->input->post('category');
				$category = $this->partners->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/partners/'.$remove_photo);
					}
				}

				$update = array(
					'id_category' => $id_category,
					'partner_logo' => $this->input->post('partner_logo'),
					'partner_name' => $this->input->post('title'),
					'partner_link' => $this->input->post('link')
				);

				$this->partners->handler_update($id_partner, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'delete':
				$id_partner = (int)$this->input->post('partner');
				$partner = $this->partners->handler_get($id_partner);
				if(empty($partner)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				@unlink('files/partners/'.$partner['partner_logo']);

				$this->partners->handler_delete($id_partner);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'change_status':
				$id_partner = (int)$this->input->post('partner');
				$partner = $this->partners->handler_get($id_partner);
				if(empty($partner)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($partner['partner_active']){
					$status = 0;
				} else{
					$status = 1;
				}

				$this->partners->handler_update($id_partner, array('partner_active' => $status));
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name': $params['sort_by'][] = 'partner_name-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				if($this->input->post('category')){
					$params['id_category'] = (int)$this->input->post('category');
				}

				$records = $this->partners->handler_get_all($params);
				$records_total = $this->partners->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать партнера неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-partner="'.$record['id_partner'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['partner_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать партнера активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-partner="'.$record['id_partner'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/partners/'.$record['partner_logo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  $record['partner_name'],
						'dt_actions'	=>  $status
											.' <a href="#" data-href="'.base_url('admin/partners/popup_forms/edit/'.$record['id_partner']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить партнера?" data-callback="delete_action" data-partner="'.$record['id_partner'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/partners';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '150';
				$config['max_size']	= '5000';
				$config['quality']	= 85;

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 150,
					'height'             => 150
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				jsonResponse('', 'success', array("file" => $info));
			break;
			// DONE
			case 'add_category':
				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|required|xss_clean|max_length[250]');
				if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'category_text_ro' => $this->input->post('text_ro'),
					'category_text_ru' => $this->input->post('text_ru'),
					'category_text_en' => $this->input->post('text_en')
				);

				$id_category = $this->partners->handler_insert_category($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_category':
				$id_category = (int)$this->input->post('id_category');
				$category = $this->partners->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->form_validation->set_rules('title_ro', 'Название RO', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'trim|required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'trim|required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$update = array(
					'category_title_ro' => $this->input->post('title_ro'),
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'category_text_ro' => $this->input->post('text_ro'),
					'category_text_ru' => $this->input->post('text_ru'),
					'category_text_en' => $this->input->post('text_en')
				);

				$this->partners->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'delete_category':
				$id_category = (int)$this->input->post('category');
				$category = $this->partners->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'category_deleted' => 1
				);

				$this->partners->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt_categories':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart']),
					'category_deleted' => 0
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name': $params['sort_by'][] = 'category_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->partners->handler_get_categories($params);
				$records_total = $this->partners->handler_count_categories($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_name'		=>  $record['category_title_ru'],
						'dt_actions'	=>  '<a href="#" data-href="'.base_url('admin/partners/popup_forms/edit_category/'.$record['id_category']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить категорию?" data-callback="delete_action" data-category="'.$record['id_category'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}

	// DONE
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$this->data['categories'] = $this->partners->handler_get_categories(array('category_deleted' => 0));
				$content = $this->load->view('admin/modules/partners/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_partner = (int) $this->uri->segment(5);
				$this->data['partner'] = $this->partners->handler_get($id_partner);
				if(empty($this->data['partner'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->data['categories'] = $this->partners->handler_get_categories(array('category_deleted' => 0));
				$content = $this->load->view('admin/modules/partners/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_category':
				$content = $this->load->view('admin/modules/partners/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_category':
				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->partners->handler_get_category($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/partners/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}