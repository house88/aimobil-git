<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Partners extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];

		$this->data['active_menu'] = 'about_us';
		
		$this->load->model("Partners_model", "partners");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$partners = arrayByKey($this->partners->handler_get_all(array('partner_active' => 1)), 'id_category', true);
		$categories = $this->partners->handler_get_categories(array('category_deleted' => 0));
		$this->data['categories'] = array();

		foreach ($categories as $category) {
			if(!isset($partners[$category['id_category']])){
				continue;
			}

			$category['partners'] = $partners[$category['id_category']];
			$this->data['categories'][] = $category;
		}

		$this->data['first_category'] = (isset($this->data['categories'][0]))?$this->data['categories'][0]:array();

		if(isset($this->data['categories'][0])){
			unset($this->data['categories'][0]);
		}

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->load->model("pages/Pages_model", "pages");
		$this->data['page_detail'] = $this->pages->handler_get_by_alias('partners');
		$this->data['page_detail']['page_blocks'] = json_decode($this->data['page_detail']['page_blocks'], true);
		$this->data['page_detail']['video'] = json_decode($this->data['page_detail']['video'], true);
		$this->data['og_meta'] = array(
			lang_column('page_title') => $this->data['page_detail'][lang_column('mt')],
			lang_column('page_mk') => $this->data['page_detail'][lang_column('mk')],
			lang_column('page_md') => $this->data['page_detail'][lang_column('md')],
			'page_image' => get_og_image('files/'.$this->data['page_detail']['page_poster'])
		);
		$this->data['stitle'] = $this->data['og_meta'][lang_column('page_title')];
		$this->data['skeywords'] = $this->data['og_meta'][lang_column('page_mk')];
		$this->data['sdescription'] = $this->data['og_meta'][lang_column('page_md')];

		$this->data['langs_uri'] = get_static_uri('partners');
		$this->data['main_content'] = 'modules/partners/index_view';
		$this->load->view(get_theme_view('page_template'), $this->data);
	}
}