<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// https://www.bnm.md/ru/official_exchange_rates?get_xml=1&date=24.03.2018
class Currency extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		$this->load->model("Currency_model", "currency_model");

		$this->data = array();
	}

	function _get_currencies($params = array()){
		return $this->currency_model->handler_get_all($params);
	}

	function _get_default_currency(){
		return $this->currency_model->handler_get_default();
	}

	function _get_current_code($params = array()){
		$currencies = arrayByKey($this->_get_currencies($params), 'currency_code');
		$cookie_currency = get_cookie('_ai_currency');
		if(!empty($cookie_currency) && array_key_exists($cookie_currency, $currencies)){
			return $currencies[$cookie_currency]['currency_code'];
		}

		$default_currency = $this->_get_default_currency();
		return $default_currency['currency_code'];
	}

	function _get_current($params = array()){
		$currencies = arrayByKey($this->_get_currencies($params), 'currency_code');
		$cookie_currency = get_cookie('_ai_currency');
		if(!empty($cookie_currency) && array_key_exists($cookie_currency, $currencies)){
			return $currencies[$cookie_currency];
		}

		$default_currency = $this->_get_default_currency();
		return $default_currency;
	}

	function _get_by_code($code = 'EUR'){
		return $this->currency_model->handler_get_by_code($code);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'set_currency':
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_currency = (int)$this->input->post('currency');
				$currency = $this->currency_model->handler_get($id_currency);
				if(empty($currency)){
					jsonResponse(lang_line('error_message_bad_data', false));
				}
				
				if($currency['currency_active'] != 1){
					jsonResponse(lang_line('error_message_bad_data', false));
				}
				
				set_cookie('_ai_currency', $currency['currency_code'], 3600, getDomain('.'), '/');

				jsonResponse('', 'success');
			break;
		}
	}
}