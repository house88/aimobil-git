<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// https://www.bnm.md/ru/official_exchange_rates?get_xml=1&date=24.03.2018
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

		if(!$this->lauth->have_right('manage_currency')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->load->model("Currency_model", "currency");

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_title'] = 'Валюты сайта';
		$this->data['main_content'] = 'admin/modules/currency/list_view';
		$this->load->view('admin/page', $this->data);
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add':
				$content = $this->load->view('admin/modules/currency/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit':
				$id_currency = (int)$this->uri->segment(5);
				$this->data['currency'] = $this->currency->handler_get($id_currency);
				if(empty($this->data['currency'])){
					jsonResponse('Данные не верны.');
				}

				$content = $this->load->view('admin/modules/currency/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('iso_code', 'ISO Код', 'required|xss_clean|max_length[3]');
				$this->form_validation->set_rules('symbol', 'Символ', 'required|xss_clean|max_length[20]');
				$this->form_validation->set_rules('exchange_rate', 'Курс', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'currency_name' => $this->input->post('title'),
					'currency_code' => $this->input->post('iso_code'),
					'currency_symbol' => $this->input->post('symbol'),
					'currency_rate' => $this->input->post('exchange_rate')
				);

				$this->currency->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('iso_code', 'ISO Код', 'required|xss_clean|max_length[3]');
				$this->form_validation->set_rules('symbol', 'Символ', 'required|xss_clean|max_length[20]');
				$this->form_validation->set_rules('exchange_rate', 'Курс', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_currency = (int)$this->input->post('currency');
				$update = array(
					'currency_name' => $this->input->post('title'),
					'currency_code' => $this->input->post('iso_code'),
					'currency_symbol' => $this->input->post('symbol'),
					'currency_rate' => $this->input->post('exchange_rate')
				);

				$this->currency->handler_update($id_currency, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_currency = (int)$this->input->post('currency');
				$currency = $this->currency->handler_get($id_currency);
				if(empty($currency)){
					jsonResponse('Данные не верны.');
				}

				if($currency['currency_default'] == 1){
					jsonResponse('Вы не можете удалить главную валюту сайта.');
				}

				$this->currency->handler_delete($id_currency);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_currency = (int)$this->input->post('currency');
				$currency = $this->currency->handler_get($id_currency);
				if(empty($currency)){
					jsonResponse('Валюта не существует.');
				}

				$action = $this->uri->segment(5);
				switch($action){
					case 'default' :
						$this->currency->handler_set_default($id_currency);
					break;
					case 'active' :
						if($currency['currency_active'] == 1){
							$status = 0;
						} else{
							$status = 1;
						}
						$this->currency->handler_update($id_currency, array('currency_active' => $status));
					break;
				}
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
		
				if ($this->input->post('iSortingCols') > 0) {
					for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
						switch ($this->input->post("mDataProp_".$this->input->post("iSortCol_{$i}"))) {
							case 'dt_name': 
								$params['sort_by'][] = 'currency_name-'.$this->input->post("sSortDir_{$i}");
							break;
							case 'dt_rate': 
								$params['sort_by'][] = 'currency_rate-'.$this->input->post("sSortDir_{$i}");
							break;
						}
					}
				}
		
				$records = $this->currency->handler_get_all($params);
				$records_total = $this->currency->handler_get_count($params);
				$default_currency = $this->currency->handler_get_default();
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$actions = '<a href="#" data-href="'.base_url('admin/currency/popup_forms/edit/'.$record['id_currency']).'" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form">
									<i class="fa fa-pencil"></i>
								</a>';
					if($record['currency_default'] == 0){						
						$btn_active = ' <a href="#" class="btn btn-default btn-xs confirm-dialog" data-dtype="primary" data-title="Сделать валюту активной" data-message="Вы уверены что хотите сделать валюту активной?" data-callback="change_currency" data-currency="'.$record['id_currency'].'" data-action="active"><i class="fa fa-eye-slash"></i></a>';
						if($record['currency_active'] == 1){
							$btn_active = ' <a href="#" class="btn btn-success btn-xs confirm-dialog" data-dtype="danger" data-title="Сделать валюту неактивной" data-message="Вы уверены что хотите сделать валюту неактивной?" data-callback="change_currency" data-currency="'.$record['id_currency'].'" data-action="active"><i class="fa fa-eye"></i></a>';
						}
						$actions .= $btn_active;
						
						$actions .= ' <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-dtype="danger" data-title="Удалить валюту" data-message="Вы уверены что хотите удалить валюту?" data-callback="delete_action" data-currency="'.$record['id_currency'].'"><i class="fa fa-trash"></i></a>';
					}

					$btn_default = '<a href="#" class="btn btn-default btn-xs confirm-dialog" data-message="Вы уверены что хотите сделать валюту главной?" data-callback="change_currency" data-currency="'.$record['id_currency'].'" data-action="default"><i class="fa fa-check-circle"></i></a>';
					if($record['currency_default'] == 1){
						$btn_default = '<span class="btn btn-success btn-xs"><i class="fa fa-check-circle"></i></span>';
					}
							
					$output['aaData'][] = array(
						'dt_name'		=>  $record['currency_name'],
						'dt_code'		=>  $record['currency_code'],
						'dt_symbol'		=>  $record['currency_symbol'],
						'dt_rate'		=>  '1 '.$default_currency['currency_symbol'].' = '.$record['currency_rate'].' '.$record['currency_symbol'],
						'dt_default'	=>  $btn_default,
						'dt_actions'	=>  $actions,
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}
}