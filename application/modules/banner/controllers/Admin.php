<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->config->set_item('language', 'russian');

		if(!$this->lauth->logged_in()){
            if (!$this->input->is_ajax_request()) {
                redirect('admin/signin');
            } else{
                jsonResponse(lang_line('error_message_not_logged', false));
            }
		}

        if(!$this->lauth->have_right('manage_banners')){
            if (!$this->input->is_ajax_request()) {
				redirect('/admin');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
        }

		$this->data = array();
		$this->data['main_title'] = 'Баннеры';
		
		$this->load->model("Banner_model", "banner");
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/banner/list_view';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$banner = $this->uri->segment(4);

		$this->data['banner'] = $this->banner->handler_get_by_alias($banner);
		if(empty($this->data['banner'])){
			$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Данные не верны. <i class="fa fa-remove"></i></li>');
			redirect('admin');
		}

		$this->data['main_title'] = $this->data['banner']['banner_name'];
		$this->data['main_content'] = 'admin/modules/banner/form_view';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
				$this->form_validation->set_rules('banner', 'Баннер', 'required|xss_clean');
				$this->form_validation->set_rules('banner_image', 'Фон баннера', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_title_ro', 'Атрибут title RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_title_ru', 'Атрибут title RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_title_en', 'Атрибут title En', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_alt_ro', 'Атрибут alt RO', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_alt_ru', 'Атрибут alt RU', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('banner_alt_en', 'Атрибут alt En', 'xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_banner = (int)$this->input->post('banner');
				$banner = $this->banner->handler_get($id_banner);
				if(empty($banner)){
					jsonResponse(lang_line('error_message_bad_action', false));
				}
				$update = array(
					'banner_image' => (file_exists('files/'.$this->input->post('banner_image')))?$this->input->post('banner_image'):'',
					'banner_title_ro' => $this->input->post('banner_title_ro'),
					'banner_title_ru' => $this->input->post('banner_title_ru'),
					'banner_title_en' => $this->input->post('banner_title_en'),
					'banner_link_ro' => $this->input->post('banner_link_ro'),
					'banner_link_ru' => $this->input->post('banner_link_ru'),
					'banner_link_en' => $this->input->post('banner_link_en'),
					'banner_alt_ro' => $this->input->post('banner_alt_ro'),
					'banner_alt_ru' => $this->input->post('banner_alt_ru'),
					'banner_alt_en' => $this->input->post('banner_alt_en'),
					'banner_text_ro' => $this->input->post('banner_text_ro'),
					'banner_text_ru' => $this->input->post('banner_text_ru'),
					'banner_text_en' => $this->input->post('banner_text_en')
				);

				$this->banner->handler_update($id_banner, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$records = $this->banner->handler_get_all();
				$records_total = $this->banner->handler_get_count();

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-dtype="warning" data-message="Вы уверены что хотите сделать баннер неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-banner="'.$record['id_banner'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['banner_active'] == 0){
						$status = '<a href="#" data-dtype="success" data-message="Вы уверены что хотите сделать баннер активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-banner="'.$record['id_banner'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name'		=>  $record['banner_name'],
						'dt_actions'	=>  $status
											.' <a href="'.base_url('admin/banner/edit/'.$record['banner_alias']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'change_status':
				$this->form_validation->set_rules('banner', 'Баннер', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_banner = (int)$this->input->post('banner');
				$banner = $this->banner->handler_get($id_banner);
				if(empty($banner)){
					jsonResponse('Данные не верны.');
				}

				if($banner['banner_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->banner->handler_update($id_banner, array('banner_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}