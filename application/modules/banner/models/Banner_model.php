<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends CI_Model{
	private $banner = "banner";
	function __construct(){
		parent::__construct();
	}

	function handler_update($id_banner = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_banner', $id_banner);
		$this->db->update($this->banner, $data);
	}

	function handler_get($id_banner = 0){
		$this->db->where('id_banner', $id_banner);
		return $this->db->get($this->banner)->row_array();
	}

	function handler_get_by_alias($banner_alias = '', $params = array()){
		$this->db->where('banner_alias', $banner_alias);
		
		extract($params);
		
		if(isset($banner_active)){
			$this->db->where('banner_active', $banner_active);
		}
		
		return $this->db->get($this->banner)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_banner ASC ";

        extract($conditions);

		$this->db->order_by($order_by);

		return $this->db->get($this->banner)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->banner);
	}
}