jQuery(document).ready(function($) {
    $(document).ready(function() {
        customTab();
        carousels();
        fixedMenu();
        mobileMenu();
        modalsPopups();
        widthResize();
        openSearch();
        contactIcons();
        hideShowMap();
        singleMapHeight();
        myDropDown();
        filterDropDown();
        stickyFilterBlock();
        collapsibleMobiles();
        fixedBanner();
        fixedObjectBlock();
        selectDropDown();
        sendForms();
        backCarouselHeight();
        var $counterImage = $('.single-page-carousel').find('.owl-dots');
        $($counterImage).wrap('<div class="dots-wrap"></div>');
        var mainHeader = $('.home-header .header-nav__menu-top');
        $('.search-button-toggler').on('click', function(e) {
            if (!mainHeader.hasClass('main-header--search-form-opened')) {
                e.preventDefault();
                mainHeader.toggleClass('main-header--search-form-opened');
                $('.secondary-menu__search-container').find('input#search-input-secondary-menu').val('').focus();
                return !1
            }
            if ($('.secondary-menu__search-container').find('input#search-input-secondary-menu')[1].val() == '') {
                e.preventDefault();
                mainHeader.toggleClass('main-header--search-form-opened')
            }
        });

        var $scrollTop = $('.scroll-top');
        $scrollTop.on('click', function (e) {
            e.preventDefault();
            $("html, body").animate({scrollTop: "0"});
        });

        var $headerBg = $('.home--bg');
        $( window ).load(function() {
            $headerBg.css('background-color', 'rgba(0,0,0,0)');
        });

        $(window).on('scroll', function() {
            if(parseInt($(window).scrollTop(), 10) > 400){
                $scrollTop.css('display', 'flex');
            }
            else {
                $scrollTop.fadeOut(1000);
            }
        });
        
        $('.checkbox-block').on('click', function(e) {
            if (!is_mobile_filter()) {
                $("html, body").animate({
                    scrollTop: "0"
                })
            }
        });
        if ($('.sticky-filter').length) {
            document.onkeydown = function(e) {
                if (e.keyCode === 13) {
                    $('.select-parent').removeClass('select-opened');
                }
            }
        }
    })
});

function fixedBanner() {
    if (window.innerWidth >= 992) {
        var $sidebarBanner = $('#sidebar-banner');
        var $fixedMenuHeight = $('.fixed-menu').height();
        var $sidebarHeight = $sidebarBanner.height();
        if ($sidebarBanner.length) {
            var top = $sidebarBanner.offset().top - $fixedMenuHeight + 48;
            $(window).scroll(function() {
                var $contentHeight = $('#filter-article').innerHeight();
                var $sidebarBottomPos = $contentHeight - $sidebarHeight;
                var $trigger = $(window).scrollTop() - $fixedMenuHeight;
                var $maxY = $sidebarBottomPos + 150;
                var y = $(this).scrollTop();
                if (y > top) {
                    $sidebarBanner.addClass('fixed').removeAttr('style').css('margin-top', '' + $fixedMenuHeight - 48 + 'px');
                    if (y > $maxY) {
                        var min = y - $maxY;
                        $sidebarBanner.css('top', '-' + min + 'px')
                    }
                } else {
                    $sidebarBanner.removeClass('fixed').css('margin-top', '0')
                }
            })
        }
    }
}

function collapsibleMobiles() {
    var $collapsibleParent = $('.mobile-collapse-parent');
    var $collapsibleToggle = $('.mobile-collapse-toggle');
    var $collapsibleInner = $('.mobile-collapse-inner');
    if ($collapsibleParent.length) {
        if (window.innerWidth < 768) {
            $collapsibleToggle.on('click', function() {
                $(this).toggleClass('open');
                $(this).siblings($collapsibleInner).slideToggle(300);
                var elementClick = $(this).siblings($collapsibleInner);
                var destination = ($(elementClick).offset().top) - 120;
                $('html,body').animate({
                    scrollTop: destination
                }, 300)
            })
        }
    }
}

function singleMapHeight() {
    var $singleMapBlock = $('.single_map');
    var $CarouselBlock = $('.block-itr');
    if (window.innerWidth < 768) {
        if ($singleMapBlock.length) {
            $singleMapBlock.height($CarouselBlock.height());
            $(window).resize(function() {
                $singleMapBlock.height($CarouselBlock.height())
            })
        }
    }
}

function hideShowMap() {
    var $overBlock = $('.block-itr');
    var $mapParent = $overBlock.find(".map-parent");
    var $openMap = $overBlock.find('.see-map');
    if ($overBlock.length) {
        $openMap.on('click', function(e) {
            e.preventDefault();
            singleMapHeight();
            $(this).closest($overBlock).find($mapParent).css("opacity", "1");
            // $(this).closest($overBlock).find('.carousel-grand-parent').hide();
        });
        $('.close-map').on('click', function(e) {
            e.preventDefault();
            $(this).closest($overBlock).find($mapParent).css("opacity", "0");
            // $(this).closest($overBlock).find($mapParent).hide();
            // $(this).closest($overBlock).find('.carousel-grand-parent').show()
        })
    }
    $('.share-link-mobile').on('click', function(event) {
        event.preventDefault();
        $(this).closest('.parent-123').toggleClass('social-open')
    })
}

function fixedObjectBlock() {
	if(window.innerWidth >= "1100" ) {
		var fixedMenu = $('.fixed-menu');
		var $objectBlock = $('.mobile-bottom-border');
		var $parentFixed = fixedMenu.find('.parent-fixed-block');
		var innerFixed = fixedMenu.find('.inner-fixed-block');
		if($objectBlock.length && $parentFixed.length) {
			$objectBlock.clone().appendTo(innerFixed);
			var blockTop = $objectBlock.offset().top + 120;
			$(window).scroll(function(evt) {
				var y = $(this).scrollTop();
				if (y > blockTop) {
					$parentFixed.addClass('visible-block');
					modalsPopups();
				} else {
					$parentFixed.removeClass('visible-block');
				}
			});
		}
	}
}

function fixedMenu() {
    var $menu = $(".fixed-menu");
    var tempScrollTop, currentScrollTop = 0;
    if (window.innerWidth >= 1100) {
        $("#masthead").clone().prependTo(".fixed-menu");
        $(window).scroll(function() {
            currentScrollTop = $(this).scrollTop();
            if ($(this).scrollTop() < 100) {
                $menu.removeClass("show-fixed-menu").addClass("hide-fixed-menu")
            } else if ($(this).scrollTop() > 100 || tempScrollTop > currentScrollTop || tempScrollTop < currentScrollTop) {
                $menu.removeClass("hide-fixed-menu").addClass("show-fixed-menu")
            }
            tempScrollTop = currentScrollTop
        })
    } else {
        $("#mobile-header").clone().appendTo(".fixed-menu");
        $(window).scroll(function() {
            currentScrollTop = $(this).scrollTop();
            if ($(this).scrollTop() < 100) {
                $menu.removeClass("show-fixed-menu").addClass("hide-fixed-menu")
            } else if ($(this).scrollTop() > 100 || tempScrollTop > currentScrollTop || tempScrollTop < currentScrollTop) {
                $menu.removeClass("hide-fixed-menu").addClass("show-fixed-menu")
            }
            tempScrollTop = currentScrollTop
        })
    }
    if (window.innerWidth <= 450) {}
    var $menuContainer = $('.home-header');
    var $hoverItem = $menuContainer.find('.header-nav__menu-bottom-left');;
    $hoverItem.hover(function() {
        $(this).closest($menuContainer).addClass('homeHover')
    }, function() {
        $(this).closest($menuContainer).removeClass('homeHover')
    })
}

function selectDropDown() {
    if ($('.sticky-filter').length) {
        var $selectInput = $('.filter-input');
        var $selectParent = $('.select-parent');
        var $selectLink = $('.select_link');
        $selectInput.on('click', function() {
            $(this).parent().addClass('select-opened')
        });
        $selectLink.on('click', function(e) {
            var $result = $(this).data("result");
            e.preventDefault();
            $(this).closest($selectParent).removeClass('select-opened').find('input.filter-input').val($result).change();
        });
        $selectParent.on('mouseleave', function() {
            $(this).removeClass('select-opened')
        })
    }
}

function myDropDown() {
    var $hotelToggle = $('.my--dropdown--toggle');
    $hotelToggle.on('click', function() {
        $(this).toggleClass('opened');
        $(this).siblings('.my--dropdown--menu').slideToggle(300)
    });
    var $touristToggle = $('.my__dropdown--toggle');
    $touristToggle.on('click', function() {
        $(this).toggleClass('opened');
        $(this).siblings('.my__dropdown--menu').slideToggle(300)
    });
    var $touristItem = $('.my__dropdown--menu ul li  a.touristBlock');
    $touristItem.on('click', function(event) {
        event.preventDefault();
        var $tour = $(this).data("tourist");
        $(this).closest('ul').find('li').removeClass('active');
        $(this).parent().addClass('active');
        $(this).closest('.my__dropdown').find('.my__dropdown--toggle').find('span').html($tour);
        $(this).closest('.my__dropdown--menu').slideUp();
        $(this).closest('.my__dropdown').find($touristToggle).removeClass('opened')
    });
    $(document).mouseup(function(e) {
        var div = $('.my--dropdown');
        div.each(function() {
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.my--dropdown--menu').slideUp();
                div.find('.my--dropdown--toggle').removeClass('opened')
            }
        })
    });
    $(document).mouseup(function(e) {
        var div1 = $('.my__dropdown');
        div1.each(function() {
            if (!div1.is(e.target) && div1.has(e.target).length === 0) {
                $('.my__dropdown--menu').slideUp();
                div1.find('.my__dropdown--toggle').removeClass('opened')
            }
        })
    })
}

function openSearch() {
    var $mobileTopContainer = $('.mobile-head > .container');
    var $openMobileSearch = $mobileTopContainer.find('.search-open');
    $openMobileSearch.click(function() {
        $mobileTopContainer.addClass('search-opened')
    });
    $('.mobile-search-close').click(function(e) {
        e.preventDefault();
        $mobileTopContainer.removeClass('search-opened')
    })
}

function customTab() {
    var $container = $('.filter-tabs > .tab-nav');
    var $selector = $container.find('div').find('a.custom-tab');
    $container.find('div').first().find('a.custom-tab').addClass('active');
    $($selector).click(function(e) {
        e.preventDefault();
        var self = $(this);
        $($selector).removeClass('active');
        self.addClass('active');
        var close = self.attr('data-close');
        var open = self.attr('data-open');
        $(close).css('display', 'none');
        $(open).fadeIn()
    })
}

function filterTabs() {}

function gridListTabs() {}

function widthResize() {
    $(window).on("orientationchange", function(event) {
        console.log("This device is in " + event.orientation + " mode!");
        location.reload()
    })
}

function carousels() {
    $('.complex-carousel').owlCarousel({
        loop: !0,
        margin: 30,
        navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
        responsive: {
            0: {
                items: 1,
                dots: !0,
                nav: !1
            },
            500: {
                items: 2,
                dots: !1,
                nav: !0
            },
            768: {
                items: 2,
                dots: !1,
                nav: !0
            },
            1000: {
                items: 3,
                dots: !1,
                nav: !0
            }
        }
    });
    var $vitrineSection = $('.vitrine-section');
    $($vitrineSection).each(function() {
        if (window.innerWidth < "768") {
            $($vitrineSection).addClass('items-carousel owl-carousel')
        }
    });

    $('.items-carousel').owlCarousel({
        loop: !0,
        dots: !0,
        nav: !1,
        margin: 0,
        navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
        responsive: {
            0: {
                items: 1
            },
            620: {
                items: 2
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $('.single-complex-carousel').owlCarousel({
        loop: !1,
        dots: !1,
        nav: !0,
        margin: 0,
        items: 1,
        navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>']
    });
    var $singleCarousel = $('.single-page-carousel');
    $singleCarousel.owlCarousel({
        loop: !0,
        dots: !0,
        nav: !1,
        margin: 0,
        items: 1,
        thumbImage: !1,
        thumbsPrerendered: !1,
        responsive: {
            0: {
                mouseDrag: !1,
                touchDrag: !0
            },
            768: {
                mouseDrag: !1,
                touchDrag: !1
            }
        }
    });
    var $backCarousel = $('.back-carousel');
    $backCarousel.owlCarousel({
        loop: !0,
        dots: !1,
        nav: !1,
        margin: 0,
        items: 1,
        thumbImage: !1,
        thumbsPrerendered: !1
    });
    var $singleNext = $('.single__buttons .next');
    var $singlePrev = $('.single__buttons .prev');
    $singleNext.click(function() {
        $singleCarousel.trigger('next.owl.carousel')
    });
    $singlePrev.click(function() {
        $singleCarousel.trigger('prev.owl.carousel')
    });
    $singleNext.click(function() {
        $backCarousel.trigger('next.owl.carousel')
    });
    $singlePrev.click(function() {
        $backCarousel.trigger('prev.owl.carousel')
    });
    var $listCarousel = $('.list-page-carousel');
    $listCarousel.owlCarousel({
        loop: !0,
        dots: !1,
        nav: !0,
        margin: 0,
        items: 1,
        navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>']
    });
    var $listNext = $('.list__buttons .next');
    var $listPrev = $('.list__buttons .prev');
    $listNext.click(function() {
        $listCarousel.trigger('next.owl.carousel')
    });
    $listPrev.click(function() {
        $listCarousel.trigger('prev.owl.carousel')
    });
    
    var $modalCarousel = $('.modal-carousel');
	$modalCarousel.owlCarousel({
		loop: false,
		nav: true,
		margin: 0,
		items: 1,
		center:true,
		dots: false,
		thumbs: true,
		thumbImage: false,
		thumbsPrerendered: true,
		navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
		URLhashListener:true,
		autoplayHoverPause:true,
		startPosition: 'URLHash'
	});

	$(document).on('keydown', function( event ) {
		if(event.keyCode === 37) {
			$modalCarousel.trigger('prev.owl')
		}
		if(event.keyCode === 39) {
			$modalCarousel.trigger('next.owl')
		}
	});
}

function backCarouselHeight() {
    var $carouselBlock = $('.block-itr').find('.carousel-parent .owl-stage');
    var $backCarousel = $('.single-object').find('.back-carousel');
    var $backCarouselItem = $backCarousel.find('.back-item');
    setTimeout(function() {
        var $parentHeight = $carouselBlock.height();
        if ($carouselBlock.length) {
            $backCarouselItem.height($parentHeight);
            $backCarousel.css("margin-top", '-' + $parentHeight + 'px');
            $(window).resize(function() {
                $backCarouselItem.height($parentHeight);
                $backCarousel.css("margin-top", '-' + $parentHeight + 'px')
            })
        }
    }, 500);
    var $carouselImage = $carouselBlock.find('a.item > img');
    var $carouselWidth = $carouselBlock.find('.owl-item').width();
    $carouselImage.each(function() {
        $carouselImage.height($carouselWidth / 1.502);
        var $imageHeight = $carouselImage.height($carouselWidth / 1.502);
        $(window).resize(function() {
            $(this).height($carouselWidth / 1.502)
        })
    })
}

function mobileMenu() {
    $('.menu_toggle').on('click touchstart', function() {
        $('.menu_toggle').toggleClass('open');
        $('.mobile_menu').slideToggle(300);
        return !1
    });
    $(".mobile_menu .top_mobile_menu > li.menu-item-has-children > a").after('<span class="arrow"></span>');
    $(".mobile_menu .top_mobile_menu > li.menu-item-has-children > ul.sub-menu li.menu-item-has-children > a").after('<span class="arrow"></span>');
    $(".mobile_menu .top_mobile_menu > li.menu-item-has-children .arrow").on('click', function() {
        if (!$(this).closest('li').hasClass('active')) {
            $(this).parent('li').parent('ul').find('li').removeClass('active');
            $(this).parent('li').parent('ul').find('li').not(this).parent().find('ul').slideUp(300);
            $(this).parent('li').parent('ul').find('li').find('.arrow').not(this).removeClass('active');
            $(this).addClass('active');
            $(this).closest('li').addClass('active');
            $(this).closest('li').find('> ul').slideDown(300);
            return !1
        } else {
            $(this).removeClass('active');
            $(this).closest('li').removeClass('active');
            $(this).parent('li').parent('ul').find('li').removeClass('active');
            $(this).closest('li').find('> ul').slideUp(300)
        }
    });
    $(".mobile_menu .top_mobile_menu > li.menu-item-has-children > a").on('click', function() {
        if ($(this).attr('href') == '#') {
            $(this).closest('li').find('ul').slideToggle();
            $(this).closest('li').find('.arrow').toggleClass('active');
            $(this).closest('li').toggleClass('active');
            return !1
        }
    });
    var $indexParent = $('.mobile-menu-block');
    if ($indexParent.length) {
        var $indexMenuBlock = $indexParent.find('.page_menu');
        $indexMenuBlock.find('.top_mobile_menu').find('li.menu-item-has-children > a').after('<span class="arrow"></span>').addClass('child');
        $(".page_menu .top_mobile_menu > li.menu-item-has-children > ul.sub-menu li.menu-item-has-children > a").after('<span class="arrow"></span>');
        $(".page_menu .top_mobile_menu > li.menu-item-has-children .arrow").on('click', function() {
            if (!$(this).closest('li').hasClass('active')) {
                $(this).parent('li').parent('ul').find('li').removeClass('active');
                $(this).parent('li').parent('ul').find('li').not(this).parent().find('ul').slideUp(300);
                $(this).parent('li').parent('ul').find('li').find('.arrow').not(this).removeClass('active');
                $(this).addClass('active');
                $(this).closest('li').addClass('active');
                $(this).closest('li').find('> ul').slideDown(300);
                return !1
            } else {
                $(this).removeClass('active');
                $(this).closest('li').removeClass('active');
                $(this).parent('li').parent('ul').find('li').removeClass('active');
                $(this).closest('li').find('> ul').slideUp(300)
            }
        });
        $(".page_menu .top_mobile_menu > li.menu-item-has-children > a").on('click', function() {
            if ($(this).attr('href') == '#') {
                $(this).closest('li').find('ul').slideToggle();
                $(this).closest('li').find('.arrow').toggleClass('active');
                $(this).closest('li').toggleClass('active');
                return !1
            }
        })
    }
}

function sendForms() {
    var $topForm = $('#offers-form');
    var $feedBackForm = $('#feedback-form');
    var $offersButton = $topForm.find('button.btn-contact');
    var feedBackButton = $feedBackForm.find('button.btn-contact');
    $topForm.validate({
        rules: {
            offers_price: {
                required: !0,
                digits: !0
            },
            offers_name: {
                required: !0
            },
            offers_phone: {
                required: !0,
                digits: !0
            }
        },
        messages: {
            offers_price: {
                required: "* Completați câmpul",
                digits: "* Doar cifre."
            },
            offers_name: {
                required: "* Completați câmpul"
            },
            offers_phone: {
                required: "* Introduceți numărul de telefon",
                digits: "* Doar cifre de la 0 la 9"
            }
        },
        submitHandler: function(form) {
            var filter = $topForm;
            msg = $topForm.serialize();
            output = msg;
            $offersButton.on('click', function() {
                $(this).closest($topForm).hide();
                $(this).closest('.schedule-column').find('.form-title').hide();
                $(this).closest('.schedule-column').find('.form-response').fadeIn()
            });
            return !1
        }
    });
    $feedBackForm.validate({
        rules: {
            soflow: {
                required: !0
            },
            feedback_email: {
                required: !0
            }
        },
        messages: {
            soflow: {
                required: "* Selectati priod",
                digits: "* Doar cifre."
            },
            feedback_email: {
                required: "* Completați câmpul",
                email: "* Introduceti adresa de e-mail corect"
            }
        },
        submitHandler: function(form) {
            feedBackButton.on('click', function() {
                $(this).closest($feedBackForm).hide();
                $(this).closest('.schedule-column').find('.form-title').hide();
                $(this).closest('.schedule-column').find('.form-response').fadeIn()
            });
            return !1
        }
    })
}

function filterDropDown() {
    var $filterLangToggle = $('.filter-lang-toggle');
    var $filterLang = $filterLangToggle.find('a');
    $filterLang.on('click', function(e) {
        e.preventDefault();
        $(this).siblings('a').not(this).removeClass('active');
        $(this).addClass('active')
    })
}

function stickyFilterBlock() {
    var $stickyFilter = $('.sticky-filter');
    var $filterParent = $stickyFilter.find('.filter-section-parent');
    var $filterSection = $stickyFilter.find('.filter-section-parent');
    if ($stickyFilter.length) {
        if ($stickyFilter.length) {
            var $fh = $filterSection.height();
            if (window.innerWidth >= 1024 ) {
				$stickyFilter.height($fh);
			}
            var $filterScrollToggle = $('.toggle-sticky-filter, .filter_toggle');
            if ($(window).width() >= 1100) {
                $(window).scroll(function() {
                    var scroll = $(window).scrollTop();
                    if (scroll > $fh + 10) {
                        $filterParent.appendTo('.fixed-menu');
                        $('.fixed-menu .filter-section-parent').addClass('prepended').hide();
                        $(".toggle-sticky-filter, .fixed-menu .filter-section-parent.prepended").on({
                            mouseenter: function() {
                                $('.filter-section-parent.prepended').show()
                            },
                            mouseleave: function() {
                                $('.filter-section-parent.prepended').hide()
                            }
                        })
                    } else {
                        $('.fixed-menu .filter-section-parent').removeClass('prepended').appendTo($stickyFilter).show()
                    }
                })
            } else {
                $(window).scroll(function() {
                    var scroll = $(window).scrollTop();
                    if (scroll >= $fh) {
                        $('.filter_toggle').show()
                    } else {
                        $('.filter_toggle').hide()
                    }
                });
                $($filterScrollToggle).on('click', function() {
                    $("html, body").animate({
                        scrollTop: "0"
                    })
                });
                var $filterMobileButton = $filterParent.find('.btn-filter');
                $filterMobileButton.on('click', function() {
                    $('html,body').animate({
                        scrollTop: $(".article-container").offset().top - 100
                    }, '600')
                })
            }
        }
    }
}

function contactIcons() {
    var $iconContainer = $('.footer-share');
    var $hideUl = $iconContainer.find('ul');
    $($iconContainer).on('click', function() {
        if (($hideUl).hasClass('hide')) {
            $(this).addClass('close-icon');
            $($hideUl).fadeIn('slow').removeClass('hide')
        } else {
            $($hideUl).fadeOut().addClass('hide');
            $(this).removeClass('close-icon')
        }
    })
}

function magnificPopups() {}

function modalsPopups() {
    var singleModal = $('.single-modal');
	var $singleCarousel = $('.single-page-carousel');
	var $openSingle = $singleCarousel.find('.owl-item').find('.open-modal');

	$singleCarousel.each(function(){
		$openSingle.on('click', function () {
			console.log('single-modal');
			$('body').addClass('over-hid');
			singleModal.show();
		});
		$('.close-single-modal').click(function(){
			singleModal.fadeOut();
			$('body').removeClass('over-hid');
		});
	});
}