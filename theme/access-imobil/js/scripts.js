$(function() {
    $('body').on('click vclick', ".call-function", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $thisBtn = $(this);
        if($thisBtn.hasClass('disabled')){
            return false;
        }
        
        var callBack = $thisBtn.data('callback');
        window[callBack]($thisBtn);
        return false;
    });
    
    $('#offer-price-btn').on('mouseenter', function() {
        var $icon = $(this).find('.fai');
        $icon.toggleClass('fai-bell-o fai-bell')
    }).on('mouseleave', function() {
        var $icon = $(this).find('.fai');
        $icon.toggleClass('fai-bell fai-bell-o')
    });
    
    $('body').on('click vclick', ".call-popup", function(e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('disabled')) {
            return !1
        }
        var inst = $('[data-remodal-id="' + $this.data('popup') + '"]').remodal();
        var popup_url = $this.data('href');
        var $popup_parent = $('[data-remodal-id="' + $this.data('popup') + '"]');
        $.ajax({
            type: 'POST',
            url: popup_url,
            data: {},
            dataType: 'JSON',
            beforeSend: function() {
                clearSystemMessages()
            },
            success: function(resp) {
                if (resp.mess_type == 'success') {
                    $popup_parent.find('.modal-content-wr').html(resp.popup_content);
                    if ($popup_parent.find('input[type="tel"]').length) {
                        $popup_parent.find('input[type="tel"]').intlTelInput({
                            geoIpLookup: function(callback) {
                                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                                    var countryCode = (resp && resp.country) ? resp.country : "";
                                    callback(countryCode)
                                })
                            },
                            initialCountry: "auto",
                            nationalMode: !1,
                            utilsScript: site_url + 'theme/access-imobil/plugins/intl-tel-input/js/utils.js',
                            preferredCountries: ['md']
                        });
                        $popup_parent.find('.selected-flag > .iti-arrow').addClass('fai fai-down-arrow-o')
                    }
                    inst.open()
                } else {
                    systemMessages(resp.message, resp.mess_type)
                }
            }
        });
        return !1
    });
    
    $('.form-select').on('click', '.icon', function() {
        $(this).closest('.form-select').find('select').trigger('mousedown')
    });
    
    $(".blog-item__post-description p > iframe").each(function() {
        $(this).parent().addClass("video-iframe")
    });
    
    $('.select_cv').fileupload({
        url: site_lang_url + 'vacancies/ajax_operations/upload',
        dataType: 'json',
        beforeSend: function() {
            clearSystemMessages()
        },
        done: function(e, response) {
            if (response.result.mess_type == 'error') {
                systemMessages(response.result.message, response.result.mess_class)
            } else {
                $(e.target).closest('.btn').find('.text').html(response.result.file.name);
                $(e.target).closest('.btn-group').find('.btn[data-callback="send_cv"]').removeClass('disabled')
            }
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    
    $.validator.addMethod("phone", function(value, element) {
        return this.optional(element) || /^\+?[0-9]+$/.test(value.replace(/\s/g, ''))
    }, "* Только цифры от 0 - 9");
    
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\u0430-\u044f\s]+$/i.test(value)
    }, "* Только буквы от A - Z, А - Я");
    
    $("#form_feedback").validate({
        rules: {
            name: {
                required: !0,
                lettersonly: 2
            },
            email: {
                required: !0,
                email: !0
            },
            phone: {
                required: !0,
                phone: !0
            },
            message: "required"
        },
        messages: {
            name: {
                required: validator_translations[site_lang].required,
                lettersonly: validator_translations[site_lang].lettersonly
            },
            email: validator_translations[site_lang].email,
            phone: validator_translations[site_lang].phone,
            message: validator_translations[site_lang].required
        },
        errorElement: "em",
        highlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error")
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").removeClass("has-error")
        }
    });

    $("#form_add_ad").validate({
        rules: {
            category: {
                required: !0
            },
            name: {
                required: !0,
                lettersonly: 2
            },
            phone: {
                required: !0,
                phone: !0
            },
            message: "required"
        },
        messages: {
            category: {
                required: validator_translations[site_lang].required
            },
            name: {
                required: validator_translations[site_lang].required,
                lettersonly: validator_translations[site_lang].lettersonly
            },
            phone: validator_translations[site_lang].phone,
            message: validator_translations[site_lang].required
        },
        errorElement: "em",
        errorPlacement: function(error, element) {
            if (element[0].type == "select-one") {
                error.insertAfter(".form-select")
            } else {
                error.insertAfter(element)
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error")
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").removeClass("has-error")
        }
    });

    $('input[type="tel"]').intlTelInput({
        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode)
            })
        },
        initialCountry: "auto",
        nationalMode: !1,
        utilsScript: site_url + 'theme/access-imobil/plugins/intl-tel-input/js/utils.js',
        preferredCountries: ['md']
    });
    
    $('[data-toggle="popover"]').popover();

    initGoogleMaps();

    if (_init_catalog_filters == !0) {
        // $('.selectpicker-filter select.catalog_filter-js').find('option:first').hide();

        $('.catalog_filter-js').change(function() {
            if (!is_mobile_filter()) {
                catalog_page = 1;
                _get_catalog()
            }
        });
        catalog_pagination();
        var $select_location = $('.selectpicker-filter select');
        if (is_mobile_filter()) {
            $select_location.selectpicker('destroy');
            $('select.select-filter').on('change', function(){
                var filter_val = $(this).val();
                $(this).closest('.select-parent').find('input.filter-input').val(filter_val);
            });
        } else {
            $select_location.selectpicker();
        }
        $('.list-page-carousel').owlCarousel({
            loop: !0,
            dots: !1,
            nav: !0,
            margin: 0,
            items: 1,
            navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
        });

        $( window ).on( "orientationchange", function( event ) {
            if(is_mobile_filter()){
                var view = 'grid';
                $.ajax({
                    type: 'POST',
                    url: site_url + 'real_estate/ajax_operations/set_catalog_view',
                    data: {
                        view: view
                    },
                    dataType: 'JSON',
                    success: function(resp) {
                        if (resp.mess_type == 'success') {
                            location.reload(true);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        jqXHR.abort()
                    }
                });
            }
        });
    }
    discounts_pagination();
    search_pagination();
    manager_catalog_pagination();

    clear_tblank();

    if(is_home){
        _get_home_items();
    }

    if(is_contact){
        if(!is_mobile_app){
            var sidebar_contacts_h = $( '#sidebar-contacts' ).height();
            $('#google-maps__contacts').css("height",sidebar_contacts_h+"px");
        }
    }
});

function _get_home_items() {
    $.ajax({
        type: 'POST',
        url: site_lang_url+'real_estate/ajax_operations/get_home_items',
        data: {},
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                $('#vitrine-section_home-wr').html(resp.content);
                $('#vitrine-section_home-wr').fadeTo(100, 1, function(){
                    $('#vitrine-section_home-wr').addClass('vitrine-section');
                    if (window.innerWidth < 768) {
                        $('#vitrine-section_home-wr').addClass('items-carousel owl-carousel');
                        $('#vitrine-section_home-wr').owlCarousel({
                            loop: !0,
                            dots: !0,
                            nav: !1,
                            margin: 0,
                            navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
                            responsive: {
                                0: {
                                    items: 1
                                },
                                620: {
                                    items: 2
                                },
                                768: {
                                    items: 2
                                },
                                1000: {
                                    items: 3
                                }
                            }
                        });
                    }
                });
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}

function clear_tblank(){
    if(is_mobile_filter()){
        $('a[data-tblank]').attr('target', '_self');
    }
}

function scrollToObj(target, offset, time) {
    $('html, body').animate({
        scrollTop: $(target).offset().top - offset
    }, time)
}

function search_pagination() {
    var $paginator = $('#search_pagination');
    $('body').on('click', '#search_pagination a[data-ci-pagination-page]', function(e) {
        e.preventDefault();
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        search_page = intval($(this).data('ci-pagination-page'));
        _get_search()
    })
}

function discounts_pagination() {
    var $paginator = $('#discounts_pagination');
    $('body').on('click', '#discounts_pagination a[data-ci-pagination-page]', function(e) {
        e.preventDefault();
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        discounts_page = intval($(this).data('ci-pagination-page'));
        _get_discounts()
    })
}
var set_discount_category = function(btn) {
    var $this = $(btn);
    discounts_page = 1;
    discounts_category = $this.data('category');
    $this.addClass('active').siblings().removeClass('active');
    _get_discounts();
}
var reset_discount_filter = function(btn) {
    var $this = $(btn);
    discounts_page = 1;
    discounts_category = null;
    $this.addClass('active').siblings().removeClass('active');
    _get_discounts()
}

function _get_discounts() {
    var url = [discounts_url];
    $.ajax({
        type: 'POST',
        url: url.join('/'),
        data: {
            id_category: discounts_category,
            page: discounts_page
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                $('#discounts_list-wr').html(resp.content);
                $('#discounts_pagination').html(resp.pagination);
                scrollToObj('body', 0, 100);
                clear_tblank();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}

function manager_catalog_pagination() {
    var $paginator = $('#manager-catalog_pagination');
    $('body').on('click', '#manager-catalog_pagination a[data-ci-pagination-page]', function(e) {
        e.preventDefault();
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        manager_catalog_page = intval($(this).data('ci-pagination-page'));
        _get_manager_catalog();
        scrollToObj('#manager-catalog_wr', 100, 100);
    })
}
var set_manager_catalog_category = function(btn) {
    var $this = $(btn);
    manager_catalog_page = 1;
    manager_catalog_category = $this.data('category');
    $this.addClass('active').siblings().removeClass('active');
    _get_manager_catalog();
}
var reset_manager_catalog_filter = function(btn) {
    var $this = $(btn);
    manager_catalog_page = 1;
    manager_catalog_category = null;
    $this.addClass('active').siblings().removeClass('active');
    _get_manager_catalog()
}

function _get_manager_catalog() {
    var url = [manager_catalog_url];
    $.ajax({
        type: 'POST',
        url: url.join('/'),
        data: {
            id_category: manager_catalog_category,
            page: manager_catalog_page
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                $('#manager-catalog_list-wr').html(resp.content);
                $('#manager-catalog_pagination').html(resp.pagination);
                clear_tblank();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}

function _get_search() {
    var url = [search_url];
    $.ajax({
        type: 'get',
        url: url.join('/'),
        data: {
            q: search_keywords,
            page: search_page
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                $('#search_list-wr').html(resp.content);
                $('#search_pagination').html(resp.pagination);
                $('#catalog-search-total-items_counter').html(resp.records_total);
                $('#catalog-search_keywords').html(resp.get_keywords_text);
                scrollToObj('body', 0, 100);
                clear_tblank();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var set_favorite = function(btn) {
    var $this = $(btn);
    var item = $this.data('item');
    var type = $this.data('type');
    $.ajax({
        type: 'POST',
        url: site_lang_url + 'favorite/ajax_operations/set_favorite',
        data: {
            item: item,
            type: type
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                if ($this.hasClass('hasWishList')) {
                    $this.removeClass('hasWishList')
                } else {
                    $this.addClass('hasWishList')
                }
            } else {
                systemMessages(resp.message, resp.mess_type)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var remove_favorite = function(btn) {
    var $this = $(btn);
    var item = $this.data('item');
    var type = $this.data('type');
    $.ajax({
        type: 'POST',
        url: site_lang_url + 'favorite/ajax_operations/set_favorite',
        data: {
            item: item,
            type: type
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                if (resp.total_records == 0) {
                    var template = '<div class="col">\
                                        <div class="error error__no-data mwp-100_i">\
                                            <div class="error__no-data-message">\
                                                <div class="icon">\
                                                    <i class="fai fai-warning-o"></i>\
                                                </div>' + resp.empty_message + '\
                                            </div>\
                                        </div>\
                                    </div>';
                    $this.closest('.favorite-tab').html(template)
                } else {
                    $this.closest('.favorite_item-wr').remove()
                }
            } else {
                systemMessages(resp.message, resp.mess_type)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var toogle_tabs = function(btn) {
    var $this = $(btn);
    var $active_tab = $($this.data('tab'));
    $this.addClass('active').siblings().removeClass('active');
    $active_tab.siblings().hide();
    $active_tab.fadeIn()
}
var set_currency = function(btn) {
    var $this = $(btn);
    var currency = $this.data('currency');
    $.ajax({
        type: 'POST',
        url: site_url + 'currency/ajax_operations/set_currency',
        data: {
            currency: currency
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                var $filtersForm = $('.filter-section form.category-filters-form');
                $filtersForm.find('.catalog_filter-js[name="property[fp][from]"] option[data-default]').prop('selected', !0);
                $filtersForm.find('.catalog_filter-js[name="property[fp][to]"] option[data-default]').prop('selected', !0);
                _get_catalog();
                location.reload()
            } else {
                systemMessages(resp.message, resp.mess_type)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var set_catalog_view = function(btn) {
    var $this = $(btn);
    var view = $this.data('view');
    $.ajax({
        type: 'POST',
        url: site_url + 'real_estate/ajax_operations/set_catalog_view',
        data: {
            view: view
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                location.reload()
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var reset_catalog_filters = function() {
    var $filtersForm = $('.filter-section form.category-filters-form');
    var checkBox = $filtersForm.find('.filled-in');
    var inputFilter = $filtersForm.find('.filter-input');
    var selectFilter = $filtersForm.find('.select-filter');
    var selectmultipleFilter = $filtersForm.find('.selectpicker-filter select');
    checkBox.prop('checked', !1);
    inputFilter.val('');
    selectFilter.val('0');
    $('select.select-filter option[data-default]').prop('selected', !0);
    if (!is_mobile_filter()) {
        // selectmultipleFilter.selectpicker('deselectAll');
        selectmultipleFilter.selectpicker('refresh');
    }
    catalog_page = 1;
    _get_catalog()
}
var remove_filter = function(btn) {
    var $this = $(btn);
    var target = $this.data('target');
    var target_type = $this.data('target-type');
    var $filtersForm = $('.filter-section form.category-filters-form');
    switch (target_type) {
        case 'checkbox':
            $filtersForm.find('input[name="' + target + '"]').prop('checked', !1);
            break;
        case 'select':
            $filtersForm.find('select[name="' + target + '"] option[data-default]').prop('selected', !0);
            $filtersForm.find('input[name="' + target + '"]').val('');
            break;
        case 'selectmultiple':
            var target_option = $this.data('target-option');
            var $select_filter = $filtersForm.find('select[name="' + target + '"]');
            var $select_filter_option = $filtersForm.find('select[name="' + target + '"] option[value="' + target_option + '"]');
            $select_filter_option.prop('selected', !1);
            if (!is_mobile_filter()) {
                $select_filter.selectpicker('refresh')
            }
            break
    }
    catalog_page = 1;
    _get_catalog()
}
var call_filter = function() {
    catalog_page = 1;
    _get_catalog()
}

function catalog_pagination() {
    var $paginator = $('#catalog_pagination');
    $('body').on('click', '#catalog_pagination a[data-ci-pagination-page]', function(e) {
        e.preventDefault();
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        catalog_page = intval($(this).data('ci-pagination-page'));
        _get_catalog()
    })
}

function _get_catalog() {
    var $filtersForm = $('.filter-section form.category-filters-form');
    var fdata = $filtersForm.serialize();
    var url = [catalog_url];
    if (catalog_page > 1) {
        url.push('page/' + catalog_page)
    }
    url = url.join('/');
    $.ajax({
        type: 'POST',
        url: url,
        data: fdata,
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                $('#catalog_list-wr').html(resp.content);
                $('#catalog_pagination').html(resp.pagination);
                $('#active-filters-list').html(resp.active_filters);
                $('#catalog-total-items_counter').html(resp.records_total);
                $('.dropdown-langs .dropdown-menu').html(resp.dropdown_languages);

                if ($(window).width() >= 1100) {
                    scrollToObj('body', 0, 100)
                } else {
                    if (is_mobile_filter()) {
                        var is_page_scrolled = false;
                        $(window).scroll(function() {
                            is_page_scrolled = true;
                        });

                        if(!is_page_scrolled){
                            scrollToObj('#catalog-container', 100, 0);
                        }
                    }
                }
                var filters_query_string = (resp.filters_query.length) ? '?filters=' + resp.filters_query : '';
                window.history.replaceState({}, "", url + filters_query_string);
                if (resp.catalog_view == 'list') {
                    $('.list-page-carousel').owlCarousel({
                        loop: !0,
                        dots: !1,
                        nav: !0,
                        margin: 0,
                        items: 1,
                        navText: ['<em class="fai fai-left-arrow-o" aria-hidden="true"></em>', '<em class="fai fai-right-arrow-o" aria-hidden="true"></em>'],
                    })
                }
                clear_tblank();
            } else {
                systemMessages(resp.message, resp.mess_type)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    })
}
var validator_translations = {
    'ru': {
        'required': '* Заполните это поле',
        'lettersonly': '* Только буквы от A - Z, А - Я',
        'email': '* Введите правильный e-mail',
        'phone': '* Только цифры от 0 - 9',
        'period': '* Выберите период подписки'
    },
    'ro': {
        'required': '* Completați câmpul',
        'lettersonly': '* Doar litere de la A - Z',
        'email': '* Introduceti adresa de e-mail corect',
        'phone': '* Doar cifre de la 0 la 9',
        'period': '* Alege-ti perioada'
    },
    'en': {
        'required': '* Fill out the field',
        'lettersonly': '* Only the letters A - Z',
        'email': '* Please enter a valid e-mail',
        'phone': '* Only numbers from 0 - 9',
        'period': '* Select subscribe period'
    }
};

function systemMessages(mess_array, ul_class) {
    typeM = typeof mess_array;
    var good_array = [];
    switch (typeM) {
        case 'string':
            good_array = [mess_array];
            break;
        case 'array':
            good_array = mess_array;
            break;
        case 'object':
            for (var i in mess_array) {
                good_array.push(mess_array[i])
            }
            break
    }
    if (!$('.system-text-messages-b').is(':visible')) {
        $('.system-text-messages-b').fadeIn('fast')
    }
    var $systMessUl = $('.system-text-messages-b ul');
    $systMessUl.html('');
    for (var li in good_array) {
        $systMessUl.prepend('<li  class="message-' + ul_class + '">' + good_array[li] + ' <i class="fai fai-x-o call-function" data-callback="delete_system_message"></i></li>');
        $systMessUl.children('li').first().addClass('zoomIn').show().delay(30000).slideUp('slow', function() {
            if ($systMessUl.children('li').length == 1) {
                $systMessUl.closest('.system-text-messages-b').slideUp();
                $(this).remove()
            } else $(this).remove()
        })
    }
}
var delete_system_message = function(btn) {
    var $li = $(btn).closest('li');
    $li.clearQueue();
    $li.slideUp('slow', function() {
        if ($('.system-text-messages-b li').length == 1) {
            $('.system-text-messages-b').slideUp();
            $li.remove()
        } else $li.remove()
    })
};

function clearSystemMessages() {
    if (!$('.system-text-messages-b').is(':visible')) {
        $('.system-text-messages-b').fadeIn('fast')
    }
    $('.system-text-messages-b ul').html('')
}

function showLoaderFixed(loaderElement) {
    var $this = $(loaderElement).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(loaderElement).prepend('<div class="ajax-loader fixed">\
									<div class="wrapper">\
										<div class="bounce1"></div>\
										<div class="bounce2"></div>\
										<div class="bounce3"></div>\
									</div>\
								</div>');
        $(loaderElement).children('.ajax-loader').show()
    }
}

function showLoader(loaderElement) {
    var $this = $(loaderElement).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(loaderElement).prepend('<div class="ajax-loader">\
									<div class="wrapper">\
										<div class="bounce1"></div>\
										<div class="bounce2"></div>\
										<div class="bounce3"></div>\
									</div>\
								</div>');
        $(loaderElement).children('.ajax-loader').show()
    }
}

function hideLoader(parentId) {
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.hide()
}
var send_cv = function(btn) {
    var $this = $(btn);
    var id_vacancy = $this.data('vacancy');
    $.ajax({
        type: 'POST',
        url: site_lang_url + 'vacancies/ajax_operations/add',
        data: {
            id_vacancy: id_vacancy
        },
        dataType: 'JSON',
        success: function(resp) {
            if (resp.mess_type == 'success') {
                var template = '<div class="success success__sent">\
                                    <h1 class="h1-title">\
                                        <div class="icon">\
                                            <i class="fai fai-check"></i>\
                                        </div>\
                                        ' + resp.message + '\
                                    </h1>\
                                </div>';
                $this.closest('.ai-acordion__body-bottom').addClass('sent-success').html(template)
            } else {
                systemMessages(resp.message, resp.mess_type)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            jqXHR.abort()
        }
    });
    return !1
}
var send_offer = function(btn) {
    var $this = $(btn);
    var $form = $this.closest('form');
    $form.validate({
        rules: {
            name: {
                required: !0,
                lettersonly: 2
            },
            phone: {
                required: !0,
                phone: !0
            },
            offer_price: {
                required: !0
            }
        },
        messages: {
            name: {
                required: validator_translations[site_lang].required,
                lettersonly: validator_translations[site_lang].lettersonly
            },
            phone: validator_translations[site_lang].phone,
            offer_price: validator_translations[site_lang].required
        },
        errorElement: "em",
        highlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error")
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").removeClass("has-error")
        }
    });
    if ($form.valid()) {
        $.ajax({
            type: 'POST',
            url: site_lang_url + 'notify/ajax_operations/send_offer',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function() {
                showLoader($form.parents('.modal-form'));
                $this.addClass('disabled')
            },
            success: function(resp) {
                if (resp.mess_type == 'success') {
                    var template = '<div class="success success__sent">\
                                        <h1 class="h1-title">\
                                            <div class="icon">\
                                                <i class="fai fai-check"></i>\
                                            </div>\
                                            ' + resp.message + '\
                                        </h1>\
                                    </div>';
                    $form.html(template).addClass('sent')
                } else {
                    $this.removeClass('disabled');
                    systemMessages(resp.message, resp.mess_type)
                }
                hideLoader($form.parents('.modal-form'))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                jqXHR.abort()
            }
        });
        console.log('form send')
    }
    return !1
}
var send_subscribe = function(btn) {
    var $this = $(btn);
    var $form = $this.closest('form');
    $form.validate({
        rules: {
            email: {
                required: !0,
                email: !0
            },
            subscribe_period: {
                required: !0,
                number: !0
            }
        },
        messages: {
            email: validator_translations[site_lang].email,
            subscribe_period: validator_translations[site_lang].period
        },
        errorElement: "em",
        highlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error")
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents(".form-group").removeClass("has-error")
        }
    });
    if ($form.valid()) {
        $.ajax({
            type: 'POST',
            url: site_lang_url + 'notify/ajax_operations/send_subscribe',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function() {
                showLoader($form.parents('.modal-form'));
                $this.addClass('disabled')
            },
            success: function(resp) {
                if (resp.mess_type == 'success') {
                    var template = '<div class="success success__sent">\
                                        <h1 class="h1-title">\
                                            <div class="icon">\
                                                <i class="fai fai-check"></i>\
                                            </div>\
                                            ' + resp.message + '\
                                        </h1>\
                                    </div>';
                    $form.html(template).addClass('sent')
                } else {
                    $this.removeClass('disabled');
                    systemMessages(resp.message, resp.mess_type)
                }
                hideLoader($form.parents('.modal-form'))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                jqXHR.abort()
            }
        })
    }
    return !1
}
var send_feedback = function(btn) {
    var $this = $(btn);
    var $form = $this.closest('form');
    if ($form.valid()) {
        $.ajax({
            type: 'POST',
            url: site_lang_url + 'notify/ajax_operations/feedback',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function() {
                showLoader($form.closest('.feedback'));
                $this.addClass('disabled')
            },
            success: function(resp) {
                if (resp.mess_type == 'success') {
                    var template = '<div class="success success__sent">\
                                        <h1 class="h1-title">\
                                            <div class="icon">\
                                                <i class="fai fai-check"></i>\
                                            </div>\
                                            ' + resp.message + '\
                                        </h1>\
                                    </div>';
                    $form.html(template).addClass('sent')
                } else {
                    $this.removeClass('disabled');
                    systemMessages(resp.message, resp.mess_type)
                }
                hideLoader($form.closest('.feedback'))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                jqXHR.abort()
            }
        })
    }
    return !1
}
var send_ad = function(btn) {
    var $this = $(btn);
    var $form = $this.closest('form');
    if ($form.valid()) {
        $.ajax({
            type: 'POST',
            url: site_lang_url + 'notify/ajax_operations/add_ad',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function() {
                showLoader($form.closest('.feedback'));
                $this.addClass('disabled')
            },
            success: function(resp) {
                if (resp.mess_type == 'success') {
                    var template = '<div class="success success__sent">\
                                        <h1 class="h1-title d-flex flex-column">\
                                            <div class="icon mb-2">\
                                                <i class="fai fai-check"></i>\
                                            </div>\
                                            ' + resp.message + '\
                                        </h1>\
                                    </div>';
                    $form.html(template).addClass('sent')
                } else {
                    $this.removeClass('disabled');
                    systemMessages(resp.message, resp.mess_type)
                }
                hideLoader($form.closest('.feedback'))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                jqXHR.abort()
            }
        })
    }
    return !1
}
var show_hidden = function(btn) {
    var $this = $(btn);
    var $parent = $($this.data('toggle-parent'));
    var $item = $($this.data('toggle-item'));
    $parent.children($item).prop('style', 'display:initial !important');
    $this.hide()
}

function number_format(num_value, view_decimals) {
    if (view_decimals) {
        return (round_up(num_value, 2)).toFixed(2)
    } else {
        return Math.ceil(num_value).toFixed()
    }
}

function round_up(num, precision) {
    precision = Math.pow(10, precision);
    return Math.ceil(num * precision) / precision
}

function display_number(n) {
    return String(n).replace(/(.)(?=(\d{3})+$)/g, '$1 ')
}

function intval(num) {
    if (typeof num == 'number' || typeof num == 'string') {
        num = num.toString();
        var dotLocation = num.indexOf('.');
        if (dotLocation > 0) {
            num = num.substr(0, dotLocation)
        }
        if (isNaN(Number(num))) {
            num = parseInt(num)
        }
        if (isNaN(num)) {
            return 0
        }
        return Number(num)
    } else if (typeof num == 'object' && num.length != null && num.length > 0) {
        return 1
    } else if (typeof num == 'boolean' && num === !0) {
        return 1
    }
    return 0
}

function floatval(mixed_var) {
    return (parseFloat(mixed_var) || 0)
}

function plural_text(text, number) {
    var return_text = text;
    switch (text) {
        case 'year':
            switch (site_lang) {
                case 'ro':
                    return_text = 'an';
                    if (number == 0 || number > 1) {
                        return_text = 'ani'
                    }
                    break;
                case 'ru':
                    var last_digit = String(number).substring(String(number).length - 1);
                    if (last_digit == 0) {
                        return_text = 'лет'
                    }
                    if (last_digit == 1) {
                        return_text = 'год'
                    }
                    if (last_digit >= 5) {
                        return_text = 'лет'
                    }
                    if (last_digit > 1 && last_digit < 5) {
                        return_text = 'года'
                    }
                    break;
                case 'en':
                    return_text = 'year';
                    if (number > 1) {
                        return_text = 'years'
                    }
                    break
            }
            break
    }
    return return_text
}
var call_inline_fancybox = function(btn) {
    var $this = $(btn);
    var element = $this.data('content');
    $.fancybox.open({
        src: element,
        type: 'inline',
        opts: {
            afterClose: function(instance, current) {
                $(element).find('[data-fancybox-close]').remove();
                $(element).show()
            }
        }
    })
}

function initGoogleMaps() {
    if (init_google_maps === true) {
        window.addEventListener('load', function() {
            var aIcon = L.icon({
                iconUrl: site_url + 'files/images/gm_marker.png'
            });
            $.each(markers, function(i, marker) {
                var uluru = {
                    lat: parseFloat(marker.latitude),
                    lng: parseFloat(marker.longitude)
                };
                var mapDivId = marker.container;
                if (document.getElementById(mapDivId)) {
                    maps[mapDivId] = L.map(document.getElementById(mapDivId), {
                        center: uluru,
                        zoom: 17,
                        scrollWheelZoom: false
                    });

                    if(is_mobile_filter()){
                        maps[mapDivId].invalidateSize();
                    }

                    L.tileLayer('https://i.simpalsmedia.com/map/1/{z}/{x}/{y}.png', {
                        attribution: '<a href="'+site_url+'" target="_blank">AccesImobil, </a><a href="http://www.openstreetmap.org/#map=15/47.770685/27.929283999999996" target="_blank">© OpenStreetMap</a>',
                        maxZoom: 18
                    }).addTo(maps[mapDivId]);
        
                    $.each(markers, function(index, marker_detail){                
                        marker = L.marker([marker_detail.latitude, marker_detail.longitude], {icon: aIcon}).addTo(maps[mapDivId]);
                    });
                }
            })
        }, !1);
    } else {
        return
    }
}
var popup_share = function(obj) {
    var $this = $(obj);
    var popupUrl = '';
    var popupTitle = $this.data('title');
    var popupOptions = 'width=550,height=400,0,status=0';
    var socialUrl = encodeURIComponent($this.data('url'));
    var socialTitle = cleanInput($this.data('title'));
    switch ($this.data('social')) {
        case 'facebook':
            popupUrl += 'https://www.facebook.com/sharer/sharer.php?u=' + socialUrl + '&title=' + socialTitle;
            break;
        case 'twitter':
            popupUrl += 'https://twitter.com/intent/tweet?status=' + socialTitle + '+' + socialUrl;
            break;
        case 'gplus':
            popupUrl += 'https://plus.google.com/share?url=' + socialUrl + '&title=' + socialTitle;
            break
    }
    winPopup(popupUrl, popupTitle, popupOptions)
}

function winPopup(mylink, title, options) {
    var mywin = window.open(mylink, title, options);
    mywin.focus();
    return mywin
}

function cleanInput(str) {
    if (str != undefined && str != '' && str != null) {
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        return str
    } else {
        return !1
    }
}