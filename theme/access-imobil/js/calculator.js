var $calc_price = null;
var $calc_rate = null;
var $calc_rate_client = null;
var $calc_years = null;
var $calc_wr = null;
$(function() {
    if (show_calc) {
        $calc_wr = $('.calculator-sliders');
        if (!calc_on_item) {
            $calc_price = $("#calc_price").slider({}).on('slide change', update_calc).data('slider');
        } else {
            $calc_price = $("#calc_price").val();
        }
        $calc_rate = $("#calc_rate").slider({}).on('slide change', update_calc).data('slider');
        $calc_rate_client = $("#calc_rate_client").slider({}).on('slide change', update_calc).data('slider');
        $calc_years = $("#calc_years").slider({}).on('slide change', update_calc).data('slider');
        update_calc();
    }
});
var update_calc = function() {
    if (!calc_on_item) {
        var calc_price = floatval($calc_price.getValue());
    } else {
        var calc_price = floatval($calc_price);
    }
    var calc_rate = floatval($calc_rate.getValue());
    var scalc_rate = floatval($calc_rate.getValue()) / 100;
    var calc_rate_client = floatval($calc_rate_client.getValue());
    var calc_years = floatval($calc_years.getValue());
    var calc_rate_client_amount = calc_price * calc_rate_client / 100;
    var calc_credit_price = calc_price - calc_rate_client_amount;
    var calc_month_amount = calc_credit_price * scalc_rate / (12 * (1 - Math.pow(1 + scalc_rate / 12, -(calc_years * 12))));
    $('#calc_price_value').text(display_number(calc_price));
    $('#calc_credit_price').text(display_number(calc_credit_price));
    $('#calc_rate_value').text(display_number(calc_rate));
    $('#calc_rate_client_value').text(display_number(number_format(calc_rate_client_amount)));
    $('#calc_rate_client_value_percent').text(display_number(calc_rate_client));
    $('#calc_years_value').text(display_number(calc_years));
    $('#calc_years_value_sufix').text(plural_text('year', calc_years));
    $('#calc_month_amount').text(display_number(number_format(calc_month_amount)));
}