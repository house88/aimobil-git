function showLoader(loaderElement,lodaerText){
	if(lodaerText==undefined)
		lodaerText = 'Обработка данных...';

	var $this = $(loaderElement).children('.ajax-loader');

	if ($this.length > 0)
		$this.show();
	else {
		
		$(loaderElement).prepend('<div class="ajax-loader">\
									<div class="wrapper">\
										<div class="bounce1"></div>\
										<div class="bounce2"></div>\
										<div class="bounce3"></div>\
									</div>\
								</div>');
		$(loaderElement).children('.ajax-loader').show();
	}
}

function hideLoader(parentId){
	var $this = $(parentId).children('.ajax-loader');

	if ($this.length > 0)
		$this.hide();
}

function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string': 
			good_array = [mess_array]; 
		break;
		case 'array': 
			good_array = mess_array; 
		break;
		case 'object': 
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;
		
	}
	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="fa fa-remove"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}

function clearSystemMessages(){	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	$('.system-text-messages-b ul').html('');
}

$(function(){
    $('body').on("click", '.system-text-messages-b li .fa-remove', function(){
        var $li = $(this).closest('li');
        $li.clearQueue();
        $li.slideUp('slow',function(){
            if($('.system-text-messages-b li').length == 1){
                $('.system-text-messages-b').slideUp();
                $li.remove();
            }else
                $li.remove();
        });
    });

	$('body').on('mouseover mouseenter', '*[data-title]', function(){
		if(!$(this).hasClass('tooltipstered')){
			$(this).tooltipster({
				content: $(this).data('title')
			});
		}
		$(this).tooltipster('show');
    });
	
	$("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
		e.preventDefault();

		var dtype = $thisBtn.data('dtype');
		var bdtype = BootstrapDialog.TYPE_PRIMARY;
		switch (dtype) {
			case 'default':
				bdtype = BootstrapDialog.TYPE_DEFAULT;
			break;
			case 'info':
				bdtype = BootstrapDialog.TYPE_INFO;
			break;
			default:
			case 'primary':
				bdtype = BootstrapDialog.TYPE_PRIMARY;
			break;
			case 'success':
				bdtype = BootstrapDialog.TYPE_SUCCESS;
			break;
			case 'warning':
				bdtype = BootstrapDialog.TYPE_WARNING;
			break;
			case 'danger':
				bdtype = BootstrapDialog.TYPE_DANGER;
			break;
		}
		BootstrapDialog.show({
			title: $thisBtn.data('title'),
			message: $thisBtn.data('message'),
			closable: false,
			draggable: true,
			type: bdtype,
			buttons: [{
				label: 'Да',
				cssClass: 'btn-success',
				action: function(dialogRef){
					var callBack = $thisBtn.data('callback');
					var $button = this;
                    $button.disable();

					window[callBack]($thisBtn);
					dialogRef.close();
				}
			},
			{
				label: 'Отменить',
				cssClass: 'btn-danger',
				action: function(dialogRef){
					dialogRef.close();
				}
			}]
		});
	});
	
	$('body').on('click', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		if($thisBtn.hasClass('disabled')){
			return false;
		}
		
		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});
	
	$('body').on('click', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		$.ajax({
			type: 'GET',
			url: popup_url,
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
					$popup_parent.html(resp.popup_content).modal('show').on('hidden.bs.modal', function(){
						$(this).html('');
					});
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});

    var url = window.location;
    var element = $('ul#side-menu a').filter(function() {
        return this.href == url;
        // return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent('li').addClass('active').parent().parent().addClass('active');
    
    if (element.is('li')) {
        element.addClass('active');
    }
});

var toggle_dt_filter = function(btn){
	var $this = $(btn);
	var widthWrFilter = 250;
	switch ($this.data('action')) {
		case 'open':
			$('.wr-filter-admin-panel').animate({'right': 0}, 350, function(){
				$('.wr-filter-admin-panel .wr-hidden').show();
			});
		break;
		case 'close':
			$('.wr-filter-admin-panel').animate({'right': (-1)*widthWrFilter}, 350, function(){
				$('.wr-filter-admin-panel .wr-hidden').hide();
			});
		break;
	}
}

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}